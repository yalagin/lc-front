import { load, getTitle } from "../pageObjects/index";

describe("React App", () => {
  it("should be titled 'Lazy Merch'", async () => {
    await load();
    expect(await getTitle()).toBe("Lazy Merch");
  });
});
