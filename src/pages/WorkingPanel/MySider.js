import React, {useEffect, useState} from 'react';
import StickyBox from "react-sticky-box";
import './MySider.less';
import { Alert, Layout, Menu, Popover, Space,  Typography } from "antd";
import Icon, {
  BarChartOutlined,
  CloudUploadOutlined,
  FormOutlined,
  HomeOutlined,
  SettingOutlined,
  ShoppingCartOutlined, WechatOutlined
} from "@ant-design/icons";
import { Link, Route, Routes, useNavigate, useLocation } from "react-router-dom";
import LogoImg from '../../assets/Icons/OldAssets/Sidebar/logo.png';
import DesignFormList from '../../components/design/DesignFormMutators';
import UserAvatarWrapper from '../../components/person/popover/UserAvatarWrapper';
import {
  CustomFooter,
  CustomHeader,
  CustomSider,
  DoubleLeftOutlinedStyled,
  DoubleRightOutlinedStyled,
  SiderStyledComponents
} from "../../components/styles/SiderStyledComponents";
import {list as personList, reset} from "../../actions/person/list";
import {reset as resetProject} from '../../actions/project/list';
import {shallowEqual, useDispatch, useSelector} from "react-redux";
import {ReactQueryDevtools} from 'react-query/devtools';
import OverviewTable from '../../components/design/table/OverviewTable';
import Upload from './Queue/Upload';
import _ from 'lodash';
import useProfile from '../../utils/hooks/useProfile';
import {reset as personUpdateReset, update} from "../../actions/person/update";
import Dashboard from './Dashboard';
import useProject from '../../utils/hooks/useProject';
import Settings from "./Settings";
import Trademarks from "./Trademark/Trademarks";
import Exit from "../../assets/Icons/ReactSvg/Exit";
import Templates from "./Template/Templates";
import useLoadProject from "../../utils/hooks/useLoadProject";
import Nprogress from "../../components/styles/nprogress/Nprogress";
import styled from "styled-components";
import Help from "./Help";
import ReportBugs from "../../components/bug/ReportBugs";
import StartTour from "../../components/tours/StartTour";

const {Text} = Typography;

const StyledMenu = styled(Menu.Item)`
  &&& .ant-menu-item-icon {
    ${props => !props.collapsed ? "min-width: 30px; font-size: 15px;  text-align: center;" : ""}
  }
`;

const MySider = () => {
  useLoadProject();
  // useLoadProfile();
  // const useLoadProfile = () => {
  const dispatch = useDispatch();
  const {updated} = useSelector(state => state.person.update);
  const {retrieved, loading, error: profileError, eventSource} = useSelector(
    state => state.person.list,
    shallowEqual
  );
  useEffect(() => {
    if (updated) {
      // setIsLoading(true);
      dispatch(personUpdateReset(eventSource));
      dispatch(personList('people?user=' + localStorage.getItem('id')));
    }
  }, [updated]);

  useEffect(() => {
    if (_.isEmpty(retrieved)) {
      // setIsLoading(true);
      dispatch(personList('people?user=' + localStorage.getItem('id')));
    }
  }, []);
  // };


  const {profile} = useProfile();
  const {projectError} = useProject();
  const [collapsed, setCollapsed] = useState(profile?.isSideBarToggled);
  const {pathname} = useLocation();
  const navigate = useNavigate();

  const [header, setHeader] = useState('');
  const [subHeader, setSubHeader] = useState('');
  const [rightHeader, setRightHeader] = useState('');


  useEffect(() => {
    if (profile) {
      setCollapsed(profile.isSideBarToggled);
    }
  }, [profile?.isSideBarToggled]);

  const toggle = () => {
    setCollapsed(prev => {
      dispatch(update(profile, {isSideBarToggled: !prev}));
      return !prev;
    });
  };

  return (
    <Layout className="layout" style={{display: 'flex', alignItems: 'flex-start'}}>
      <Nprogress isLoading={loading} />
      <StickyBox>
        <CustomSider
          theme={'light'}
          collapsible
          collapsed={collapsed}
          onCollapse={() =>
            setCollapsed(prevState => {dispatch(update(profile, {isSideBarToggled: !prevState}));
             return  !prevState;
            })
          }
        >
          <div className="logo">
            {!collapsed && (
              <Link to="/dashboard">
                <img className="logo-img" src={LogoImg} alt="logo" />
              </Link>
            )}
            <span style={{color: 'white !important'}} className="trigger" onClick={toggle}>
              {collapsed ? <DoubleRightOutlinedStyled /> : <DoubleLeftOutlinedStyled />}
            </span>
          </div>
          <Menu mode="inline" selectedKeys={[pathname]}>
            <StyledMenu $collapsed={collapsed} key="/dashboard" icon={<HomeOutlined />}>
              <Link className={'dashboard'}  to="/dashboard">Dashboard</Link>
            </StyledMenu>
            <StyledMenu $collapsed={collapsed} key="/dashboard/workspace" icon={<FormOutlined />}>
              <Link className={'dashboard-workspace'} to="/dashboard/workspace">Workspace</Link>
            </StyledMenu>
            <StyledMenu $collapsed={collapsed} key="/dashboard/overview" icon={<BarChartOutlined />}>
              <Link to="/dashboard/overview">Overview</Link>
            </StyledMenu>
            <StyledMenu
              $collapsed={collapsed}
              key="/dashboard/upload"
              icon={<CloudUploadOutlined />}
            >
              <Link to="/dashboard/upload">Queue</Link>
            </StyledMenu>
            <StyledMenu
              className={'templates'}
              $collapsed={collapsed}
              key="/dashboard/templates"
              icon={<ShoppingCartOutlined />}
            >
              <Link to="/dashboard/templates">Templates</Link>
            </StyledMenu>
            <StyledMenu
              $collapsed={collapsed}
              className={"trademarks"}
              key="/dashboard/trademarks"
              icon={
                <Text style={{color: 'inherit'}} type="secondary">
                  TM
                </Text>
              }
            >
              <Link to="/dashboard/trademarks">Trademarks</Link>
            </StyledMenu>
            <StyledMenu
              $collapsed={collapsed}
              key="/dashboard/account_settings"
              icon={<SettingOutlined />}
            >
              <Link to="/dashboard/account_settings">Settings</Link>
            </StyledMenu>
            <StyledMenu $collapsed={collapsed} key="/dashboard/help" icon={<WechatOutlined />}>
              <Link to="/dashboard/help">Help</Link>
            </StyledMenu>
            <StyledMenu $collapsed={collapsed} key="exit" icon={<Icon component={Exit} />}>
              {' '}
              <Link
                onClick={() => {
                  dispatch(reset(eventSource));
                  dispatch(resetProject(eventSource));
                  localStorage.clear();
                }}
                to="/"
              >
                {' '}
                Exit
              </Link>
            </StyledMenu>
          </Menu>
        </CustomSider>
      </StickyBox>
      <Layout className="site-layout" style={{padding: '1rem 0'}}>
        <CustomHeader
          className="site-layout-background"
          title={
            <>
              {_.startCase(
                pathname.split('/').pop().replaceAll('/', ' ').replaceAll('_', ' ')
              )}{' '}
              {header}{' '}
            </>
          }
          subTitle={subHeader}
          onBack={() => navigate(-1)}
          extra={[
            rightHeader,
            <StartTour key={'tour'} />,
            <ReportBugs key={'bags'} />,
            <Popover
              key={'popover'}
              placement="bottomRight"
              title={'Settings'}
              content={
                <Space direction={'vertical'}>
                  <Link to="/dashboard/account_settings">Profile</Link>
                  <Link
                    onClick={() => {
                      dispatch(reset(eventSource));
                      dispatch(resetProject(eventSource));
                      localStorage.clear();
                    }}
                    to="/"
                  >
                    Logout
                  </Link>
                </Space>
              }
              trigger="click"
            >
              <div style={{display: 'none'}}>.</div>
              <UserAvatarWrapper />
            </Popover>,
          ]}
        />
        <SiderStyledComponents className="site-layout-background">
          {profileError && <Alert message={profileError} type="error" showIcon closable />}
          {projectError && <Alert message={projectError} type="error" showIcon closable />}
          <Routes>
            <Route index element={ <Dashboard />}/>
            <Route path="workspace" element={<DesignFormList />}/>
            <Route path="trademarks" element={ <Trademarks />}/>
            <Route path="templates" element={<Templates />}/>
            <Route path="account_settings" element={<Settings />}/>
            <Route path="help" element={<Help />}/>
            <Route path="overview" element= {profile && <OverviewTable setHeader={setHeader} setSubHeader={setSubHeader} />} />
            <Route path="upload" element={<Upload
              setHeader={setHeader}
              setSubHeader={setSubHeader}
              setRightHeader={setRightHeader}
            />}/>
          </Routes>
        </SiderStyledComponents>
        <CustomFooter>© Lazymerch | ALL RIGHTS RESERVED</CustomFooter>
      </Layout>
      <ReactQueryDevtools initialIsOpen={false} />
    </Layout>
  );
};
export default MySider;



