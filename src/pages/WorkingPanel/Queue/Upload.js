import React, { useEffect, useState } from "react";
import * as constants from '../../../utils/constants/constants';
import UploadDesignTabs from '../../../components/tabs/UploadDesignTabs';
import SubDesignTable from '../../../components/design/table/subdesignUpload/SubDesignTable';
import useProfile from "../../../utils/hooks/useProfile";

const  Upload = ({setSubHeader,setHeader,setRightHeader}) => {
  const [currentTab, setCurrentTab] = useState(constants.designSpreadshirtCom);
  const [selectedUploadOption, setUploadOption] = useState('Awaiting Upload');
  const tableProps = {setSubHeader,setHeader,setRightHeader,selectedUploadOption, setUploadOption};

  useEffect(() => {
    setUploadOption("Awaiting Upload");
  }, [currentTab]);

  return (
      <UploadDesignTabs setCurrentTab={setCurrentTab} isShowCounter={selectedUploadOption==="Awaiting Upload"} tableProps={tableProps} />
  );
};

export default Upload;
