import { Select, Tabs } from 'antd';
import React from 'react';
import ProductsForm from '../../../components/template/ProductsForm';
import BlueprintsForm from "../../../components/blueprints/BlueprintsForm";

const { TabPane } = Tabs;

function callback(key) {
  console.log(key);
}

const Templates = () => {
  return (
    <>
      <Tabs defaultActiveKey="1" onChange={callback}>
        <TabPane tab="Products" key="1">
          <ProductsForm />
        </TabPane>
        <TabPane tab="Blueprints" key="2">
         <BlueprintsForm/>
        </TabPane>
      </Tabs>
    </>
  );
};

export default Templates;
