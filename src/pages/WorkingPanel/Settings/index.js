import React from 'react';
import { Button, Typography } from 'antd';
import GoogleAuthenticator from '../../../assets/Icons/ReactSvg/GoogleAuthenticator';
import EditProfileForm from '../../../components/person/EditProfileForm';
import ResetPasswordForm from '../../../components/auth/resetPassword/ResetPasswordForm';
import UpdatableProfile from '../../../components/mediaobject/settings/UpdatableProfile';
import useWindowDimensions from '../../../utils/hooks/useWindowDimensions';
import { FlexDivCol, GoogleIcon, SettingsCard, SettingsContainer } from "../../../components/styles/ProfileStyles";

const { Title, Text } = Typography;

const Settings = () => {
  const { width } = useWindowDimensions()

  const flexDirection = width <= 740 ? { flexDirection: 'column-reverse' } : { flexDirection: 'row' };

  return (
    <SettingsContainer>
      <div style={{ margin: "1rem" }}>
        <UpdatableProfile />
      </div>
      <SettingsCard>
        <EditProfileForm />
      </SettingsCard>
      <SettingsCard>
        <div style={{ display: 'flex', width: '100%', justifyContent: "space-between", ...flexDirection }}>
          <FlexDivCol>
            <Title level={4}>Setup Google Two-Factor Authenticator</Title>
            <Text>Use the Authenticator app to get free verification codes, even when your phone is offline.</Text>
            <Text>Available for Android and iPhone.</Text>
            <Button size={'large'} style={{ maxWidth: "7rem", margin: '2rem 1rem 1rem 0' }}
                    type={'primary'}>Setup</Button>
          </FlexDivCol>
          <GoogleIcon component={GoogleAuthenticator} />
        </div>
      </SettingsCard>
      <SettingsCard>
        <FlexDivCol>
          <Title level={4}>Change password</Title>
          <Text style={{ maxWidth: '460px' }}>Enter your email address and we'll send you an email with instructions to
            change your password.</Text>
          <ResetPasswordForm />
        </FlexDivCol>
      </SettingsCard>
    </SettingsContainer>
  );
};

export default Settings;







