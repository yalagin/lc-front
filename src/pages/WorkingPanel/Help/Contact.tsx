import React from 'react';
import {FlexDivCol, GoogleIcon} from "../../../components/styles/ProfileStyles";
import {Button, Card, Col, Row, Space, Typography} from "antd";
import useWindowDimensions from "../../../utils/hooks/useWindowDimensions";
import EmailHelp from "../../../assets/Icons/ReactSvg/EmailHelp";
import GreyEllipse from "../../../assets/Icons/ReactSvg/GreyEllipse";
import HeadphonesHelp from "../../../assets/Icons/ReactSvg/HeadphonesHelp";
import SocialMediaGroup from "../../../assets/Icons/ReactSvg/SocialMedeiaGroup";

const Contact = () => {
  const {width} = useWindowDimensions();
  const {Title, Text} = Typography;

  const flexDirection = width <= 1000 ? {flexDirection: 'column'} : {flexDirection: 'row'} as {};
  return (
    <>
      <Row gutter={[12, 12]} style={{margin: '1rem'}}>
        <Col span={12}>
          <Card>
            <div
              style={{display: 'flex', width: '100%', justifyContent: "flex-start", ...flexDirection}}>
              <div style={{position: 'relative', width: "160px", height: "160px", minWidth: '130px'}}>
                <GoogleIcon style={{position: 'absolute'}} component={GreyEllipse}/>
                <GoogleIcon style={{position: 'absolute', top: '2rem', left: '2rem'}} component={EmailHelp}/>
              </div>
              <FlexDivCol>
                <Title level={4}>Need a helping hand? We’re all ears.</Title>
                <Text>You can contact us with anything repalted to our Products. We’ll get in</Text>
                <Text>touch with you as soon as possible.</Text>
                <Button size={'large'} style={{maxWidth: "7rem", margin: '1rem 0rem 0rem 0'}}
                        type={'primary'}>Email us</Button>
              </FlexDivCol>
            </div>
          </Card>
        </Col>
        <Col span={12}>
          <Card>
            <div style={{display: 'flex', width: '100%', justifyContent: "flex-start", ...flexDirection}}>
              <div style={{position: 'relative', width: "160px", height: "160px", minWidth: '130px'}}>
                <GoogleIcon style={{position: 'absolute'}} component={GreyEllipse}/>
                <GoogleIcon style={{position: 'absolute', top: '2rem', left: '2rem'}} component={HeadphonesHelp}/>
              </div>
              <FlexDivCol>
                <Title level={4}>Give us a ring!</Title>
                <Text>You can contact us with anything repalted to our Products not found what you</Text>
                <Text> needed ? Speack us directly.</Text>
                <Button size={'large'} style={{maxWidth: "7rem", margin: '1rem 0rem 0rem 0'}}
                        type={'primary'}>Contact us</Button>
              </FlexDivCol>
            </div>
          </Card>
        </Col>
      </Row>
      <Card style={{margin: '1.3rem'}}>
        <div style={{marginBottom:'-0.5rem',display: 'flex', width: '100%', justifyContent: "space-between", ...flexDirection}}>
          <Title level={4}>The user to our discord server.</Title>
          <Button type={'primary'}>Email us</Button>
        </div>
      </Card>
      <Card style={{margin: '1.3rem'}}>
        <div style={{marginBottom:'-0.5rem',  display: 'flex',  width: '100%', justifyContent: "space-between", ...flexDirection}}>
          <Title level={4}>How to write a good ticket</Title>
          <Button type={'primary'}>Learn How</Button>
        </div>
      </Card>
      <Card style={{margin: '1.3rem'}}>
        <div style={{marginBottom:'-0.5rem',  display: 'flex',  width: '100%', justifyContent: "space-between", ...flexDirection}}>
          <Title level={4}>Other social media channels</Title>
          <GoogleIcon component={SocialMediaGroup}/>
        </div>
      </Card>
    </>
  );
};

export default Contact;
