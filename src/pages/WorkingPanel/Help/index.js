import { Tabs } from 'antd';
import Faqs from "./Faqs";
import Tutorials from "./Tutorials";
import Contact from "./Contact";



const Help = () => {
  const { TabPane } = Tabs;

  return <Tabs defaultActiveKey="1">
    <TabPane tab="Faqs" key="1">
      <Faqs/>
    </TabPane>
    <TabPane tab="Tutorials" key="2">
     <Tutorials/>
    </TabPane>
    <TabPane tab="Contact" key="3">
     <Contact/>
    </TabPane>
  </Tabs>
}
export default Help;