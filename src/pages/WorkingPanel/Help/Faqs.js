import {Tabs, Typography} from 'antd';
import Collapsable from "../../../components/UI/Collapsable";
import FaqsTab from "../../../components/styles/FaqsTab";

function Faqs(props) {
  const {Title} = Typography;
  const {TabPane} = Tabs;
  return (<>
      <Title level={4}>Frequently asked questions </Title>
      <FaqsTab tabPosition={'left'}>
        <TabPane tab="Privacy" key="1">
          <Collapsable/>
        </TabPane>
        <TabPane tab="Usage" key="2">
          Content of Tab 2
        </TabPane>
        <TabPane tab="Polish" key="3">
          Content of Tab 3
        </TabPane>
        <TabPane tab="Privacy" key="4">
          Content of Tab 3
        </TabPane>
        <TabPane tab="Help center" key="5">
          Content of Tab 3
        </TabPane>
      </FaqsTab>
    </>
  );
}

export default Faqs;