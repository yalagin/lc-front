import { Card, Space, Tabs, Typography} from "antd";
import FaqsTab from "../../../components/styles/FaqsTab";

const {Meta} = Card;

new Array(10)

const Tutorials = () => {
    const {Title,Text} = Typography;
    const {TabPane} = Tabs;
    return (<>
        <Title level={4}>Tutorials</Title>
        <FaqsTab tabPosition={'left'}>
            <TabPane tab="Design" key="1">

                <Space size={[16, 16]} wrap>
                    {new Array(20).fill(null).map((_, index) => (
                        <Card
                        style={{width: 300}}
                        key={index}
                        cover={
                            <img
                                style={{borderRadius:'10px'}}
                                alt="example"
                                src="https://gw.alipayobjects.com/zos/rmsportal/JiqGstEfoWAOHiTxclqi.png"
                            />
                        }
                        bordered={false}
                    >
                        <Meta
                            title={<Text style={{color:"#7f67ff"}} >Tutorials</Text>}
                            description="This is the description"
                        />
                    </Card>
                        ))}
                </Space>
            </TabPane>
            <TabPane tab="Templates" key="2">
                Content of Tab 2
            </TabPane>
            <TabPane tab="Trademarks" key="3">
                Content of Tab 3
            </TabPane>
            <TabPane tab="Privacy" key="4">
                Content of Tab 3
            </TabPane>
            <TabPane tab="Lazymerch" key="5">
                Content of Tab 3
            </TabPane>
        </FaqsTab>
    </>);
}

export default Tutorials;