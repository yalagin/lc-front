import React from 'react';
import { Tabs, Tag, Typography } from 'antd';
import styled from 'styled-components';
import Check from "../../../components/trademarks/Check/Check";
import Track from "../../../components/trademarks/Track/Track";
import Watch from "../../../components/trademarks/Watch/Watch";


const WildTabs = styled(Tabs)`
  &&& .ant-tabs-tab + .ant-tabs-tab {
    margin: 0 0 0 70px;
  }
`;

const Trademarks = () => {
  const { TabPane } = Tabs;
  return (
    <div>
      <WildTabs defaultActiveKey="Check">
        <TabPane tab={<div> Check</div>} key="Check">
          <Check />
        </TabPane>
        <TabPane  tab={<>Track</>} key="Track">
          <Track/>
        </TabPane>
        <TabPane  tab={<> Watch </>} key="Watch">
          <Watch/>
        </TabPane>
      </WildTabs>
    </div>
  );
};

export default Trademarks;
