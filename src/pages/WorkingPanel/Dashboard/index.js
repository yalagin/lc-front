import React  from "react";
import { Card, List, Spin, Typography } from "antd";
import useProfile from '../../../utils/hooks/useProfile';
import News from "../../../components/dashboard/News";
import StatsInfo from "../../../components/dashboard/StatsInfo";
import ChangeLog from "../../../components/dashboard/ChangeLog";
import DesignUploadedLines from "../../../components/dashboard/statsInfo/DesignUploadedLines";
import _ from "lodash";
import useProject from "../../../utils/hooks/useProject";

const Dashboard = () => {
  const {profile} = useProfile();
  const {project} = useProject();
  return (
    <>
      <ChangeLog />
      <Typography.Title>Welcome back, {profile?.givenName}.</Typography.Title>
      <Typography.Title type="secondary" level={4}>
        Hi {profile?.givenName}, this is your personal space for everything{' '}
      </Typography.Title>
      <br />
      <Typography.Title level={3}>Recent News </Typography.Title>
      <News />
      <Spin spinning={!!!profile} />
      {!!project && !_.isEmpty( project?.id) && <DesignUploadedLines />}
      <StatsInfo />
    </>
  );
};

export default Dashboard;
