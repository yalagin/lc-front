import React from 'react';
import {fireEvent, render, screen} from './../../../utils/test-utils';
import '@testing-library/jest-dom/extend-expect';
import Landing from '../Landing';

describe('Landing', () => {
  test('load the landing and click on login and see submit btn', async () => {
    await render(<Landing />);
    expect(screen.getByRole('heading')).toHaveTextContent('your landing page');
    await fireEvent.click(screen.getByText('Login'));
    expect(screen.getAllByRole('button')[1]).toHaveTextContent('Submit');
  });
});
