import { Layout, Menu, Typography } from "antd";
import './Landing.css';
import { Link } from 'react-router-dom';
import { UploadOutlined } from '@ant-design/icons';
import CreateUserDrawerForm from '../../components/user/CreateUserDrawerForm';
import LoginDrawerForm from '../../components/auth/login/LoginDrawerForm';
import ForgotPasswordDrawer from '../../components/auth/login/ForgotPasswordDrawer';
import ResetPasswordModal from '../../components/auth/resetPasswordWithToken/ResetPasswordModal';
import React from 'react';
import ConfirmEmail from '../../components/auth/confirmEmail/ConfirmEmail';

const {Title} = Typography;

const { Header } = Layout;

export const Landing = () => {
  return (
    <Layout className="layout" style={{ minHeight: '100vh' }}>
      <Header>
        <Menu theme="dark" mode="horizontal">
          {!localStorage.getItem('token') && <Menu.Item key="create_user"> <CreateUserDrawerForm /></Menu.Item>}
          {!localStorage.getItem('token') && <Menu.Item key="forget_password"> <ForgotPasswordDrawer /></Menu.Item>}
          <Menu.Item key="dashboard"><Link to={'/dashboard'}> Dashboard (only for registered) </Link></Menu.Item>
          {localStorage.getItem('token') ?
            <Menu.Item
              key="logout"
              icon={<UploadOutlined />}
              onClick={() => {
                localStorage.clear()
              }}
            >
              <Link to="/">Logout</Link>
            </Menu.Item> :
            <Menu.Item key="login" disabled={localStorage.getItem('token')}>
              <LoginDrawerForm />
            </Menu.Item>}
        </Menu>
      </Header>
       {/*those two using url queries*/}
      <ResetPasswordModal />
      <ConfirmEmail />
      <Title data-testid={'landing'}>your landing page</Title>
    </Layout>
  );
}

export default Landing;
