import { fetch2 } from "../dataAccess";

export const mbaProducts = () =>
  fetch2('/template_settings_mba_products?groups%5B%5D=product%3Aget');
