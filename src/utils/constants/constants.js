import { designsNames } from '../designHelperFunctions';

export const HYDRAMEMBER = 'hydra:member';
export const DISABLED = 'disabled';
export const designDisplate = 'designDisplate';
export const designTeespring = 'designTeespring';
export const designMBA = 'designMBA';
export const designPrintful = 'designPrintful';
export const designZazzle = 'designZazzle';
export const designShirtee = 'designShirtee';
export const designTeepublic = 'designTeepublic';
export const designSociety6 = 'designSociety6';
export const designRedBubble = 'designRedBubble';
export const designSpreadshirtEu = 'designSpreadshirtEu';
export const designSpreadshirtCom = 'designSpreadshirtCom';
export const general = 'General';
export const MBA = 'MBA';
export const MIN_COLUMN_WIDTH = 70;
export const createdAt = 'createdAt';
export const updatedAt = 'updatedAt';
export const uploadStatus = 'uploadStatus';
export const isDeleted = 'isDeleted';
export const classification = 'classification';
export const region = 'region';
export const country = 'country';
export const bulletpoints = 'bulletpoints';
export const bulletpoints2 = 'bulletpoints2';
export const title26 = 'title26';
export const title40 = 'title40';
export const title50 = 'title50';
export const title60 = 'title60';
export const title100 = 'title100';
export const description140 = 'description140';
export const description200 = 'description200';
export const description250 = 'description250';
export const description2000 = 'description2000';
export const brand = 'brand';
export const fitMan = 'fitMan';
export const fitWoman = 'fitWoman';
export const fitYouth = 'fitYouth';
export const isAdult = 'isAdult';
export const isUpload = 'isUpload';
export const isUploaded = 'isUploaded';
export const tags = 'tags';
export const isEdit = 'isEdit';
export const image = 'image';
export const translations = 'translations';
export const inspirationLinks = 'inspirationLinks';
export const text = 'text';
export const description = 'description';
export const workingFiles = 'workingFiles';
export const isPlaceholder = 'isPlaceholder';
export const spreadshirtId = 'spreadshirtId';
export const redBubbleId = 'redBubbleId';
export const mbaId = 'mbaId';
export const mbaProductPrice = 'mbaProductPrice';
export const mbaColors = 'mbaColors';
export const mbaScale = 'mbaScale';
export const mbaProductCheckbox = 'mbaProductCheckbox';
export const mbaProductPublished = 'mbaProductPublished';
export const teespringDuplicationURL = 'teespringDuplicationURL';
export const society6Colors = 'society6Colors';
export const printfulProductName = 'printfulProductName';
export const printfulStoreName = 'printfulStoreName';
export const printfulColors = 'printfulColors';
export const zazzleStore = 'zazzleStore';
export const displateColorPicker = 'displateColorPicker';
export const redBubbleDuplicationURL = 'redBubbleDuplicationURL';
export const zazzleSelectDesignTemplate = 'zazzleSelectDesignTemplate';
export const shirteeSelectDesignTemplate = 'shirteeSelectDesignTemplate';
export const spreadshirtComSelectDesignTemplate = 'spreadshirtComSelectDesignTemplate';
export const teepublicColors = 'teepublicColors';
export const spreadshirtEuSelectDesignTemplate = 'spreadshirtEuSelectDesignTemplate';
export const generalTranslatedFields = 'generalTranslatedFields';
export const designMBATranslated = 'designMBATranslated';
export const teepublicProductEnabled = 'isTeepublicProductEnabled';
export const teepublicProduct = 'teepublicProduct';
export const teepublicShirtPrintingIsEnabled = 'teepublicShirtPrintingIsEnabled';
export const teepublicShirtSelection = 'teepublicShirtSelection';
export const teepublicShirtPrintingSelection = 'teepublicShirtPrintingSelection';
export const Publication = 'Publication';
export const timeStamps = [createdAt, updatedAt];
export const designs = [
  // general,
  designSpreadshirtCom,
  designSpreadshirtEu,
  designRedBubble,
  designSociety6,
  designTeepublic,
  designShirtee,
  designZazzle,
  designPrintful,
  designMBA,
  designTeespring,
  designDisplate,
];

export const designsWithFixedMba = [
  // general,
  designSpreadshirtCom,
  designSpreadshirtEu,
  designRedBubble,
  designSociety6,
  designTeepublic,
  designShirtee,
  designZazzle,
  designPrintful,
  'designMba',
  designTeespring,
  designDisplate,
];

//this for greendots on general window
export const arrayOfCopiedFields = [
  title40,
  title50,
  title60,
  title100,
  description200,
  description250,
  description2000,
];
export const arrayOfTitles = [title26, title40, title50, title60, title100];
export const arrayOfDescriptions = [
  description140,
  description200,
  description250,
  description2000,
];
export const arrayOfMBAFields = [
  title60,
  description2000,
  bulletpoints,
  bulletpoints2,
  brand,
  fitMan,
  fitWoman,
  fitYouth,
];
export const objectOfDesignFieldsForTransferring = {
  designSpreadshirtCom: [title50, description200, tags],
  designSpreadshirtEu: [title50, description200, tags],
  designRedBubble: [title50, description200, tags],
  designSociety6: [title100, description250, tags],
  designTeepublic: [title50, description200, tags],
  designShirtee: [title40, description200, tags],
  designZazzle: [title50, description2000, tags], //there are other fields
  designPrintful: [title50, description200, tags], //there are other fields
  designMBA: arrayOfMBAFields,
  designTeespring: [title40, description200],
  designDisplate: [title26, description140, tags], //there are other fields
};
export const notTranslationFields = [fitMan, fitWoman, fitYouth];
export const objectOfDesignsAmountOfTags = {
  designSpreadshirtCom: 25,
  designSpreadshirtEu: 25,
  designRedBubble: 50,
  designSociety6: 20,
  designTeepublic: 15,
  designShirtee: 10,
  designZazzle: 10,
  designPrintful: 25,
  designDisplate: 20,
};

//this is for displaying the data
export const ForTreeFilteringInDesignTable = {
  Miscellaneous: [image, /*isEdit,*/ isPlaceholder, isUpload, /*isUploaded,*/ ...timeStamps],
  general: [
    isAdult,
    title26,
    title40,
    title50,
    title60,
    title100,
    description140,
    description200,
    description250,
    description2000,
    tags,
  ],
  designSpreadshirtCom: [
    title50,
    description200,
    tags,
    spreadshirtComSelectDesignTemplate,
    'spreadshirtComIsShop',
    'spreadshirtComShop',
    'spreadshirtComIsMarketplace',
    spreadshirtId,
  ],
  designSpreadshirtEu: [
    title50,
    description200,
    tags,
    spreadshirtEuSelectDesignTemplate,
    'spreadshirtEuIsMarketplace',
    'spreadshirtEuIsShop',
    'spreadshirtEuShop',
    spreadshirtId,
    'spreadshirtEuTargetmarket',
  ],
  designRedBubble: [title50, description200, tags, redBubbleDuplicationURL, redBubbleId],
  designSociety6: [title100, description250, tags, society6Colors, 'society6Id'],
  designTeepublic: [title50, description200, tags, 'teepublicId'],
  designShirtee: [
    title40,
    description200,
    tags,
    shirteeSelectDesignTemplate,
    'shirteeShop',
    'category',
    'shirteeMarketplace',
    'shirteeDescriptionTemplate',
  ],
  designZazzle: [
    title50,
    description2000,
    tags,
    zazzleSelectDesignTemplate,
    zazzleStore,
    'occasion',
    'recipient',
    'category',
    'audience',
  ],
  designPrintful: [
    title50,
    description200,
    tags,
    printfulColors,
    'shopName',
    printfulStoreName,
    printfulProductName,
  ],
  designMBA: [...arrayOfMBAFields, mbaId, Publication],
  designTeespring: [title40, description200, teespringDuplicationURL, 'teespringShop'],
  designDisplate: [title26, description140, tags, displateColorPicker, 'collection', 'category'],
};
export const keyAliasesForTreeData = { Miscellaneous: general };
//this is for displaying the data
//this is all fields
export const objectOfDesignFieldsForProcessingSubDesigns = {
  general: [
    title26,
    title40,
    title50,
    title60,
    title100,
    description140,
    description200,
    description250,
    description2000,
    brand,
    bulletpoints,
    bulletpoints2,
    tags,
    uploadStatus,
  ],
  designSpreadshirtCom: [
    title50,
    description200,
    tags,
    spreadshirtComSelectDesignTemplate,
    'spreadshirtComIsShop',
    'spreadshirtComShop',
    'spreadshirtComIsMarketplace',
    spreadshirtId,
    uploadStatus,
  ],
  designSpreadshirtEu: [
    title50,
    description200,
    tags,
    spreadshirtEuSelectDesignTemplate,
    'spreadshirtEuIsMarketplace',
    'spreadshirtEuIsShop',
    'spreadshirtEuShop',
    'spreadshirtEuTargetmarket',
    spreadshirtId,
    uploadStatus,
  ],
  designRedBubble: [
    title50,
    description200,
    tags,
    redBubbleDuplicationURL,
    redBubbleId,
    uploadStatus,
  ],
  designSociety6: [title100, description250, tags, society6Colors, 'society6Id', uploadStatus],
  designTeepublic: [
    title50,
    description200,
    tags,
    'teepublicColors',
    'teepublicId',
    teepublicProductEnabled,
    teepublicProduct,
    teepublicShirtPrintingIsEnabled,
    teepublicShirtSelection,
    uploadStatus,
  ],
  designShirtee: [
    title40,
    description200,
    tags,
    shirteeSelectDesignTemplate,
    uploadStatus,
    'shirteeShop',
    'category',
    'shirteeMarketplace',
    'shirteeDescriptionTemplate',
  ],
  designZazzle: [
    title50,
    description2000,
    tags,
    zazzleSelectDesignTemplate,
    zazzleStore,
    'occasion',
    'recipient',
    'category',
    'audience',
    uploadStatus,
  ],
  designPrintful: [
    title50,
    description200,
    tags,
    printfulColors,
    'shopName',
    uploadStatus,
    printfulStoreName,
    printfulProductName,
  ],
  designMBA: [
    ...arrayOfMBAFields,
    mbaId,
    mbaProductPrice,
    mbaColors,
    mbaScale,
    mbaProductCheckbox,
    uploadStatus,
    mbaProductPublished,
  ],
  designTeespring: [
    title40,
    description200,
    teespringDuplicationURL,
    uploadStatus,
    'teespringShop',
  ],
  designDisplate: [
    title26,
    description140,
    tags,
    displateColorPicker,
    'collection',
    'category',
    uploadStatus,
  ],
};
//this is all fields

export const timeFormat = 'YYYY-MM-DDThh:mm:ss.sZ';
export const numberOfShowedFieldsInDesignTable = 25;
export const decodeTranslationObject = {
  en: 'English',
  de: 'German',
  fr: 'French',
  es: 'Spanish',
  it: 'Italian',
  jp: 'Japanese',
};
export const creation = 'creation';
export const listing = 'listing';
export const days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
export const months = [
  'January',
  'February',
  'March',
  'April',
  'May',
  'June',
  'July',
  'August',
  'September',
  'October',
  'November',
  'December',
];
export const initialQueries = [{ field: 'isEdit', value: false, type: 'boolean' }];
export const templateFields = [
  spreadshirtEuSelectDesignTemplate,
  'spreadshirtEuIsMarketplace',
  'spreadshirtEuIsShop',
  'spreadshirtEuShop',
  'spreadshirtEuTargetmarket',
  spreadshirtComSelectDesignTemplate,
  'spreadshirtComIsShop',
  'spreadshirtComShop',
  'spreadshirtComIsMarketplace',
  shirteeSelectDesignTemplate,
  'shirteeShop',
  'shirteeMarketplace',
  'shirteeDescriptionTemplate',
  zazzleSelectDesignTemplate,
  redBubbleDuplicationURL,
  displateColorPicker,
  zazzleStore,
  society6Colors,
  printfulStoreName,
  printfulProductName,
  printfulColors,
  teespringDuplicationURL,
  'teespringShop',
  teepublicColors,
  teepublicProductEnabled,
  teepublicProduct,
  teepublicShirtPrintingIsEnabled,
  teepublicShirtSelection,
  mbaProductPrice,
  mbaColors,
  mbaScale,
  mbaProductCheckbox,
];
export const hasIdField = [
  designSpreadshirtCom,
  designSpreadshirtEu,
  designRedBubble,
  designSociety6,
  designTeepublic,
  designMBA,
];

//generalfields
export const creationGeneralFields = [inspirationLinks, text, description, workingFiles];
export const generalFields = [
  title26,
  title40,
  title50,
  title60,
  title100,
  description140,
  description200,
  description250,
  description2000,
  brand,
  fitMan,
  fitWoman,
  fitYouth,
  isAdult,
  bulletpoints,
  bulletpoints2,
  tags,
];
export const generalTableFields = [
  image,
  /* isEdit,*/ isPlaceholder,
  isAdult,
  isUpload,
  /*isUploaded,*/ ...timeStamps,
];
export const objectOfFakeData = {
  bulletpoints: bulletpoints,
  bulletpoints2: bulletpoints2,
  title26: title26,
  title40: title40,
  title50: title50,
  title60: title60,
  title100: title100,
  description140: description140,
  description200: description200,
  description250: description250,
  description2000: description2000,
  brand: brand,
  fitMan: true,
  fitWoman: false,
  fitYouth: true,
  adult: true,
  tags: [...generalFields, ...designs],
};
export const getTypeOfField = field => {
  let copy = field.slice();
  designsNames.forEach(design => {
    copy = copy.replace(design, '');
  });
  if (/tags$/.test(copy)) return tags;
  if (/ColorPicker$/.test(copy)) return 'string';
  if (/Links$/.test(copy)) return tags;
  if (/Placeholder$/.test(copy)) return isPlaceholder;
  if (/image$/.test(copy)) return image;
  if (/Files$/.test(copy)) return 'file';
  if (/fit/.test(copy)) return 'bool';
  if (/is|Is/.test(copy)) return 'bool';
  if (/At/.test(copy)) return 'time';
  if (/uploadStatus/.test(copy)) return uploadStatus;
  if (/Publication/.test(copy)) return Publication;
  return 'string';
};
export const trademarkFilter = {
  region: ['US', 'DE', 'UK', 'FR', 'IT', 'ES', 'JP', 'EN'],
  classification: [9, 16, 18, 20, 21, 22, 24, 25],
};

export const APPAREL = 'Apparel';
export const STICKERS = 'Stickers';
export const CASES = 'Cases';
export const MUGS = 'Mugs';
export const WALL_ART = 'Wall art';
export const NOTEBOOKS = 'Notebook';
export const PILLOWS = 'Pillows';
export const TOTES = 'Totes';
export const TAPESTRIES = 'Tapestries';
export const PINS = 'Pins';
export const MAGNETS = 'Magnets';
export const MASKS = 'Masks';

export const urlsForDesigns = {
  designSpreadshirtCom: 'design_spreadshirt_coms',
  designSpreadshirtEu: 'design_spreadshirt_eus',
  designRedBubble: 'design_red_bubbles',
  designSociety6: 'design_society6s',
  designTeepublic: 'design_teepublics',
  designShirtee: 'design_shirtees',
  designZazzle: 'design_zazzles',
  designPrintful: 'design_printfuls',
  designMBA: 'design_mbas',
  designTeespring: 'design_teesprings',
  designDisplate: 'design_displates'
}
