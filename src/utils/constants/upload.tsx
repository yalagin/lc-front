import {
  arrayOfMBAFields, brand, bulletpoints, bulletpoints2, description,
  description140,
  description200,
  description2000,
  description250, displateColorPicker, fitMan, fitWoman, fitYouth, generalTableFields,
  image,
  isAdult,
  isPlaceholder,
  isUpload, mbaColors, mbaId, mbaProductCheckbox, mbaProductPrice, printfulColors, Publication,mbaScale,
  redBubbleDuplicationURL,
  redBubbleId,
  shirteeSelectDesignTemplate,
  society6Colors,
  printfulProductName,
  printfulStoreName,
  spreadshirtComSelectDesignTemplate,
  spreadshirtEuSelectDesignTemplate,
  spreadshirtId,
  tags, teespringDuplicationURL, text,
  timeStamps,
  title100,
  title26,
  title40,
  title50,
  title60, uploadStatus, zazzleSelectDesignTemplate, zazzleStore
} from "./constants";

export const ForTreeFilteringInDesignTable = {
  Miscellaneous: [image, /*isEdit,*/ isPlaceholder, isUpload,  /*isUploaded,*/ ...timeStamps],
  general: [isAdult, title26,
    title40,
    title50,
    title60,
    title100,
    description140,
    description200,
    description250,
    description2000,
    tags,uploadStatus],
  designSpreadshirtCom: [title50, description200, tags, spreadshirtComSelectDesignTemplate,'spreadshirtComIsShop','spreadshirtComShop', 'spreadshirtComIsMarketplace',uploadStatus],
  designSpreadshirtEu: [title50, description200, tags,spreadshirtEuSelectDesignTemplate,"spreadshirtEuIsMarketplace",'spreadshirtEuIsShop', "spreadshirtEuShop",'spreadshirtEuTargetmarket',uploadStatus],
  designRedBubble: [title50, description200, tags,redBubbleDuplicationURL,redBubbleId,uploadStatus],
  designSociety6: [title100, description250, tags,society6Colors,printfulStoreName,printfulProductName,uploadStatus],
  designTeepublic: [title50, description200, tags  /*"teepublicColors",*/  ,uploadStatus],
  designShirtee: [title40, description200, tags,shirteeSelectDesignTemplate,uploadStatus,'shirteeShop','category','shirteeMarketplace','shirteeDescriptionTemplate'],
  designZazzle: [title50, description2000, tags,zazzleSelectDesignTemplate,zazzleStore,  "occasion","recipient","category","audience",uploadStatus],
  designPrintful: [title50, description200, tags,printfulColors, "shopName",uploadStatus],
  designMBA: [...arrayOfMBAFields, mbaId,/* mbaProductPrice,mbaColors,mbaProductCheckbox*/uploadStatus],
  designTeespring: [title40, description200,teespringDuplicationURL,uploadStatus,'teespringShop'],
  designDisplate: [title26, description140, tags,displateColorPicker  ,"collection","category",uploadStatus],
};
//translation fields
export const objectOfDesignsWithTranslatedFields = {
  general: [
    title26,
    title40,
    title50,
    title60,
    title100,
    description140,
    description200,
    description250,
    description2000,
    brand,
    bulletpoints,
    bulletpoints2,
    tags],
  designSpreadshirtEu: [title50, description200, tags],
  designRedBubble: [title50, description200, tags],
  designShirtee: [title40, description200, tags],
  designMBA: [title60, description2000, bulletpoints, bulletpoints2, brand],
};
export const objectOfDesignsWithNotTranslatedFields = {
  general: [...generalTableFields, isAdult, /*inspirationLinks,*/ text, description, /*workingFiles*/uploadStatus],
  designSpreadshirtEu: [spreadshirtEuSelectDesignTemplate,"spreadshirtEuIsMarketplace",'spreadshirtEuIsShop', "spreadshirtEuShop",'spreadshirtEuTargetmarket', /*spreadshirtId*/uploadStatus,],
  designRedBubble: [/*redBubbleId*/uploadStatus],
  designShirtee: [uploadStatus],
  designMBA: [/*fitMan, fitWoman, fitYouth, mbaProductPrice, mbaColors, mbaProductCheckbox, *//*mbaId*/uploadStatus,Publication],
};


/**
 *  We have to differ here It's upload states, properties each design has. These will be:
 *      - idle - design has been saved and is in our overview. It isn't uploaded yet, nor in queue.
 *      - uploaded - design was in UQ and has been succesfully uploaded by the automation tool
 *      - failed - design was in UQ but the upload has been failed. Design remains in UQ until the user manually removes it
 *                from there. Removing won't change the upload uploadStatus (so in overview you can still see for which platforms the upload failed)
 *      - queue - design is currently in the UQ for a platform but hasn't been uploaded by the automation tool so far
 *  And then there's "states" to filter within the UQ (where you created this dropdown) so users can have more flexibility
 *  by tracking which design hasn't been uploaded yet, which was succesfully and which failed:
 *      Awaiting Upload - this will show the designs that are currently in the queue and the failed ones
 *      Uploaded - this will just show the succesfully uploaded ones
 *      All - this will show all the designs whether they are uploaded or not but they are sorted as mentioned above.
 */
export const canceled   = "canceled";
export const queue      = "queue";
export const failed     = "failed";
export const uploaded   = "uploaded";
export const idle       = 'idle';
export const changed    = 'changed';
export const updated    = 'updated';
export const UploadStatusesNumberToString = {
  1: idle,
  2: uploaded,
  3: failed,
  4: queue,
  5: canceled,
  6: changed,
  7: updated
} as { [key: number ]: string; }
export const UploadStatusesForFilter = {
   idle :1,
   uploaded :2,
   failed:3,
   queue:4,
  changed:6,
  updated:7,
   canceled:5
}
export const UploadStatusesForFilterUpload = {
  idle :1,
  uploaded :2,
  failed:3,
  queue:4,
  changed:6,
  updated:7,
  canceled:5
}
export enum UploadStatusesEnum {
  zero,
  idle ,
  uploaded ,
  failed,
  queue,
  canceled,
  changed,
  updated
}

export const UploadStatusesHelpObject= {
  canceled: 'canceled',
  idle : "design has been saved and is in our overview. It isn't uploaded yet, nor in queue.",
  uploaded : "design was in UQ and has been successfully uploaded by the automation tool",
  failed : "design was in UQ but the upload has been failed. Design remains in UQ until the user manually removes it from there. Removing won't change the upload status (so in overview you can still see for which platforms the upload failed)",
  queue : "design is currently in the UQ for a platform but hasn't been uploaded by the automation tool so far",
  changed: "If an already uploaded design for any other then MBA platform  will be changed, the design gets the \"Changed\" state. Special state for MBA, can not edit uploaded products/marketplaces, can edit new products/marketplaces or design properties",
  updated: "Special state for MBA, can not edit uploaded products/marketplaces, can edit new products/marketplaces or design properties"
}  as { [key: string ]: string; }
const IDLE_STATUS = 1;
const UPLOADED_STATUS = 2;
const FAILED_STATUS = 3;
const QUEUE_STATUS = 4;
const CANCELED_STATUS = 5;
const CHANGED_STATUS = 6;
const UPDATED_STATUS = 7;


export const designRoutes = {
  designSpreadshirtCom:'/design_spreadshirt_coms',
  designSpreadshirtEu:'/design_spreadshirt_eus',
  designRedBubble:'/design_red_bubbles',
  designSociety6:'/design_society6s',
  designTeepublic:'/design_teepublics',
  designShirtee:'/design_shirtees',
  designZazzle:'/design_zazzles',
  designPrintful:'/design_printfuls',
  designMBA:'/design_mbas',
  designTeespring:'/design_teesprings',
  designDisplate:'/design_displates',
}

