class MyDate extends Date {
  constructor(x = null) {
    super(x)
  }

  getFirstDayOfWeek = function() {
    return new Date(this.setDate(this.getDate() - this.getDay() + (this.getDay() === 0 ? -6 : 1)));
  }

  getLastDayOfWeek = function() {
    return new Date(this.setDate(this.getDate() - (this.getDay() === 0 ? 7 : this.getDay()) + 7));
  }

  getMinutesAgo = function(number) {
    const currentDate = new Date();
    return new Date(currentDate.getTime() - number*60000);
  }

  getDaysAgo = function(number) {
    return new Date(new Date().getTime() - (number*24 * 60 * 60 * 1000));
  }
}

export default MyDate;
