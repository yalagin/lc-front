import { ENTRYPOINT } from '../config/entrypoint';
import { SubmissionError } from 'redux-form';
import get from 'lodash/get';
import has from 'lodash/has';
import mapValues from 'lodash/mapValues';
import { saveAppToken } from "../actions/user/login";
import { FORM_ERROR } from "final-form";

export const MIME_TYPE = 'application/ld+json';
export const MIME_TYPE_PATCH = 'application/merge-patch+json';

//this is for redux
export function fetch(id, options = {}) {
  if ('undefined' === typeof options.headers) options.headers = new Headers();
  if (null === options.headers.get('Accept'))
    options.headers.set('Accept', MIME_TYPE);

  if (
    'undefined' !== options.body &&
    !(options.body instanceof FormData) &&
    null === options.headers.get('Content-Type')
  )
    options.headers.set('Content-Type', MIME_TYPE);

  if(localStorage.getItem('token')) {
    options.headers.set('Authorization', `Bearer ${localStorage.getItem('token')}`)
  }

  return global.fetch(new URL(id, ENTRYPOINT), options).then(response => {
    if (response.ok) return response;

    return response.json().then(
      json => {
        const error =
          json['hydra:description'] ||
          json['hydra:title'] ||
          json['message'] ||
          'An error occurred.';
        if (!json.violations) throw Error(error);

        let errors = { _error: error };
        json.violations.forEach(violation =>
          errors[violation.propertyPath]
            ? (errors[violation.propertyPath] +=
                '\n' + errors[violation.propertyPath])
            : (errors[violation.propertyPath] = violation.message)
        );

        throw new SubmissionError(errors);
      },
      () => {
        throw new Error(response.statusText || 'An error occurred.');
      }
    );
  });
}

//this is for simple fetch
export const fetch2 = async (id, init = {}) => {
  await checkToken();
  if (typeof init.headers === "undefined") init.headers = {};
  if (!init.headers.hasOwnProperty("Accept")) init.headers = { ...init.headers, Accept: MIME_TYPE };
  if (
      init.body !== undefined &&
      !(init.body instanceof FormData) &&
      !init.headers.hasOwnProperty("Content-Type")
  )
    init.headers = { ...init.headers, "Content-Type": MIME_TYPE };
  if(localStorage.getItem('token')) {
    init.headers = { ...init.headers, "Authorization":  `Bearer ${localStorage.getItem('token')}` };
  }

  const resp = await global.fetch(ENTRYPOINT + id, init);
  if (resp.status === 204) return;

  const json = await resp.json();
  if (resp.ok) return json;

  const error = json["hydra:description"] ||json.message || resp.statusText;
  if (!json.violations) throw new SubmissionError({ [FORM_ERROR]: error });
  // if (!json.violations) throw Error(error);

  const errors = { _error: error };
  json.violations.map(
      (violation) =>
          (errors[violation.propertyPath] = violation.message)
  );

  throw new SubmissionError(errors);
};


export function mercureSubscribe(url, topics) {
  topics.forEach(topic =>
    url.searchParams.append('topic', new URL(topic, ENTRYPOINT))
  );

  return new EventSource(url.toString());
}

export function normalize(data) {
  if (has(data, 'hydra:member')) {
    // Normalize items in collections
    data['hydra:member'] = data['hydra:member'].map(item => normalize(item));

    return data;
  }

  // Flatten nested documents
  return mapValues(data, value =>
    Array.isArray(value)
      ? value.map(v => get(v, '@id', v))
      : get(value, '@id', value)
  );
}

export function extractHubURL(response) {
  const linkHeader = response.headers.get('Link');
  if (!linkHeader) return null;

  const matches = linkHeader.match(
    /<([^>]+)>;\s+rel=(?:mercure|"[^"]*mercure[^"]*")/
  );
  return false;
  // return matches && matches[1] ? new URL(matches[1], ENTRYPOINT) : null;
}

export function checkToken (){

  if (localStorage.getItem('user')) {
    // decode jwt so that we know if and when it expires
    const tokenExpiration = localStorage.getItem('user') && JSON.parse(localStorage.getItem('user')).exp;
    if (tokenExpiration && Date.now() >= tokenExpiration * 1000) {
      // make sure we are not already refreshing the token
      const formData = new FormData();
      formData.append('refresh_token', localStorage.getItem('refresh_token'));
      return global.fetch(new URL('/token/refresh', ENTRYPOINT),{ method: 'POST', body: formData })
        .then((response) => {
          return response.json();
        })
        .then((t) => {
          saveAppToken(t);
          return t.token ? Promise.resolve(t.token) : Promise.reject(new Error({
            message: 'could not refresh token'
          }));
        })
        .catch((e) => {
          localStorage.clear();
          window.location.reload();
          return Promise.resolve();
        });
    }
  }
}

