import _ from 'lodash';
import {workingFiles} from './constants/constants';

export function capitalizeFirstLetter(string) {
  return string.charAt(0).toUpperCase() + string.slice(1);
}

export function isMacintosh() {
  return navigator.platform.indexOf('Mac') > -1
}

export function isWindows() {
  return navigator.platform.indexOf('Win') > -1
}

export function validURL(str) {
  var pattern = new RegExp('^(https?:\\/\\/)?' + // protocol
    '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|' + // domain name
    '((\\d{1,3}\\.){3}\\d{1,3}))' + // OR ip (v4) address
    '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*' + // port and path
    '(\\?[;&a-z\\d%_.~+=-]*)?' + // query string
    '(\\#[-a-z\\d_]*)?$', 'i'); // fragment locator
  return !!pattern.test(str);
}

export const areArraysEqualComparisonWithUploadFileNames =(array1, array2, field) => {
  if (field === workingFiles && !_.isEmpty(array1) && !_.isEmpty(array2) ) {
     array1 = array1.map(file => file?.originalName)
     array2 = array2.map(file => file?.originalName)
  }

  return _.isEqual(array1,array2);
}

export const convertToUrl =( string ) => string.replace(/[A-Z]/g, m => '_' + m.toLowerCase()) + 's';

export const getFileExt = (fileName) => {
  const lastDot = fileName.lastIndexOf('.');

  return fileName.substring(lastDot + 1);
};
export const isEmpty = obj => [Object, Array].includes((obj || {}).constructor) && !Object.entries((obj || {})).length;


export function isItDark(imageSrc,callback) {
  var fuzzy = 0.3;
  var img = document.createElement("img");
  img.src = imageSrc;
  // img.style.display = "none";
  // document.body.appendChild(img);
  img.setAttribute('crossOrigin', '');

  img.onload = function() {
    // create canvas
    var canvas = document.createElement("canvas");
    canvas.width = this.width;
    canvas.height = this.height;

    var ctx = canvas.getContext("2d");
    ctx.drawImage(this,0,0);

    var imageData = ctx.getImageData(0,0,canvas.width,canvas.height);
    var data = imageData.data;
    var r,g,b, max_rgb;
    var light = 0, dark = 0;

    for(var x = 0, len = data.length; x < len; x+=4) {
      r = data[x];
      g = data[x+1];
      b = data[x+2];
      // Ignore transparent pixels
      if (data[x+3] === 0) {
        continue;
      }

      max_rgb = Math.max(Math.max(r, g), b);
      if (max_rgb < 128)
        dark++;
      else
        light++;
    }

    // Calculate the dark to light ratio
    var dl_diff = ((light - dark) / (light + dark));

    // return dl_diff + fuzzy < 0;

    if (dl_diff + fuzzy < 0)
      callback(true); /* Dark. */
    else
      callback(false);  /* Not dark. */
  }
}


export   function removeDisabledAndEmptyFields(obj) {
  _.forOwn(obj, function(value, key) {
    if (key.endsWith('disabled')) {
      return _.unset(obj, key);
    }

    if (_.isObject(value)) {
      removeDisabledAndEmptyFields(value);
    }
    if (
      !Number.isInteger(value) &&
      !_.isBoolean(value) &&
      _.isEmpty(value) &&
      key !== 'translations' &&
      key !== 'workingFiles'
    ) {
      // return (values[HYDRAMEMBER][obj][key] = null);
      return _.set(obj, key, null);
    }
  });
}
