import _ from "lodash";

export const combinations= (array: string[]):string[][] =>{
  const result = [];
  result.push(array);
  for (let i = 1; i < array.length; i++) {
    result.push(array.slice(0,-i))
  }
  return result;
}

export const collectResults = (array: string[]) => {
  const result = [];
  for (let i = 0; i < array.length; i++) {
    result.push(...combinations(array.slice(i)));
  }
  return _.uniqWith(result, _.isEqual);
}

export function splitSentenceToWords(value: string):string[] {
  const array = value
    .replace(/\r\n|\r|\n/g, '')
    .trim()
    .split(' ');
  let col = collectResults(array);
  return col.map((array : string[])=> array.join(' '));
}
// this is for all possible variations of array
// export function fromOneToFiveCombinationArray(array){
//   const combined = [];
//   for (let i = 1; i < 6; i++) {
//     combined.push(...combinations(array,i));
//   }
//   return combined
// }
// // Generate all k-combinations of first n array elements:
// export function* combinations(array, k, n = array.length) {
//   if (k < 1) {
//     yield [];
//   } else {
//     for (let i = --k; i < n; i++) {
//       for (let combination of combinations(array, k, i)) {
//         combination.push(array[i]);
//         yield combination;
//       }
//     }
//   }
// }
