const { useEffect } = require("react");
const UseLockPage = () =>{

  useEffect(() => {
    document.body.style.overflow = 'hidden';
    window.scroll({
      top: 0,
      left: 0,
      behavior: 'smooth',
    });
    return () => (document.body.style.overflow = 'unset');
  }, []);


}
