import React, { useEffect } from "react";
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import { list as projectList } from "../../actions/project/list";

const useLoadProject = () => {

  const dispatch = useDispatch();
  const { retrieved, loading, error } = useSelector(state => state.project.list, shallowEqual);

  useEffect(() => {
    if (!loading && !error && !retrieved) {
      dispatch(projectList('projects?founder=' + localStorage.getItem('id')));
    }
  }, [retrieved, loading, error]);

};

export default useLoadProject;
