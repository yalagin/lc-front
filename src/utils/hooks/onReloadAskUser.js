import React, { useEffect } from "react";

const onReloadAskUser = () => {
  useEffect(() => {
    window.onbeforeunload = function() {
      return "are you sure?";
    };

    return () => {
      window.onbeforeunload = null;
    };
  }, []);
  return null;
};

export default onReloadAskUser;
