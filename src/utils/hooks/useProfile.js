import { useEffect, useState } from 'react';
import { shallowEqual, useDispatch, useSelector } from 'react-redux';
import { list as personList } from '../../actions/person/list';
import { reset as personUpdateReset } from '../../actions/person/update';
import { HYDRAMEMBER } from '../constants/constants';

export default function useProfile() {
  const [profileError, setProfileError] = useState(null);

  const { retrieved, loading, error, eventSource, deletedItem } = useSelector(
    state => state.person.list,
    shallowEqual
  );
  const { updated } = useSelector(state => state.person.update, shallowEqual);

  const [profile, setProfile] = useState(
    retrieved && retrieved[HYDRAMEMBER] && retrieved[HYDRAMEMBER][0]
      ? retrieved[HYDRAMEMBER][0]
      : null
  );

  useEffect(() => {
    if (retrieved && retrieved[HYDRAMEMBER] && retrieved[HYDRAMEMBER].length !== 1) {
      setProfileError(
        `You dont have a profile, please create and fill up your profile info !`
      );
    }

    if (retrieved && retrieved[HYDRAMEMBER] && retrieved[HYDRAMEMBER][0]) {
      setProfile(retrieved[HYDRAMEMBER][0]);
    }

  }, [retrieved]);

  return { profile, retrieved, loading, error, eventSource, deletedItem, profileError };
}
