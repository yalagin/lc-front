import React, { useEffect } from "react";
import { reset as personUpdateReset } from "../../actions/person/update";
import { list as personList } from "../../actions/person/list";
import { shallowEqual, useDispatch, useSelector } from "react-redux";

const useLoadProfile = () => {
  const dispatch = useDispatch();
  const { updated } = useSelector(state => state.person.update);
  const { retrieved, loading, error, eventSource } = useSelector(
    state => state.person.list,
    shallowEqual
  );

  useEffect(() => {
    if (updated) {
      // setIsLoading(true);
      dispatch(personUpdateReset(eventSource));
      dispatch(personList('people?user=' + localStorage.getItem('id')));
    }
  }, [updated]);


  useEffect(() => {
    if (!loading && !error && !retrieved) {
      // setIsLoading(true);
      dispatch(personList('people?user=' + localStorage.getItem('id')));
    }
  }, [retrieved, loading, error]);

};

export default useLoadProfile;
