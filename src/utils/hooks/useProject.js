import {useEffect, useState} from 'react';
import {HYDRAMEMBER} from '../constants/constants';
import {shallowEqual, useSelector} from 'react-redux';

export default function useProject() {
  const [projectError, setProjectError] = useState(null);

  const {retrieved, loading, error, eventSource, deletedItem} = useSelector(
    state => state.project.list,
    shallowEqual
  );

  const [project, setProject] = useState(
    retrieved && retrieved[HYDRAMEMBER] && retrieved[HYDRAMEMBER][0]
      ? retrieved[HYDRAMEMBER][0]
      : null
  );
  useEffect(() => {
    if (retrieved && retrieved[HYDRAMEMBER] && retrieved[HYDRAMEMBER].length < 1) {
      setProjectError(`You dont have any project, please contact support !`);
    }

    if (retrieved && retrieved[HYDRAMEMBER] && retrieved[HYDRAMEMBER][0]) {
      setProject(retrieved[HYDRAMEMBER][0]);
    }
  }, [retrieved]);

  return {project, retrieved, loading, error, eventSource, deletedItem, projectError};
}
