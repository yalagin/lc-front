import _ from 'lodash';
import {
  designs, translations
} from "./constants/constants";
import { capitalizeFirstLetter } from "./utilsFunctions";


export const designsNames = [
  "spreadshirtCom",
  "spreadshirtEu",
  "spreadshirt",
  "redBubble",
  "society",
  "teepublic",
  "shirtee",
  "zazzle",
  "printful",
  "mba",
  "teespring",
  "displate",
];
export function getListOfSelectedDesigns(values) {
  const arrayOfTransferDesigns = [];
  if (_.isObject(values['Edit'])) {
    for (const key in values['Edit']) {
      if (values['Edit'].hasOwnProperty(key) && values['Edit'][key]) {
        arrayOfTransferDesigns.push(key);
      }
    }
  } else {
    designs.forEach(design => arrayOfTransferDesigns.push(design));
  }
  return arrayOfTransferDesigns;
}


export function makeDesignsStringLookPretty(design) {
  if (design.indexOf('design') > -1) {
    design = design.substr(6);
    if (design === "RedBubble") return design
    if (design.indexOf('Com') > -1||design.indexOf('Eu') > -1) {
      const array = _.startCase(design).split(' ');
      array[1] = array[1].toUpperCase();
      return array.join(' ');
    }
    return _.startCase(design);
  }
  return _.startCase(design);
}

export function makeTitleStringLookPretty(title,replaceNumbers = true) {
  if (/^bulletpoints2/.test(title)) return "Bulletpoint 2";
  if (/^bulletpoints/.test(title)) return "Bulletpoint 1";
  if(replaceNumbers){
    title = title.replace(/[0-9]/g, '')
  }
  designsNames.forEach(design => {
    title = title.replace(design, '');
  });
  title = title.replace("Select", '');
  title = title.replace("ColorPicker", 'BackgroundColor');
  if (/^is/.test(title)) return _.startCase(title.slice(2));
  if (/At$/.test(title)) return _.startCase(title.slice(0,-2));

  return _.startCase(title);
}

export function makeTitleStringLookPrettyForGeneral(title) {
  if (/^is/.test(title)) return _.startCase(title.slice(2));
  if (/At$/.test(title)) return _.startCase(title.slice(0,-2));
  if (/title26/.test(title)) return capitalizeFirstLetter(title.slice(0,-2)+' (Displate)');
  if (/title40/.test(title)) return capitalizeFirstLetter(title.slice(0,-2)+' (Shirtee, Teespring)');
  if (/title50/.test(title)) return capitalizeFirstLetter(title.slice(0,-2)+' (Spreadshirt Com, Spreadshirt Eu, Red Bubble, Teepublic, Zazzle, Printful)');
  if (/title60/.test(title)) return capitalizeFirstLetter(title.slice(0,-2)+' (MBA)');
  if (/title100/.test(title)) return capitalizeFirstLetter(title.slice(0,-3)+' (Society6)');
  if (/description2000/.test(title)) return capitalizeFirstLetter(title.slice(0,-4)+' (Zazzle, MBA)');
  if (/description140/.test(title)) return capitalizeFirstLetter(title.slice(0,-3)+' (Displate)');
  if (/description200/.test(title)) return capitalizeFirstLetter(title.slice(0,-3)+' (Displate, Spreadshirt Com, Spreadshirt Eu, RedBubble, Teepublic, Shirtee, Printful Teespring)');
  if (/description250/.test(title)) return capitalizeFirstLetter(title.slice(0,-3)+' (Society6)');
  return _.startCase(title);
}


export function getTitleForDesign(parent,title) {
  if (_.isArray(parent)) {
    if (_.first(parent) === translations) {
      return `${makeTitleStringLookPretty(title,false)} (General, ${(_.last(parent)).toUpperCase()} )`;
      // return `${title} (${_.last(parent)} )`;
    } else {
      return ` ${makeTitleStringLookPretty(title)} (${makeDesignsStringLookPretty(_.first(parent))}, ${(_.last(parent)).toUpperCase()})`;
    }
  } else {
    return `${makeTitleStringLookPretty(title)} (${makeDesignsStringLookPretty(parent)})`;
  }
}

export function getTitleForUploadDesign(parent,title) {
  if (_.isArray(parent)) {
    if (_.first(parent) === translations) {
      return `${makeTitleStringLookPretty(title)} ( ${(_.last(parent)).toUpperCase()} )`;
    }
  } else {
    return `${makeTitleStringLookPretty(title)}`;
  }
}
