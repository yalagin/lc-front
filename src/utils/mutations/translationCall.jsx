import qs from "qs";
import _ from "lodash";

const translationCall =  async ({selectedLanguageValue,selectedFieldValue,translateFrom,profile}) => {
    let response =  await fetch(
      (profile?.apiKey.slice(-2) === 'fx'
        ? 'https://api-free.deepl.com/v2/translate?'
        : 'https://api.deepl.com/v2/translate?') +
      qs.stringify(
        {
          auth_key: profile?.apiKey,
          target_lang: selectedLanguageValue,
          text: _.isEmpty(selectedFieldValue)?'':selectedFieldValue,
          source_lang: translateFrom,
        },
        { arrayFormat: 'repeat', skipNulls: true }
      ), {cache: "force-cache"}
    );

    response = await response.json();

    if (_.isArray(selectedFieldValue)) {
      return response.translations.map((value) => value.text);
    }
    return response.translations[0]["text"];
}

export default translationCall;
