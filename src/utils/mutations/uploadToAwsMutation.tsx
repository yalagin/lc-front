import {getFileExt} from '../utilsFunctions';
import { fetch2 } from "../dataAccess";
import {MEDIA_OBJECTS, MEDIA_OBJECTS_PUT_LINK} from '../constants/url';

export function uploadToAwsMutation(type: string = 'design') {
    return async (file:File) => {
        const configHeader = {
          method: 'PUT',
          headers: {
            'Content-Type': file.type,
          } as {[p: string]: string},
          body: file,
        };
        const fileType = {
          fileExtension: getFileExt(file.name),
        };
        const {url} = await fetch2(MEDIA_OBJECTS_PUT_LINK, {
          method: 'POST',
          body: JSON.stringify(fileType),
        });
        await global.fetch(url, configHeader);
        const urlToBucket = {
          name: file.name,
          url,
          type,
        };
      return await fetch2(MEDIA_OBJECTS, {
          method: 'POST',
          body: JSON.stringify(urlToBucket),
        });
      }
}
