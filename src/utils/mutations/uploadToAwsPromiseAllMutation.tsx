import {UploadFile} from 'antd/lib/upload/interface';
import {getFileExt} from '../utilsFunctions';
import { fetch2 } from "../dataAccess";
import {MEDIA_OBJECTS, MEDIA_OBJECTS_PUT_LINK} from '../constants/url';

export function uploadToAwsPromiseAllMutation(
  project: {'@id': string},
  type: string = 'design',
  addToDesign: boolean = false
) {
  return (uploadList: UploadFile[]) => {
    return Promise.all(
      uploadList.map(async file => {
        const configHeader = {
          method: 'PUT',
          headers: {
            'Content-Type': file.type,
          } as {[p: string]: string},
          body: file.originFileObj,
        };
        const fileType = {
          fileExtension: getFileExt(file.name),
        };
        const {url} = await fetch2(MEDIA_OBJECTS_PUT_LINK, {
          method: 'POST',
          body: JSON.stringify(fileType),
        });
        await global.fetch(url, configHeader);
        const urlToBucket = {
          name: file.name,
          url,
          type,
        };
        const retrieved = await fetch2(MEDIA_OBJECTS, {
          method: 'POST',
          body: JSON.stringify(urlToBucket),
        });
        const val = {
          image: retrieved,
          project: project['@id'],
        };

        if (addToDesign) {
          return await fetch2('/designs', {
            method: 'POST',
            body: JSON.stringify(val),
          });
        } else {
          return retrieved;
        }
      })
    );
  };
}
