import { useState } from "react";
import { useMutation } from "react-query";
import { fetch2 } from "../dataAccess";
import qs from "qs";

export default function useCheckForTrademark() {
  const [queryParams, setQueryParams] = useState<{classes:number[],country:string[]}>({
    classes: [9, 16, 18, 20, 21, 22, 24, 25],
    country: ['ALL'],
  });
  const {isLoading, mutate, mutateAsync} = useMutation((selectedFieldValue: string) => {
    return fetch2(
      '/check-for-trade-mark?' +
      qs.stringify(queryParams, {
        arrayFormat: 'brackets',
        skipNulls: true,
      }),
      {
        method: 'POST',
        body: JSON.stringify({msg: [selectedFieldValue]}),
      }
    );
  });
  return {setQueryParams, isLoading, mutate,queryParams,mutateAsync};
}
