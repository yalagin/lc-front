import { useMutation, useQueryClient } from "react-query";
import { fetch2 } from "../dataAccess";
import { notification } from "antd";

const useTrackTrademarkMutation = ()=> {
  const queryClient = useQueryClient();

  return useMutation(
    async (data: object) =>
      await fetch2('/trademark_tracks', {
        method: 'POST',
        body: JSON.stringify(data),
      }),
    {
      onSuccess: (data, variables, context) => {
        notification.success({message: `Tracking added!`});
        queryClient.invalidateQueries('track_trademarks');
      },
    }
  );
}

export default useTrackTrademarkMutation;
