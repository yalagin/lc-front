import React from 'react';
import {useMutation} from 'react-query';
import {fetch2, MIME_TYPE_PATCH} from '../dataAccess';
import { message } from "antd";

function usePatchMutation() {
  return useMutation(
    (data:any) =>
      fetch2(data['@id'], {
        method: 'PATCH',
        headers: {'Content-Type': MIME_TYPE_PATCH},
        body: JSON.stringify(data),
      }),
    {
      onSuccess: (data, variables, context) => {
        // Boom baby!
        console.log('Boom baby!');
      },
      onError: (data, variables, context) => {
        message.error(`failed =(`);
      },
    }
  );
}

export default usePatchMutation;
