import React from 'react';

const EmailHelp = () => {
  return (
    <svg width="60" height="60" viewBox="0 0 60 60" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path d="M39.9992 20.7705H19.9996V24.6162H39.9992V20.7705Z" fill="white"/>
      <path d="M39.9992 13.0769H19.9996V16.9226H39.9992V13.0769Z" fill="white"/>
      <path d="M51.5375 16.5383V3.46143H8.46128V16.5383H0V48.8449C0 53.0761 3.46138 56.5375 7.69261 56.5375H52.3074C56.5386 56.5375 60 53.0761 60 48.8449V16.5383H51.5375ZM29.9994 40.3645L5.11175 24.3094L7.19762 21.0777L12.3082 24.3749V7.30714H47.6918V24.3725L52.8036 21.0753L54.8894 24.307L29.9994 40.3645Z" fill="white"/>
    </svg>
  );
};

export default EmailHelp;
