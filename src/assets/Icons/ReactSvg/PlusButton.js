import React from "react";

const PlusButton = () => {
  return (
    <svg width="40" height="40" viewBox="0 0 40 40" fill="none" xmlns="http://www.w3.org/2000/svg">
      <rect x="0.5" y="0.5" width="39" height="39" rx="4.5" stroke="#C4C4C4" stroke-dasharray="2 2"/>
      <path d="M26.4531 19.4531H20.5469V13.5469C20.5469 13.2448 20.302 13 20 13C19.698 13 19.4531 13.2448 19.4531 13.5469V19.4531H13.5469C13.2448 19.4531 13 19.698 13 20C13 20.302 13.2448 20.5469 13.5469 20.5469H19.4531V26.4531C19.4531 26.7552 19.698 27 20 27C20.302 27 20.5469 26.7552 20.5469 26.4531V20.5469H26.4531C26.7552 20.5469 27 20.302 27 20C27 19.698 26.7552 19.4531 26.4531 19.4531Z" fill="#748AA1"/>
    </svg>
  );
};

export default PlusButton;
