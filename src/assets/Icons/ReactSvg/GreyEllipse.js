import React from 'react';

const GreyEllipse = () => {
  return (
    <svg width="130" height="130" viewBox="0 0 130 130" fill="none" xmlns="http://www.w3.org/2000/svg">
      <circle cx="65" cy="65" r="65" fill="#777777"/>
    </svg>
  );
};

export default GreyEllipse;
