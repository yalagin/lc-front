import React from "react";

const TM = () => {
  return (
    <svg width="28" height="15" viewBox="0 0 28 15" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path d="M11.4746 2.32422H6.9043V15H5.03906V2.32422H0.478516V0.78125H11.4746V2.32422ZM16.0059 0.78125L20.6543 12.3828L25.3027 0.78125H27.7344V15H25.8594V9.46289L26.0352 3.48633L21.3672 15H19.9316L15.2734 3.51562L15.459 9.46289V15H13.584V0.78125H16.0059Z" fill="white"/>
    </svg>
  );
};

export default TM;
