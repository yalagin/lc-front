import RefreshSvg from './Header/Refresh.svg';
import Icon1Svg from './Header/Icon1.svg';
import Icon2Svg from './Header/Icon2.svg';
import Icon3Svg from './Header/Icon3.svg';
import Icon1Png from './Header/Icon1.png';
import Icon2Png from './Header/Icon2.png';
import Icon3Png from './Header/Icon3.png';
import MenuIcon from './Sidebar/Menu.png';
import DownIcon from './Common/chevron-down.png';
import SettingIcon from './Common/setting.png';
import UploadIcon from './Common/file-upload.png';
import UploadIcon2 from './Common/file-upload@2x.png';
import UploadAction1Icon from './Common/upload-action1.png';
import UploadAction2Icon from './Common/upload-action2.png';
import UploadAction3Icon from './Common/upload-action3.png';
import UploadAction4Icon from './Common/upload-action4.png';
import UploadAction0Icon from './Common/upload-action0.png';
import ChevronUploadDown from './Common/chevron-upload-down.png';
import ComHeaderTrans from './Common/com-header-trans.png';
import EnterIcon from './Common/enter.png';
import UploadLabelIcon from './Common/label-icon.png';
import SliderLeft from './Common/slider-arrow-left.png';
import SliderRight from './Common/slider-arrow-right.png';
import DeleteIcon from './Common/deletebg.png';
import AddFriendIcon from './Common/add-friend.png';
import AddFriendIcon2 from './Common/add-friend@2x.png';

import { ReactComponent as StatsSvg } from './Sidebar/Stats.svg';
import { ReactComponent as ShoppingSvg } from './Sidebar/Shopping.svg';
import { ReactComponent as ExitSvg } from './Sidebar/Exit.svg';
import { ReactComponent as MapsSvg } from './Sidebar/Maps.svg';
import { ReactComponent as MessagesSvg } from './Sidebar/Messages.svg';
import { ReactComponent as SettingsSvg } from './Sidebar/Settings.svg';
import DashboardPng from './Sidebar/Dashboard.png';
import TMPng from './Sidebar/TM.png';
import { ReactComponent as UploadSvg } from './Sidebar/Upload.svg';

import EmailPng from './HelpCenter/email.png';
import FacebookPng from './HelpCenter/facebook.png';
import HeadphonePng from './HelpCenter/headphones.png';
import TwitterPng from './HelpCenter/twitter.png';
import YoutubePng from './HelpCenter/youtube.png';
import DefaultProfile from './Settings/defaultProfile.png';

import DefaultUploadPng from './Profile/defaultUpload.png';
import DefaultProfilePng from './Profile/defaultProfile.png';

export default {
  RefreshSvg,
  MenuIcon,
  DownIcon,
  SettingIcon,
  UploadIcon,
  UploadIcon2,
  UploadAction1Icon,
  UploadAction2Icon,
  UploadAction3Icon,
  UploadAction4Icon,
  UploadAction0Icon,
  ChevronUploadDown,
  ComHeaderTrans,
  EnterIcon,
  Icon1Svg,
  Icon2Svg,
  Icon3Svg,
  Icon1Png,
  Icon2Png,
  Icon3Png,
  StatsSvg,
  ShoppingSvg,
  ExitSvg,
  MapsSvg,
  MessagesSvg,
  SettingsSvg,
  DashboardPng,
  TMPng,
  UploadSvg,
  UploadLabelIcon,
  SliderLeft,
  SliderRight,
  DeleteIcon,
  AddFriendIcon,
  AddFriendIcon2,
  EmailPng,
  FacebookPng,
  TwitterPng,
  YoutubePng,
  HeadphonePng,
  DefaultProfile,
  DefaultUploadPng,
  DefaultProfilePng,
};
