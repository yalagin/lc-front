import defaultAvatar from './avatar.png';
import avatar1 from './1.jpeg';
import avatar2 from './2.jpeg';
import avatar3 from './3.jpeg';
import avatar4 from './4.png';
import avatar5 from './5.jpeg';
import avatar6 from './6.png';

export default {
  defaultAvatar,
  avatar1,
  avatar2,
  avatar3,
  avatar4,
  avatar5,
  avatar6,
};
