const puppeteer = require('puppeteer')
let browser
let page

beforeAll(async () => {
  browser = await puppeteer.launch()
  // const browser = await puppeteer.launch({
  //   headless: false,
  //   slowMo: 250, // slow down by 250ms
  // });

  page = await browser.newPage();
})

afterAll(async () => {
  await browser.close()
})
jest.setTimeout(60000);
describe('Alibaba Search', () => {
  test('has search input', async () => {
    await page.setViewport({ width: 1280, height: 800 })
    await page.goto('https://www.alibaba.com', { waitUntil: 'networkidle0' })
    const searchInput = await page.$('input.ui-searchbar-keyword')
    expect(searchInput).toBeTruthy()
  }, 10000)

  test('shows search results after search input', async () => {
    await page.type('input.ui-searchbar-keyword', 'lucky cat')
    await page.click('input.ui-searchbar-submit')
    await page.waitForSelector('[data-content="abox-ProductNormalList"]')
    const firstProduct = await page.$('.list-no-v2-outter')
    await page.screenshot({ path: "screen.png" })
    expect(firstProduct).toBeTruthy()
  })
})
1010111
