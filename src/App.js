import React from 'react';
import store from './store';
import { QueryClient, QueryClientProvider } from 'react-query';
import { BrowserRouter, Route, Routes } from "react-router-dom";
import userRoutes from './routes/user';
import authRoutes from './routes/auth';
import PrivateRouteOld from './routes/PrivateRouteOld';
import MySider from './pages/WorkingPanel/MySider';
import Landing from './pages/Landing/Landing';
import { Provider } from 'react-redux';
import './App.less';
import PrivateRoute from "./routes/PrivateRoute";
import { notification } from "antd";

const App = () => {
  const queryClient = new QueryClient({
    defaultOptions: {
      queries: {
        refetchOnWindowFocus: false,
      },
    },
  });
  notification.config({
    maxCount: 1,
  });

  return (
    <Provider store={store()}>
      <QueryClientProvider client={queryClient}>
        <BrowserRouter>
          <Routes>
            {userRoutes}
            {authRoutes}
            <Route path="/dashboard/*" element={ <PrivateRoute><MySider/></PrivateRoute>} />
            <Route path={'/'} element={<Landing/>} />
          </Routes>
        </BrowserRouter>
      </QueryClientProvider>
    </Provider>
  );
};

export default App;
