export interface Person {
  '@id'?: string;
  familyName: string;
  givenName: string;
  user?: string;
  country: string;
  city: string;
  apiKey?: string;
  region?: string;
  zip?: string;
  street?: string;
  myColor?: string;
  isSideBarToggled?: boolean;
  company?: string;
  telephone?: string;
  image?: any;
  editableDesignsForWorkspace?: any;
  colSettings?: any;
  uploadSettings?: any;
}
