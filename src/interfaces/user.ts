export interface User {
  '@id'?: string;
  email?: string;
  password?: string;
  first_name?: string;
  last_name?: string;
  street?: string;
  city?: string;
  zip?: string;
  country?: string;
}
