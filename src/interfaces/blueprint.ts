export interface Blueprint {
  '@id'?: string;
  name?: string;
  owner?: string;
  translations?: any;
  readonly createdAt?: Date;
  readonly updatedAt?: Date;
}
