export interface DesignTeepublic {
  '@id'?: string;
  title50?: string;
  description200?: string;
  tags?: any;
  design?: any;
  teepublicColors?: any;
  isTeepublicProductEnabled?: any;
  teepublicProduct?: any;
  teepublicShirtPrintingIsEnabled?: any;
  teepublicShirtPrintingSelection?: "front"|"back"|"both";
  teepublicId?: string;
  isUpload?: boolean;
  isUploaded?: boolean;
  readonly createdAt?: Date;
  readonly updatedAt?: Date;
}
