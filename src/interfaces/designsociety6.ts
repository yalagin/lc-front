export interface DesignSociety6 {
  '@id'?: string;
  title100?: string;
  description250?: string;
  tags?: any;
  design?: any;
  society6Colors?: string;
  // society6StoreName?: string;
  // society6ProductName?: string;
  society6Id?: string;
  isUpload?: boolean;
  isUploaded?: boolean;
  readonly createdAt?: Date;
  readonly updatedAt?: Date;
}
