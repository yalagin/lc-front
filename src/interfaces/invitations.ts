export interface Invitations {
  '@id'?: string;
  id?: string;
  users?: string[];
  project?: string;
  status?: string;
  readonly createdAt?: Date;
  readonly updatedAt?: Date;
}
