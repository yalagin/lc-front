export interface DesignSpreadshirtCom {
  '@id'?: string;
  title50?: string;
  description200?: string;
  tags?: any;
  design?: any;
  spreadshirtComSelectDesignTemplate?: any;
  spreadshirtId?: string;
  isUpload?: boolean;
  isUploaded?: boolean;
  readonly createdAt?: Date;
  readonly updatedAt?: Date;
}
