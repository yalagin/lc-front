export interface Changelog {
  '@id'?: string;
  name?: string;
  text?: string;
}
