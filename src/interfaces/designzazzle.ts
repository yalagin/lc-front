export interface DesignZazzle {
  '@id'?: string;
  title50?: string;
  description2000?: string;
  tags?: any;
  occasion?: string;
  audience?: string;
  category?: string;
  event?: string;
  recipient?: string;
  store?: string;
  design?: any;
  zazzleSelectDesignTemplate?: any;
  zazzleStore?: string;
  isUpload?: boolean;
  isUploaded?: boolean;
  readonly createdAt?: Date;
  readonly updatedAt?: Date;
}
