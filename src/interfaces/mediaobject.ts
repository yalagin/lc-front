export interface MediaObject {
  '@id'?: string;
  contentUrl: string;
  originalName?: string;
  owner?: string;
  type?: string;
}
