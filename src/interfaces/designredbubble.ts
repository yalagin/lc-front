export interface DesignRedBubble {
  '@id'?: string;
  translations?: any;
  design?: any;
  createdAt?: Date;
  updatedAt?: Date;
  redBubbleDuplicationURL?: string;
  selectedLanguages?: any;
  redBubbleId?: string;
  isUpload?: boolean;
  isUploaded?: boolean;
}
