import { DesignMba } from "./designmba";

export interface Design {
  '@id'?: string;
  image?: any;
  project?: string;
  fitMan?: boolean;
  fitWoman?: boolean;
  fitYouth?: boolean;
  isEdit?: boolean;
  isAdult?: boolean;
  designDisplate?: any;
  designMBA?: DesignMba;
  designPrintful?: any;
  designRedBubble?: any;
  designShirtee?: any;
  designSociety6?: any;
  designSpreadshirtCom?: any;
  designSpreadshirtEu?: any;
  designTeepublic?: any;
  designTeespring?: any;
  designZazzle?: any;
  owner?: string;
  translations?: any;
  inspirationLinks?: any;
  text?: string;
  description?: string;
  createdAt?: Date;
  updatedAt?: Date;
  workingFiles?: any;
  templateName?: string;
  template?: string;
  isUpload?: boolean;
  isUploaded?: boolean;
  ids: any;
  readonly copyFromMaster?: boolean;
}
