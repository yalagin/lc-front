export interface DesignShirtee {
  '@id'?: string;
  translations?: any;
  design?: any;
  createdAt?: Date;
  updatedAt?: Date;
  shirteeSelectDesignTemplate?: any;
  shirteeShop?: any;
  category?: any;
  shirteeMarketplace?: any;
  shirteeDescriptionTemplate?: any;
  selectedLanguages?: any;
  isUpload?: boolean;
  isUploaded?: boolean;
}
