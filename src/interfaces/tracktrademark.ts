export interface TrackTrademark {
  '@id'?: string;
  keyword?: {data: any[],keyword:string};
  territory?: any;
  data?: any[];
  newEntries?: number;
  entries?: number;
  owner?: string;
}
