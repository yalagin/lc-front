import { TemplateSettingsMbaProductColor } from "./templatesettingsmbaproductcolor";
import { TemplateSettingsMbaProductPrice } from "./templatesettingsmbaproductprice";

export interface TemplateSettingsMbaMarketPlace {
  '@id': string;
  name: string;
  currency: string;
  currencySymbol: string;
  country: string;
  // products: string[];
  pricesForProducts: TemplateSettingsMbaProductPrice[];
  ordering: number;
  readonly createdAt?: Date;
  readonly updatedAt?: Date;
}
