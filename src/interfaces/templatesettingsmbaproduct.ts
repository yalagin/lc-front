import { TemplateSettingsMbaProductColor } from "./templatesettingsmbaproductcolor";
import { TemplateSettingsMbaMarketPlace } from "./templatesettingsmbamarketplace";
import { TemplateSettingsMbaProductPrice } from "./templatesettingsmbaproductprice";

export interface TemplateSettingsMbaProduct {
  '@id': string;
  name: string;
  productColors:TemplateSettingsMbaProductColor[];
  // marketPlaces: TemplateSettingsMbaMarketPlace[];
  priceForMarketplace: TemplateSettingsMbaProductPrice[];
  ordering?: number;
  readonly createdAt?: Date;
  readonly updatedAt?: Date;
}
