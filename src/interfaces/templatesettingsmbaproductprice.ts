import { TemplateSettingsMbaMarketPlace } from "./templatesettingsmbamarketplace";
import { TemplateSettingsMbaProduct } from "./templatesettingsmbaproduct";

export interface TemplateSettingsMbaProductPrice {
  '@id'?: string;
  product?: TemplateSettingsMbaProduct;
  marketPlace: TemplateSettingsMbaMarketPlace;
  lowerBoundary?: number;
  upperBoundary?: number;
  readonly createdAt?: Date;
  readonly updatedAt?: Date;
}
