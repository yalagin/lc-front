export interface TemplateSettingsMbaProductColor {
  '@id'?: string;
  color?: string;
  product?: string;
  image?: string;
  isNotAvilableInJapan?: boolean;
  readonly createdAt?: Date;
  readonly updatedAt?: Date;
}
