export interface DesignTeespring {
  '@id'?: string;
  title40?: string;
  description200?: string;
  design?: any;
  teespringDuplicationURL?: string;
  isUpload?: boolean;
  isUploaded?: boolean;
  readonly createdAt?: Date;
  readonly updatedAt?: Date;
}
