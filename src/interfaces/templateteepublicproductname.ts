import { TemplateSettingsColor } from "./templatesettingscolor";

export interface TemplateTeepublicProductName {
  '@id': string;
  id?: string;
  name?: string;
  products?: [];
}
