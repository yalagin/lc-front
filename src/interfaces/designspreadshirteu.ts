export interface DesignSpreadshirtEu {
  '@id'?: string;
  translations?: any;
  design?: any;
  createdAt?: Date;
  updatedAt?: Date;
  spreadshirtEuSelectDesignTemplate?: any;
  selectedLanguages?: any;
  spreadshirtId?: string;
  isUpload?: boolean;
  isUploaded?: boolean;
}
