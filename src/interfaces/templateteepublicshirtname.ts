import { TemplateSettingsColor } from "./templatesettingscolor";

export interface TemplateTeepublicShirtName {
  '@id'?: string;
  id?: string;
  name?: string;
  colors?: TemplateSettingsColor[]
}
