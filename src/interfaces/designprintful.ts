export interface DesignPrintful {
  '@id'?: string;
  title50?: string;
  description200?: string;
  tags?: any;
  shopName?: string;
  design?: any;
  printfulColors?: string;
  isUpload?: boolean;
  isUploaded?: boolean;
  readonly createdAt?: Date;
  readonly updatedAt?: Date;
}
