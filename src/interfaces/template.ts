import { mbaColors, mbaScale } from "./designmba";

export interface Template {
  '@id'?: string;
  name: string;
  owner?: string;
  spreadshirtEuSelectDesignTemplate?: any;
  spreadshirtComSelectDesignTemplate?: any;
  shirteeSelectDesignTemplate?: any;
  zazzleSelectDesignTemplate?: any;
  redBubbleDuplicationURL?: string;
  displateColorPicker?: string;
  zazzleStore?: string;
  society6Colors?: string;
  printfulStoreName?: string;
  printfulProductName?: string;
  printfulColors?: string;
  teespringDuplicationURL?: string;
  teepublicColors?: any;
  isTeepublicProductEnabled?: any;
  teepublicProduct?: any;
  teepublicShirtPrintingIsEnabled?: any;
  teepublicShirtSelection?: any;
  teepublicShirtPrintingSelection?: "front"|"back"|"both";
  mbaProductPrice?: any;
  mbaColors?: mbaColors;
  mbaScale?: mbaScale;
  mbaProductCheckbox?: any;
}
