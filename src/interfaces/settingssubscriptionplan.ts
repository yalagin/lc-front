export interface SettingsSubscriptionPlan {
  '@id'?: string;
  name?: string;
  storageLimitGb?: number;
}
