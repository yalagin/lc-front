import { MediaObject } from "./mediaobject";

export interface TemplateSettingsColor {
  '@id': string;
  name?: string;
  color?: string;
  colorUrl?: string;
  isTeepublic?: boolean;
  image?: MediaObject;
  isDark?: boolean;
  isLight?: boolean;
  ordering?: number;
  templateTeepublicShirtName?: string;
  readonly createdAt?: Date;
  readonly updatedAt?: Date;
}
