export interface DesignDisplate {
  '@id'?: string;
  title26?: string;
  description140?: string;
  tags?: any;
  collection?: string;
  category?: string;
  design?: any;
  displateColorPicker?: string;
  isUpload?: boolean;
  isUploaded?: boolean;
  readonly createdAt?: Date;
  readonly updatedAt?: Date;
}
