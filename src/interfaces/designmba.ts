export interface DesignMba {
  '@id'?: string;
  id: string;
  translations?: any;
  fitMan?: boolean;
  fitWoman?: boolean;
  fitYouth?: boolean;
  design?: any;
  createdAt?: Date;
  updatedAt?: Date;
  mbaColors?: mbaColors;
  mbaScale?: mbaScale;
  mbaProductPrice: mbaProductPrice;
  mbaProductCheckbox: mbaProductCheckbox;
  mbaProductPublished: mbaProductCheckbox;
  selectedLanguages: any;
  mbaId?: string;
  isUpload?: boolean;
  isUploaded?: boolean;
}

export interface mbaProductCheckbox {
  [index: string]: {
    [index: string]: boolean
  },
  "/template_settings_mba_products/1ec8f305-f282-6fd2-9d6e-95d56505a8da": {
    "/template_settings_mba_market_places/1ec8f305-f28a-6a34-b08a-95d56505a8da": true
  }
}
export interface mbaProductPrice {
  [index: string]: {
    [index: string]: number
  },
  "/template_settings_mba_products/1ec8f305-f282-6fd2-9d6e-95d56505a8da": {
    "/template_settings_mba_market_places/1ec8f305-f28a-6a34-b08a-95d56505a8da": 222
  }
}
export interface mbaColors {
  [index: string]: string[],
  "/template_settings_mba_products/1ec8f305-f282-6fd2-9d6e-95d56505a8da": [
    "/template_settings_mba_market_places/1ec8f305-f28a-6a34-b08a-95d56505a8da",
    "/template_settings_mba_market_places/1ec8f305-f28a-6a34-b08a-95d56505a8da",
  ]
}
export interface mbaScale {
  [index: string]: number,
  "/template_settings_mba_products/1ec8f305-f282-6fd2-9d6e-95d56505a8da": 1
}
