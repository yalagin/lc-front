import {saveAppToken} from "../user/login";
import {ENTRYPOINT} from "../../config/entrypoint";

export function error(err) {
    return { type: 'AUTH_REFRESH_ERROR', err };
}

export function loading(load) {
    return { type: 'AUTH_REFRESH_LOADING', load };
}

export function success(created) {
    return { type: 'AUTH_REFRESH_SUCCESS', created };
}

export const signOut = () => {
    return () => {
        localStorage.clear();
        window.location.reload();
    };
};

export function refreshToken(dispatch) {
    dispatch(loading(true));
    const formData = new FormData();
    formData.append('refresh_token', localStorage.getItem('refresh_token'));
    const freshTokenPromise = fetch(new URL('/token/refresh', ENTRYPOINT),{ method: 'POST', body: formData })
        .then((response) => {
            dispatch(loading(false));

            return response.json();
        })
        .then((t) => {
            dispatch(success(t));
            saveAppToken(t);
            return t.token ? Promise.resolve(t.token) : Promise.reject(new Error({
                message: 'could not refresh token'
            }));
        })
        .catch((e) => {
            dispatch(loading(false));
            dispatch(error(e.message));
            dispatch(signOut());
            return Promise.resolve();
        });

    dispatch({
        type: 'REFRESHING_TOKEN',
        // we want to keep track of token promise in the state so that we don't try to refresh
        // the token again while refreshing is in process
        freshTokenPromise
    });

    return freshTokenPromise;
}

export function Jwt({ dispatch, getState }) {
    return (next) => (action) => {
        // only worry about expiring token for async actions
        if (typeof action === 'function') {
            if (localStorage.getItem('user')) {
                // decode jwt so that we know if and when it expires
                const tokenExpiration = localStorage.getItem('user') && JSON.parse(localStorage.getItem('user')).exp;
                if (tokenExpiration && Date.now() >= tokenExpiration * 1000) {
                    // make sure we are not already refreshing the token
                    if (!getState().auth.freshTokenPromise) {
                        return refreshToken(dispatch).then(() => next(action));
                    }
                    return getState().auth.freshTokenPromise.then(() => next(action));
                }
            }
        }
        return next(action);
    };
}
