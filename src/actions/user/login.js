import { SubmissionError } from 'redux-form';
import { fetch } from '../../utils/dataAccess';
import jwt_decode from "jwt-decode";
import { list as personList } from '../person/list';
import { list as projectList } from '../project/list';

export function error(error) {
  return { type: 'USER_LOGIN_ERROR', error };
}

export function loading(loading) {
  return { type: 'USER_LOGIN_LOADING', loading };
}

export function success(loggedIn) {
  return { type: 'USER_LOGIN_SUCCESS', loggedIn };
}

// todo change to more secure way of storing data
export function saveAppToken(retrieved) {
  localStorage.setItem('token',retrieved.token);
  localStorage.setItem('user',JSON.stringify(jwt_decode(retrieved.token)));
  localStorage.setItem('id',jwt_decode(retrieved.token)['@id']);
  localStorage.setItem('refresh_token', retrieved.refresh_token);
}


export function login(values) {
  return dispatch => {
    dispatch(loading(true));
    dispatch(error(null));

    return fetch('authentication_token', { method: 'POST', body: JSON.stringify(values) })
      .then(response => {
        dispatch(loading(false));

        return response.json();
      })
      .then(retrieved => {
        saveAppToken(retrieved);
        dispatch(personList('people?user='+jwt_decode(retrieved.token)['@id']));
        dispatch(projectList('projects?founder='+jwt_decode(retrieved.token)['@id']));
        // return dispatch(reset());
        return dispatch(success(retrieved));
      })
      .catch(e => {
        dispatch(loading(false));

        if (e instanceof SubmissionError) {
          dispatch(error(e.errors._error));
          throw e;
        }

        dispatch(error(e.message));
      });
  };
}

export function reset() {
  return dispatch => {
    dispatch(loading(false));
    dispatch(error(null));
  };
}
