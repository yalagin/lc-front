import { SubmissionError } from 'redux-form';
import { fetch } from '../../utils/dataAccess';
import { create as createDesign } from '../design/create';
import { getFileExt } from "../../utils/utilsFunctions";
import { MEDIA_OBJECTS, MEDIA_OBJECTS_PUT_LINK } from "../../utils/constants/url";

export function error(error) {
  return { type: 'MEDIAOBJECT_CREATE_ERROR', error };
}

export function loading(loading) {
  return { type: 'MEDIAOBJECT_CREATE_LOADING', loading };
}

export function success(created) {
  return { type: 'MEDIAOBJECT_CREATE_SUCCESS', created };
}

export function createMediaObjectAndDesign(values, project) {
  return dispatch => {
    dispatch(loading(true));

    return fetch(MEDIA_OBJECTS, { method: 'POST', body: values })
      .then(response => {
        dispatch(loading(false));

        return response.json();
      })
      .then(retrieved => {
          dispatch(success(retrieved))
            const val = {
              "image": retrieved,
              "project": project["@id"],
            };
          return dispatch(createDesign(val));
        }
      )
      .catch(e => {
        dispatch(loading(false));

        if (e instanceof SubmissionError) {
          dispatch(error(e.errors._error));
          throw e;
        }

        dispatch(error(e.message));
      });
  };
}

export const uploadImageToS3AndCreateMediaObjectAndDesign = (file, project) => {
  return async (dispatch) => {
    dispatch(loading(true));
    try {
    const configHeader = {
      method: 'PUT',
      headers: {
        'Content-Type': file.type,
      },
      body: file
    };
    const fileType = {
      fileExtension: getFileExt(file.name)
    };
    const {url} = await fetch(MEDIA_OBJECTS_PUT_LINK, {
      method: 'POST',
      body: JSON.stringify(fileType),
    }).then(response => response.json());
    await global.fetch(url, configHeader)/*.catch((err) => { console.error(err); });*/ ;
    const urlToBucket = {
      name: file.name,
      url,
    };
    const retrieved = await fetch(MEDIA_OBJECTS, { method: 'POST', body:  JSON.stringify(urlToBucket) }).then(response => response.json());
      dispatch(loading(false));
      dispatch(success(retrieved));
      const val = {
        "image": retrieved,
        "project": project["@id"],
      };
      return dispatch(createDesign(val));
    } catch(e) {
      dispatch(loading(false));

      if (e instanceof SubmissionError) {
        dispatch(error(e.errors._error));
        throw e;
      }

      dispatch(error(e.message));
    }
  };
};

export const uploadTestImageToS3AndCreateMediaObjectAndDesign = (imageUrl, project) => {
  return async (dispatch) => {
    dispatch(loading(true));
    try {
      const urlToBucket = {
        name: imageUrl,
        url: imageUrl,
      };
      const retrieved = await fetch(MEDIA_OBJECTS, { method: 'POST', body:  JSON.stringify(urlToBucket) }).then(response => response.json());
      dispatch(loading(false));
      dispatch(success(retrieved));
      const val = {
        "image": retrieved,
        "project": project["@id"],
      };
      return dispatch(createDesign(val));
    } catch(e) {
      dispatch(loading(false));

      if (e instanceof SubmissionError) {
        dispatch(error(e.errors._error));
        throw e;
      }

      dispatch(error(e.message));
    }
  };
};

export function reset() {
  return dispatch => {
    dispatch(loading(false));
    dispatch(error(null));
  };
}
