import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Form } from 'react-final-form';
import arrayMutators from 'final-form-arrays';
import { listDesignEditWithProject, reset } from '../../actions/design/list';
import _ from 'lodash';
import {
  arrayOfCopiedFields,
  arrayOfDescriptions,
  arrayOfMBAFields,
  arrayOfTitles,
  creationGeneralFields,
  designMBA,
  designs,
  DISABLED,
  fitYouth,
  general,
  generalFields,
  HYDRAMEMBER,
  MBA, notTranslationFields,
  objectOfDesignFieldsForProcessingSubDesigns,
  objectOfDesignFieldsForTransferring,
  objectOfFakeData,
  templateFields,
  translations,
  uploadStatus
} from "../../utils/constants/constants";
import { useMutation, useQueryClient } from 'react-query';
import { fetch2 } from '../../utils/dataAccess';
import { Alert, notification, Spin } from 'antd';
import {
   removeDisabledAndEmptyFields
} from "../../utils/utilsFunctions";
import { FORM_ERROR } from 'final-form';
import useProject from '../../utils/hooks/useProject';
import { getListOfSelectedDesigns } from '../../utils/designHelperFunctions';
import { useLocation } from 'react-router-dom';
import qs from 'qs';
import useProfile from '../../utils/hooks/useProfile';
import DesignForm from './DesignForm';
import { CheckCircleOutlined } from '@ant-design/icons';
import { changed, updated, uploaded, UploadStatusesEnum } from "../../utils/constants/upload";
import {
  designAndTheirLanguages,
  objectOfDesignsWithTranslatedFields,
} from '../../utils/constants/translation';
import populateMainFieldsFromHydramemberGetFieldsFromArrayOfFieldsAndCopyToUserEditableFields
  , { isEmpty } from "./designMutationHelpers";
import { objectOfFakeDataHuge } from "../../utils/constants/fakeData";

const DesignFormMutators = () => {
  // Get QueryClient from the context
  const queryClient = useQueryClient();

  const { eventSource, loading: loadingDesignList } = useSelector(state => state.design.list);
  const { loading: loadingDesignCreate } = useSelector(state => state.design.create);
  const { updateLoading: loadingDesignCreateEdit } = useSelector(state => state.design.update);
  const { loading: loadingMedia } = useSelector(state => state.mediaobject.create);
  const { updateLoading } = useSelector(state => state.design.update);
  const { loading: loadingDeleted, error: errorDeleted } = useSelector(state => state.design.del);
  const dispatch = useDispatch();
  const { project } = useProject();
  const { profile } = useProfile();
  const { state } = useLocation();

  const mutationSave = useMutation(
    designs => {
      return Promise.all(
        designs[HYDRAMEMBER].map(async design => {
          await fetch2(design['@id'], {
            method: 'PATCH',
            body: JSON.stringify(design),
            headers: { 'Content-Type': 'application/merge-patch+json' },
          });
        })
      );
    },
    {
      onError: error => {
        notification.warning({ message: `Something wrong`, description: error.message });
      },
    }
  );

  useEffect(() => {
    if (!updateLoading && !!project && state?.ids) {
      dispatch(
        listDesignEditWithProject(
          `designs?project=${project['@id']}&${qs.stringify({ id: state?.ids }, { skipNulls: false })}`
        )
      );
    }
    return () => {
      dispatch(reset(eventSource));
    };
  }, [dispatch, updateLoading, project, state]);


  const onSubmit = (values, form) => {
    console.log(values);
    if (Array.isArray(values[HYDRAMEMBER])) {
      values[HYDRAMEMBER].forEach(value => {
        value.isEdit = false;
        if (!isEmpty(value.workingFiles)) {
          value.workingFiles = value.workingFiles.map(value => value['@id']);
        }
      });
      values[HYDRAMEMBER].forEach(function(value) {
        removeDisabledAndEmptyFields(value);
        designs.forEach(function(design) {
          if (value[design]?.uploadStatus === UploadStatusesEnum[uploaded]) {
            _.set(value, [design, uploadStatus], UploadStatusesEnum[changed]);
            if(design===designMBA){
              _.set(value, [design, uploadStatus], UploadStatusesEnum[updated]);
            }
          }
        });
      });

      const onSuccess = () => {
        // form.change(HYDRAMEMBER, []);
        // form.restart();
        queryClient.refetchQueries(['design']);
        notification.success({
          message: 'Saved',
          // description: 'This is the content of the notification. This is the content of the notification. This is the content of the notification.',
          icon: <CheckCircleOutlined style={{ color: '#7f67ff' }} />,
        });
      };
      try {
        return mutationSave.mutate(values, {
          onSuccess,
          onError: data => {
            notification.error({
              message: 'Something wrong!',
            });
          },
        });
      } catch (e) {
        return { [FORM_ERROR]: e.message };
      }
    }
  };

  if (!profile) return <Spin />;

  return (
    <>
      <Spin
        tip="Loading..."
        size="large"
        spinning={
          mutationSave.isLoading ||
          loadingDeleted ||
          loadingDesignCreate ||
          loadingDesignCreateEdit ||
          loadingMedia ||
          loadingDesignList
          //|| profile.loading
        }
      >
        {mutationSave.error && (
          <Alert message={mutationSave.error.message} type="error" showIcon closable />
        )}
        {/*{error && <Alert message={error} type="error" showIcon closable />}*/}
        {errorDeleted && <Alert message={errorDeleted} type="error" showIcon closable />}
        <Form
          subscription={{ submitting: true, pristine: true  }}
          onSubmit={onSubmit}
          initialValues={{
            Edit: profile?.editableDesignsForWorkspace,
            setSelectedTitle: 'title26',
            selectedDescription: 'description140',
            tags: [],
            currentTab: general,
            translateFrom: 'en',
            disabledAll: false,
          }}
          initialValuesEqual={() => true}
          debug={process.env.REACT_APP_SPY && console.log}
          mutators={{
            // potentially other mutators could be merged here
            ...arrayMutators,
            //this is for setting the same hydra:member fields onChange general
            // or subdesigns field
            onChangeMainField: (
              [field, value, ignoreCopyFromMaster = false],
              state,
              { changeValue }
            ) => {
              changeValue(state, HYDRAMEMBER, oldValue => {
                if (_.isArray(oldValue)) {
                  const newValue = JSON.parse(JSON.stringify(oldValue));
                  for (const member of newValue) {
                    if (member.copyFromMaster || ignoreCopyFromMaster) {
                      _.set(member, field, value);
                    }
                  }
                  return newValue;
                } else {
                  return oldValue;
                }
              });
            },
            onChangeWorkingFiles: ([value, ignoreCopyFromMaster = false], state, { changeValue }) => {
              changeValue(state, HYDRAMEMBER, oldValue => {
                if (_.isArray(oldValue)) {
                  for (const member of oldValue) {
                    if (member.copyFromMaster || ignoreCopyFromMaster) {
                      const workingFiles = _.get(member, 'workingFiles');
                      if (workingFiles && _.isArray(workingFiles)) {
                        _.set(member, 'workingFiles', [...workingFiles, ...value]);
                      } else {
                        _.set(member, 'workingFiles', value);
                      }
                    }
                  }
                }
                return oldValue;
              });
              const {
                formState: { values },
              } = state;
              const workingFiles = _.get(values, 'workingFiles');
              if (workingFiles && _.isArray(workingFiles)) {
                _.set(values, 'workingFiles', [...workingFiles, ...value]);
              } else {
                _.set(values, 'workingFiles', [value]);
              }
            },
            //this is for setting the same hydra:member but with translations
            // fields onChange general or sub designs field
            onChangeTranslatedField: (
              [field, value, ignoreCopyFromMaster = false],
              state,
              { changeValue }
            ) => {
              const {
                formState: { values },
              } = state;

              const selectedLanguage = _.get(values, 'selectedLanguage');

              changeValue(state, HYDRAMEMBER, oldValue => {
                if (_.isArray(oldValue)) {
                  const hydraMember = values[HYDRAMEMBER];
                  for (const member of hydraMember) {
                    if (isEmpty(member.translations)) {
                      member.translations = {};
                    }
                    if (member.copyFromMaster || ignoreCopyFromMaster) {
                      const splitValue = _.toPath(field);
                      const filed = splitValue.pop();
                      const subDesign = splitValue.shift();
                      _.set(
                        member,
                        _.compact([subDesign, 'translations', selectedLanguage, filed]),
                        value
                      );
                      _.set(
                        member,
                        _.compact([subDesign, 'translations', selectedLanguage, 'locale']),
                        selectedLanguage
                      );
                    }
                  }
                  return hydraMember;
                } else {
                  return oldValue;
                }
              });
            },
            //this is for transferring from hydra:member translations
            // to hydra:member sub design and sub design translations
            onTransferTranslatedHydraMemberFieldToTranslatedAndNonTranslatedFields: (
              [
                design,
                field,
                checkSettings,
                ignoreCopyFromMaster = false,
                dontOverrideFields = null,
              ],
              state,
              { changeValue }
            ) => {
              const {
                formState: { values },
              } = state;
              //if user unchecked field in settings
              if (_.isObject(values[checkSettings])) {
                if (
                  values[checkSettings].hasOwnProperty(design) &&
                  !values[checkSettings][design]
                ) {
                  return null;
                }
              }
              changeValue(state, HYDRAMEMBER, oldValue => {
                if (_.isArray(oldValue)) {
                  for (const member of oldValue) {
                    if (member.copyFromMaster || ignoreCopyFromMaster) {
                      if (isEmpty(member.translations)) {
                        member.translations = {};
                      }
                      if (isEmpty(_.get(member, [design, 'translations']))) {
                        _.set(member, [design, 'translations'], {});
                      }
                      for (const translation in member.translations) {
                        if (member.translations.hasOwnProperty(translation)) {
                          // if user selected not overwrite field during warning pop up
                          if (
                            _.isArray(_.get(dontOverrideFields, [design, translation])) &&
                            _.get(dontOverrideFields, [design, translation]).includes(field)
                          )
                            continue;
                          const translationValue = _.get(member, [
                            'translations',
                            translation,
                            field,
                          ]);
                          if (
                            _.has(designAndTheirLanguages, design) &&
                            objectOfDesignFieldsForTransferring[design]?.includes(field) &&
                            !notTranslationFields.includes(field)
                          ) {
                            _.set(
                              member,
                              _.compact([design, 'translations', translation, field]),
                              translationValue
                            );
                            _.set(
                              member,
                              _.compact([design, 'translations', translation, 'locale']),
                              translation
                            );
                          }
                          //for designs without translations
                          if (
                            translation === 'en' &&
                            !_.has(designAndTheirLanguages, design) || notTranslationFields.includes(field) &&
                            !!objectOfDesignFieldsForTransferring[design]?.includes(field)
                          ) {
                            _.set(member, _.compact([design, field]), _.get(member, [
                              field,
                            ]));
                          }
                        }
                      }
                    }
                  }
                }
                return oldValue;
              });
            },
            setFakeData: ([form], state, { changeValue, getIn }) => {
              Object.entries(objectOfFakeDataHuge).forEach(([key, value]) => {
                changeValue(state, key, () => value);
                form.mutators.onChangeMainField(key, value);
                form.mutators.onChangeTranslatedField(key, value);
              });
              const {
                formState: { values },
              } = state;
              form.mutators.populateMainFieldsFromHydramember(values[HYDRAMEMBER], form);
            },
            deselectAllDesigns: ([form], state, { changeValue }) => {
              changeValue(state, HYDRAMEMBER, oldValue => {
                if (_.isArray(oldValue)) {
                  const newValue = JSON.parse(JSON.stringify(oldValue));
                  for (const member of newValue) {
                    member.copyFromMaster = false;
                  }
                  return newValue;
                } else {
                  return oldValue;
                }
              });
              const {
                formState: { values },
              } = state;
              form.mutators.populateMainFieldsFromHydramember(values[HYDRAMEMBER], form);
            },
            selectAllDesigns: ([form], state, formValue) => {
              formValue.changeValue(state, HYDRAMEMBER, oldValue => {
                if (_.isArray(oldValue)) {
                  const newValue = JSON.parse(JSON.stringify(oldValue));
                  for (const member of newValue) {
                    member.copyFromMaster = true;
                  }
                  return newValue;
                } else {
                  return oldValue;
                }
              });
              const {
                formState: { values },
              } = state;
              form.mutators.populateMainFieldsFromHydramember(values[HYDRAMEMBER], form);
            },
            clickOnDesignImage: (
              [name, form, ctrlKey = false, shiftKey = false],
              state,
              { changeValue }
            ) => {
              const {
                formState: { values },
              } = state;
              const clickedCopyFromMaster = _.get(values, name + '.copyFromMaster');

              changeValue(state, '', oldValue => {
                if (_.isArray(oldValue[HYDRAMEMBER])) {
                  if (!clickedCopyFromMaster && !ctrlKey && !shiftKey) {
                    for (const member of oldValue[HYDRAMEMBER]) {
                      member.copyFromMaster = false;
                    }
                    _.set(oldValue, name + '.copyFromMaster', true);
                  } else if (!clickedCopyFromMaster && ctrlKey) {
                    _.set(oldValue, name + '.copyFromMaster', true);
                  } else if (!clickedCopyFromMaster && shiftKey) {
                    const indexOfClickedImage = name.slice(13, -1);
                    if (indexOfClickedImage > values.firstEditableDesign) {
                      oldValue[HYDRAMEMBER].forEach((val, index) => {
                        if (
                          (index > values.firstEditableDesign || !values.firstEditableDesign) &&
                          index <= parseInt(indexOfClickedImage)
                        ) {
                          val.copyFromMaster = true;
                        }
                      });
                    } else {
                      oldValue[HYDRAMEMBER].forEach((val, index) => {
                        if (index < values.firstEditableDesign && index >= indexOfClickedImage) {
                          val.copyFromMaster = true;
                        }
                      });
                    }
                  }
                  if (clickedCopyFromMaster) {
                    _.set(oldValue, name + '.copyFromMaster', false);
                  }
                  return oldValue;
                } else {
                  return oldValue;
                }
              });
              form.mutators.populateMainFieldsFromHydramember(values[HYDRAMEMBER], form);
            },
            markAdult: ([form], state, { changeValue }) => {
              changeValue(state, HYDRAMEMBER, oldValue => {
                if (_.isArray(oldValue)) {
                  const newValue = JSON.parse(JSON.stringify(oldValue));
                  const editedValues = JSON.parse(JSON.stringify(oldValue)).filter(
                    val => val.copyFromMaster
                  );
                  if (isEmpty(editedValues)) {
                    return oldValue;
                  }
                  const initialValue = editedValues[0].isAdult;
                  for (const member of newValue) {
                    if (member.copyFromMaster) {
                      member.isAdult = !initialValue;
                      //uncheck fit youth if design is adult
                      if (!initialValue) {
                        _.set(member, fitYouth, false);
                        _.set(member, [designMBA, fitYouth], false);
                      }
                    }
                  }
                  changeValue(
                    state,
                    'haveIsAdult',
                    () => !!newValue.find(member => member.copyFromMaster && member.isAdult)
                  );
                  return newValue;
                } else {
                  changeValue(
                    state,
                    'haveIsAdult',
                    () => !!oldValue.find(member => member.copyFromMaster && member.isAdult)
                  );
                  return oldValue;
                }
              });
              const {
                formState: { values },
              } = state;
              form.mutators.populateMainFieldsFromHydramember(values[HYDRAMEMBER], form);
            },
            transfer: (
              [form, dontOverrideFields = null, transferAllDesignsEvenTheyAreNotSelected = false],
              state,
              tools
            ) => {
              const {
                formState: { values },
              } = state;
              const arrayOfTransferDesigns = getListOfSelectedDesigns(values);
              //
              form.batch(() => {
                arrayOfTransferDesigns.forEach(design => {
                  arrayOfTitles.forEach(title => {
                    form.mutators.onTransferTranslatedHydraMemberFieldToTranslatedAndNonTranslatedFields(
                      design,
                      title,
                      'Title',
                      transferAllDesignsEvenTheyAreNotSelected,
                      dontOverrideFields
                    );
                  });
                  arrayOfDescriptions.forEach(description => {
                    form.mutators.onTransferTranslatedHydraMemberFieldToTranslatedAndNonTranslatedFields(
                      design,
                      description,
                      'Description',
                      transferAllDesignsEvenTheyAreNotSelected,
                      dontOverrideFields
                    );
                  });
                  form.mutators.onTransferTranslatedHydraMemberFieldToTranslatedAndNonTranslatedFields(
                    design,
                    'tags',
                    'Tags',
                    transferAllDesignsEvenTheyAreNotSelected,
                    dontOverrideFields
                  );
                });
                if (arrayOfTransferDesigns.includes(designMBA)) {
                  arrayOfMBAFields.forEach(mbaField => {
                    form.mutators.onTransferTranslatedHydraMemberFieldToTranslatedAndNonTranslatedFields(
                      designMBA,
                      mbaField,
                      MBA,
                      transferAllDesignsEvenTheyAreNotSelected,
                      dontOverrideFields
                    );
                  });
                }
              });
              form.mutators.selectedLanguageOnChange(form);
            },
            addDesignToListOfDesignsAndSetDataFromTheFirstDesign: (
              [value, form],
              state,
              { changeValue, getIn }
            ) => {
              const {
                formState: { values },
              } = getIn(state, '');
              if (isEmpty(values[HYDRAMEMBER])) {
                changeValue(state, HYDRAMEMBER, oldValue => {
                  form.mutators.populateMainFieldsFromHydramember([value], form);
                  return [value];
                });
                return;
              }
              const result = _.omit(values[HYDRAMEMBER][0], [
                '@id',
                '@context',
                '@type',
                HYDRAMEMBER,
                'hydra:search',
                'hydra:totalItems',
                'hydra:view',
                'image',
                'project',
                'createdAt',
                'id',
                'owner',
                'project',
              ]);
              const obj = Object.assign({}, value, result);
              changeValue(state, HYDRAMEMBER, oldValue => {
                if (_.isArray(oldValue)) {
                  const newValue = JSON.parse(JSON.stringify(oldValue));
                  newValue.push(obj);
                  form.mutators.populateMainFieldsFromHydramember(newValue, form);
                  return newValue;
                } else {
                  form.mutators.populateMainFieldsFromHydramember([obj], form);
                  return [obj];
                }
              });
            },
            populateMainFields: ([form], state, tools) => {
              const {
                formState: { values },
              } = state;
              form.mutators.populateMainFieldsFromHydramember(values[HYDRAMEMBER], form);
            },
            populateMainFieldsFromHydramember: (
              [arrayOfMembers, form, checkTouchedField = false],
              state,
              tools
            ) => {
              form.batch(() => {
                if (!_.isArray(arrayOfMembers)) {
                  return;
                }
                let counterOfEditableFields = 0;
                let firstEditableDesign = null;
                arrayOfMembers.some((member, index) => {
                  if (member.copyFromMaster) {
                    counterOfEditableFields++;
                    // if (_.isNull(firstEditableDesign)) {
                    firstEditableDesign = index;
                    return true;
                    // }
                  }
                });
                //process general Tab
                populateMainFieldsFromHydramemberGetFieldsFromArrayOfFieldsAndCopyToUserEditableFields(
                  [...creationGeneralFields, ...generalFields],
                  arrayOfMembers,
                  form,
                  checkTouchedField
                );
                //process all other designs Tabs
                designs.forEach(design => {
                  populateMainFieldsFromHydramemberGetFieldsFromArrayOfFieldsAndCopyToUserEditableFields(
                    objectOfDesignFieldsForProcessingSubDesigns[design].map(
                      field => `${design}.${field}`
                    ),
                    arrayOfMembers,
                    form,
                    checkTouchedField
                  );
                });
                form.change(HYDRAMEMBER, arrayOfMembers);
                form.change('selectedCounter', counterOfEditableFields);
                form.change('firstEditableDesign', firstEditableDesign);
                form.change(
                  'haveIsAdult',
                  !!arrayOfMembers.find(member => member.copyFromMaster && member.isAdult)
                );
              });
            },
            selectedLanguageOnChange: ([form], state, tools) => {
              const {
                formState: { values },
              } = state;
              const selectedLanguage = _.get(values, 'selectedLanguage');
              if (values[HYDRAMEMBER]) {
                values[HYDRAMEMBER].forEach(member => {
                  //we rewrite already translated fields
                  objectOfDesignsWithTranslatedFields['general'].forEach(field => {
                    _.set(member, field, _.get(member, ['translations', selectedLanguage, field]));
                  });
                  //we replace already translated fields for subDesigns
                  _.forOwn(objectOfDesignsWithTranslatedFields, function(value, key) {
                    value.forEach(field => {
                      _.set(
                        member,
                        [key, field],
                        _.get(member, [key, 'translations', selectedLanguage, field])
                      );
                    });
                  });
                });
              }
              form.mutators.populateMainFieldsFromHydramember(values[HYDRAMEMBER], form);
            },
            copyFieldsFromLanguage: ([form, language], state, tools) => {
              const {
                formState: { values },
              } = state;
              const selectedLanguage = _.get(values, 'selectedLanguage');
              if (values[HYDRAMEMBER]) {
                values[HYDRAMEMBER].forEach(member => {
                  // for general
                  const translation = _.omit(_.get(member, ['translations', language]), [
                    '@id',
                    '@context',
                    '@type',
                  ]);
                  _.set(member, ['translations', selectedLanguage], translation);
                  // for subDesigns
                  Object.keys(designAndTheirLanguages).forEach(design => {
                    const translationForSubDesigns = _.get(member, [
                      design,
                      'translations',
                      language,
                    ]);
                    _.set(
                      member,
                      [design, 'translations', selectedLanguage],
                      translationForSubDesigns
                    );
                  });
                });
              }
              form.mutators.selectedLanguageOnChange(form);
            },
            removeDesignsButtonHandler: ([form], state, tools) => {
              const {
                formState: { values },
              } = state;
              if (values[HYDRAMEMBER]) {
                const array = [];
                values[HYDRAMEMBER].forEach((member, index) => {
                  if (member.copyFromMaster) {
                    array.push(index);
                  }
                });
                form.mutators.removeBatch(HYDRAMEMBER, array);
              }
            },
            addFromStorage: ([item], state, tools) => {
              tools.changeValue(state, HYDRAMEMBER, prev => {
                for (let member of prev) {
                  if (member.copyFromMaster) {
                    _.set(member, translations, item.translations);
                    // creationGeneralFields.forEach(field=>{
                    //   _.set(member,field,item.field);
                    // })
                  }
                }
                return prev;
              });
            },
            ifEmptyDisable: ([form], state, tools) => {
              const {
                formState: { values },
              } = state;
              if (values[HYDRAMEMBER]) {
                if(isEmpty(values[HYDRAMEMBER])){
                  document.getElementById('filedsetid').disabled = true;
                  form.restart();
                }
              }
            },
            selectTemplate: ([template, form], state, tools) => {
              const {
                formState: { values },
              } = state;
              const arrayOfTransferDesigns = getListOfSelectedDesigns(values);
              // form.batch(() => {
              arrayOfTransferDesigns.forEach(design => {
                objectOfDesignFieldsForProcessingSubDesigns[design].forEach(title => {
                  if (templateFields.includes(title)) {
                    values[HYDRAMEMBER].forEach((member, index) => {
                      if (member.copyFromMaster === true) {
                        _.set(member, [design, title], _.get(template, title));
                        _.set(member, 'templateName', _.get(template, 'name'));
                        _.set(member, 'template', _.get(template, '@id'));
                      }
                      // member[design][title] = template[title];
                    });
                  }
                });
              });
              // });
              form.mutators.populateMainFieldsFromHydramember(values[HYDRAMEMBER], form);
            },
            removeTemplateName: ([form], state, tools) => {
              const {
                formState: { values },
              } = state;
              const arrayOfTransferDesigns = getListOfSelectedDesigns(values);
              // form.batch(() => {
              arrayOfTransferDesigns.forEach(design => {
                objectOfDesignFieldsForProcessingSubDesigns[design].forEach(title => {
                  if (templateFields.includes(title)) {
                    values[HYDRAMEMBER].forEach((member, index) => {
                      if (member.copyFromMaster === true) {
                        // _.set(member, [design, title], _.get(template, title));
                        _.set(member, 'templateName', null);
                        _.set(member, 'template', null);
                      }
                      // member[design][title] = template[title];
                    });
                  }
                });
              });
              // });
              form.mutators.populateMainFieldsFromHydramember(values[HYDRAMEMBER], form);
            },
            removeSpecialChars: ([name, availablePrefixes], state, tools) => {
              tools.changeValue(state, name, prev => {
                for (let char of availablePrefixes) {
                  prev = prev.split(char).join('');
                }
                return prev;
              });
            },
            addToHydraMember: ([retrieved], state, tools) => {
              tools.changeValue(state, HYDRAMEMBER, prev => {
                return _.unionBy(prev, retrieved, 'id');
              });
            },
            loadListLanguageOnChange: ([retrieved], state, tools) => {
              const {
                formState: { values },
              } = state;
              const selectedLanguage = _.get(values, 'loadListLanguageFields.selectedLanguage');
              // tools.changeValue(state, loadListLanguageFields, prev => {
              //     objectOfDesignTextFieldsForUseColState[generalTranslatedFields].forEach(field => {
              //       _.set(prev,field, _.get(member, ['translations', selectedLanguage, field]));
              //     });
              //     objectOfDesignTextFieldsForUseColState[designMBATranslated].forEach(field => {
              //       _.set(member,field, _.get(member, ['translations', selectedLanguage, field]));
              //     });
              // });
            },
          }}
          component={DesignForm}
        />
      </Spin>
    </>
  );
};

export default React.memo(DesignFormMutators);
