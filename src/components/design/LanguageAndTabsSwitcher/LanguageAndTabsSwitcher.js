import { Button, Space } from 'antd';
import WorkspaceDesignTabs from '../../tabs/WorkspaceDesignTabs';
import React, {memo} from 'react';
import LoadListingModal from '../LoadListing/LoadListingModal';
import LanguageSelector from './LanguageSelector';
import TradeMarkChecker from "./TradeMarkChecker";
import TranslateEverythingButton from "./TranslateEverythingButton";
import LanguageSelectorForDesignForm from "./LanguageSelectorForDesignForm";

const LanguageAndTabsSwitcher = ({ setCurrentTab, onChange, onClick, value,currentTab }) => {

  return (
    <>
      <Space style={{float: 'right'}}>
        <LoadListingModal />
        {process.env.NODE_ENV === 'development' && <Button onClick={onClick}>Fake Data</Button>}
       <TradeMarkChecker/>
       <TranslateEverythingButton/>
        <LanguageSelectorForDesignForm value={value} onChange={onChange} currentTab={currentTab} />
      </Space>
      <WorkspaceDesignTabs setCurrentTab={setCurrentTab} currentTab={currentTab} />
    </>
  );
}

export default memo(LanguageAndTabsSwitcher);
