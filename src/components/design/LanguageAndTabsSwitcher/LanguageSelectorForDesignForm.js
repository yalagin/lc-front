import { Select } from 'antd';
import RoundedSelect from '../../input/RoundedSelect';
import React from 'react';
import {  translationKeys } from "../../../utils/constants/translation";
import { decodeTranslationObject } from '../../../utils/constants/constants';

const { Option } = Select;

export default function LanguageSelectorForDesignForm({ currentTab, onChange, value, right = false }) {

  const options = translationKeys.hasOwnProperty(currentTab) ? translationKeys[currentTab].map(language =>
    <Option value={language}>{decodeTranslationObject[language]}</Option>) : <Option value="en">English</Option>;
  return (
    <div className={'five-step-WorkspaceWalkthroughIntroduction'}>
      <RoundedSelect value={value} onChange={onChange} right={right}>
        {options}
      </RoundedSelect>
    </div>
  );
}
