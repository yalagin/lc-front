import {Select} from 'antd';
import RoundedSelect from '../../input/RoundedSelect';
import React from 'react';

const {Option} = Select;

export default function LanguageSelector({ onChange, value, right=false}) {

  return (
    <div className={'five-step-WorkspaceWalkthroughIntroduction'}>
    <RoundedSelect value={value} onChange={onChange} right={right}>
      <Option value="en">English</Option>
      <Option value="de">German {/*(Shirtee, SpreadshirtEU, RedBubble, MBA)*/}</Option>
      <Option value="fr">French {/*(MBA, Redbubble)*/}</Option>
      <Option value="es">Spanish {/*(MBA, Redbubble)*/}</Option>
      <Option value="it">Italian {/*(MBA)*/}</Option>
      <Option value="jp">Japanese{/* (MBA)*/}</Option>
    </RoundedSelect>
    </div>
  );
}
