import React, { useState } from "react";
import _, { isEmpty } from "lodash";
import useProfile from "../../../utils/hooks/useProfile";
import { Button, notification, Popover, Select, Tooltip } from "antd";
import { OkButtonHolder, TranslationHolder, TranslationImage } from "../TMSetting";
import { CloseOutlined, SwapOutlined } from "@ant-design/icons";
import Icons from '../../../assets/Icons/OldAssets';
import { decodeTranslationObject, general } from "../../../utils/constants/constants";
import { FormSpy, useField, useForm } from "react-final-form";
import { useMutation } from "react-query";
import translationCall from "../../../utils/mutations/translationCall";
import { objectOfDesignTextFieldsForTranslation } from "../../../utils/constants/translation";

function TranslateEverythingButton() {
  const { Option } = Select;
  const form = useForm();
  const { profile } = useProfile();
  const [isVisible, setIsVisible] = useState<boolean>(false);

  const { input: { value: selectedLanguageValue } } = useField('selectedLanguage', { subscription: { value: true } });
  const { input: { value: translateFrom,onChange:setTranslateFrom } } = useField('translateFrom', { subscription: { value: true } });


  const handleTranslateAllButton =   (values:any) =>{

    const currentTab = _.get(values, 'currentTab') ;
    const selectedLanguageValue =_.get(values, 'selectedLanguage');
    const translateFrom = _.get(values,'translateFrom');
     form.batch(  () => {

      if (currentTab === general) {
        objectOfDesignTextFieldsForTranslation[general].forEach(async field => {
          const selectedFieldValue = _.get(values, field);

          const data =  await  translationCall({ selectedLanguageValue, selectedFieldValue, translateFrom, profile });
          console.log(data);
          form.change( field,  data);
        });
      } else {
        objectOfDesignTextFieldsForTranslation[currentTab].forEach(async field => {
          const selectedFieldValue = _.get(values, currentTab + "." + field);

          const data =  await  translationCall({ selectedLanguageValue, selectedFieldValue, translateFrom, profile });
          console.log(data);
          form.change(currentTab + "." + field, data);
        })
      }
    })
  }

  const popContentForNoApiKey = () => {
      return (
        <div>
          <div className="modal-tm-select-container">
            <div className="cross-content" onClick={() => setIsVisible(false)}>
              <CloseOutlined />
            </div>
            <div style={{ marginBottom: '0.5rem' }} />
            <div className="modal-select-content mt-2">
              please add a valid api key for <a href="https://www.deepl.com/pro">deepl</a> first at settings panel
            </div>
          </div>
        </div>
      );
  }


  const popContentForApiKey = () => {
    return (
      <div>
        <div className="modal-tm-select-container">
          <div className="cross-content" onClick={() => setIsVisible(false)}>
            <CloseOutlined />
          </div>
          <div style={{ marginBottom: '0.5rem' }} />
          <div className="modal-select-content mt-2">
            <TranslationHolder>
              <div className="">
                <Select value={translateFrom} style={{ width: 110 }} onChange={setTranslateFrom}>
                  {Object.keys(decodeTranslationObject).map(function(keyName:string, keyIndex:number) {
                    // @ts-ignore
                    return <Option value={keyName}>{decodeTranslationObject[keyName] as string}</Option>;
                  })}
                </Select>
              </div>
              <SwapOutlined />
              {/* @ts-ignore */}
              <div className="">{decodeTranslationObject[selectedLanguageValue]}</div>
            </TranslationHolder>
            <br />
            <OkButtonHolder>
              {isVisible && (
                //just for dev purposes we need to see whats inside form
                <FormSpy subscription={{ values: true }}>
                  {({ values }) => {
                    return <Button
                      className="btn-tm mt-2"
                      onClick={() => {
                        handleTranslateAllButton(values);
                        setIsVisible(false);
                      }}
                    >
                      OK
                    </Button>;
                  }}
                </FormSpy>
              )}
            </OkButtonHolder>
          </div>
        </div>
      </div>
    );
  }

  return (
    <>
      <Popover
        placement="bottomRight"
        className="tm-setting-content"
        content={_.isEmpty(profile?.apiKey)?popContentForNoApiKey:popContentForApiKey}
        trigger={'click'}
        style={{width: 100}}
        visible={isVisible}
        onVisibleChange={setIsVisible}
        popupVisible={false}
      ><Tooltip placement="top" title={"Translate Fields in current page"}><Button>
        <TranslationImage
          src={Icons.ComHeaderTrans}
          alt="com-header-trans"
          className="cursor-pointer"
        /></Button> </Tooltip>
      </Popover>
    </>
  );
}

export default TranslateEverythingButton;
