import React from "react";
import { useField } from "react-final-form";
import { Button } from "antd";
import TMButton from "../TMSetting/TMButton";

function TradeMarkChecker() {
  const { input: {value: description140}, } = useField('description140', {subscription: {value: true}});
  const { input: {value: description200}, } = useField('description200', {subscription: {value: true}});
  const { input: {value: description250}, } = useField('description250', {subscription: {value: true}});
  const { input: {value: description2000}, } = useField('description2000', {subscription: {value: true}});
  const { input: {value: title26}, } = useField('title26', {subscription: {value: true}});
  const { input: {value: title40}, } = useField('title40', {subscription: {value: true}});
  const { input: {value: title50}, } = useField('title50', {subscription: {value: true}});
  const { input: {value: title60}, } = useField('title60', {subscription: {value: true}});
  const { input: {value: title100}, } = useField('title100', {subscription: {value: true}});

  const { input: {value: tags}, } = useField('tags', {subscription: {value: true}});

  return (
    <TMButton selectedFieldValue={[description140,description200,description250,description2000,title26,title40,title50,title60,title100,...tags]}
              SelectedButton={Button} />
  );
}

export default TradeMarkChecker;
