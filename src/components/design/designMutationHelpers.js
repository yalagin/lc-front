import _ from 'lodash';
import {
  areArraysEqualComparisonWithUploadFileNames,
  capitalizeFirstLetter,
} from '../../utils/utilsFunctions';
import {arrayOfCopiedFields, DISABLED} from '../../utils/constants/constants';

const populateMainFieldsFromHydramemberGetFieldsFromArrayOfFieldsAndCopyToUserEditableFields = (
  arrayOfFields,
  arrayOfMembers,
  form,
  checkTouchedField
) => {
  loop1: for (const field of arrayOfFields) {
    if (isEmpty(arrayOfMembers)) {
      // form.change(field, null);
      // form.change(field + DISABLED, false);
      process.env.REACT_APP_SPY &&console.log({
          "valueEMPTY": field,
      });
      return;
    }
    let initialValue = null;
    //  this is checking null
    //  let isThereAlreadyWasNullBefore = false;

    for (const member of arrayOfMembers) {
      if (member.copyFromMaster !== true) {
        continue;
      }
      const extractedValueFromMember = _.get(member, field);
      process.env.REACT_APP_SPY && console.log({
          "value": field,
          "extractedValueFromMember": extractedValueFromMember,
          "initialValue": initialValue
      });
      //  this is checking null
      // if (!_.isEqual(initialValue,extractedValueFromMember)  && !isEmpty(initialValue)
      //     ||isThereAlreadyWasNullBefore &&!isEmpty(extractedValueFromMember)
      //     ||_.isArray(extractedValueFromMember) &&  !_.isEqual(_.sortBy(initialValue), _.sortBy(extractedValueFromMember))&& !isEmpty(initialValue) ) {
      //     form.change(field, null) // listeners not notified
      //     form.change(field + DISABLED, true) // listeners not notified
      //     // console.log({
      //     //     "valueIsNotSame": field,
      //     //     "extractedValueFromMember": extractedValueFromMember,
      //     //     "initialValue": initialValue
      //     // });
      //     continue loop1;
      // }
      // this is not checking null
      if (
        (!_.isEqual(initialValue, extractedValueFromMember) &&
          !isEmpty(initialValue) &&
          !isEmpty(extractedValueFromMember) &&
          !_.isArray(extractedValueFromMember) &&
          !_.isArray(initialValue)) ||
        (_.isArray(extractedValueFromMember) &&
          !areArraysEqualComparisonWithUploadFileNames(
            initialValue,
            extractedValueFromMember,
            field
          ) &&
          !isEmpty(initialValue) &&
          !isEmpty(extractedValueFromMember))
      ) {
        form.change(field, undefined);
        form.change(field + DISABLED, true);
        process.env.REACT_APP_SPY &&
          console.log({
            valueIsNotSame: field,
            extractedValueFromMember: extractedValueFromMember,
            initialValue: initialValue,
          });
        if (checkTouchedField && arrayOfCopiedFields.includes(field)) {
          form.change('touched' + capitalizeFirstLetter(field), true);
        }
        continue loop1;
      }
      // if (!isEmpty(extractedValueFromMember) ||( typeof extractedValueFromMember )=== "boolean") {
      initialValue = extractedValueFromMember;
      // }
      // this is disabling null checking
      // if(_.isNull(extractedValueFromMember)){
      //     isThereAlreadyWasNullBefore = true;
      // }
    }
    form.change(field, initialValue); // listeners not notified
    form.change(field + DISABLED, false); // listeners not notified

    // green buttons
    if (checkTouchedField && arrayOfCopiedFields.includes(field) && !isEmpty(initialValue)) {
      form.change('touched' + capitalizeFirstLetter(field), true);
    } else if (checkTouchedField && arrayOfCopiedFields.includes(field) && isEmpty(initialValue)) {
      form.change('touched' + capitalizeFirstLetter(field), false);
    }
    process.env.REACT_APP_SPY &&
      console.log({
        valueSame: field,
        initialValue: initialValue,
      });
  }
};
export default populateMainFieldsFromHydramemberGetFieldsFromArrayOfFieldsAndCopyToUserEditableFields;

export function isEmpty(value) {
  return (
    value === undefined ||
    value === null ||
    (typeof value === 'object' && Object.keys(value).length === 0) ||
    (typeof value === 'string' && value.trim().length === 0)
  );
}
