import React, { useEffect } from 'react';
import useProfile from '../../../utils/hooks/useProfile';
import { useField } from 'react-final-form';
import { update } from '../../../actions/person/update';
import { useDispatch } from 'react-redux';
import _ from 'lodash';

const useSaveEditOfDesignStateOnUpdate = () => {
  const {
    input: { value: valueOfSelectedTitle },
  } = useField('Edit', { subscription: { value: true } });
  const { profile } = useProfile();
  const dispatch = useDispatch();

  useEffect(() => {
    if (
      profile &&
      valueOfSelectedTitle &&
      !_.isEqual(valueOfSelectedTitle, profile?.editableDesignsForWorkspace)
    ) {
      dispatch(update(profile, { editableDesignsForWorkspace: valueOfSelectedTitle }));
    }
  }, [valueOfSelectedTitle]);
};

export default useSaveEditOfDesignStateOnUpdate;
