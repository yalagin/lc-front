import { Alert, Spin } from 'antd';
import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { reset as resetCreate } from '../../../actions/design/create';
import { reset as resetMedia } from '../../../actions/mediaobject/create';
import '../../mediaobject/AddToDesignCreatedAndUpdated.css';
import { HYDRAMEMBER } from '../../../utils/constants/constants';
import _ from 'lodash';

const AddToDesignCreatedAndUpdated = ({ form }) => {
    const { created, error } = useSelector(state => state.design.create);
    const { error: errorMedia, loading: loadingMedia } = useSelector(state => state.mediaobject.create);
    const dispatch = useDispatch();

    useEffect(() => {
        if (!_.isNull(created)) {
            form.mutators.addDesignToListOfDesignsAndSetDataFromTheFirstDesign(created, form);
            document.getElementById('filedsetid').disabled = false;
        }
    }, [created]);

    const { retrieved } = useSelector(state => state.design.list);

    useEffect(() => {
        if (retrieved && retrieved[HYDRAMEMBER] && !_.isEmpty(retrieved[HYDRAMEMBER])) {
            form.mutators.addToHydraMember(retrieved[HYDRAMEMBER]);
            form.mutators.selectedLanguageOnChange(form);
            document.getElementById('filedsetid').disabled = false;
            // form.mutators.populateMainFieldsFromHydramember(retrieved[HYDRAMEMBER],form);
        }
        if (_.isNull(retrieved) || _.isEmpty(retrieved[HYDRAMEMBER])) {
            form.change(HYDRAMEMBER, []);
            document.getElementById('filedsetid').disabled = true;
        }
    }, [retrieved]);

    return (
      <>
          {error && (
            <>
                <Alert
                  message={error}
                  type="error"
                  showIcon
                  closable
                  afterClose={() => {
                      dispatch(resetCreate());
                  }}
                />
            </>
          )}
          {loadingMedia && <Spin tip="Loading..." />}
          {errorMedia && (
            <>
                <Alert
                  message={errorMedia}
                  type="error"
                  showIcon
                  closable
                  afterClose={() => {
                      dispatch(resetMedia());
                  }}
                />
            </>
          )}
      </>
    );
};
export default AddToDesignCreatedAndUpdated;
