import React, { useRef } from "react";
import { GreyHeader, StyledAddMore } from '../../styles/ImageStyles';
import Icons from '../../../assets/Icons/OldAssets';
import { Button, InputNumber } from 'antd';
import { UploadOutlined } from '@ant-design/icons';
import styled from 'styled-components';
import { create as createDesign } from '../../../actions/design/create';
import { useDispatch } from 'react-redux';
import useProject from '../../../utils/hooks/useProject';

const NonPropagatedDivForPlaceHolder = styled.div`
  display: flex;
  justify-content: center;

  > * {
    margin: 0.5rem;
  }
`;
const PlaceholderNumber = styled(InputNumber)`
  min-width: 60px;
  max-height: 34px;
`;


const UploadButtons = ({ setPopoverVisible }) => {
  const dispatch = useDispatch();
  const { project } = useProject();
  const numberRef = useRef();

  const submit = () =>{
    const number =  numberRef.current.value;
    for (var i = 0; i < number; i++) {
      dispatch(createDesign({
        "project": project["@id"],
      }));
    }
    if(setPopoverVisible){
      setPopoverVisible(false);
    }
  }

  const onClick = e => {
    e.stopPropagation();
  };

  const onClickUpload = e => {
    document.getElementById('filedsetid').disabled = false;
  };

  return (
    <StyledAddMore id={'add-design-here'}>
      <img src={Icons.UploadIcon} alt="file-upload" style={{ maxHeight: '80' }} />
      <GreyHeader>Upload or drag your design here</GreyHeader>
      <Button type={'primary'} id={'upload-design-button'} onClick={onClickUpload} >
        <UploadOutlined />
        Upload Design
      </Button>
      <NonPropagatedDivForPlaceHolder onClick={onClick}>
        <PlaceholderNumber ref={numberRef} onPressEnter={onClick} min={1} max={50} defaultValue={3} />
        <Button id={'add-placeholders'} onClick={submit} type={'primary'}>Add Placeholders</Button>
      </NonPropagatedDivForPlaceHolder>
    </StyledAddMore>
  );
}

export default UploadButtons;
