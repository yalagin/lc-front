import React, { useEffect, useRef, useState } from 'react';
import { Button } from 'antd';
import { FieldArray } from 'react-final-form-arrays';
import { HYDRAMEMBER } from '../../../utils/constants/constants';
import './ImageCarousel.less';
import ImageWIthBorderAsACheckbox from '../../mediaobject/image/ImageWIthBorderAsACheckbox';
import { Field, useField } from 'react-final-form';
import StyledBottomRow from '../bottomButtonsOfCarousel/StyledBottomRow';
import _ from 'lodash';
import { ImageCarousel, StyledSpace, StyledUpload } from '../../styles/ImageStyles';
import UploadButtons from './UploadButtons';

const MyImageCarousel = ({
                           setOnePicInSlider,
                           onePicInSlider,
                           form,
                           uploadImage,
                           setOpenModal,
                           handleDeleteDesigns,
                           settings = null,
                           hideButtons = false,
                           handleRemoveDesigns,
                         }) => {
  const [selectedDesignsCounter, setSelectedDesignsCounter] = useState(0);
  const {
    input: { value: valueOfFirstEditableDesign },
  } = useField('firstEditableDesign', { subscription: { value: true } });
  const {
    input: { value: values },
  } = useField(HYDRAMEMBER, { subscription: { value: true } });

  let slider = useRef(null);
  useEffect(() => {
    if (slider.current) {
      console.log(slider);
      if (onePicInSlider) {
        console.dir(valueOfFirstEditableDesign);
        slider.current.goTo(valueOfFirstEditableDesign ? valueOfFirstEditableDesign : 0);
      } else {
        slider.current.goTo(0);
      }
    }
  }, [onePicInSlider]);

  return (
    <FieldArray name={HYDRAMEMBER} subscription={{}}>
      {({ fields }, length = fields.length) => (
        <>
          {!!length && (
            <>
              <StyledSpace justify="space-between" align="middle" >
                <div>
                  <Field name={'selectedCounter'} subscription={{ value: true }}>
                    {({ input }) => {
                      setSelectedDesignsCounter(input.value);
                      return input.value;
                    }}
                  </Field>{' '}
                  of {length}
                </div>
                <div className={'select-all'}>
                {selectedDesignsCounter === length && (
                  <Button
                    onClick={() => {
                      form.mutators.deselectAllDesigns(form);
                      form.change('selectedCounter', 0);
                    }}
                  >
                    Deselect All
                  </Button>
                )}
                {selectedDesignsCounter !== length && (
                  <Button
                    onClick={() => {
                      form.mutators.selectAllDesigns(form);
                      form.change('selectedCounter', length);
                    }}
                  >
                    Select All
                  </Button>
                )}
                </div>
              </StyledSpace>
              <ImageCarousel {...settings} ref={node => (slider.current = node)}>
                {fields.map((name, index) => (
                  <div key={_.get(values, `[${index}]["id"]`)}>
                    <ImageWIthBorderAsACheckbox
                      key={_.get(values, `[${index}]["id"]`)}
                      name={name}
                      form={form}
                      index={index}
                    />
                  </div>
                ))}
              </ImageCarousel>
            </>
          )}
          {!length && (
            <div className="upload-main">
              <StyledUpload showUploadList={false} multiple customRequest={uploadImage} accept="image/*">
                <UploadButtons />
              </StyledUpload>
            </div>
          )}
          {!hideButtons && (
            <StyledBottomRow
              uploadImage={uploadImage}
              form={form}
              setOnePicInSlider={setOnePicInSlider}
              setOpenModal={setOpenModal}
              handleDeleteDesigns={handleDeleteDesigns}
              handleRemoveDesigns={handleRemoveDesigns}
            />
          )}
        </>
      )}
    </FieldArray>
  );
};

export default MyImageCarousel;
