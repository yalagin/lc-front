import React, { useEffect, useState } from 'react';
import { HYDRAMEMBER } from '../../../utils/constants/constants';
import { useInfiniteQuery } from 'react-query';
import { fetch2 } from '../../../utils/dataAccess';
import qs from 'qs';
import useProject from '../../../utils/hooks/useProject';
import { StyledSearch } from '../../styles/TableStyles';
import { Button, Image, List, Skeleton, Spin, Tag } from 'antd';
import { ENTRYPOINT } from '../../../config/entrypoint';
import _ from 'lodash';
import styled from 'styled-components';

const StyledListItem = styled(List.Item.Meta)`
  border: ${props => (props.checkedValue ? ' 3px solid #7f67ff' : '')};
  border-radius: 5px;

  :hover {
    cursor: pointer;
    //box-shadow: 0 1px 2px -2px rgb(0 0 0 / 16%), 0 3px 6px 0 rgb(0 0 0 / 12%),
    //0 5px 12px 4px rgb(0 0 0 / 9%);
  }
`;


const Storage = ({ selectedStorage, setSelectedStorage}) => {
  const { project } = useProject();
  const [dataSource, setDataSource] = useState([]);

  const [queryParams, setQueryParams] = useState({
    pageSize: 30,
    from: 0,
    queries: [{ field: 'isEdit', value: false, type: 'boolean' }],
    project: {
      match_phrase_prefix: { project: project?.id },
    },
  });

  useEffect(() => {
    if (project) {
      setQueryParams(prevState => ({
        ...prevState,
        project: {
          match_phrase_prefix: { project: project.id },
        },
      }));
      // refetch()
    }
  }, [project]);

  function fetchDesignsFromElasticSearch({ pageParam = 0 }) {
    if (!pageParam) {
      return fetch2(
        `/es/search?${qs.stringify(queryParams, {  skipNulls: true })}`
      );
    }
    return fetch2(pageParam);
  }

  const { data, error, isFetching, fetchNextPage, refetch, hasNextPage, isLoading } = useInfiniteQuery(
    ['design'],
    fetchDesignsFromElasticSearch,
    {
      keepPreviousData: true,
      staleTime: 10000,
      getNextPageParam: lastPage => lastPage['hydra:view']['hydra:next'],
      enabled: false,
    }
  );
  useEffect(() => {
    let paginatedData = [];
    data?.pages.forEach(page => {
      paginatedData = paginatedData.concat(page[HYDRAMEMBER]);
    });
    setDataSource(paginatedData);
  }, [data]);

  useEffect(() => {
    if (queryParams.project.match_phrase_prefix['project']) {
      refetch();
    }
  }, [queryParams]);

  const onSearch = value => {
    console.log(value);
    setQueryParams(prevState => ({ ...prevState, search_all: value }));
  };

  const loadMore =
    hasNextPage && (!isFetching ? (
        <div
          style={{
            textAlign: 'center',
            marginTop: 12,
            height: 32,
            lineHeight: '32px',
          }}
        >
          <Button onClick={fetchNextPage}>load more</Button>
        </div>
      ) :
      <div
        style={{
          textAlign: 'center',
          marginTop: 12,
          height: 32,
          lineHeight: '32px',
        }}
      ><Spin /></div>);

  const onCheck = (item) => {

  };

  return (
    <div>
      <div style={{ width: '100%' }}>
        <StyledSearch
          size="large"
          bordered={false}
          allowClear
          placeholder={'Search'}
          onSearch={onSearch}
          enterButton
        />
      </div>
      <br />
      <br />
      <List
        dataSource={dataSource}
        loadMore={loadMore}
        renderItem={item => (
          <List.Item onClick={()=>setSelectedStorage(item)}>
            <Skeleton avatar title={false} loading={isLoading} active>
              <StyledListItem
                checkedValue={ _.isEqual(item,selectedStorage)}
                style={{ alignItems: 'center' }}
                avatar={
                  <Image
                    style={{ height: '100px' }}
                    src={
                      item?.image
                        ? new URL(item?.image?.contentUrl, ENTRYPOINT)
                        : 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAMIAAADDCAYAAADQvc6UAAABRWlDQ1BJQ0MgUHJvZmlsZQAAKJFjYGASSSwoyGFhYGDIzSspCnJ3UoiIjFJgf8LAwSDCIMogwMCcmFxc4BgQ4ANUwgCjUcG3awyMIPqyLsis7PPOq3QdDFcvjV3jOD1boQVTPQrgSkktTgbSf4A4LbmgqISBgTEFyFYuLykAsTuAbJEioKOA7DkgdjqEvQHEToKwj4DVhAQ5A9k3gGyB5IxEoBmML4BsnSQk8XQkNtReEOBxcfXxUQg1Mjc0dyHgXNJBSWpFCYh2zi+oLMpMzyhRcASGUqqCZ16yno6CkYGRAQMDKMwhqj/fAIcloxgHQqxAjIHBEugw5sUIsSQpBobtQPdLciLEVJYzMPBHMDBsayhILEqEO4DxG0txmrERhM29nYGBddr//5/DGRjYNRkY/l7////39v///y4Dmn+LgeHANwDrkl1AuO+pmgAAADhlWElmTU0AKgAAAAgAAYdpAAQAAAABAAAAGgAAAAAAAqACAAQAAAABAAAAwqADAAQAAAABAAAAwwAAAAD9b/HnAAAHlklEQVR4Ae3dP3PTWBSGcbGzM6GCKqlIBRV0dHRJFarQ0eUT8LH4BnRU0NHR0UEFVdIlFRV7TzRksomPY8uykTk/zewQfKw/9znv4yvJynLv4uLiV2dBoDiBf4qP3/ARuCRABEFAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghgg0Aj8i0JO4OzsrPv69Wv+hi2qPHr0qNvf39+iI97soRIh4f3z58/u7du3SXX7Xt7Z2enevHmzfQe+oSN2apSAPj09TSrb+XKI/f379+08+A0cNRE2ANkupk+ACNPvkSPcAAEibACyXUyfABGm3yNHuAECRNgAZLuYPgEirKlHu7u7XdyytGwHAd8jjNyng4OD7vnz51dbPT8/7z58+NB9+/bt6jU/TI+AGWHEnrx48eJ/EsSmHzx40L18+fLyzxF3ZVMjEyDCiEDjMYZZS5wiPXnyZFbJaxMhQIQRGzHvWR7XCyOCXsOmiDAi1HmPMMQjDpbpEiDCiL358eNHurW/5SnWdIBbXiDCiA38/Pnzrce2YyZ4//59F3ePLNMl4PbpiL2J0L979+7yDtHDhw8vtzzvdGnEXdvUigSIsCLAWavHp/+qM0BcXMd/q25n1vF57TYBp0a3mUzilePj4+7k5KSLb6gt6ydAhPUzXnoPR0dHl79WGTNCfBnn1uvSCJdegQhLI1vvCk+fPu2ePXt2tZOYEV6/fn31dz+shwAR1sP1cqvLntbEN9MxA9xcYjsxS1jWR4AIa2Ibzx0tc44fYX/16lV6NDFLXH+YL32jwiACRBiEbf5KcXoTIsQSpzXx4N28Ja4BQoK7rgXiydbHjx/P25TaQAJEGAguWy0+2Q8PD6/Ki4R8EVl+bzBOnZY95fq9rj9zAkTI2SxdidBHqG9+skdw43borCXO/ZcJdraPWdv22uIEiLA4q7nvvCug8WTqzQveOH26fodo7g6uFe/a17W3+nFBAkRYENRdb1vkkz1CH9cPsVy/jrhr27PqMYvENYNlHAIesRiBYwRy0V+8iXP8+/fvX11Mr7L7ECueb/r48eMqm7FuI2BGWDEG8cm+7G3NEOfmdcTQw4h9/55lhm7DekRYKQPZF2ArbXTAyu4kDYB2YxUzwg0gi/41ztHnfQG26HbGel/crVrm7tNY+/1btkOEAZ2M05r4FB7r9GbAIdxaZYrHdOsgJ/wCEQY0J74TmOKnbxxT9n3FgGGWWsVdowHtjt9Nnvf7yQM2aZU/TIAIAxrw6dOnAWtZZcoEnBpNuTuObWMEiLAx1HY0ZQJEmHJ3HNvGCBBhY6jtaMoEiJB0Z29vL6ls58vxPcO8/zfrdo5qvKO+d3Fx8Wu8zf1dW4p/cPzLly/dtv9Ts/EbcvGAHhHyfBIhZ6NSiIBTo0LNNtScABFyNiqFCBChULMNNSdAhJyNSiECRCjUbEPNCRAhZ6NSiAARCjXbUHMCRMjZqBQiQIRCzTbUnAARcjYqhQgQoVCzDTUnQIScjUohAkQo1GxDzQkQIWejUogAEQo121BzAkTI2agUIkCEQs021JwAEXI2KoUIEKFQsw01J0CEnI1KIQJEKNRsQ80JECFno1KIABEKNdtQcwJEyNmoFCJAhELNNtScABFyNiqFCBChULMNNSdAhJyNSiECRCjUbEPNCRAhZ6NSiAARCjXbUHMCRMjZqBQiQIRCzTbUnAARcjYqhQgQoVCzDTUnQIScjUohAkQo1GxDzQkQIWejUogAEQo121BzAkTI2agUIkCEQs021JwAEXI2KoUIEKFQsw01J0CEnI1KIQJEKNRsQ80JECFno1KIABEKNdtQcwJEyNmoFCJAhELNNtScABFyNiqFCBChULMNNSdAhJyNSiECRCjUbEPNCRAhZ6NSiAARCjXbUHMCRMjZqBQiQIRCzTbUnAARcjYqhQgQoVCzDTUnQIScjUohAkQo1GxDzQkQIWejUogAEQo121BzAkTI2agUIkCEQs021JwAEXI2KoUIEKFQsw01J0CEnI1KIQJEKNRsQ80JECFno1KIABEKNdtQcwJEyNmoFCJAhELNNtScABFyNiqFCBChULMNNSdAhJyNSiEC/wGgKKC4YMA4TAAAAABJRU5ErkJggg=='
                    }
                  />
                }
                title={item?.translations?.en?.title26}
                description={<>
                  {item?.translations?.en?.description140}
                  <br />
                  {item?.translations?.en?.tags&&  item?.translations?.en?.tags.map(tag => <Tag color="#7F67FF" style={{ marginBottom: "0.5rem"/*,borderRadius: "23px"*/ }}> {tag}</Tag>)}
                </>}
              />
            </Skeleton>
          </List.Item>
        )}
      />
    </div>
  );
};

export default Storage;
