import React, { useEffect, useState } from "react";
import { Menu, Select, Typography } from "antd";
import { useField,useForm } from "react-final-form";
import { decodeTranslationObject, HYDRAMEMBER } from "../../../utils/constants/constants";
import _ from "lodash";

const LoadLanguage = () => {
  const form = useForm();
  const { Text, Link } = Typography;
  const [languages, setLanguages] = useState([]);
  const firstEditableDesign = useField('firstEditableDesign', {subscription: {value: true}});
  const selectedLanguage = useField('selectedLanguage', {subscription: {value: true}});
  const hydraField = useField(HYDRAMEMBER, {subscription: {value: true}});
  const {
    input: { value: selectedLanguageValue, onChange: selectedLanguageOnChange },
  } = useField('loadListLanguageFields.selectedLanguage', { subscription: { value: true } });

  useEffect(() => {
    const numberOfSelectedDesign = firstEditableDesign.input.value;
    const hydra = hydraField.input.value;
    if((!!numberOfSelectedDesign||numberOfSelectedDesign===0)&&hydra&&hydra[numberOfSelectedDesign]["translations"]){
      setLanguages(_.filter((Object.keys(hydra[numberOfSelectedDesign]["translations"])), function(n) {
        return n !== selectedLanguage.input.value;
      }));
    }
  }, [firstEditableDesign.input.value,selectedLanguage.input.value]);

  return (
    <>
      <Menu>
        {languages && Object.keys(decodeTranslationObject).map(language => (
          <Menu.Item key={language} disabled={!languages.includes(language)}>
            <a onClick={()=>form.mutators.copyFieldsFromLanguage(form,language)}>
              From {decodeTranslationObject[language]}
            </a>
          </Menu.Item>
        ))}
      </Menu>
      {/*<br/><br/>*/}
      {/*<Text mark>this is the Work In Progress implementation (Design is not finished) </Text>*/}
      {/*<br/>*/}
      {/*<Select style={{minWidth:"180px"}} placeholder="Select a language" value={selectedLanguageValue}  onChange={e => {*/}
      {/*  selectedLanguageOnChange(e);*/}
      {/*  form.mutators.loadListLanguageOnChange();*/}
      {/*}} >*/}
      {/*  <Option value="en" disabled={!languages.includes('en')}>English</Option>*/}
      {/*  <Option value="de" disabled={!languages.includes("de")}>German (Shirtee, SpreadshirtEU, RedBubble, MBA)</Option>*/}
      {/*  <Option value="fr" disabled={!languages.includes("fr")}>French (MBA, Redbubble)</Option>*/}
      {/*  <Option value="es" disabled={!languages.includes("es")}>Spanish (MBA, Redbubble)</Option>*/}
      {/*  <Option value="it" disabled={!languages.includes("it")}>Italian (MBA)</Option>*/}
      {/*  <Option value="jp" disabled={!languages.includes("jp")}>Japanese (MBA)</Option>*/}
      {/*</Select>*/}
      {/*<br/>*/}
      {/*<br/>*/}
      {/*<CommonFields prefix={'loadListLanguageFields.'}  />*/}
    </>
  );
};

export default LoadLanguage;
