import React, {useState} from 'react';
import {Button, Modal, Tabs} from 'antd';
import Storage from './Storage';
import { listDesignEditWithProject } from "../../../actions/design/list";
import qs from "qs";
import { useDispatch } from "react-redux";
import useProject from "../../../utils/hooks/useProject";
import _ from 'lodash';
import LoadLanguage from "./LoadLanguage";
import Edit from "./Edit";
import { useForm } from "react-final-form";

const LoadListingModal = () => {
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [editSelectedIds, setEditSelectedIds] = useState([]);
  const [selectedStorage, setSelectedStorage] = useState(null);
  const {TabPane} = Tabs;
  const dispatch = useDispatch();
  const {project} = useProject();
  const form = useForm();

  const showModal = () => {
    setIsModalVisible(true);
  };

  const handleOk = () => {
    if(!_.isEmpty(editSelectedIds)) {dispatch(
        listDesignEditWithProject(
          `designs?project=${project['@id']}&${qs.stringify(
            {id: editSelectedIds},
            { skipNulls: false}
          )}`
        )
      );
      setEditSelectedIds([]);
    }
    if(!_.isEmpty(selectedStorage)) {
      form.mutators.addFromStorage(selectedStorage);
      form.mutators.selectedLanguageOnChange(form);
    }
    handleCancel();
  };

  const handleCancel = () => {
    setIsModalVisible(false);
  };

  return (
    <>
      <Button id={'load-listing-btn'} onClick={showModal}>Load Listing</Button>
      {isModalVisible &&
        <Modal
          width={'30%'}
          title="Load Listing"
          visible={isModalVisible}
          onOk={handleOk}
          onCancel={handleCancel}
        >
          <div id={'load-listing-tabs'}  className={"load-listing-tabs"} style={{marginTop: -25}}>
          <Tabs  defaultActiveKey="2" >
            <TabPane tab="Other Languages" key="1" id={'load-language'}>
              <LoadLanguage />
            </TabPane>
            <TabPane tab="Storage" key="2" className={'selected-storage'} id={'selected-storage'}>
              <Storage selectedStorage={selectedStorage} setSelectedStorage={setSelectedStorage} />
            </TabPane>
            <TabPane tab="Blueprints" key="3"  id={'blueprints'}>
              Work in Progress
            </TabPane>
            {/*<TabPane tab="Add designs to workspace" key="4">*/}
            {/*  <Edit selectedIds={editSelectedIds} setSelectedIds={setEditSelectedIds}/>*/}
            {/*</TabPane>*/}
          </Tabs>
          </div>
        </Modal>
      }
    </>
  );
};
export default LoadListingModal;
