import React from "react";
import { Typography } from "antd";

const {Title} = Typography;

const PopoverTitle = ({title}) => <Title  onClick={e => (e.stopPropagation())} level={4}> {title} Settings</Title>;

export default PopoverTitle;
