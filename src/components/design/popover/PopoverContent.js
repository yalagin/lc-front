import React from "react";
import { ExclamationCircleOutlined } from "@ant-design/icons";
import { designs } from "../../../utils/constants/constants";
import { Checkbox, Col, Row } from "antd";
import _ from "lodash";
import { Field, useField } from "react-final-form";
import { makeDesignsStringLookPretty } from "../../../utils/designHelperFunctions";

const PopoverContent = ({title}) => {
  const { input: { value: valueOfSelectedTitle } } = useField("Edit", { subscription: { value: true } });

  return (
    <div className="edit-fields">
      <div>
        <ExclamationCircleOutlined style={{ color: "#7f67ff" }} /> Entries Effect
      </div>
      <Row gutter={[16, 16]} style={{ width: "400px" }}>
        <>
        {!valueOfSelectedTitle && designs.map(element => {
            return (<Col span={12} key={element}>
              <label style={{cursor: "pointer"}}>
                <Field
                  name={title+"."+element}
                  component="input"
                  type="checkbox"
                  initialValue={true}
                >
                  { ({input}) =>  <Checkbox {...input} style={{ float: "right" }}  />}
                </Field>{'  '}{_.startCase(element)}
              </label>
            </Col>);
          }
        )}
          {valueOfSelectedTitle && Object.keys(valueOfSelectedTitle).map(key => {
              return ( valueOfSelectedTitle[key] && <Col span={12} key={key}>
                <label style={{cursor: "pointer"}}>
                  <Field
                    name={title+"."+key}
                    component="input"
                    type="checkbox"
                    initialValue={true}
                  >
                    { ({input}) =>  <Checkbox {...input} style={{ float: "right" }}  />}
                  </Field>{'  '}{makeDesignsStringLookPretty(key)}
                </label>
              </Col>);
            }
          )}
        </>
      </Row>
    </div>
  );
};

export default PopoverContent;
