import React from "react";
import { ExclamationCircleOutlined } from "@ant-design/icons";
import { arrayOfMBAFields, designs } from "../../../utils/constants/constants";
import { Checkbox, Col, Row } from "antd";
import _ from "lodash";
import { Field, useField } from "react-final-form";

const PopoverContentMBA = ({title}) => {
  return (
    <div onClick={e => (e.stopPropagation())}>
      <div>
        <ExclamationCircleOutlined style={{ color: "#7f67ff" }} /> Entries Effect
      </div>
      <Row gutter={[16, 16]} style={{ width: "400px" }}>
          {arrayOfMBAFields.map(element => {
              return (<Col span={12} key={element} >
                <label style={{cursor: "pointer"}} >
                  <Field
                    name={title+"."+element}
                    component="input"
                    type="checkbox"
                    initialValue={true}
                  >
                    { ({input}) =>  <Checkbox {...input} style={{ float: "right" }}/>}
                  </Field>{'  '}{_.startCase(element)}
                </label>
              </Col>);
            }
          )}
      </Row>
    </div>
  );
};

export default PopoverContentMBA;
