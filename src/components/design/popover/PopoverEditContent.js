import React from "react";
import { ExclamationCircleOutlined } from "@ant-design/icons";
import { designs } from "../../../utils/constants/constants";
import { Checkbox, Col, Row } from "antd";
import _ from "lodash";
import { Field, useField } from "react-final-form";
import { makeDesignsStringLookPretty } from "../../../utils/designHelperFunctions";

const PopoverEditContent = ({title}) => {
  return (
    <>
      <p>
        <ExclamationCircleOutlined style={{ color: "#7f67ff" }} /> Entries Effect
      </p>
      <Row gutter={[16, 16]} style={{ width: "400px" }}>
        {designs.map(element => {
            return (<Col span={12} key={element}>
              <label style={{cursor: "pointer"}}>
                <Field
                  name={title+"."+element}
                  component="input"
                  type="checkbox"
                  defaultValue={true}
                >
                  { ({input}) =>  <Checkbox {...input} style={{ float: "right" }}  />}
                </Field>{'  '}{makeDesignsStringLookPretty(element)}
              </label>
            </Col>);
          }
        )}
      </Row>
    </>
  );
};

export default PopoverEditContent;
