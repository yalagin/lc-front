import React from 'react';
import { Col, Divider, Input, Space, Tag, Typography } from 'antd';
import _ from 'lodash';
import { Field } from 'react-final-form';
import { TextField } from 'react-final-form-antd';
import { OnChange } from 'react-final-form-listeners';
import Tags from '../../input/Tags';
import { fieldDisabledText } from '../../input/fieldDisabledText';
import StyledBadge from '../../input/StyledBadge';
import { designSociety6 } from '../../../utils/constants/constants';
import { ExclamationCircleOutlined } from '@ant-design/icons';
import MyTextField from '../../input/MyFields/MyTextField';
import TranslationSetting from '../TMSetting';

const { TextArea } = Input;
const { Title } = Typography;
const DesignSociety6 = ({
                          handleSubmit,
                          form,
                          currentTab,
                          onChangeField,
                          submitting,
                          pristine,
                        }) => {
  return (
    <Col xs={24} sm={24} md={24} lg={24} xl={12}>
      <Divider> {_.startCase(currentTab)}</Divider>
      <div>
        <div>
          <Field
            name={designSociety6 + "." + "title100disabled"}
            subscription={{ value: true }}
          >
            {({ input, meta }) => (
              <>
                <Field
                  name={designSociety6 + "." + `title100`}
                  disabled={input.value}
                  maxLength={50}
                  subscription={{ value: true }}
                  allowNull={true}
                >
                  {(props) => (
                    <>
                      <OnChange name={designSociety6 + "." + "title100"}>
                        {(value) => {
                          if (!input.value) {
                            onChangeField(props.input.name, value);
                          }
                        }}
                      </OnChange>
                      <Title level={5}>
                        <Space align="baseline">
                          Title
                          <StyledBadge
                            count={100}
                            overflowCount={999}
                            selected={true}
                            touched={props.input.value}
                          />
                        </Space>
                      </Title>
                      <TextArea
                        maxLength={100}
                        showCount
                        rows={1}
                        autoSize
                        size="large"
                        disabled={props.disabled}
                        value={props.input.value}
                        onChange={props.input.onChange}
                      />
                    </>
                  )}
                </Field>
                <br />
                <br />
                {input.value && fieldDisabledText}
              </>
            )}
          </Field>
        </div>
        <div>
          <Field
            name={designSociety6 + "." + "description250disabled"}
            subscription={{ value: true }}
          >
            {({ input }) => (
              <>
                <Field
                  name={designSociety6 + "." + `description250`}
                  component={TextField}
                  disabled={input.value}
                  subscription={{ value: true }}
                  allowNull={true}
                >
                  {(props) => (
                    <>
                      <OnChange name={designSociety6 + "." + "description250"}>
                        {(value) => {
                          if (!input.value) {
                            onChangeField(props.input.name, value);
                          }
                        }}
                      </OnChange>
                      <Title level={5}>
                        <Space align="baseline">
                          Description
                          <StyledBadge
                            count={250}
                            overflowCount={999}
                            selected={true}
                            touched={props.input.value}
                          />
                        </Space>
                      </Title>
                      <TextArea
                        size="large"
                        disabled={props.disabled}
                        value={props.input.value}
                        maxLength={250}
                        showCount
                        rows={6}
                        onChange={props.input.onChange}
                      />
                    </>
                  )}
                </Field>
                {input.value && fieldDisabledText}
              </>
            )}
          </Field>
        </div>
        <div>
          <br />
          <br />
          <Title level={5}>
            <Space align="baseline">
              Tags
              <Tag color="rgba(255,168,249,0.76)">20</Tag>
            </Space>
          </Title>
          <Field
            name={designSociety6 + "." + "tagsdisabled"}
            subscription={{ value: true }}
          >
            {({ input, meta }) => (
              <>
                <Field
                  name={designSociety6 + "." + `tags`}
                  // component={Tags}
                  disabled={input.value}
                  subscription={{ value: true }}
                  allowNull={true}
                >
                  {(props) => (
                    <>
                      <OnChange name={designSociety6 + "." + "tags"}>
                        {(value) => {
                          if (!input.value) {
                            onChangeField(props.input.name, value);
                          }
                        }}
                      </OnChange>
                      <Tags
                        disabled={input.value}
                        input={props.input}
                        color={"rgba(255,168,249,0.76)"}
                        maxLength={20}
                      />
                    </>
                  )}
                </Field>
                <br />
                <br />
                {input.value && fieldDisabledText}
              </>
            )}
          </Field>
        </div>
        <div>
          <br />

          <Title level={5}><ExclamationCircleOutlined style={{ color: '#7f67ff' }} /> Colors</Title>
          <Field name={designSociety6 + '.' + 'society6Colorsdisabled'} subscription={{ value: true }}>
            {({ input }) => (
              <>
                <Field
                  subscription={{ value: true }}
                  size={'large'}
                  name={designSociety6 + '.' + "society6Colors"}
                  hasFeedback
                  allowNull={true}
                  parse={x => x}
                >
                  {props => (
                    <>
                      <OnChange name={props.input.name}>
                        {value => {
                          if (!input.value) {
                            onChangeField(props.input.name, value);
                          }
                        }}
                      </OnChange>
                      <MyTextField {...props} />
                    </>
                  )}
                </Field>
                {input.value && fieldDisabledText}
              </>
            )}
          </Field>
          <div>
            <Field name={designSociety6 + '.' + 'society6Iddisabled'} subscription={{ value: true }}>
              {({ input, meta }) => (
                <>
                  <Field
                    name={designSociety6 + '.' + `society6Id`}
                    component={TextField}
                    disabled={input.value}
                    maxLength={26}
                    subscription={{ value: true }}
                    allowNull={true}
                  >
                    {props => (
                      <>
                        <OnChange name={designSociety6 + '.' + 'society6Id'}>
                          {value => {
                            if (!input.value) {
                              // onChangeTranslatedField(props.input.name, value);
                              onChangeField(props.input.name, value);
                            }
                          }}
                        </OnChange>
                        <Title level={5}>
                          <Space align="baseline">
                            ID
                            {/*<StyledBadge count={50} selected={true} touched={props.input.value} />*/}
                          </Space>
                          <TranslationSetting style={{ align: 'right' }} selectedField={props.input.name} />
                        </Title>
                        <TextArea
                          maxLength={255}
                          showCount
                          rows={1}
                          autoSize
                          size="large"
                          disabled={props.disabled}
                          value={props.input.value}
                          placeholder={'Id for society6 comes here'}
                          onChange={props.input.onChange}
                        />
                      </>
                    )}
                  </Field>
                  <br />
                  <br />
                  {input.value && fieldDisabledText}
                </>
              )}
            </Field>
          </div>
          <br />
        </div>
      </div>
      {/*<Divider />*/}
      {/*<SubmitAndTransferButtons*/}
      {/*  form={form}*/}
      {/*  handleSubmit={handleSubmit}*/}
      {/*  pristine={pristine}*/}
      {/*  submitting={submitting}*/}
      {/*/>*/}
    </Col>
  );
};

export default DesignSociety6;
