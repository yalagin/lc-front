import React from 'react';
import { Col, Divider, Input, Radio, Space, Tag, Typography } from 'antd';
import _ from 'lodash';
import { Field } from 'react-final-form';
import { TextField } from 'react-final-form-antd';
import { OnChange } from 'react-final-form-listeners';
import Tags from '../../input/Tags';
import { fieldDisabledText } from '../../input/fieldDisabledText';
import StyledBadge from '../../input/StyledBadge';
import { designZazzle } from '../../../utils/constants/constants';
import { ExclamationCircleOutlined } from '@ant-design/icons';
import MyTextField from '../../input/MyFields/MyTextField';
import MyRadioGroup from '../../input/MyFields/MyRadioGroup';

const { TextArea } = Input;
const { Title } = Typography;

const DesignZazzle = ({ handleSubmit, form, currentTab, onChangeField, submitting, pristine }) => {
  return (
    <Col xs={24} sm={24} md={24} lg={24} xl={12}>
      <Divider> {_.startCase(currentTab)}</Divider>
      <div>
        <div>
          <Field name={designZazzle + '.' + 'title50disabled'} subscription={{ value: true }}>
            {({ input, meta }) => (
              <>
                <Field
                  name={designZazzle + '.' + `title50`}
                  disabled={input.value}
                  maxLength={50}
                  subscription={{ value: true }}
                  allowNull={true}
                >
                  {props => (
                    <>
                      <OnChange name={designZazzle + '.' + 'title50'}>
                        {value => {
                          if (!input.value) {
                            onChangeField(props.input.name, value);
                          }
                        }}
                      </OnChange>
                      <Title level={5}>
                        <Space align="baseline">
                          Title
                          <StyledBadge count={50} selected={true} touched={props.input.value} />
                        </Space>
                      </Title>
                      <TextArea
                        maxLength={50}
                        showCount
                        rows={1}
                        autoSize
                        size="large"
                        disabled={props.disabled}
                        value={props.input.value}
                        onChange={props.input.onChange}
                      />
                    </>
                  )}
                </Field>
                <br />
                <br />
                {input.value && fieldDisabledText}
              </>
            )}
          </Field>
        </div>
        <div>
          <Field name={designZazzle + '.' + 'description2000disabled'} subscription={{ value: true }}>
            {({ input }) => (
              <>
                <Field
                  name={designZazzle + '.' + `description2000`}
                  component={TextField}
                  disabled={input.value}
                  maxLength={200}
                  subscription={{ value: true }}
                  allowNull={true}
                >
                  {props => (
                    <>
                      <OnChange name={designZazzle + '.' + 'description2000'}>
                        {value => {
                          if (!input.value) {
                            onChangeField(props.input.name, value);
                          }
                        }}
                      </OnChange>
                      <Title level={5}>
                        <Space align="baseline">
                          Description
                          <StyledBadge
                            count={2000}
                            overflowCount={2099}
                            selected={true}
                            touched={props.input.value}
                          />
                        </Space>
                      </Title>
                      <TextArea
                        size="large"
                        disabled={props.disabled}
                        value={props.input.value}
                        maxLength={2000}
                        showCount
                        rows={6}
                        onChange={props.input.onChange}
                      />
                    </>
                  )}
                </Field>
                {input.value && fieldDisabledText}
              </>
            )}
          </Field>
        </div>
        <div>
          <br />
          <br />
          <Title level={5}>
            <Space align="baseline">
              Tags
              <Tag color="#FF9F9F">10</Tag>
            </Space>
          </Title>
          <Field name={designZazzle + '.' + 'tagsdisabled'} subscription={{ value: true }}>
            {({ input, meta }) => (
              <>
                <Field
                  name={designZazzle + '.' + `tags`}
                  // component={Tags}
                  disabled={input.value}
                  subscription={{ value: true }}
                  allowNull={true}
                >
                  {props => (
                    <>
                      <OnChange name={designZazzle + '.' + 'tags'}>
                        {value => {
                          if (!input.value) {
                            onChangeField(props.input.name, value);
                          }
                        }}
                      </OnChange>
                      <Tags
                        disabled={input.value}
                        input={props.input}
                        color={'#FF9F9F'}
                        maxLength={10}
                      />
                    </>
                  )}
                </Field>
                <br />
                <br />
                {input.value && fieldDisabledText}
              </>
            )}
          </Field>
        </div>
        <div>
          <label> <Title level={5}>Events & Occasions</Title></label>
          <Field name={designZazzle + '.' + 'occasiondisabled'} subscription={{ value: true }}>
            {({ input }) => (
              <>
                <Field
                  subscription={{ value: true }}
                  size={'large'}
                  name={designZazzle + '.' + "occasion"}
                  hasFeedback
                  allowNull={true}
                  parse={x => x}
                  placeholder={"(Optional)"}
                >
                  {props => (
                    <>
                      <OnChange name={props.input.name}>
                        {value => {
                          if (!input.value) {
                            onChangeField(props.input.name, value);
                          }
                        }}
                      </OnChange>
                      <MyTextField {...props} />
                    </>
                  )}
                </Field>
                {input.value && fieldDisabledText}
              </>
            )}
          </Field>
        </div>
        <div>
          <label> <Title level={5}>Recipient</Title> </label>
          <Field name={designZazzle + '.' + 'recipientdisabled'} subscription={{ value: true }}>
            {({ input }) => (
              <>
                <Field
                  subscription={{ value: true }}
                  size={'large'}
                  name={designZazzle + '.' + "recipient"}
                  hasFeedback
                  allowNull={true}
                  placeholder={"(Optional)"}
                  parse={x => x}
                >
                  {props => (
                    <>
                      <OnChange name={props.input.name}>
                        {value => {
                          if (!input.value) {
                            onChangeField(props.input.name, value);
                          }
                        }}
                      </OnChange>
                      <MyTextField {...props} />
                    </>
                  )}
                </Field>
                {input.value && fieldDisabledText}
              </>
            )}
          </Field>
        </div>
        <div>
          <label><Title level={5}>Store Category</Title></label>
          <Field name={designZazzle + '.' + 'categorydisabled'} subscription={{ value: true }}>
            {({ input }) => (
              <>
                <Field
                  subscription={{ value: true }}
                  size={'large'}
                  name={designZazzle + '.' + "category"}
                  hasFeedback
                  allowNull={true}
                  parse={x => x}
                  placeholder={"(Required)"}
                >
                  {props => (
                    <>
                      <OnChange name={props.input.name}>
                        {value => {
                          if (!input.value) {
                            onChangeField(props.input.name, value);
                          }
                        }}
                      </OnChange>
                      <MyTextField {...props} />
                    </>
                  )}
                </Field>
                {input.value && fieldDisabledText}
              </>
            )}
          </Field>
        </div>
        <div>
          <label><Title level={5}>Suitable Audience</Title></label>
          <Field name={designZazzle + '.' + 'audiencedisabled'} subscription={{ value: true }}>
            {({ input }) => (
              <>
                <Field
                  subscription={{ value: true }}
                  size={'large'}
                  name={designZazzle + '.' + "audience"}
                  hasFeedback
                  allowNull={true}
                  parse={x => x}
                  required
                >
                  {props => (
                    <>
                      <OnChange name={props.input.name}>
                        {value => {
                          if (!input.value) {
                            onChangeField(props.input.name, value);
                          }
                        }}
                      </OnChange>
                      <MyRadioGroup {...props}>
                        <Radio value={'G'}>G</Radio>
                        <Radio value={'PG-13'}>PG-13</Radio>
                        <Radio value={'R'}>R</Radio>
                      </MyRadioGroup>
                    </>
                  )}
                </Field>
                {input.value && fieldDisabledText}
              </>
            )}
          </Field>
        </div>
        <Title level={5}><ExclamationCircleOutlined style={{ color: '#7f67ff' }} /> Design Template </Title>
        <Field name={designZazzle + '.' + 'zazzleSelectDesignTemplatedisabled'} subscription={{ value: true }}>
          {({ input }) => (
            <>
              <Field
                subscription={{ value: true }}
                name={designZazzle + '.' + "zazzleSelectDesignTemplate"}
                hasFeedback
                placeholder={'select templates'}
                size={'large'}
              >
                {props => (
                  <>
                    <OnChange name={props.input.name}>
                      {value => {
                        if (!input.value) {
                          onChangeField(props.input.name, value);
                        }
                      }}
                    </OnChange>
                    <MyTextField {...props} />
                  </>
                )}
              </Field>
              {input.value && fieldDisabledText}
            </>
          )}
        </Field>
        <Title level={5}><ExclamationCircleOutlined style={{ color: '#7f67ff' }} /> Store </Title>
        <Field name={designZazzle + '.' + 'zazzleStoredisabled'} subscription={{ value: true }}>
          {({ input }) => (
            <>
              <Field
                subscription={{ value: true }}
                size={'large'}
                name={designZazzle + '.' + "zazzleStore"}
                hasFeedback
                allowNull={true}
                parse={x => x}
              >
                {props => (
                  <>
                    <OnChange name={props.input.name}>
                      {value => {
                        if (!input.value) {
                          onChangeField(props.input.name, value);
                        }
                      }}
                    </OnChange>
                    <MyTextField {...props} />
                  </>
                )}
              </Field>
              {input.value && fieldDisabledText}
            </>
          )}
        </Field>
      </div>
      {/*<Divider />*/}
      {/*<SubmitAndTransferButtons*/}
      {/*  form={form}*/}
      {/*  handleSubmit={handleSubmit}*/}
      {/*  pristine={pristine}*/}
      {/*  submitting={submitting}*/}
      {/*/>*/}
    </Col>
  );
};
export default DesignZazzle;
