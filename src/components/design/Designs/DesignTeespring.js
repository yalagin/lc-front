import React from 'react';
import { Col, Divider, Input, Space, Typography } from 'antd';
import _ from 'lodash';
import { Field } from 'react-final-form';
import { TextField } from 'react-final-form-antd';
import { OnChange } from 'react-final-form-listeners';
import { fieldDisabledText } from '../../input/fieldDisabledText';
import StyledBadge from '../../input/StyledBadge';
import { designTeespring } from '../../../utils/constants/constants';
import { ExclamationCircleOutlined } from '@ant-design/icons';
import MyTextField from '../../input/MyFields/MyTextField';

const { TextArea } = Input;
const { Title } = Typography;

const DesignTeespring = ({ handleSubmit, form, currentTab, onChangeField, submitting, pristine }) => {
  return (
    <Col xs={24} sm={24} md={24} lg={24} xl={12}>
      <Divider> {_.startCase(currentTab)}</Divider>
      <div>
        <div>
          <Field name={designTeespring + '.' + 'title40disabled'} subscription={{ value: true }}>
            {({ input, meta }) => (
              <>
                <Field
                  name={designTeespring + '.' + `title40`}
                  disabled={input.value}
                  maxLength={50}
                  subscription={{ value: true }}
                  allowNull={true}
                >
                  {props => (
                    <>
                      <OnChange name={designTeespring + '.' + 'title40'}>
                        {value => {
                          if (!input.value) {
                            onChangeField(props.input.name, value);
                          }
                        }}
                      </OnChange>
                      <Title level={5}>
                        <Space align="baseline">
                          Title
                          <StyledBadge count={40} selected={true} touched={props.input.value} />
                        </Space>
                      </Title>
                      <TextArea
                        maxLength={40}
                        showCount
                        rows={1}
                        size="large"
                        autoSize
                        disabled={props.disabled}
                        value={props.input.value}
                        onChange={props.input.onChange}
                      />
                    </>
                  )}
                </Field>
                <br />
                <br />
                {input.value && fieldDisabledText}
              </>
            )}
          </Field>
        </div>
        <div>
          <Field
            name={designTeespring + '.' + 'description200disabled'}
            subscription={{ value: true }}
          >
            {({ input }) => (
              <>
                <Field
                  name={designTeespring + '.' + `description200`}
                  component={TextField}
                  disabled={input.value}
                  maxLength={200}
                  subscription={{ value: true }}
                  allowNull={true}
                >
                  {props => (
                    <>
                      <OnChange name={props.input.name}>
                        {value => {
                          if (!input.value) {
                            onChangeField(props.input.name, value);
                          }
                        }}
                      </OnChange>
                      <Title level={5}>
                        <Space align="baseline">
                          Description
                          <StyledBadge
                            count={200}
                            overflowCount={999}
                            selected={true}
                            touched={props.input.value}
                          />
                        </Space>
                      </Title>
                      <TextArea
                        size="large"
                        disabled={props.disabled}
                        value={props.input.value}
                        maxLength={200}
                        showCount
                        rows={6}
                        onChange={props.input.onChange}
                      />
                    </>
                  )}
                </Field>
                <br />
                <br />
                {input.value && fieldDisabledText}
              </>
            )}
          </Field>
        </div>

        <div>
          <Field
            name={designTeespring + '.' + 'teespringShopdisabled'}
            subscription={{ value: true }}
          >
            {({ input }) => (
              <>
                <Field
                  name={designTeespring + '.' + `teespringShop`}
                  component={TextField}
                  disabled={input.value}
                  subscription={{ value: true }}
                  allowNull={true}
                >
                  {props => (
                    <>
                      <OnChange name={props.input.name}>
                        {value => {
                          if (!input.value) {
                            onChangeField(props.input.name, value);
                          }
                        }}
                      </OnChange>
                      <Title level={5}>
                        <Space align="baseline">
                          <ExclamationCircleOutlined style={{ color: '#7f67ff' }} /> Shop
                        </Space>
                      </Title>
                      <TextArea
                        size="large"
                        disabled={props.disabled}
                        value={props.input.value}
                        // maxLength={200}
                        showCount
                        rows={1}
                        onChange={props.input.onChange}
                      />
                    </>
                  )}
                </Field>
                <br />
                <br />
                {input.value && fieldDisabledText}
              </>
            )}
          </Field>
        </div>

        <div>
          <Space align="baseline"><ExclamationCircleOutlined style={{ color: '#7f67ff' }} /><Title level={5}> Duplication URL</Title></Space>
          <Field name={designTeespring + '.' + 'teespringDuplicationURLdisabled'} subscription={{ value: true }}>
            {({ input }) => (
              <>
                <Field
                  subscription={{ value: true }}
                  size={'large'}
                  name={designTeespring + '.' + "teespringDuplicationURL"}
                  hasFeedback
                  allowNull={true}
                  parse={x => x}
                >
                  {props => (
                    <>
                      <OnChange name={props.input.name}>
                        {value => {
                          if (!input.value) {
                            onChangeField(props.input.name, value);
                          }
                        }}
                      </OnChange>
                      <MyTextField {...props} />
                    </>
                  )}
                </Field>
                {input.value && fieldDisabledText}
              </>
            )}
          </Field>
        </div>
      </div>
      {/*<Divider />*/}
      {/*<SubmitAndTransferButtons*/}
      {/*  form={form}*/}
      {/*  handleSubmit={handleSubmit}*/}
      {/*  pristine={pristine}*/}
      {/*  submitting={submitting}*/}
      {/*/>*/}
    </Col>
  );
};


export default DesignTeespring;
