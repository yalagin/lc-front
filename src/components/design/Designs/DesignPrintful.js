import React from 'react';
import { Col, Divider, Input, Space, Tag, Typography } from 'antd';
import _ from 'lodash';
import { Field } from 'react-final-form';
import { TextField } from 'react-final-form-antd';
import { OnChange } from 'react-final-form-listeners';
import Tags from '../../input/Tags';
import { fieldDisabledText } from '../../input/fieldDisabledText';
import StyledBadge from '../../input/StyledBadge';
import { designPrintful } from '../../../utils/constants/constants';
import { ExclamationCircleOutlined } from '@ant-design/icons';
import MyTextField from '../../input/MyFields/MyTextField';

const { TextArea } = Input;
const { Title } = Typography;

const DesignPrintful = ({ handleSubmit, form, currentTab, onChangeField, submitting, pristine }) => {
  const onChangeTitle50Field = _.debounce(
    value => form.mutators.onChangeMainField('title50', value),
    540
  );
  return (
    <Col xs={24} sm={24} md={24} lg={24} xl={12}>
      <Divider> {_.startCase(currentTab)}</Divider>
      <div>
        <div>
          <Field name={designPrintful + '.' + 'title50disabled'} subscription={{ value: true }}>
            {({ input, meta }) => (
              <>
                <Field
                  name={designPrintful + '.' + `title50`}
                  disabled={input.value}
                  maxLength={50}
                  subscription={{ value: true }}
                  allowNull={true}
                >
                  {props => (
                    <>
                      <OnChange name={designPrintful + '.' + 'title50'}>
                        {value => {
                          if (!input.value) {
                            onChangeField(props.input.name, value);
                          }
                        }}
                      </OnChange>
                      <Title level={5}>
                        <Space align="baseline">
                          Title
                          <StyledBadge count={50} selected={true} touched={props.input.value} />
                        </Space>
                      </Title>
                      <TextArea
                        maxLength={50}
                        showCount
                        autoSize
                        rows={1}
                        size="large"
                        disabled={props.disabled}
                        value={props.input.value}
                        onChange={props.input.onChange}
                      />
                    </>
                  )}
                </Field>
                <br />
                <br />
                {input.value && fieldDisabledText}
              </>
            )}
          </Field>
        </div>
        <div>
          <Field
            name={designPrintful + '.' + 'description200disabled'}
            subscription={{ value: true }}
          >
            {({ input }) => (
              <>
                <Field
                  name={designPrintful + '.' + `description200`}
                  component={TextField}
                  disabled={input.value}
                  maxLength={200}
                  subscription={{ value: true }}
                  allowNull={true}
                >
                  {props => (
                    <>
                      <OnChange name={designPrintful + '.' + 'description200'}>
                        {value => {
                          if (!input.value) {
                            onChangeField(props.input.name, value);
                          }
                        }}
                      </OnChange>
                      <Title level={5}>
                        <Space align="baseline">
                          Description
                          <StyledBadge
                            count={200}
                            overflowCount={999}
                            selected={true}
                            touched={props.input.value}
                          />
                        </Space>
                      </Title>
                      <TextArea
                        size="large"
                        disabled={props.disabled}
                        value={props.input.value}
                        maxLength={200}
                        showCount
                        rows={6}
                        onChange={props.input.onChange}
                      />
                    </>
                  )}
                </Field>
                {input.value && fieldDisabledText}
              </>
            )}
          </Field>
        </div>
        <div>
          <br />
          <br />
          <Title level={5}>
            <Space align="baseline">
              Tags
              <Tag color="#91d5ff">25</Tag>
            </Space>
          </Title>
          <Field name={designPrintful + '.' + 'tagsdisabled'} subscription={{ value: true }}>
            {({ input, meta }) => (
              <>
                <Field
                  name={designPrintful + '.' + `tags`}
                  // component={Tags}
                  disabled={input.value}
                  subscription={{ value: true }}
                  allowNull={true}
                >
                  {props => (
                    <>
                      <OnChange name={designPrintful + '.' + 'tags'}>
                        {value => {
                          if (!input.value) {
                            onChangeField(props.input.name, value);
                          }
                        }}
                      </OnChange>
                      <Tags
                        disabled={input.value}
                        input={props.input}
                        color={'#91d5ff'}
                        maxLength={25}
                      />
                    </>
                  )}
                </Field>
                <br />
                <br />
                {input.value && fieldDisabledText}
              </>
            )}
          </Field>
        </div>
        <div>
          <label> <Title level={5}>Shop name</Title></label>
          <Field name={designPrintful + '.' + 'shopNamedisabled'} subscription={{ value: true }}>
            {({ input }) => (
              <>
                <Field
                  subscription={{ value: true }}
                  size={'large'}
                  name={designPrintful + '.' + "shopName"}
                  hasFeedback
                  allowNull={true}
                  parse={x => x}
                >
                  {props => (
                    <>
                      <OnChange name={props.input.name}>
                        {value => {
                          if (!input.value) {
                            onChangeField(props.input.name, value);
                          }
                        }}
                      </OnChange>
                      <MyTextField {...props} />
                    </>
                  )}
                </Field>
                {input.value && fieldDisabledText}
              </>
            )}
          </Field>
        </div>
        <div>
          <Title level={5}><ExclamationCircleOutlined style={{ color: '#7f67ff' }} /> Colors </Title>
          <Field name={designPrintful + '.' + 'printfulColorsdisabled'} subscription={{ value: true }}>
            {({ input }) => (
              <>
                <Field
                  subscription={{ value: true }}
                  size={'large'}
                  name={designPrintful + '.' + "printfulColors"}
                  hasFeedback
                  allowNull={true}
                  parse={x => x}
                >
                  {props => (
                    <>
                      <OnChange name={props.input.name}>
                        {value => {
                          if (!input.value) {
                            onChangeField(props.input.name, value);
                          }
                        }}
                      </OnChange>
                      <MyTextField {...props} />
                    </>
                  )}
                </Field>
                {input.value && fieldDisabledText}
              </>
            )}
          </Field>
          <br />
          <Title level={5}><ExclamationCircleOutlined style={{ color: '#7f67ff' }} /> Product Name</Title>
          <Field name={designPrintful + '.' + 'printfulProductNamedisabled'} subscription={{ value: true }}>
            {({ input }) => (
              <>
                <Field
                  subscription={{ value: true }}
                  size={'large'}
                  name={designPrintful + '.' + "printfulProductName"}
                  hasFeedback
                  allowNull={true}
                  parse={x => x}
                >
                  {props => (
                    <>
                      <OnChange name={props.input.name}>
                        {value => {
                          if (!input.value) {
                            onChangeField(props.input.name, value);
                          }
                        }}
                      </OnChange>
                      <MyTextField {...props} />
                    </>
                  )}
                </Field>
                {input.value && fieldDisabledText}
              </>
            )}
          </Field>
          <br />
          <Title level={5}><ExclamationCircleOutlined style={{ color: '#7f67ff' }} /> Store Name</Title>
          <Field name={designPrintful + '.' + 'printfulStoreNamedisabled'} subscription={{ value: true }}>
            {({ input }) => (
              <>
                <Field
                  subscription={{ value: true }}
                  size={'large'}
                  name={designPrintful + '.' + "printfulStoreName"}
                  hasFeedback
                  allowNull={true}
                  parse={x => x}
                >
                  {props => (
                    <>
                      <OnChange name={props.input.name}>
                        {value => {
                          if (!input.value) {
                            onChangeField(props.input.name, value);
                          }
                        }}
                      </OnChange>
                      <MyTextField {...props} />
                    </>
                  )}
                </Field>
                {input.value && fieldDisabledText}
              </>
            )}
          </Field>
        </div>
      </div>
      {/*<Divider />*/}
      {/*<SubmitAndTransferButtons*/}
      {/*  form={form}*/}
      {/*  handleSubmit={handleSubmit}*/}
      {/*  pristine={pristine}*/}
      {/*  submitting={submitting}*/}
      {/*/>*/}
    </Col>
  );
};
export default DesignPrintful;
