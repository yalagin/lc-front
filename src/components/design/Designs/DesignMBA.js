import React from 'react';
import { Col, Divider, Input, Row, Space, Spin, Typography } from 'antd';
import _ from 'lodash';
import { Field, useField } from 'react-final-form';
import { SwitchField, TextField } from 'react-final-form-antd';
import { OnChange } from 'react-final-form-listeners';
import { fieldDisabledText } from '../../input/fieldDisabledText';
import StyledBadge from '../../input/StyledBadge';
import MySwitchField from '../../input/MySwitchField';
import { designMBA } from '../../../utils/constants/constants';
import TranslationSetting from '../TMSetting';
import MbaPanel from '../../template/mba/MbaPanel';
import { useQuery } from 'react-query';
import Nprogress from '../../styles/nprogress/Nprogress';
import { mbaProducts } from '../../../utils/urls';

const { TextArea } = Input;
const { Title } = Typography;

const DesignMBA = ({
                     form,
                     currentTab,
                     onChangeField,
                     onChangeTranslatedField,
                   }) => {
  const { isLoading, data: products } = useQuery('mba_products', mbaProducts, { staleTime: 60 * 60 * 1000 });

  const { input: { value: mbaProductCheckboxdisabled } } = useField(designMBA + "." + "mbaProductCheckboxdisabled", { subscription: { value: true } });
  const { input: { value: mbaColorsdisabled } } = useField(designMBA + "." + "mbaColorsdisabled", { subscription: { value: true } });
  const { input: { value: mbaScaledisabled } } = useField(designMBA + "." + "mbaScaledisabled", { subscription: { value: true } });
  const { input: { value: mbaProductPricedisabled } } = useField(designMBA + "." + "mbaProductPricedisabled", { subscription: { value: true } });

  const { input: { value: fitYouthdisabled }, } = useField(designMBA + "." + 'fitYouthdisabled', { subscription: { value: true } });
  const { input: { value: haveIsAdult }, } = useField('haveIsAdult', { subscription: { value: true } });

  return (
    <>
      <Col xs={24} sm={24} md={24} lg={24} xl={12}>
        <Nprogress isLoading={isLoading} />
        <Divider> {_.startCase(currentTab)}</Divider>
        <div>
          <div>
            <Field name={designMBA + '.' + 'title60disabled'} subscription={{ value: true }}>
              {({ input, meta }) => (
                <>
                  <Field
                    name={designMBA + '.' + `title60`}
                    disabled={input.value}
                    subscription={{ value: true }}
                    allowNull={true}
                  >
                    {props => (
                      <>
                        <OnChange name={designMBA + '.' + 'title60'}>
                          {value => {
                            if (!input.value) {
                              onChangeTranslatedField(props.input.name, value);
                              onChangeField(props.input.name, value);
                            }
                          }}
                        </OnChange>
                        <Title level={5}>
                          <Space align="baseline">
                            Title
                            <StyledBadge count={60} selected={true} touched={props.input.value} />
                          </Space>
                          <TranslationSetting style={{ align: 'right' }} selectedField={props.input.name} />
                        </Title>
                        <TextArea
                          maxLength={60}
                          showCount
                          autoSize
                          rows={1}
                          size="large"
                          disabled={props.disabled}
                          value={props.input.value}
                          onChange={props.input.onChange}
                        />
                      </>
                    )}
                  </Field>
                  <br />
                  <br />
                  {input.value && fieldDisabledText}
                </>
              )}
            </Field>
          </div>
          <div>
            <Field name={designMBA + '.' + 'description2000disabled'} subscription={{ value: true }}>
              {({ input }) => (
                <>
                  <Field
                    name={designMBA + '.' + `description2000`}
                    component={TextField}
                    disabled={input.value}
                    subscription={{ value: true }}
                    allowNull={true}
                  >
                    {props => (
                      <>
                        <OnChange name={designMBA + '.' + 'description2000'}>
                          {value => {
                            if (!input.value) {
                              onChangeTranslatedField(props.input.name, value);
                              onChangeField(props.input.name, value);
                            }
                          }}
                        </OnChange>
                        <Title level={5}>
                          <Space align="baseline">
                            Description
                            <StyledBadge
                              count={2000}
                              overflowCount={2999}
                              selected={true}
                              touched={props.input.value}
                            />
                          </Space>
                          <TranslationSetting style={{ align: 'right' }} selectedField={props.input.name} />
                        </Title>
                        <TextArea
                          size="large"
                          disabled={props.disabled}
                          value={props.input.value}
                          maxLength={2000}
                          showCount
                          rows={6}
                          onChange={props.input.onChange}
                        />
                      </>
                    )}
                  </Field>
                  <br />
                  <br />
                  {input.value && fieldDisabledText}
                </>
              )}
            </Field>
          </div>
          <div>
            <Field name={designMBA + '.' + 'branddisabled'} subscription={{ value: true }}>
              {({ input, meta }) => (
                <>
                  <Field
                    name={designMBA + '.' + `brand`}
                    disabled={input.value}
                    maxLength={50}
                    subscription={{ value: true }}
                    allowNull={true}
                  >
                    {props => (
                      <>
                        <OnChange name={designMBA + '.' + 'brand'}>
                          {value => {
                            if (!input.value) {
                              onChangeTranslatedField(props.input.name, value);
                              onChangeField(props.input.name, value);
                            }
                          }}
                        </OnChange>
                        <Title level={5}>
                          <Space align="baseline">
                            Brand
                            <StyledBadge count={50} selected={true} touched={props.input.value} />
                          </Space>
                          <TranslationSetting style={{ align: 'right' }} selectedField={props.input.name} />
                        </Title>
                        <TextArea
                          maxLength={50}
                          showCount
                          rows={1}
                          autoSize
                          size="large"
                          disabled={props.disabled}
                          value={props.input.value}
                          onChange={props.input.onChange}
                        />
                      </>
                    )}
                  </Field>
                  <br />
                  <br />
                  {input.value && fieldDisabledText}
                </>
              )}
            </Field>
          </div>
          <div>
            <Field name={designMBA + '.' + 'bulletpointsdisabled'} subscription={{ value: true }}>
              {({ input, meta }) => (
                <>
                  <Field
                    name={designMBA + '.' + `bulletpoints`}
                    component={TextField}
                    disabled={input.value}
                    maxLength={26}
                    subscription={{ value: true }}
                    allowNull={true}
                  >
                    {props => (
                      <>
                        <OnChange name={designMBA + '.' + 'bulletpoints'}>
                          {value => {
                            if (!input.value) {
                              onChangeTranslatedField(props.input.name, value);
                              onChangeField(props.input.name, value);
                            }
                          }}
                        </OnChange>
                        <Title level={5}>
                          <Space align="baseline">
                            Bulletpoints
                            <StyledBadge
                              count={255}
                              overflowCount={999}
                              selected={true}
                              touched={props.input.value}
                            />
                          </Space>
                          <TranslationSetting style={{ align: 'right' }} selectedField={props.input.name} />
                        </Title>
                        <TextArea
                          maxLength={255}
                          showCount
                          rows={1}
                          autoSize
                          size="large"
                          disabled={props.disabled}
                          value={props.input.value}
                          placeholder={'Bulletpoint 1 goes here'}
                          onChange={props.input.onChange}
                        />
                      </>
                    )}
                  </Field>
                  {input.value && fieldDisabledText}
                </>
              )}
            </Field>
            <Field name={designMBA + '.' + 'bulletpoints2disabled'} subscription={{ value: true }}>
              {({ input, meta }) => (
                <>
                  <Field
                    name={designMBA + '.' + `bulletpoints2`}
                    component={TextField}
                    disabled={input.value}
                    maxLength={26}
                    subscription={{ value: true }}
                    allowNull={true}
                  >
                    {props => (
                      <>
                        <OnChange name={designMBA + '.' + 'bulletpoints2'}>
                          {value => {
                            if (!input.value) {
                              onChangeTranslatedField(props.input.name, value);
                              onChangeField(props.input.name, value);
                            }
                          }}
                        </OnChange>
                        <br />
                        <TranslationSetting style={{ align: 'right' }} selectedField={props.input.name} />
                        <TextArea
                          maxLength={255}
                          showCount
                          rows={1}
                          autoSize
                          size="large"
                          disabled={props.disabled}
                          value={props.input.value}
                          placeholder={'Bulletpoint 2 goes here'}
                          onChange={props.input.onChange}
                        />
                      </>
                    )}
                  </Field>
                  <br />
                  <br />
                  {input.value && fieldDisabledText}
                </>
              )}
            </Field>
          </div>
          <div>
            <Row>
              <Title level={5}>Fit types</Title>
            </Row>
            <Row gutter={[16, 16]} style={{ marginBottom: ' 56px' }}>
              <Col span={8}>
                <Field name={designMBA + '.' + 'fitMandisabled'} subscription={{ value: true }}>
                  {({ input, meta }) => (
                    <>
                      <Field
                        name={designMBA + '.' + `fitMan`}
                        component={SwitchField}
                        disabled={input.value}
                        maxLength={26}
                        subscription={{ value: true }}
                        allowNull={true}
                        type="checkbox"
                      >
                        {props => (
                          <>
                            <OnChange name={designMBA + '.' + 'fitMan'}>
                              {value => {
                                if (!input.value) {
                                  onChangeField(props.input.name, value);
                                }
                              }}
                            </OnChange>
                            <MySwitchField
                              label={'Man'}
                              disabled={props.disabled}
                              input={props.input}
                            />
                          </>
                        )}
                      </Field>
                      {input.value && fieldDisabledText}
                    </>
                  )}
                </Field>
              </Col>
              <Col span={8}>
                <Field
                  name={designMBA + '.' + `fitYouth`}
                  component={SwitchField}
                  disabled={fitYouthdisabled || haveIsAdult}
                  maxLength={26}
                  subscription={{ value: true }}
                  allowNull={true}
                  type="checkbox"
                >
                  {props => (
                    <>
                      <OnChange name={designMBA + '.' + 'fitYouth'}>
                        {value => {
                          if (!fitYouthdisabled || !haveIsAdult) {
                            onChangeField(props.input.name, value);
                          }
                        }}
                      </OnChange>
                      <MySwitchField
                        label={'Youth'}
                        disabled={props.disabled}
                        input={props.input}
                      />
                    </>
                  )}
                </Field>
                {fitYouthdisabled && fieldDisabledText}
              </Col>
              <Col span={8}>
                <Field name={designMBA + '.' + 'fitWomandisabled'} subscription={{ value: true }}>
                  {({ input, meta }) => (
                    <>
                      <Field
                        name={designMBA + '.' + `fitWoman`}
                        component={SwitchField}
                        disabled={input.value}
                        maxLength={26}
                        subscription={{ value: true }}
                        allowNull={true}
                        type="checkbox"
                      >
                        {props => (
                          <>
                            <OnChange name={designMBA + '.' + 'fitWoman'}>
                              {value => {
                                if (!input.value) {
                                  onChangeField(props.input.name, value);
                                }
                              }}
                            </OnChange>
                            <MySwitchField
                              label={'Woman'}
                              disabled={props.disabled}
                              input={props.input}
                            />
                          </>
                        )}
                      </Field>
                      {input.value && fieldDisabledText}
                    </>
                  )}
                </Field>
              </Col>
            </Row>
            <div style={{ marginTop: '-16px' }}>
              <Field name={designMBA + '.' + 'mbaIddisabled'} subscription={{ value: true }}>
                {({ input, meta }) => (
                  <>
                    <Field
                      name={designMBA + '.' + `mbaId`}
                      component={TextField}
                      disabled={input.value}
                      maxLength={26}
                      subscription={{ value: true }}
                      allowNull={true}
                    >
                      {props => (
                        <>
                          <OnChange name={designMBA + '.' + 'mbaId'}>
                            {value => {
                              if (!input.value) {
                                // onChangeTranslatedField(props.input.name, value);
                                onChangeField(props.input.name, value);
                              }
                            }}
                          </OnChange>
                          <Title level={5}>
                            <Space align="baseline">
                              ID
                              {/*<StyledBadge count={50} selected={true} touched={props.input.value} />*/}
                            </Space>
                            <TranslationSetting style={{ align: 'right' }} selectedField={props.input.name} />
                          </Title>
                          <TextArea
                            maxLength={255}
                            showCount
                            rows={1}
                            autoSize
                            size="large"
                            disabled={props.disabled}
                            value={props.input.value}
                            placeholder={'Id for MBA comes here'}
                            onChange={props.input.onChange}
                          />
                        </>
                      )}
                    </Field>
                    <br />
                    <br />
                    {input.value && fieldDisabledText}
                  </>
                )}
              </Field>
            </div>
          </div>
        </div>

        {/*<Divider />*/}
        {/*<SubmitAndTransferButtons*/}
        {/*  form={form}*/}
        {/*  handleSubmit={handleSubmit}*/}
        {/*  pristine={pristine}*/}
        {/*  submitting={submitting}*/}
        {/*/>*/}
        <Spin spinning={isLoading} />
      </Col>
      <Col span={24}>
        {(!mbaProductCheckboxdisabled && !mbaColorsdisabled && !mbaProductPricedisabled && !mbaScaledisabled) && products &&
        <MbaPanel data={products} form={form} prefix={designMBA + '.'} />}
        <OnChange name={designMBA + "." + "mbaProductPrice"}>
          {value => {
            if (!mbaProductCheckboxdisabled && !mbaColorsdisabled && !mbaProductPricedisabled && !mbaScaledisabled ) {
              onChangeField(designMBA + "." + "mbaProductPrice", value);
            }
          }}
        </OnChange>
        <OnChange name={designMBA + "." + "mbaColors"}>
          {value => {
            if (!mbaProductCheckboxdisabled && !mbaColorsdisabled && !mbaProductPricedisabled && !mbaScaledisabled) {
              onChangeField(designMBA + "." + "mbaColors", value);
            }
          }}
        </OnChange>
        <OnChange name={designMBA + "." + "mbaScale"}>
          {value => {
            if (!mbaProductCheckboxdisabled && !mbaColorsdisabled && !mbaProductPricedisabled && !mbaScaledisabled) {
              onChangeField(designMBA + "." + "mbaScale", value);
            }
          }}
        </OnChange>
        <OnChange name={designMBA + "." + "mbaProductCheckbox"}>
          {value => {
            if (!mbaProductCheckboxdisabled && !mbaColorsdisabled && !mbaProductPricedisabled&& !mbaScaledisabled) {
              onChangeField(designMBA + "." + "mbaProductCheckbox", value);
            }
          }}
        </OnChange>
        {(mbaProductCheckboxdisabled || mbaColorsdisabled || mbaProductPricedisabled || mbaScaledisabled) && fieldDisabledText}
      </Col>
    </>
  );
};

export default DesignMBA;
