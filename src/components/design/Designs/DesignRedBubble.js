import React from 'react';
import { Col, Divider, Input, Space, Tag, Typography } from 'antd';
import _ from 'lodash';
import { Field } from 'react-final-form';
import { OnChange } from 'react-final-form-listeners';
import { TextField } from 'react-final-form-antd';
import Tags from '../../input/Tags';
import { fieldDisabledText } from '../../input/fieldDisabledText';
import StyledBadge from '../../input/StyledBadge';
import { designRedBubble } from '../../../utils/constants/constants';
import TranslationSetting from '../TMSetting';
import MyTextField from '../../input/MyFields/MyTextField';
import { ExclamationCircleOutlined } from '@ant-design/icons';

const { TextArea } = Input;
const { Title } = Typography;

const DesignRedBubble = ({
                           handleSubmit,
                           form,
                           currentTab,
                           onChangeField,
                           submitting,
                           pristine,
                           onChangeTranslatedField,
                         }) => {
  const onChangeTitle50Field = _.debounce(
    value => form.mutators.onChangeMainField('title50', value),
    540
  );
  return (
    <Col xs={24} sm={24} md={24} lg={24} xl={12}>
      <Divider> {_.startCase(currentTab)}</Divider>
      <div>
        <div>
          <Field name={designRedBubble + '.' + 'title50disabled'} subscription={{ value: true }}>
            {({ input, meta }) => (
              <>
                <Field
                  name={designRedBubble + '.' + `title50`}
                  disabled={input.value}
                  maxLength={50}
                  subscription={{ value: true }}
                  allowNull={true}
                >
                  {props => (
                    <>
                      <OnChange name={designRedBubble + '.' + 'title50'}>
                        {value => {
                          if (!input.value) {
                            onChangeTranslatedField(props.input.name, value);
                            onChangeField(props.input.name, value);
                          }
                        }}
                      </OnChange>
                      <Title level={5}>
                        <Space align="baseline">
                          Title
                          <StyledBadge count={50} selected={true} touched={props.input.value} />
                        </Space>
                        <TranslationSetting style={{ align: 'right' }} selectedField={props.input.name} />
                      </Title>
                      <TextArea
                        maxLength={50}
                        autoSize
                        showCount
                        rows={1}
                        size="large"
                        disabled={props.disabled}
                        value={props.input.value}
                        onChange={props.input.onChange}
                      />
                    </>
                  )}
                </Field>
                <br />
                <br />
                {input.value && fieldDisabledText}
              </>
            )}
          </Field>
        </div>
        <div>
          <Field
            name={designRedBubble + '.' + 'description200disabled'}
            subscription={{ value: true }}
          >
            {({ input }) => (
              <>
                <Field
                  name={designRedBubble + '.' + `description200`}
                  component={TextField}
                  disabled={input.value}
                  maxLength={200}
                  subscription={{ value: true }}
                  allowNull={true}
                >
                  {props => (
                    <>
                      <OnChange name={designRedBubble + '.' + 'description200'}>
                        {value => {
                          if (!input.value) {
                            onChangeTranslatedField(props.input.name, value);
                            onChangeField(props.input.name, value);
                          }
                        }}
                      </OnChange>
                      <Title level={5}>
                        <Space align="baseline">
                          Description
                          <StyledBadge
                            count={200}
                            overflowCount={999}
                            selected={true}
                            touched={props.input.value}
                          />
                        </Space>
                        <TranslationSetting style={{ align: 'right' }} selectedField={props.input.name} />
                      </Title>
                      <TextArea
                        size="large"
                        disabled={props.disabled}
                        value={props.input.value}
                        maxLength={200}
                        showCount
                        rows={6}
                        onChange={props.input.onChange}
                      />
                    </>
                  )}
                </Field>
                {input.value && fieldDisabledText}
              </>
            )}
          </Field>
        </div>
        <div>
          <br />
          <br />
          <Title level={5}>
            <Space align="baseline">
              Tags
              <Tag color="#F9C371C2">50</Tag>
            </Space>
            <TranslationSetting style={{ align: 'right' }} selectedField={designRedBubble + '.' + `tags`} />
          </Title>
          <Field name={designRedBubble + '.' + 'tagsdisabled'} subscription={{ value: true }}>
            {({ input, meta }) => (
              <>
                <Field
                  name={designRedBubble + '.' + `tags`}
                  // component={Tags}
                  disabled={input.value}
                  subscription={{ value: true }}
                  allowNull={true}
                >
                  {props => (
                    <>
                      <OnChange name={designRedBubble + '.' + 'tags'}>
                        {value => {
                          if (!input.value) {
                            onChangeTranslatedField(props.input.name, value);
                            onChangeField(props.input.name, value);
                          }
                        }}
                      </OnChange>
                      <Tags
                        disabled={input.value}
                        input={props.input}
                        color={'#F9C371C2'}
                        maxLength={50}
                      />
                    </>
                  )}
                </Field>
                <br />
                <br />
                {input.value && fieldDisabledText}
              </>
            )}
          </Field>
        </div>
      </div>
      <div>
        <Field name={designRedBubble + '.' + 'redBubbleIddisabled'} subscription={{ value: true }}>
          {({ input, meta }) => (
            <>
              <Field
                name={designRedBubble + '.' + `redBubbleId`}
                component={TextField}
                disabled={input.value}
                maxLength={26}
                subscription={{ value: true }}
                allowNull={true}
              >
                {props => (
                  <>
                    <OnChange name={designRedBubble + '.' + 'redBubbleId'}>
                      {value => {
                        if (!input.value) {
                          // onChangeTranslatedField(props.input.name, value);
                          onChangeField(props.input.name, value);
                        }
                      }}
                    </OnChange>
                    <Title level={5}>
                      <Space align="baseline">
                        ID
                        {/*<StyledBadge count={50} selected={true} touched={props.input.value} />*/}
                      </Space>
                      <TranslationSetting style={{ align: 'right' }} selectedField={props.input.name} />
                    </Title>
                    <TextArea
                      maxLength={255}
                      showCount
                      rows={1}
                      autoSize
                      size="large"
                      disabled={props.disabled}
                      value={props.input.value}
                      placeholder={'Id for RedBubble comes here'}
                      onChange={props.input.onChange}
                    />
                  </>
                )}
              </Field>
              <br />
              <br />
              {input.value && fieldDisabledText}
            </>
          )}
        </Field>
      </div>
      <br />
      <Title level={5}>
        <Space align="baseline">
          <ExclamationCircleOutlined style={{ color: '#7f67ff' }} /> Duplication URL
        </Space>
      </Title>
      <br />
      <Field name={designRedBubble + '.' + 'redBubbleDuplicationURLdisabled'} subscription={{ value: true }}>
        {({ input }) => (
          <>
            <Field
              subscription={{ value: true }}
              size={'large'}
              name={designRedBubble + '.' + "redBubbleDuplicationURL"}
              hasFeedback
              allowNull={true}
              parse={x => x}
            >
              {props => (
                <>
                  <OnChange name={props.input.name}>
                    {value => {
                      if (!input.value) {
                        onChangeField(props.input.name, value);
                      }
                    }}
                  </OnChange>
                  <MyTextField {...props} />
                </>
              )}
            </Field>
            {input.value && fieldDisabledText}
          </>
        )}
      </Field>
      {/*<Divider />*/}
      {/*<SubmitAndTransferButtons*/}
      {/*  form={form}*/}
      {/*  handleSubmit={handleSubmit}*/}
      {/*  pristine={pristine}*/}
      {/*  submitting={submitting}*/}
      {/*/>*/}
    </Col>
  );
};

export default DesignRedBubble;
