import React from 'react';
import { Col, Collapse, Divider, Input, Radio, Row, Space, Tag, Typography } from "antd";
import _ from 'lodash';
import { Field, useField } from 'react-final-form';
import { OnChange } from 'react-final-form-listeners';
import { TextField } from 'react-final-form-antd';
import Tags from '../../input/Tags';
import { fieldDisabledText } from '../../input/fieldDisabledText';
import StyledBadge from '../../input/StyledBadge';
import { designSpreadshirtCom, designSpreadshirtEu } from "../../../utils/constants/constants";
import TranslationSetting from '../TMSetting';
import { ExclamationCircleOutlined } from '@ant-design/icons';
import MyTextField from '../../input/MyFields/MyTextField';
import MySwitch from '../../input/MyFields/MySwitch';
import WhenFieldChanges from '../../input/MyFields/WhenFieldChanges';
import MyRadioGroup from "../../input/MyFields/MyRadioGroup";

const { TextArea } = Input;
const { Title } = Typography;

const DesignSpreadshirtEu = ({
                               handleSubmit,
                               form,
                               currentTab,
                               onChangeField,
                               submitting,
                               pristine,
                               onChangeTranslatedField,
                             }) => {
  const { Panel } = Collapse;
  const wrapperCol = { span: 24 };
  const spreadshirtEuIsShop = useField(designSpreadshirtEu + '.spreadshirtEuIsShop', { subscription: { value: true },type:"checkbox" });

  return (
    <Col xs={24} sm={24} md={24} lg={24} xl={12}>
      <Divider> {_.startCase(currentTab)}</Divider>
      <div>
        <div>
          <Field name={designSpreadshirtEu + '.' + 'title50disabled'} subscription={{ value: true }}>
            {({ input, meta }) => (
              <>
                <Field
                  name={designSpreadshirtEu + '.' + `title50`}
                  disabled={input.value}
                  maxLength={50}
                  subscription={{ value: true }}
                  allowNull={true}
                >
                  {props => (
                    <>
                      <OnChange name={designSpreadshirtEu + '.' + 'title50'}>
                        {value => {
                          if (!input.value) {
                            onChangeTranslatedField(props.input.name, value);
                            onChangeField(props.input.name, value);
                          }
                        }}
                      </OnChange>
                      <Title level={5}>
                        <Space align="baseline">
                          Title
                          <StyledBadge count={50} selected={true} touched={props.input.value} />
                        </Space>
                        <TranslationSetting style={{ align: 'right' }} selectedField={props.input.name} />
                      </Title>
                      <TextArea
                        maxLength={50}
                        showCount
                        autoSize
                        rows={1}
                        size="large"
                        disabled={props.disabled}
                        value={props.input.value}
                        onChange={props.input.onChange}
                      />
                    </>
                  )}
                </Field>
                <br />
                <br />
                {input.value && fieldDisabledText}
              </>
            )}
          </Field>
        </div>
        <div>
          <Field
            name={designSpreadshirtEu + '.' + 'description200disabled'}
            subscription={{ value: true }}
          >
            {({ input }) => (
              <>
                <Field
                  name={designSpreadshirtEu + '.' + `description200`}
                  component={TextField}
                  disabled={input.value}
                  maxLength={200}
                  subscription={{ value: true }}
                  allowNull={true}
                >
                  {props => (
                    <>
                      <OnChange name={designSpreadshirtEu + '.' + 'description200'}>
                        {value => {
                          if (!input.value) {
                            onChangeTranslatedField(props.input.name, value);
                            onChangeField(props.input.name, value);
                          }
                        }}
                      </OnChange>
                      <Title level={5}>
                        <Space align="baseline">
                          Description
                          <StyledBadge
                            count={200}
                            overflowCount={999}
                            selected={true}
                            touched={props.input.value}
                          />
                        </Space>
                        <TranslationSetting style={{ align: 'right' }} selectedField={props.input.name} />
                      </Title>
                      <TextArea
                        size="large"
                        disabled={props.disabled}
                        value={props.input.value}
                        maxLength={200}
                        showCount
                        rows={6}
                        onChange={props.input.onChange}
                      />
                    </>
                  )}
                </Field>
                {input.value && fieldDisabledText}
              </>
            )}
          </Field>
        </div>
        <div>
          <br />
          <br />
          <Title level={5}>
            <Space align="baseline">
              Tags
              <Tag color="#91d5ff">25</Tag>
            </Space>
            <TranslationSetting
              style={{ align: 'right' }}
              selectedField={designSpreadshirtEu + '.' + `tags`}
            />
          </Title>
          <Field name={designSpreadshirtEu + '.' + 'tagsdisabled'} subscription={{ value: true }}>
            {({ input, meta }) => (
              <>
                <Field
                  name={designSpreadshirtEu + '.' + `tags`}
                  // component={Tags}
                  disabled={input.value}
                  subscription={{ value: true }}
                  allowNull={true}
                >
                  {props => (
                    <>
                      <OnChange name={designSpreadshirtEu + '.' + 'tags'}>
                        {value => {
                          if (!input.value) {
                            onChangeTranslatedField(props.input.name, value);
                            onChangeField(props.input.name, value);
                          }
                        }}
                      </OnChange>
                      <Tags
                        disabled={input.value}
                        input={props.input}
                        color={'#91d5ff'}
                        maxLength={25}
                      />
                    </>
                  )}
                </Field>
                <br />
                <br />
                {input.value && fieldDisabledText}
              </>
            )}
          </Field>
        </div>
      </div>
      <div>
        <Field name={designSpreadshirtEu + '.' + 'spreadshirtIddisabled'} subscription={{ value: true }}>
          {({ input, meta }) => (
            <>
              <Field
                name={designSpreadshirtEu + '.' + `spreadshirtId`}
                component={TextField}
                disabled={input.value}
                maxLength={26}
                subscription={{ value: true }}
                allowNull={true}
              >
                {props => (
                  <>
                    <OnChange name={designSpreadshirtEu + '.' + 'spreadshirtId'}>
                      {value => {
                        if (!input.value) {
                          // onChangeTranslatedField(props.input.name, value);
                          onChangeField(props.input.name, value);
                        }
                      }}
                    </OnChange>
                    <Title level={5}>
                      <Space align="baseline">
                        ID
                        {/*<StyledBadge count={50} selected={true} touched={props.input.value} />*/}
                      </Space>
                      <TranslationSetting style={{ align: 'right' }} selectedField={props.input.name} />
                    </Title>
                    <TextArea
                      maxLength={255}
                      showCount
                      rows={1}
                      autoSize
                      size="large"
                      disabled={props.disabled}
                      value={props.input.value}
                      placeholder={'Id for spreadshirt comes here'}
                      onChange={props.input.onChange}
                    />
                  </>
                )}
              </Field>
              <br />
              <br />
              {input.value && fieldDisabledText}
            </>
          )}
        </Field>
      </div>
      <>
        <Title level={5}><ExclamationCircleOutlined style={{ color: '#7f67ff' }} /> Design Template</Title>
        <br />
        <Field name={designSpreadshirtEu + '.' + 'spreadshirtEuSelectDesignTemplatedisabled'}
               subscription={{ value: true }}>
          {({ input }) => (
            <>
              <Field
                subscription={{ value: true }}
                name={designSpreadshirtEu + '.' + "spreadshirtEuSelectDesignTemplate"}
                wrapperCol={wrapperCol}
                hasFeedback
                placeholder={'select templates'}
                size={'large'}
                disabled={input.value}
              >
                {props => (
                  <>
                    <OnChange name={props.input.name}>
                      {value => {
                        if (!input.value) {
                          onChangeField(props.input.name, value);
                        }
                      }}
                    </OnChange>
                    <MyTextField  disabled={input.value} {...props} />
                  </>
                )}
              </Field>
              {input.value && fieldDisabledText}
            </>
          )}
        </Field>
        <Row gutter={[16, 16]}>
          <Col span={4}>
            <Title level={5}> <ExclamationCircleOutlined style={{ color: '#7f67ff' }} /> Marketplce:</Title>
          </Col>
          <Col span={24}>
            <Field name={designSpreadshirtEu + '.' + 'spreadshirtEuIsMarketplacedisabled'}
                   subscription={{ value: true }}>
              {({ input }) => (
                <>
                  <Field
                    subscription={{ value: true }}
                    name={designSpreadshirtEu + '.'+'spreadshirtEuIsMarketplace'}
                    hasFeedback
                    // disabled={input.value}
                    allowNull
                    type={"checkbox"}
                    size={'large'}>
                    {props => (
                      <>
                        <OnChange name={props.input.name}>
                          {value => {
                            if (!input.value) {
                              onChangeField(props.input.name, value);
                            }
                          }}
                        </OnChange>
                        <MySwitch  disabled={input.value} {...props} />
                      </>
                    )}
                  </Field>

                  {input.value && fieldDisabledText}
                </>
              )}
            </Field>

          </Col>
          <WhenFieldChanges
            field={designSpreadshirtEu + '.' +'spreadshirtEuIsShop'}
            becomes={false}
            set={designSpreadshirtEu + '.' + 'spreadshirtEuShop'}
            to={undefined}
          />
          <Col span={24}>
            <Title level={5}><ExclamationCircleOutlined style={{ color: '#7f67ff' }} /> Shop: </Title>
          </Col>
          <Col span={4}>

            <Field name={designSpreadshirtEu + '.' + 'spreadshirtEuIsShopdisabled'}
                   subscription={{ value: true }}>
              {({ input }) => (
                <>
                  <OnChange name={spreadshirtEuIsShop.input.name}>
                    {value => {
                      if (!input.value) {
                        onChangeField(spreadshirtEuIsShop.input.name, value);
                      }
                    }}
                  </OnChange>
                  <MySwitch disabled={input.value} {...spreadshirtEuIsShop} />
                  {input.value && fieldDisabledText}
                </>
              )}
            </Field>

          </Col>
          <Col span={20}>
            <Field name={designSpreadshirtEu + '.' + 'spreadshirtEuShopdisabled'}
                   subscription={{ value: true }}>
              {({ input }) => (
                <>
                  <Field
                    subscription={{ value: true }}
                    wrapperCol={wrapperCol}
                    name={designSpreadshirtEu + '.'+ 'spreadshirtEuShop'}
                    initialValue={null}
                    hasFeedback
                    allowNull
                    placeholder={'(optional)'}
                    disabled={input.value || !spreadshirtEuIsShop.input.value}
                    size={'large'}>
                    {props => (
                      <>
                        <OnChange name={props.input.name}>
                          {value => {
                            if (!input.value) {
                              onChangeField(props.input.name, value);
                            }
                          }}
                        </OnChange>
                        <MyTextField   {...props} disabled={input.value || !spreadshirtEuIsShop.input.checked} />
                      </>
                    )}

                  </Field>

                  {input.value && fieldDisabledText}
                </>
              )}
            </Field>
          </Col>
          <Col span={24}>
            <Title level={5}> <ExclamationCircleOutlined style={{color: '#7f67ff'}} /> Targetmarket:</Title>
          </Col>
          <Col span={24}>
            <Field name={designSpreadshirtEu + '.' + 'spreadshirtEuTargetmarketdisabled'}
                   subscription={{ value: true }}>
              {({ input }) => (
                <>
                  <Field
                    subscription={{ value: true }}
                    name={designSpreadshirtEu + '.'+ 'spreadshirtEuTargetmarket'}
                    initialValue={null}
                    hasFeedback
                    allowNull
                    disabled={input.value}
                    size={'large'}>
                    {props => (
                      <>
                        <OnChange name={props.input.name}>
                          {value => {
                            if (!input.value) {
                              onChangeField(props.input.name, value);
                            }
                          }}
                        </OnChange>
                        <MyRadioGroup {...props}>
                          <Radio value={'DE'}>DE</Radio>
                          <Radio value={'UK'}>UK</Radio>
                        </MyRadioGroup>
                      </>
                    )}
                  </Field>
                  {input.value && fieldDisabledText}
                </>
              )}
            </Field>
          </Col>
        </Row>
      </>
      {/*<Divider />*/}
      {/*<SubmitAndTransferButtons*/}
      {/*  form={form}*/}
      {/*  handleSubmit={handleSubmit}*/}
      {/*  pristine={pristine}*/}
      {/*  submitting={submitting}*/}
      {/*/>*/}
    </Col>
  );
};

export default DesignSpreadshirtEu;
