import React from 'react';
import { Col, Divider, Input, Radio, Select, Space, Tag, Typography } from "antd";
import _ from 'lodash';
import { Field } from 'react-final-form';
import { TextField } from 'react-final-form-antd';
import { OnChange } from 'react-final-form-listeners';
import Tags from '../../input/Tags';
import { fieldDisabledText } from '../../input/fieldDisabledText';
import StyledBadge from '../../input/StyledBadge';
import { designShirtee, designTeespring } from "../../../utils/constants/constants";
import TranslationSetting from '../TMSetting';
import { ExclamationCircleOutlined } from '@ant-design/icons';
import MyTextField from '../../input/MyFields/MyTextField';
import MyRadioGroup from "../../input/MyFields/MyRadioGroup";
import MySelect from "../../input/MyFields/MySelect";

const { TextArea } = Input;
const { Title } = Typography;
const { Option } = Select;
const DesignShirtee = ({
                         handleSubmit,
                         form,
                         currentTab,
                         onChangeField,
                         submitting,
                         pristine,
                         onChangeTranslatedField,
                       }) => {
  return (
    <Col xs={24} sm={24} md={24} lg={24} xl={12}>
      <Divider> {_.startCase(currentTab)}</Divider>
      <div>
        <div>
          <Field name={designShirtee + '.' + 'title40disabled'} subscription={{ value: true }}>
            {({ input, meta }) => (
              <>
                <Field
                  name={designShirtee + '.' + `title40`}
                  disabled={input.value}
                  maxLength={50}
                  subscription={{ value: true }}
                  allowNull={true}
                >
                  {props => (
                    <>
                      <OnChange name={designShirtee + '.' + 'title40'}>
                        {value => {
                          if (!input.value) {
                            onChangeTranslatedField(props.input.name, value);
                            onChangeField(props.input.name, value);
                          }
                        }}
                      </OnChange>
                      <Title level={5}>
                        <Space align="baseline">
                          Title
                          <StyledBadge count={40} selected={true} touched={props.input.value} />
                        </Space>
                        <TranslationSetting style={{ align: 'right' }} selectedField={props.input.name} />
                      </Title>
                      <TextArea
                        maxLength={40}
                        showCount
                        autoSize
                        rows={1}
                        size="large"
                        disabled={props.disabled}
                        value={props.input.value}
                        onChange={props.input.onChange}
                      />
                    </>
                  )}
                </Field>
                <br />
                <br />
                {input.value && fieldDisabledText}
              </>
            )}
          </Field>
        </div>
        <div>
          <Field name={designShirtee + '.' + 'description200disabled'} subscription={{ value: true }}>
            {({ input }) => (
              <>
                <Field
                  name={designShirtee + '.' + `description200`}
                  component={TextField}
                  disabled={input.value}
                  maxLength={200}
                  subscription={{ value: true }}
                  allowNull={true}
                >
                  {props => (
                    <>
                      <OnChange name={designShirtee + '.' + 'description200'}>
                        {value => {
                          if (!input.value) {
                            onChangeTranslatedField(props.input.name, value);
                            onChangeField(props.input.name, value);
                          }
                        }}
                      </OnChange>
                      <Title level={5}>
                        <Space align="baseline">
                          Description
                          <StyledBadge
                            count={200}
                            overflowCount={999}
                            selected={true}
                            touched={props.input.value}
                          />
                        </Space>
                        <TranslationSetting style={{ align: 'right' }} selectedField={props.input.name} />
                      </Title>
                      <TextArea
                        size="large"
                        disabled={props.disabled}
                        value={props.input.value}
                        maxLength={200}
                        showCount
                        rows={6}
                        onChange={props.input.onChange}
                      />
                    </>
                  )}
                </Field>
                {input.value && fieldDisabledText}
              </>
            )}
          </Field>
        </div>
        <div>
          <br />
          <br />
          <Title level={5}>
            <Space align="baseline">
              Tags
              <Tag color="#FF9F9F">10</Tag>
            </Space>
            <TranslationSetting style={{ align: 'right' }} selectedField={designShirtee + '.' + `tags`} />
          </Title>
          <Field name={designShirtee + '.' + 'tagsdisabled'} subscription={{ value: true }}>
            {({ input, meta }) => (
              <>
                <Field
                  name={designShirtee + '.' + `tags`}
                  // component={Tags}
                  disabled={input.value}
                  subscription={{ value: true }}
                  allowNull={true}
                >
                  {props => (
                    <>
                      <OnChange name={designShirtee + '.' + 'tags'}>
                        {value => {
                          if (!input.value) {
                            // onChangeTranslatedField(props.input.name, value);
                            onChangeField(props.input.name, value);
                          }
                        }}
                      </OnChange>
                      <Tags
                        disabled={input.value}
                        input={props.input}
                        color={'#FF9F9F'}
                        maxLength={10}
                      />
                    </>
                  )}
                </Field>
                <br />
                <br />
                {input.value && fieldDisabledText}
              </>
            )}
          </Field>
        </div>
        <div>
          <Field
            name={designShirtee + '.' + 'categorydisabled'}
            subscription={{ value: true }}
          >
            {({ input }) => (
              <>
                <Field
                  name={designShirtee + '.' + `category`}
                  component={TextField}
                  disabled={input.value}
                  subscription={{ value: true }}
                  allowNull={true}
                >
                  {props => (
                    <>
                      <OnChange name={props.input.name}>
                        {value => {
                          if (!input.value) {
                            onChangeField(props.input.name, value);
                          }
                        }}
                      </OnChange>
                      <Title level={5}>
                        <Space align="baseline">
                          Category
                        </Space>
                      </Title>
                      <TextArea
                        size="large"
                        disabled={props.disabled}
                        value={props.input.value}
                        // maxLength={200}
                        showCount
                        rows={1}
                        onChange={props.input.onChange}
                      />
                    </>
                  )}
                </Field>
                <br />
                {input.value && fieldDisabledText}
              </>
            )}
          </Field>
        </div>
        <div>
          <Field
            name={designShirtee + '.' + 'shirteeShopdisabled'}
            subscription={{ value: true }}
          >
            {({ input }) => (
              <>
                <Field
                  name={designShirtee + '.' + `shirteeShop`}
                  component={TextField}
                  disabled={input.value}
                  subscription={{ value: true }}
                  allowNull={true}
                >
                  {props => (
                    <>
                      <OnChange name={props.input.name}>
                        {value => {
                          if (!input.value) {
                            onChangeField(props.input.name, value);
                          }
                        }}
                      </OnChange>
                      <Title level={5}>
                        <Space align="baseline">
                          <ExclamationCircleOutlined style={{ color: '#7f67ff' }} /> Shop
                        </Space>
                      </Title>
                      <TextArea
                        size="large"
                        disabled={props.disabled}
                        value={props.input.value}
                        // maxLength={200}
                        showCount
                        rows={1}
                        onChange={props.input.onChange}
                      />
                    </>
                  )}
                </Field>
                <br />
                {input.value && fieldDisabledText}
              </>
            )}
          </Field>
        </div>

        <div>
          <Field
            name={designShirtee + '.' + 'shirteeMarketplacedisabled'}
            subscription={{ value: true }}
          >
            {({ input }) => (
              <>
                <Field
                  name={designShirtee + '.' + `shirteeMarketplace`}
                  component={TextField}
                  disabled={input.value}
                  subscription={{ value: true }}
                  allowNull={true}
                >
                  {props => (
                    <>
                      <OnChange name={props.input.name}>
                        {value => {
                          if (!input.value) {
                            onChangeField(props.input.name, value);
                          }
                        }}
                      </OnChange>
                      <Title level={5}>
                        <Space align="baseline">
                          <ExclamationCircleOutlined style={{ color: '#7f67ff' }} />   Marketplace
                        </Space>
                      </Title>
                      <MyRadioGroup {...props}>
                        <Radio value={'en'}>English</Radio>
                        <Radio value={'de'}>German</Radio>
                        <Radio value={'es'}>Shirtee ES</Radio>
                        <Radio value={'it'}>Shirtee IT</Radio>
                        <Radio value={'fr'}>Shirtee FR</Radio>
                      </MyRadioGroup>
                    </>
                  )}
                </Field>
                {input.value && fieldDisabledText}
              </>
            )}
          </Field>
        </div>
        <div>
          <Field
            name={designShirtee + '.' + 'shirteeDescriptionTemplatedisabled'}
            subscription={{ value: true }}
          >
            {({ input }) => (
              <>
                <Field
                  name={designShirtee + '.' + `shirteeDescriptionTemplate`}
                  component={TextField}
                  disabled={input.value}
                  subscription={{ value: true }}
                  allowNull={true}
                  size={'large'}
                >
                  {props => (
                    <>
                      <OnChange name={props.input.name}>
                        {value => {
                          if (!input.value) {
                            onChangeField(props.input.name, value);
                          }
                        }}
                      </OnChange>
                      <Title level={5}>
                        <Space align="baseline">
                          <ExclamationCircleOutlined style={{ color: '#7f67ff' }} /> Description Template
                        </Space>
                      </Title>
                      <MySelect {...props}>
                        <Option value={'Deutsch (Deutschland-Österreich-Schweiz)'}>German</Option>
                        <Option value={'English (US/UK)'}>English</Option>
                        <Option value={'French (France)'}>French</Option>
                        <Option value={'Italiana (Italian)'}>Italian</Option>
                        <Option value={'Español (Spain)'}>Spain</Option>
                      </MySelect>
                    </>
                  )}
                </Field>
                {input.value && fieldDisabledText}
              </>
            )}
          </Field>
        </div>
        <div>
          <Title level={5}> <ExclamationCircleOutlined style={{ color: '#7f67ff' }} /> Design Template
            {/*<TranslationSetting style={{align:'right'}} selectedField={designShirtee + '.' + `shirteeSelectDesignTemplate`} />*/}
          </Title>
          <Field name={designShirtee + '.' + 'shirteeSelectDesignTemplatedisabled'} subscription={{ value: true }}>
            {({ input }) => (
              <>
                <Field
                  subscription={{ value: true }}
                  name={designShirtee + '.' + "shirteeSelectDesignTemplate"}
                  hasFeedback
                  placeholder={'select templates'}
                  size={'large'}
                  disabled={input.value}
                >
                  {props => (
                    <>
                      <OnChange name={props.input.name}>
                        {value => {
                          if (!input.value) {
                            onChangeField(props.input.name, value);
                          }
                        }}
                      </OnChange>
                      <MyTextField {...props} />
                    </>
                  )}
                </Field>
                {input.value && fieldDisabledText}
              </>
            )}
          </Field>
        </div>
      </div>
      {/*<Divider />*/}
      {/*<SubmitAndTransferButtons*/}
      {/*  form={form}*/}
      {/*  handleSubmit={handleSubmit}*/}
      {/*  pristine={pristine}*/}
      {/*  submitting={submitting}*/}
      {/*/>*/}
    </Col>
  );
};

export default DesignShirtee;
