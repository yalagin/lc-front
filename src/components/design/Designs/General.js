import React from 'react';
import {Col, Collapse, Divider, Input, Popover, Row, Space, Tag, Tooltip, Typography} from 'antd';
import _ from 'lodash';
import {Field, useField} from 'react-final-form';
import {OnChange} from 'react-final-form-listeners';
import {SwitchField, TextField} from 'react-final-form-antd';
import Tags from '../../input/Tags';
import MySwitchField from '../../input/MySwitchField';
import StyledBadge from '../../input/StyledBadge';
import MyCustomSettingGearOutlined from '../../input/MyCustomSettingGearOutlined';
import PopoverTitle from '../popover/PopoverTitle';
import PopoverContent from '../popover/PopoverContent';
import {MBA} from '../../../utils/constants/constants';
import SubmitAndTransferButtons from '../../input/button/SubmitAndTransferButtons';
import PopoverContentMBA from '../popover/PopoverContentMBA';
import {fieldDisabledText} from '../../input/fieldDisabledText';
import TranslationSetting from '../TMSetting';
import MyTextAreaWithMentionsField from '../../input/MyFields/MyTextAreaWithMentionsField';
import styled from 'styled-components';

export const VisibleTooltip = styled(Tooltip)`
  &&& .ant-tooltip-content {
    display: ${props => (props.enabled ? '' : 'none')};
  }
`;

const General = ({
  handleSubmit,
  form,
  currentTab,
  onChangeField,
  submitting,
  pristine,
  onChangeTranslatedField,
}) => {
  const {Panel} = Collapse;
  const {TextArea} = Input;
  const {Title} = Typography;

  const { input: {value: selectedTitle, onChange:setSelectedTitle}} = useField("setSelectedTitle", {subscription: {value: true}});
  const { input: {value: valueOfSelectedTitle}, } = useField(selectedTitle, {subscription: {value: true}});
  const { input: {value: title26}, } = useField('title26', {subscription: {value: true}});
  const { input: {value: title40}, } = useField('title40', {subscription: {value: true}});
  const { input: {value: title50}, } = useField('title50', {subscription: {value: true}});
  const { input: {value: title60}, } = useField('title60', {subscription: {value: true}});
  const { input: {value: title100}, } = useField('title100', {subscription: {value: true}});

  const { input: {value: selectedDescription, onChange:setSelectedDescription}} = useField("selectedDescription", {subscription: {value: true}});
  const { input: {value: valueOfSelectedDescription}, } = useField(selectedDescription, {subscription: {value: true}});
  const { input: {value: description140}, } = useField('description140', {subscription: {value: true}});
  const { input: {value: description200}, } = useField('description200', {subscription: {value: true}});
  const { input: {value: description250}, } = useField('description250', {subscription: {value: true}});
  const { input: {value: description2000}, } = useField('description2000', {subscription: {value: true}});

  const { input: {value: fitYouthdisabled}, } = useField('fitYouthdisabled', {subscription: {value: true}});
  const { input: {value: haveIsAdult}, } = useField('haveIsAdult', {subscription: {value: true}});

  const tags = useField('tags', {subscription: {value: true}});

  const { input: {value: valueOfEditSettings}, } = useField('Edit', {subscription: {value: true}});
  const { input: {value: valueOfTitleSettings}, } = useField('Title', {subscription: {value: true}});
  const { input: {value: valueOfDescriptionSettings}, } = useField('Description', {subscription: {value: true}});
  const { input: {value: valueOfTagsSettings}, } = useField('Tags', {subscription: {value: true}});


  return (
    <>
      <Col className={'general-fields'} xs={24} sm={24} md={24} lg={24} xl={12}>
        <Divider> {_.startCase(currentTab)}</Divider>
        <div>
          <Title level={5}>
            <Row align="middle" gutter={[0, 24]}>
              <Col xs={4} sm={2} md={2} lg={1} xl={1} xxl={1}>
                <Popover
                  placement="bottom"
                  title={<PopoverTitle title={'Title'} />}
                  content={
                    <PopoverContent
                      title={'Title'}
                      className="eight-step-WorkspaceWalkthroughSettings"
                    />
                  }
                  trigger="click"
                >
                  <MyCustomSettingGearOutlined id="gear-general-title" />
                </Popover>
              </Col>
              <Col xs={20} sm={20} md={3} lg={2} xl={2} xxl={2}>
                Title
              </Col>
              <Col xs={24} sm={24} md={20} lg={20} xl={20} xxl={20} pull={1} id="title-selector">
                <StyledBadge
                  style={{marginLeft: '1rem'}}
                  count={26}
                  id={'title26-general-badge'}
                  onClick={() => {
                    setSelectedTitle('title26');
                    if (!title26) form.change('title26', valueOfSelectedTitle);
                  }}
                  selected={selectedTitle === 'title26'}
                  touched={!!title26}
                  hidden={
                    valueOfEditSettings.designDisplate === false ||
                    valueOfTitleSettings.designDisplate === false
                  }
                />
                <StyledBadge
                  count={40}
                  id={'title40-general-badge'}
                  onClick={() => {
                    setSelectedTitle('title40');
                    if (!title40) form.change('title40', valueOfSelectedTitle);
                  }}
                  selected={selectedTitle === 'title40'}
                  touched={!!title40}
                  hidden={
                    (valueOfEditSettings.designShirtee === false &&
                      valueOfEditSettings.designTeespring === false) ||
                    (valueOfTitleSettings.designShirtee === false &&
                      valueOfTitleSettings.designTeespring === false)
                  }
                />
                <StyledBadge
                  count={50}
                  id={'title50-general-badge'}
                  onClick={() => {
                    setSelectedTitle('title50');
                    if (!title50) form.change('title50', valueOfSelectedTitle);
                  }}
                  selected={selectedTitle === 'title50'}
                  touched={!!title50}
                  hidden={
                    (valueOfEditSettings.designSpreadshirtCom === false &&
                      valueOfEditSettings.designSpreadshirtEu === false &&
                      valueOfEditSettings.designRedBubble === false &&
                      valueOfEditSettings.designTeepublic === false &&
                      valueOfEditSettings.designZazzle === false &&
                      valueOfEditSettings.designPrintful === false) ||
                    (valueOfTitleSettings.designSpreadshirtCom === false &&
                      valueOfTitleSettings.designSpreadshirtEu === false &&
                      valueOfTitleSettings.designRedBubble === false &&
                      valueOfTitleSettings.designTeepublic === false &&
                      valueOfTitleSettings.designZazzle === false &&
                      valueOfTitleSettings.designPrintful === false)
                  }
                />
                <StyledBadge
                  count={60}
                  onClick={() => {
                    setSelectedTitle('title60');
                    if (!title60) form.change('title60', valueOfSelectedTitle);
                  }}
                  selected={selectedTitle === 'title60'}
                  touched={!!title60}
                  hidden={
                    valueOfEditSettings.designMBA === false ||
                    valueOfTitleSettings.designMBA === false
                  }
                />
                <StyledBadge
                  count={100}
                  overflowCount={999}
                  onClick={() => {
                    setSelectedTitle('title100');
                    if (!title100) form.change('title100', valueOfSelectedTitle);
                  }}
                  selected={selectedTitle === 'title100'}
                  touched={!!title100}
                  hidden={
                    valueOfEditSettings.designSociety6 === false ||
                    valueOfTitleSettings.designSociety6 === false
                  }
                />
              </Col>
              <Col span={1}>
                <TranslationSetting selectedField={selectedTitle} />
              </Col>
            </Row>
          </Title>
          <div className="ten-step-WorkspaceWalkthroughSettings" id={'title-target'}>
            <div style={{display: selectedTitle === 'title26' ? '' : 'none'}}>
              <Field name={'title26disabled'} subscription={{value: true}}>
                {({input, meta}) => (
                  <>
                    <Field
                      name={`title26`}
                      disabled={input.value}
                      subscription={{value: true}}
                      allowNull={true}
                    >
                      {props => (
                        <>
                          <OnChange name={props.input.name}>
                            {value => {
                              if (!input.value) {
                                onChangeTranslatedField(props.input.name, value);
                                onChangeField(props.input.name, value);
                              }
                            }}
                          </OnChange>
                          <MyTextAreaWithMentionsField
                            id={'title26-general'}
                            autoSize
                            maxLength={26}
                            meta={props.meta}
                            showCount
                            rows={1}
                            size="large"
                            disabled={props.disabled}
                            value={props.input.value}
                            onChange={props.input.onChange}
                            input={props.input}
                            atData={tags.input.value}
                          />
                        </>
                      )}
                    </Field>
                    {input.value && fieldDisabledText}
                  </>
                )}
              </Field>
            </div>
            <div style={{display: selectedTitle === 'title40' ? '' : 'none'}}>
              <Field name={'title40disabled'} subscription={{value: true}}>
                {({input, meta}) => (
                  <>
                    <Field
                      name={`title40`}
                      disabled={input.value}
                      maxLength={40}
                      subscription={{value: true}}
                      allowNull={true}
                    >
                      {props => (
                        <>
                          <OnChange name={props.input.name}>
                            {value => {
                              if (!input.value) {
                                onChangeTranslatedField(props.input.name, value);
                                onChangeField(props.input.name, value);
                              }
                            }}
                          </OnChange>
                          <MyTextAreaWithMentionsField
                            id={'title40-general'}
                            input={props.input}
                            atData={tags.input.value}
                            meta={props.meta}
                            autoSize
                            maxLength={40}
                            showCount
                            rows={1}
                            size="large"
                            disabled={props.disabled}
                            value={props.input.value}
                            onChange={props.input.onChange}
                          />
                        </>
                      )}
                    </Field>
                    {input.value && fieldDisabledText}
                  </>
                )}
              </Field>
            </div>
            <div style={{display: selectedTitle === 'title50' ? '' : 'none'}}>
              <Field name={'title50disabled'} subscription={{value: true}}>
                {({input, meta}) => (
                  <>
                    <Field
                      name={`title50`}
                      disabled={input.value}
                      maxLength={50}
                      subscription={{value: true}}
                      allowNull={true}
                    >
                      {props => (
                        <>
                          <OnChange name={props.input.name}>
                            {value => {
                              if (!input.value) {
                                onChangeTranslatedField(props.input.name, value);
                                onChangeField(props.input.name, value);
                              }
                            }}
                          </OnChange>
                          <MyTextAreaWithMentionsField
                            input={props.input}
                            atData={tags.input.value}
                            meta={props.meta}
                            autoSize
                            maxLength={50}
                            showCount
                            rows={1}
                            size="large"
                            disabled={props.disabled}
                            value={props.input.value}
                            onChange={props.input.onChange}
                          />
                        </>
                      )}
                    </Field>
                    {input.value && fieldDisabledText}
                  </>
                )}
              </Field>
            </div>
            <div style={{display: selectedTitle === 'title60' ? '' : 'none'}}>
              <Field name={'title60disabled'} subscription={{value: true}}>
                {({input, meta}) => (
                  <>
                    <Field
                      name={`title60`}
                      component={TextField}
                      disabled={input.value}
                      maxLength={60}
                      subscription={{value: true}}
                      allowNull={true}
                    >
                      {props => (
                        <>
                          <OnChange name={props.input.name}>
                            {value => {
                              if (!input.value) {
                                onChangeTranslatedField(props.input.name, value);
                                onChangeField(props.input.name, value);
                              }
                            }}
                          </OnChange>
                          <MyTextAreaWithMentionsField
                            input={props.input}
                            atData={tags.input.value}
                            meta={props.meta}
                            autoSize
                            maxLength={60}
                            showCount
                            rows={1}
                            size="large"
                            disabled={props.disabled}
                            value={props.input.value}
                            onChange={props.input.onChange}
                          />
                        </>
                      )}
                    </Field>
                    {input.value && fieldDisabledText}
                  </>
                )}
              </Field>
            </div>
            <div style={{display: selectedTitle === 'title100' ? '' : 'none'}}>
              <Field name={'title100disabled'} subscription={{value: true}}>
                {({input, meta}) => (
                  <>
                    <Field
                      name={`title100`}
                      component={TextField}
                      disabled={input.value}
                      maxLength={100}
                      subscription={{value: true}}
                      allowNull={true}
                    >
                      {props => (
                        <>
                          <OnChange name={props.input.name}>
                            {value => {
                              if (!input.value) {
                                onChangeTranslatedField(props.input.name, value);
                                onChangeField(props.input.name, value);
                              }
                            }}
                          </OnChange>
                          <MyTextAreaWithMentionsField
                            input={props.input}
                            atData={tags.input.value}
                            meta={props.meta}
                            autoSize
                            maxLength={100}
                            showCount
                            rows={1}
                            size="large"
                            disabled={props.disabled}
                            value={props.input.value}
                            onChange={props.input.onChange}
                          />
                        </>
                      )}
                    </Field>
                    {input.value && fieldDisabledText}
                  </>
                )}
              </Field>
            </div>
          </div>

          <Title level={5}>
            <Row align="middle" gutter={[0, 24]}>
              <Col xs={4} sm={2} md={1}>
                <Popover
                  placement="bottom"
                  title={<PopoverTitle title={'Description'} />}
                  content={<PopoverContent title={'Description'} />}
                  trigger="click"
                >
                  <MyCustomSettingGearOutlined className={'gear-general-desc'} />
                </Popover>
              </Col>
              <Col xs={20} sm={22} md={5} lg={3} xl={5} xxl={3}>
                Description
              </Col>
              <Col xs={24} sm={24} md={16} lg={18} xl={16} xxl={19}>
                <StyledBadge
                  count={140}
                  overflowCount={999}
                  onClick={() => {
                    setSelectedDescription('description140');
                    if (!description140) form.change('description140', valueOfSelectedDescription);
                  }}
                  selected={selectedDescription === 'description140'}
                  touched={!!description140}
                  hidden={
                    valueOfEditSettings.designDisplate === false ||
                    valueOfDescriptionSettings.designDisplate === false
                  }
                />
                <StyledBadge
                  count={200}
                  onClick={() => {
                    setSelectedDescription('description200');
                    if (!description200) form.change('description200', valueOfSelectedDescription);
                  }}
                  selected={selectedDescription === 'description200'}
                  touched={!!description200}
                  overflowCount={999}
                  hidden={
                    (valueOfEditSettings.designDisplate === false &&
                      valueOfEditSettings.designSpreadshirtCom === false &&
                      valueOfEditSettings.designSpreadshirtEu === false &&
                      valueOfEditSettings.designRedBubble === false &&
                      valueOfEditSettings.designTeepublic === false &&
                      valueOfEditSettings.designShirtee === false &&
                      valueOfEditSettings.designPrintful === false &&
                      valueOfEditSettings.designTeespring === false) ||
                    (valueOfDescriptionSettings.designDisplate === false &&
                      valueOfDescriptionSettings.designSpreadshirtCom === false &&
                      valueOfDescriptionSettings.designSpreadshirtEu === false &&
                      valueOfDescriptionSettings.designRedBubble === false &&
                      valueOfDescriptionSettings.designTeepublic === false &&
                      valueOfDescriptionSettings.designShirtee === false &&
                      valueOfDescriptionSettings.designPrintful === false &&
                      valueOfDescriptionSettings.designTeespring === false)
                  }
                />
                <StyledBadge
                  count={250}
                  onClick={() => {
                    setSelectedDescription('description250');
                    if (!description250) form.change('description250', valueOfSelectedDescription);
                  }}
                  selected={selectedDescription === 'description250'}
                  touched={!!description250}
                  overflowCount={999}
                  hidden={
                    valueOfEditSettings.designSociety6 === false ||
                    valueOfDescriptionSettings.designSociety6 === false
                  }
                />
                <StyledBadge
                  count={2000}
                  onClick={() => {
                    setSelectedDescription('description2000');
                    if (!description2000)
                      form.change('description2000', valueOfSelectedDescription);
                  }}
                  selected={selectedDescription === 'description2000'}
                  touched={!!description2000}
                  overflowCount={2001}
                  hidden={
                    (valueOfEditSettings.designZazzle === false &&
                      valueOfEditSettings.designMBA === false) ||
                    (valueOfDescriptionSettings.designZazzle === false &&
                      valueOfDescriptionSettings.designMBA === false)
                  }
                />
              </Col>
              <Col span={1}>
                <TranslationSetting selectedField={selectedDescription} />
              </Col>
            </Row>
          </Title>
          <div style={{display: selectedDescription === 'description140' ? '' : 'none'}}>
            <Field name={'description140disabled'} subscription={{value: true}}>
              {({input}) => (
                <>
                  <Field
                    name={`description140`}
                    component={TextField}
                    disabled={input.value}
                    maxLength={140}
                    subscription={{value: true}}
                    allowNull={true}
                  >
                    {props => (
                      <>
                        <OnChange name={props.input.name}>
                          {value => {
                            if (!input.value) {
                              onChangeTranslatedField(props.input.name, value);
                              onChangeField(props.input.name, value);
                            }
                          }}
                        </OnChange>
                        {/*<MyTextField size="large" disabled={props.disabled} input={props.input} />*/}
                        <MyTextAreaWithMentionsField
                          input={props.input}
                          atData={tags.input.value}
                          meta={props.meta}
                          maxLength={140}
                          showCount
                          rows={6}
                          size="large"
                          disabled={props.disabled}
                          value={props.input.value}
                          onChange={props.input.onChange}
                        />
                      </>
                    )}
                  </Field>
                  {input.value && fieldDisabledText}
                </>
              )}
            </Field>
          </div>
          <div style={{display: selectedDescription === 'description200' ? '' : 'none'}}>
            <Field name={'description200disabled'} subscription={{value: true}}>
              {({input, meta}) => (
                <>
                  <Field
                    name={`description200`}
                    component={TextField}
                    disabled={input.value}
                    maxLength={200}
                    subscription={{value: true}}
                    allowNull={true}
                  >
                    {props => (
                      <>
                        <OnChange name={props.input.name}>
                          {value => {
                            if (!input.value) {
                              onChangeTranslatedField(props.input.name, value);
                              onChangeField(props.input.name, value);
                            }
                          }}
                        </OnChange>
                        <MyTextAreaWithMentionsField
                          input={props.input}
                          atData={tags.input.value}
                          meta={props.meta}
                          size="large"
                          disabled={props.disabled}
                          maxLength={200}
                          showCount
                          rows={6}
                        />
                      </>
                    )}
                  </Field>
                  {input.value && fieldDisabledText}
                </>
              )}
            </Field>
          </div>
          <div style={{display: selectedDescription === 'description250' ? '' : 'none'}}>
            <Field name={'description250disabled'} subscription={{value: true}}>
              {({input, meta}) => (
                <>
                  <Field
                    name={`description250`}
                    component={TextField}
                    disabled={input.value}
                    subscription={{value: true}}
                    allowNull={true}
                  >
                    {props => (
                      <>
                        <OnChange name={props.input.name}>
                          {value => {
                            if (!input.value) {
                              onChangeTranslatedField(props.input.name, value);
                              onChangeField(props.input.name, value);
                            }
                          }}
                        </OnChange>
                        <MyTextAreaWithMentionsField
                          input={props.input}
                          atData={tags.input.value}
                          meta={props.meta}
                          disabled={props.disabled}
                          value={props.input.value}
                          size="large"
                          showCount
                          rows={6}
                          maxLength={250}
                          onChange={props.input.onChange}
                        />
                      </>
                    )}
                  </Field>
                  {input.value && fieldDisabledText}
                </>
              )}
            </Field>
          </div>
          <div style={{display: selectedDescription === 'description2000' ? '' : 'none'}}>
            <Field name={'description2000disabled'} subscription={{value: true}}>
              {({input, meta}) => (
                <>
                  <Field
                    name={`description2000`}
                    component={TextField}
                    disabled={input.value}
                    subscription={{value: true}}
                    allowNull={true}
                  >
                    {props => (
                      <>
                        <OnChange name={props.input.name}>
                          {value => {
                            if (!input.value) {
                              onChangeTranslatedField(props.input.name, value);
                              onChangeField(props.input.name, value);
                            }
                          }}
                        </OnChange>
                        <MyTextAreaWithMentionsField
                          input={props.input}
                          atData={tags.input.value}
                          meta={props.meta}
                          disabled={props.disabled}
                          value={props.input.value}
                          size="large"
                          maxLength={2000}
                          showCount
                          rows={6}
                          onChange={props.input.onChange}
                        />
                      </>
                    )}
                  </Field>
                  {input.value && fieldDisabledText}
                </>
              )}
            </Field>
          </div>
          <div>
            <Title level={5}>
              <Row align="middle" gutter={[16, 16]}>
                <Col xs={4} sm={2} md={2} lg={1} xl={1} xxl={1}>
                  <Popover
                    placement="bottom"
                    title={<PopoverTitle title={'Tags'} />}
                    content={<PopoverContent title={'Tags'} />}
                    trigger="click"
                  >
                    <MyCustomSettingGearOutlined className={'gear-general-tags'} />
                  </Popover>
                </Col>
                <Col xs={20} sm={20} md={3} lg={2} xl={2} xxl={2} flex={'auto'}>
                  Tags
                </Col>
                <Col
                  xs={24}
                  sm={24}
                  md={20}
                  lg={20}
                  xl={20}
                  xxl={20}
                  pull={1}
                  className={'example-tags-general'}
                >
                  <Tag color="#FF9F9F" style={{marginLeft: '1rem'}}>
                    10
                  </Tag>
                  <Tag color="#ABA8FFC2">15</Tag>
                  <Tag color="#FFA8F9C2">20</Tag>
                  <Tag color="#91d5ff">25</Tag>
                  <Tag color="#F9C371C2">50</Tag>
                </Col>
                <Col span={1}>
                  {' '}
                  <TranslationSetting selectedField={'tags'} />
                </Col>
              </Row>
            </Title>
            <Field name={'tagsdisabled'} subscription={{value: true}}>
              {({input, meta}) => (
                <div className={'tags-input'}>
                  <OnChange name={tags.input.name}>
                    {value => {
                      if (!input.value) {
                        onChangeTranslatedField(tags.input.name, value);
                        onChangeField(tags.input.name, value);
                      }
                    }}
                  </OnChange>
                  <Tags disabled={tags.value} input={tags.input} />
                  {input.value && fieldDisabledText}
                </div>
              )}
            </Field>
          </div>
        </div>
      </Col>
      {(!valueOfEditSettings || valueOfEditSettings.designMBA === true) && (
        <Collapse
          defaultActiveKey={['1']}
          ghost
          style={{width: '100%'}}
          expandIconPosition="right"
          className={'mba-general'}
        >
          <Panel
            header={
              <Space align="baseline">
                <Popover
                  onClick={e => e.stopPropagation()}
                  placement="topLeft"
                  title={<PopoverTitle title={MBA} />}
                  content={<PopoverContentMBA title={MBA} />}
                  trigger="click"
                >
                  <MyCustomSettingGearOutlined />
                </Popover>
                <Title level={5}>MBA</Title>
              </Space>
            }
            key="1"
          >
            <Row gutter={[16, 16]}>
              <Col xs={24} sm={24} md={24} lg={24} xl={12}>
                <div>
                  <Title level={5}>
                    Brand
                    <TranslationSetting selectedField={'brand'} />
                  </Title>
                  <Field name={'branddisabled'} subscription={{value: true}}>
                    {({input, meta}) => (
                      <>
                        <Field
                          name={`brand`}
                          component={TextField}
                          disabled={input.value}
                          maxLength={26}
                          subscription={{value: true}}
                          allowNull={true}
                        >
                          {props => (
                            <>
                              <OnChange name={props.input.name}>
                                {value => {
                                  if (!input.value) {
                                    onChangeTranslatedField(props.input.name, value);
                                    onChangeField(props.input.name, value);
                                  }
                                }}
                              </OnChange>
                              <TextArea
                                input={props.input}
                                atData={tags.input.value}
                                meta={props.meta}
                                autoSize
                                maxLength={50}
                                showCount
                                rows={1}
                                size="large"
                                disabled={props.disabled}
                                value={props.input.value}
                                placeholder={'Brand goes here'}
                                onChange={props.input.onChange}
                              />
                            </>
                          )}
                        </Field>
                        {input.value && fieldDisabledText}
                      </>
                    )}
                  </Field>
                </div>
                <br />
                <br />
                <div>
                  <Title level={5}>
                    Bulletpoints
                    <TranslationSetting selectedField={'bulletpoints'} />
                  </Title>
                  <Field name={'bulletpointsdisabled'} subscription={{value: true}}>
                    {({input, meta}) => (
                      <>
                        <Field
                          name={`bulletpoints`}
                          component={TextField}
                          disabled={input.value}
                          maxLength={255}
                          subscription={{value: true}}
                          allowNull={true}
                        >
                          {props => (
                            <>
                              <OnChange name={props.input.name}>
                                {value => {
                                  if (!input.value) {
                                    onChangeTranslatedField(props.input.name, value);
                                    onChangeField(props.input.name, value);
                                  }
                                }}
                              </OnChange>
                              <TextArea
                                input={props.input}
                                meta={props.meta}
                                atData={tags.input.value}
                                autoSize
                                maxLength={255}
                                showCount
                                rows={1}
                                size="large"
                                disabled={props.disabled}
                                value={props.input.value}
                                placeholder={'Bulletpoint 1 goes here'}
                                onChange={props.input.onChange}
                              />
                            </>
                          )}
                        </Field>
                        {input.value && fieldDisabledText}
                      </>
                    )}
                  </Field>
                </div>
              </Col>
              <Col xs={24} sm={24} md={24} lg={24} xl={12}>
                <div>
                  <Row>
                    <Title level={5} style={{marginBottom: '1.7rem'}}>
                      Fit types
                    </Title>
                  </Row>
                  <Row gutter={[16, 16]} style={{marginBottom: '2rem'}}>
                    <Col xs={24} sm={24} md={8} lg={8} xl={8} xxl={8} span={8}>
                      <Field name={'fitMandisabled'} subscription={{value: true}}>
                        {({input, meta}) => (
                          <>
                            <Field
                              name={`fitMan`}
                              component={SwitchField}
                              disabled={input.value}
                              maxLength={26}
                              subscription={{value: true}}
                              allowNull={true}
                              type="checkbox"
                            >
                              {props => (
                                <>
                                  <OnChange name={props.input.name}>
                                    {value => {
                                      if (!input.value) {
                                        onChangeField(props.input.name, value);
                                      }
                                    }}
                                  </OnChange>
                                  <MySwitchField
                                    label={'Man'}
                                    disabled={props.disabled}
                                    input={props.input}
                                  />
                                </>
                              )}
                            </Field>
                            {input.value && fieldDisabledText}
                          </>
                        )}
                      </Field>
                    </Col>
                    <Col xs={24} sm={24} md={8} lg={8} xl={8} xxl={8} span={8}>
                      <Field
                        name={`fitYouth`}
                        component={SwitchField}
                        disabled={fitYouthdisabled || haveIsAdult}
                        maxLength={26}
                        subscription={{value: true}}
                        allowNull={true}
                        type="checkbox"
                      >
                        {props => (
                          <>
                            <OnChange name={props.input.name}>
                              {value => {
                                if (!fitYouthdisabled && !haveIsAdult) {
                                  onChangeField(props.input.name, value);
                                }
                              }}
                            </OnChange>
                            <MySwitchField
                              label={'Youth'}
                              disabled={props.disabled}
                              input={props.input}
                            />
                          </>
                        )}
                      </Field>
                      {fitYouthdisabled && fieldDisabledText}
                      {/*{haveIsAdult && "Fit youth not allowed, please uncheck the Adult"}*/}
                    </Col>
                    <Col xs={24} sm={24} md={8} lg={8} xl={8} xxl={8}>
                      <Field name={'fitWomandisabled'} subscription={{value: true}}>
                        {({input, meta}) => (
                          <>
                            <Field
                              name={`fitWoman`}
                              component={SwitchField}
                              disabled={input.value}
                              maxLength={26}
                              subscription={{value: true}}
                              allowNull
                              type="checkbox"
                            >
                              {props => (
                                <>
                                  <OnChange name={props.input.name}>
                                    {value => {
                                      if (!input.value) {
                                        onChangeField(props.input.name, value);
                                      }
                                    }}
                                  </OnChange>
                                  <MySwitchField
                                    label={'Woman'}
                                    disabled={props.disabled}
                                    input={props.input}
                                  />
                                </>
                              )}
                            </Field>
                            {input.value && fieldDisabledText}
                          </>
                        )}
                      </Field>
                    </Col>
                  </Row>
                </div>
                <div style={{marginBottom: '2.9rem'}} />
                <div>
                  <TranslationSetting selectedField={'bulletpoints'} />
                  <Field name={'bulletpoints2disabled'} subscription={{value: true}}>
                    {({input, meta}) => (
                      <>
                        <Field
                          name={`bulletpoints2`}
                          component={TextField}
                          disabled={input.value}
                          maxLength={26}
                          subscription={{value: true}}
                          allowNull={true}
                        >
                          {props => (
                            <>
                              <OnChange name={props.input.name}>
                                {value => {
                                  if (!input.value) {
                                    onChangeTranslatedField(props.input.name, value);
                                    onChangeField(props.input.name, value);
                                  }
                                }}
                              </OnChange>
                              <TextArea
                                input={props.input}
                                meta={props.meta}
                                atData={tags.input.value}
                                autoSize
                                maxLength={255}
                                showCount
                                rows={1}
                                size="large"
                                disabled={props.disabled}
                                value={props.input.value}
                                placeholder={'Bulletpoint 2 goes here'}
                                onChange={props.input.onChange}
                              />
                            </>
                          )}
                        </Field>
                        {input.value && fieldDisabledText}
                      </>
                    )}
                  </Field>
                </div>
              </Col>
            </Row>
          </Panel>
        </Collapse>
      )}
      {(!valueOfEditSettings || valueOfEditSettings.designMBA === true) && <Divider />}
      <Col xs={0} sm={0} md={0} lg={0} xl={12} xxl={12} span={12} />
      <Col xs={24} sm={24} md={24} lg={24} xl={12} xxl={12} span={12}>
        <SubmitAndTransferButtons
          form={form}
          handleSubmit={handleSubmit}
          pristine={pristine}
          submitting={submitting}
        />
      </Col>
    </>
  );
};

export default General;
