import React from 'react';
import { Col, Divider, Input, Space, Spin, Tag, Typography } from 'antd';
import _ from 'lodash';
import { Field, useField } from 'react-final-form';
import { TextField } from 'react-final-form-antd';
import { OnChange } from 'react-final-form-listeners';
import Tags from '../../input/Tags';
import { fieldDisabledText } from '../../input/fieldDisabledText';
import StyledBadge from '../../input/StyledBadge';
import {
  designTeepublic,
  teepublicColors,
  teepublicProduct,
  teepublicProductEnabled,
  teepublicShirtPrintingIsEnabled,
  teepublicShirtSelection,
} from '../../../utils/constants/constants';
import Teepublic from '../../template/teepublic/Teepublic';
import { useQuery } from 'react-query';
import { fetch2 } from '../../../utils/dataAccess';
import TranslationSetting from '../TMSetting';

const { TextArea } = Input;
const { Title, Text } = Typography;

const DesignTeepublic = ({ handleSubmit, form, currentTab, onChangeField, submitting, pristine }) => {
  const {
    isLoading: isLoadingColor,
    error: errorColor,
    data: dataColor
  } = useQuery("template_settings_colors?isTeepublic=true", () =>
    fetch2("/template_settings_colors?pagination=false&isTeepublic=true")
  );

  const { input: { value: teepublicColorsdisableddisabled } } = useField(designTeepublic + "." + teepublicColors + "disabled", { subscription: { value: true } });
  const { input: { value: teepublicProductEnableddisabled } } = useField(designTeepublic + "." + teepublicProductEnabled + "disabled", { subscription: { value: true } });
  const { input: { value: teepublicProductdisabled } } = useField(designTeepublic + "." + teepublicProduct + "disabled", { subscription: { value: true } });
  const { input: { value: teepublicShirtPrintingIsEnableddisabled } } = useField(designTeepublic + "." + teepublicShirtPrintingIsEnabled + "disabled", { subscription: { value: true } });
  const { input: { value: teepublicShirtSelectiondisabled } } = useField(designTeepublic + "." + teepublicShirtSelection + "disabled", { subscription: { value: true } });

  return (
    <>
      <Col xs={24} sm={24} md={24} lg={24} xl={12}>
        <Divider> {_.startCase(currentTab)}</Divider>
        <div>
          <div>
            <Field name={designTeepublic + "." + "title50disabled"} subscription={{ value: true }}>
              {({ input, meta }) => (
                <>
                  <Field
                    name={designTeepublic + "." + `title50`}
                    disabled={input.value}
                    maxLength={50}
                    subscription={{ value: true }}
                    allowNull={true}
                  >
                    {props => (
                      <>
                        <OnChange name={designTeepublic + "." + "title50"}>
                          {value => {
                            if (!input.value) {
                              onChangeField(props.input.name, value);
                            }
                          }}
                        </OnChange>
                        <Title level={5}>
                          <Space align="baseline">
                            Title
                            <StyledBadge count={50} selected={true} touched={props.input.value} />
                          </Space>
                        </Title>
                        <TextArea
                          maxLength={50}
                          showCount
                          autoSize
                          rows={1}
                          size="large"
                          disabled={props.disabled}
                          value={props.input.value}
                          onChange={props.input.onChange}
                        />
                      </>
                    )}
                  </Field>
                  <br />
                  <br />
                  {input.value && fieldDisabledText}
                </>
              )}
            </Field>
          </div>
          <div>
            <Field
              name={designTeepublic + "." + "description200disabled"}
              subscription={{ value: true }}
            >
              {({ input }) => (
                <>
                  <Field
                    name={designTeepublic + "." + `description200`}
                    component={TextField}
                    disabled={input.value}
                    maxLength={200}
                    subscription={{ value: true }}
                    allowNull={true}
                  >
                    {props => (
                      <>
                        <OnChange name={designTeepublic + "." + "description200"}>
                          {value => {
                            if (!input.value) {
                              onChangeField(props.input.name, value);
                            }
                          }}
                        </OnChange>
                        <Title level={5}>
                          <Space align="baseline">
                            Description
                            <StyledBadge
                              count={200}
                              overflowCount={999}
                              selected={true}
                              touched={props.input.value}
                            />
                          </Space>
                        </Title>
                        <TextArea
                          size="large"
                          disabled={props.disabled}
                          value={props.input.value}
                          maxLength={200}
                          showCount
                          rows={6}
                          onChange={props.input.onChange}
                        />
                      </>
                    )}
                  </Field>
                  {input.value && fieldDisabledText}
                </>
              )}
            </Field>
          </div>
          <div>
            <br />
            <br />
            <Title level={5}>
              <Space align="baseline">
                Tags
                <Tag color="rgba(171,168,255,0.76)">15</Tag>
              </Space>
            </Title>
            <Field name={designTeepublic + "." + "tagsdisabled"} subscription={{ value: true }}>
              {({ input }) => (
                <>
                  <Field
                    name={designTeepublic + "." + `tags`}
                    // component={Tags}
                    disabled={input.value}
                    subscription={{ value: true }}
                    allowNull={true}
                  >
                    {props => (
                      <>
                        <OnChange name={designTeepublic + "." + "tags"}>
                          {value => {
                            if (!input.value) {
                              onChangeField(props.input.name, value);
                            }
                          }}
                        </OnChange>
                        <Tags
                          disabled={input.value}
                          input={props.input}
                          color={"rgba(171,168,255,0.76)"}
                          maxLength={15}
                        />
                      </>
                    )}
                  </Field>
                  <br />
                  <br />
                  {input.value && fieldDisabledText}
                </>
              )}
            </Field>
          </div>
        </div>
        <div>
          <Field name={designTeepublic + '.' + 'teepublicIddisabled'} subscription={{ value: true }}>
            {({ input, meta }) => (
              <>
                <Field
                  name={designTeepublic + '.' + `teepublicId`}
                  component={TextField}
                  disabled={input.value}
                  maxLength={26}
                  subscription={{ value: true }}
                  allowNull={true}
                >
                  {props => (
                    <>
                      <OnChange name={designTeepublic + '.' + 'teepublicId'}>
                        {value => {
                          if (!input.value) {
                            // onChangeTranslatedField(props.input.name, value);
                            onChangeField(props.input.name, value);
                          }
                        }}
                      </OnChange>
                      <Title level={5}>
                        <Space align="baseline">
                          ID
                          {/*<StyledBadge count={50} selected={true} touched={props.input.value} />*/}
                        </Space>
                        <TranslationSetting style={{ align: 'right' }} selectedField={props.input.name} />
                      </Title>
                      <TextArea
                        maxLength={255}
                        showCount
                        rows={1}
                        autoSize
                        size="large"
                        disabled={props.disabled}
                        value={props.input.value}
                        placeholder={'Id for teepublic comes here'}
                        onChange={props.input.onChange}
                      />
                    </>
                  )}
                </Field>
                <br />
                <br />
                {input.value && fieldDisabledText}
              </>
            )}
          </Field>
        </div>
      </Col>
      <Col>
        <Spin spinning={isLoadingColor}>
          <Title level={5}>Product colors</Title>
          <Text type="secondary">
            Activate the background colors that you want to make available for your enabled
            products.
          </Text>
          <br />
          <br />
          <OnChange name={designTeepublic + "." + teepublicColors}>
            {value => onChangeField(designTeepublic + "." + teepublicColors, value)}
          </OnChange>
          <OnChange name={designTeepublic + "." + teepublicProductEnabled}>
            {value => onChangeField(designTeepublic + "." + teepublicProductEnabled, value)}
          </OnChange>
          <OnChange name={designTeepublic + "." + teepublicProduct}>
            {value => onChangeField(designTeepublic + "." + teepublicProduct, value)}
          </OnChange>
          <OnChange name={designTeepublic + "." + teepublicShirtPrintingIsEnabled}>
            {value => onChangeField(designTeepublic + "." + teepublicShirtPrintingIsEnabled, value)}
          </OnChange>
          <OnChange name={designTeepublic + "." + teepublicShirtSelection}>
            {value => onChangeField(designTeepublic + "." + teepublicShirtSelection, value)}
          </OnChange>
          {(!teepublicColorsdisableddisabled
            && !teepublicProductEnableddisabled
            && !teepublicProductdisabled
            && !teepublicShirtPrintingIsEnableddisabled
            && !teepublicShirtSelectiondisabled
          ) && dataColor &&
          <Teepublic wrapperCol={{ span: 12 }} colors={dataColor} form={form} prefix={designTeepublic + "."} />}
          {(teepublicColorsdisableddisabled
            || teepublicProductEnableddisabled
            || teepublicProductdisabled
            || teepublicShirtPrintingIsEnableddisabled
            || teepublicShirtSelectiondisabled
          ) && fieldDisabledText}
        </Spin>
      </Col>
    </>
  );
};

export default DesignTeepublic
