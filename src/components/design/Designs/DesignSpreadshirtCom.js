import React from 'react';
import { Col, Divider, Input, Radio, Row, Space, Tag, Typography } from "antd";
import _ from 'lodash';
import { Field, useField } from "react-final-form";
import { OnChange } from 'react-final-form-listeners';
import { TextField } from 'react-final-form-antd';
import Tags from '../../input/Tags';
import { fieldDisabledText } from '../../input/fieldDisabledText';
import StyledBadge from '../../input/StyledBadge';
import { designSpreadshirtCom } from "../../../utils/constants/constants";
import { ExclamationCircleOutlined } from '@ant-design/icons';
import TranslationSetting from '../TMSetting';
import MyTextField from '../../input/MyFields/MyTextField';
import MySwitch from "../../input/MyFields/MySwitch";
import WhenFieldChanges from "../../input/MyFields/WhenFieldChanges";
import MyRadioGroup from "../../input/MyFields/MyRadioGroup";

const { TextArea } = Input;
const { Title } = Typography;

const DesignSpreadshirtCom = ({
                                handleSubmit,
                                form,
                                currentTab,
                                onChangeField,
                                submitting,
                                pristine,
                              }) => {
  const spreadshirtComIsShop = useField(designSpreadshirtCom+'.spreadshirtComIsShop', { subscription: { value: true },type:'checkbox' })
  return (
    <Col xs={24} sm={24} md={24} lg={24} xl={12}>
      <Divider> {_.startCase(currentTab)}</Divider>
      <div className={'DesignSpreadshirtCom'}>
        <div>
          <Field name={designSpreadshirtCom + '.' + 'title50disabled'} subscription={{ value: true }}>
            {({ input, meta }) => (
              <>
                <Field
                  name={designSpreadshirtCom + '.' + `title50`}
                  disabled={input.value}
                  maxLength={50}
                  subscription={{ value: true }}
                  allowNull={true}
                >
                  {props => (
                    <>
                      <OnChange name={designSpreadshirtCom + '.' + 'title50'}>
                        {value => {
                          if (!input.value) {
                            onChangeField(props.input.name, value);
                          }
                        }}
                      </OnChange>
                      <Title level={5}>
                        <Space align="baseline">
                          Title
                          <StyledBadge count={50} selected={true} touched={props.input.value} />
                        </Space>
                      </Title>
                      <TextArea
                        maxLength={50}
                        showCount
                        rows={1}
                        autoSize
                        size="large"
                        disabled={props.disabled}
                        value={props.input.value}
                        onChange={props.input.onChange}
                      />
                    </>
                  )}
                </Field>
                <br />
                <br />
                {input.value && fieldDisabledText}
              </>
            )}
          </Field>
        </div>
        <div>
          <Field
            name={designSpreadshirtCom + '.' + 'description200disabled'}
            subscription={{ value: true }}
          >
            {({ input }) => (
              <>
                <Field
                  name={designSpreadshirtCom + '.' + `description200`}
                  component={TextField}
                  disabled={input.value}
                  maxLength={200}
                  subscription={{ value: true }}
                  allowNull={true}
                >
                  {props => (
                    <>
                      <OnChange name={designSpreadshirtCom + '.' + 'description200'}>
                        {value => {
                          if (!input.value) {
                            onChangeField(props.input.name, value);
                          }
                        }}
                      </OnChange>
                      <Title level={5}>
                        <Space align="baseline">
                          Description
                          <StyledBadge
                            count={200}
                            overflowCount={999}
                            selected={true}
                            touched={props.input.value}
                          />
                        </Space>
                      </Title>
                      <TextArea
                        size="large"
                        disabled={props.disabled}
                        value={props.input.value}
                        maxLength={200}
                        showCount
                        rows={6}
                        onChange={props.input.onChange}
                      />
                    </>
                  )}
                </Field>
                {input.value && fieldDisabledText}
              </>
            )}
          </Field>
        </div>
        <div>
          <br />
          <br />
          <Title level={5}>
            <Space align="baseline">
              Tags
              <Tag color="#91d5ff">25</Tag>
            </Space>
          </Title>
          <Field name={designSpreadshirtCom + '.' + 'tagsdisabled'} subscription={{ value: true }}>
            {({ input, meta }) => (
              <>
                <Field
                  name={designSpreadshirtCom + '.' + `tags`}
                  disabled={input.value}
                  subscription={{ value: true }}
                  allowNull={true}
                >
                  {props => (
                    <>
                      <OnChange name={designSpreadshirtCom + '.' + 'tags'}>
                        {value => {
                          if (!input.value) {
                            onChangeField(props.input.name, value);
                          }
                        }}
                      </OnChange>
                      <Tags
                        disabled={input.value}
                        input={props.input}
                        color={'#91d5ff'}
                        maxLength={25}
                      />
                    </>
                  )}
                </Field>
                <br />
                <br />
                {input.value && fieldDisabledText}
              </>
            )}
          </Field>
        </div>
      </div>
      <div><Field name={designSpreadshirtCom + '.' + 'spreadshirtIddisabled'} subscription={{ value: true }}>
        {({ input, meta }) => (
          <>
            <Field
              name={designSpreadshirtCom + '.' + `spreadshirtId`}
              component={TextField}
              disabled={input.value}
              maxLength={26}
              subscription={{ value: true }}
              allowNull={true}
            >
              {props => (
                <>
                  <OnChange name={designSpreadshirtCom + '.' + 'spreadshirtId'}>
                    {value => {
                      if (!input.value) {
                        // onChangeTranslatedField(props.input.name, value);
                        onChangeField(props.input.name, value);
                      }
                    }}
                  </OnChange>
                  <Title level={5}>
                    <Space align="baseline">
                      ID
                      {/*<StyledBadge count={50} selected={true} touched={props.input.value} />*/}
                    </Space>
                    <TranslationSetting style={{ align: 'right' }} selectedField={props.input.name} />
                  </Title>
                  <TextArea
                    maxLength={255}
                    showCount
                    rows={1}
                    autoSize
                    size="large"
                    disabled={props.disabled}
                    value={props.input.value}
                    placeholder={'Id for spreadshirt comes here'}
                    onChange={props.input.onChange}
                  />
                </>
              )}
            </Field>
            <br />
            <br />
            {input.value && fieldDisabledText}
          </>
        )}
      </Field></div>
      {/*<br />*/}
      {/*<Title level={5}> <ExclamationCircleOutlined style={{ color: '#7f67ff' }} /> Design Template</Title>*/}
      {/*<br />*/}

      <>
        <Title level={5}> <ExclamationCircleOutlined style={{color: '#7f67ff'}} /> Design Template </Title>
        <br />
        <Field name={designSpreadshirtCom + '.' + 'spreadshirtComSelectDesignTemplatedisabled'}
               subscription={{ value: true }}>
          {({ input }) => (
            <>
              <Field
                subscription={{ value: true }}
                name={designSpreadshirtCom + '.' + "spreadshirtComSelectDesignTemplate"}
                hasFeedback
                placeholder={'select templates'}
                size={'large'}
              >
                {props => (
                  <>
                    <OnChange name={props.input.name}>
                      {value => {
                        if (!input.value) {
                          onChangeField(props.input.name, value);
                        }
                      }}
                    </OnChange>
                    <MyTextField disabled={input.value} {...props} />
                  </>
                )}
              </Field>
              {input.value && fieldDisabledText}
            </>
          )}
        </Field>
        <Row gutter={[16, 16]}>
          <Col span={4}>
            <Title level={5}>  <ExclamationCircleOutlined style={{color: '#7f67ff'}} /> Marketplce:</Title>
          </Col>
          <Col span={24}>
            <Field name={designSpreadshirtCom + '.' + 'spreadshirtComIsMarketplacedisabled'}
                   subscription={{ value: true }}>
              {({ input }) => (
                <>
                  <Field
                    subscription={{ value: true }}
                    name={designSpreadshirtCom + '.' + 'spreadshirtComIsMarketplace'}
                    initialValue={null}
                    hasFeedback
                    disabled={input.value}
                    allowNull
                    type={"checkbox"}
                    size={'large'}>
                    {props => (
                      <>
                        <OnChange name={props.input.name}>
                          {value => {
                            if (!input.value) {
                              onChangeField(props.input.name, value);
                            }
                          }}
                        </OnChange>
                        <MySwitch {...props} />
                      </>
                    )}
                  </Field>
                  {input.value && fieldDisabledText}
                </>
              )}
            </Field>
          </Col>
          <Col span={24}>
            <Title level={5}> <ExclamationCircleOutlined style={{color: '#7f67ff'}} /> Shop:</Title>
          </Col>
          <Col span={4}>
            <Field name={designSpreadshirtCom + '.' + 'spreadshirtComIsShopdisabled'}
                   subscription={{ value: true }}>
              {({ input }) => (
                <>
                  <OnChange name={spreadshirtComIsShop.input.name}>
                    {value => {
                      if (!input.value) {
                        onChangeField(spreadshirtComIsShop.input.name, value);
                      }
                    }}
                  </OnChange>
                  <MySwitch disabled={input.value} {...spreadshirtComIsShop} />
                  {input.value && fieldDisabledText}
                </>
              )}
            </Field>
          </Col>
          <WhenFieldChanges
            field={designSpreadshirtCom + '.' +  'spreadshirtComIsShop'}
            becomes={false}
            set={ designSpreadshirtCom + '.' + 'spreadshirtComShop'}
            to={undefined}
          />
          <Col span={20}>
            <Field name={designSpreadshirtCom + '.' + 'spreadshirtComShopdisabled'}
                   subscription={{ value: true }}>
              {({ input }) => (
                <>
                  <Field
                    subscription={{ value: true }}
                    name={designSpreadshirtCom + '.'+ 'spreadshirtComShop'}
                    initialValue={null}
                    hasFeedback
                    allowNull
                    placeholder={'(optional)'}
                    disabled={input.value || !spreadshirtComIsShop.input.value}
                    size={'large'}>
                    {props => (
                      <>
                        <OnChange name={props.input.name}>
                          {value => {
                            if (!input.value) {
                              onChangeField(props.input.name, value);
                            }
                          }}
                        </OnChange>
                        <MyTextField {...props}  disabled={input.value || !spreadshirtComIsShop.input.checked} />
                      </>
                    )}

                  </Field>

                  {input.value && fieldDisabledText}
                </>
              )}
            </Field>
          </Col>
        </Row>
      </>

      {/*<Divider />*/}
      {/*<SubmitAndTransferButtons*/}
      {/*  form={form}*/}
      {/*  handleSubmit={handleSubmit}*/}
      {/*  pristine={pristine}*/}
      {/*  submitting={submitting}*/}
      {/*/>*/}
    </Col>
  );
};

export default DesignSpreadshirtCom;
