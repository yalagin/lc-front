import React from 'react';
import { Col, Divider, Input, Space, Tag, Typography,Select } from 'antd';
import _ from 'lodash';
import { Field } from 'react-final-form';
import { TextField } from 'react-final-form-antd';
import { OnChange } from 'react-final-form-listeners';
import Tags from '../../input/Tags';
import { fieldDisabledText } from '../../input/fieldDisabledText';
import StyledBadge from '../../input/StyledBadge';
import { designDisplate } from "../../../utils/constants/constants";
import MyColorPicker from '../../input/MyFields/MyColorPicker';
import MySelect from "../../input/MyFields/MySelect";

const { TextArea } = Input;
const { Title } = Typography;

const DesignDisplate = ({ handleSubmit, form, currentTab, onChangeField, submitting, pristine }) => {

  return (
    <Col xs={24} sm={24} md={24} lg={24} xl={12}>
      <Divider> {_.startCase(currentTab)}</Divider>
      <div>
        <div>
          <Field name={designDisplate + '.' + 'title26disabled'} subscription={{ value: true }}>
            {({ input, meta }) => (
              <>
                <Field
                  name={designDisplate + '.' + `title26`}
                  disabled={input.value}
                  maxLength={50}
                  subscription={{ value: true }}
                  allowNull={true}
                >
                  {props => (
                    <>
                      <OnChange name={designDisplate + '.' + 'title26'}>
                        {value => {
                          if (!input.value) {
                            onChangeField(props.input.name, value);
                          }
                        }}
                      </OnChange>
                      <Title level={5}>
                        <Space align="baseline">
                          Title
                          <StyledBadge count={26} selected={true} touched={props.input.value} />
                        </Space>
                      </Title>
                      <TextArea
                        maxLength={26}
                        showCount
                        autoSize
                        rows={1}
                        size="large"
                        disabled={props.disabled}
                        value={props.input.value}
                        onChange={props.input.onChange}
                      />
                    </>
                  )}
                </Field>
                <br />
                <br />
                {input.value && fieldDisabledText}
              </>
            )}
          </Field>
        </div>
        <div>
          <Field
            name={designDisplate + '.' + 'description140disabled'}
            subscription={{ value: true }}
          >
            {({ input }) => (
              <>
                <Field
                  name={designDisplate + '.' + `description140`}
                  component={TextField}
                  disabled={input.value}
                  maxLength={200}
                  subscription={{ value: true }}
                  allowNull={true}
                >
                  {props => (
                    <>
                      <OnChange name={designDisplate + '.' + 'description140'}>
                        {value => {
                          if (!input.value) {
                            onChangeField(props.input.name, value);
                          }
                        }}
                      </OnChange>
                      <Title level={5}>
                        <Space align="baseline">
                          Description
                          <StyledBadge
                            count={140}
                            overflowCount={999}
                            selected={true}
                            touched={props.input.value}
                          />
                        </Space>
                      </Title>
                      <TextArea
                        size="large"
                        disabled={props.disabled}
                        value={props.input.value}
                        maxLength={140}
                        showCount
                        rows={6}
                        onChange={props.input.onChange}
                      />
                    </>
                  )}
                </Field>
                <br />
                <br />
                {input.value && fieldDisabledText}
              </>
            )}
          </Field>
        </div>
        <div>
          <Title level={5}>
            <Space align="baseline">
              Tags
              <Tag color="rgba(255,168,249,0.76)">20</Tag>
            </Space>
          </Title>
          <Field name={designDisplate + '.' + 'tagsdisabled'} subscription={{ value: true }}>
            {({ input, meta }) => (
              <>
                <Field
                  name={designDisplate + '.' + `tags`}
                  // component={Tags}
                  disabled={input.value}
                  subscription={{ value: true }}
                  allowNull={true}
                  parse={x => x}
                >
                  {props => (
                    <>
                      <OnChange name={designDisplate + '.' + 'tags'}>
                        {value => {
                          if (!input.value) {
                            onChangeField(props.input.name, value);
                          }
                        }}
                      </OnChange>
                      <Tags
                        disabled={input.value}
                        input={props.input}
                        color={'rgba(255,168,249,0.76)'}
                        maxLength={20}
                      />
                    </>
                  )}
                </Field>
                <br />
                <br />
                {input.value && fieldDisabledText}
              </>
            )}
          </Field>
        </div>
        <div>
          <Field
            name={designDisplate + '.' + 'collectiondisabled'}
            subscription={{ value: true }}
          >
            {({ input }) => (
              <>
                <Field
                  name={designDisplate + '.' + `collection`}
                  component={TextField}
                  disabled={input.value}
                  subscription={{ value: true }}
                  allowNull={true}
                >
                  {props => (
                    <>
                      <OnChange name={props.input.name}>
                        {value => {
                          if (!input.value) {
                            onChangeField(props.input.name, value);
                          }
                        }}
                      </OnChange>
                      <Title level={5}>
                        <Space align="baseline">
                          Collection
                        </Space>
                      </Title>
                      <TextArea
                        size="large"
                        disabled={props.disabled}
                        value={props.input.value}
                        // maxLength={200}
                        showCount
                        rows={1}
                        onChange={props.input.onChange}
                      />
                    </>
                  )}
                </Field>
                <br />
                <br />
                {input.value && fieldDisabledText}
              </>
            )}
          </Field>
        </div>
        <div>
          <Title level={5}>
            <Space align="baseline">
              Category
              <Tag color="grey">3</Tag>
            </Space>
          </Title>
          <Field name={designDisplate + '.' + 'categorydisabled'} subscription={{ value: true }}>
            {({ input, meta }) => (
              <>
                <Field
                  name={designDisplate + '.' + `category`}
                  // component={Tags}
                  disabled={input.value}
                  subscription={{ value: true }}
                  allowNull={true}
                  parse={x => x}
                >
                  {props => (
                    <>
                      <OnChange name={designDisplate + '.' + 'category'}>
                        {value => {
                          if (!input.value) {
                            onChangeField(props.input.name, value);
                          }
                        }}
                      </OnChange>
                      <Tags
                        disabled={input.value}
                        input={props.input}
                        color={'grey'}
                        maxLength={3}
                        limitOfTags={3}
                      />
                    </>
                  )}
                </Field>
                <br />
                <br />
                {input.value && fieldDisabledText}
              </>
            )}
          </Field>
        </div>

        <div>
          <Field name={designDisplate + "." + "displateColorPickerdisabled"} subscription={{ value: true }}>
            {({ input }) => (
              <>
                <OnChange name={designDisplate + "." + "displateColorPicker"}>
                  {value => {
                    if (!input.value) {
                      onChangeField(designDisplate + "." + "displateColorPicker", value);
                    }
                  }}
                </OnChange>
                {!input.value && <Field
                  subscription={{ value: true }}
                  name={designDisplate + '.' + "displateColorPicker"}
                  component={MyColorPicker}
                  hasFeedback
                />}

                {input.value && fieldDisabledText}
              </>
            )}
          </Field>
        </div>
      </div>
      {/*<Divider />*/}
      {/*<SubmitAndTransferButtons*/}
      {/*  form={form}*/}
      {/*  handleSubmit={handleSubmit}*/}
      {/*  pristine={pristine}*/}
      {/*  submitting={submitting}*/}
      {/*/>*/}
    </Col>
  );
};

export default DesignDisplate;
