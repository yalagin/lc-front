import * as constants from '../../utils/constants/constants';
import General from './Designs/General';
import DesignSpreadshirtCom from './Designs/DesignSpreadshirtCom';
import DesignSpreadshirtEu from './Designs/DesignSpreadshirtEu';
import DesignRedBubble from './Designs/DesignRedBubble';
import DesignSociety6 from './Designs/DesignSociety6';
import DesignTeepublic from './Designs/DesignTeepublic';
import DesignShirtee from './Designs/DesignShirtee';
import DesignZazzle from './Designs/DesignZazzle';
import DesignPrintful from './Designs/DesignPrintful';
import DesignMBA from './Designs/DesignMBA';
import DesignTeespring from './Designs/DesignTeespring';
import DesignDisplate from './Designs/DesignDisplate';
import React from 'react';

function DesignSwitcher(props) {
  return (
    <>
      {props.currentTab === constants.general && (
        <General
          handleSubmit={props.handleSubmit}
          form={props.form}
          currentTab={props.currentTab}
          submitting={props.submitting}
          pristine={props.pristine}
          onChangeField={props.onChangeField}
          onChangeTranslatedField={props.onChangeTranslatedField}
        />
      )}
      {props.currentTab === constants.designSpreadshirtCom && (
        <DesignSpreadshirtCom
          handleSubmit={props.handleSubmit}
          form={props.form}
          currentTab={props.currentTab}
          submitting={props.submitting}
          pristine={props.pristine}
          onChangeField={props.onChangeField}
        />
      )}
      {props.currentTab === constants.designSpreadshirtEu && (
        <DesignSpreadshirtEu
          handleSubmit={props.handleSubmit}
          form={props.form}
          currentTab={props.currentTab}
          submitting={props.submitting}
          pristine={props.pristine}
          onChangeField={props.onChangeField}
          onChangeTranslatedField={props.onChangeTranslatedField}
        />
      )}
      {props.currentTab === constants.designRedBubble && (
        <DesignRedBubble
          handleSubmit={props.handleSubmit}
          form={props.form}
          currentTab={props.currentTab}
          submitting={props.submitting}
          pristine={props.pristine}
          onChangeField={props.onChangeField}
          onChangeTranslatedField={props.onChangeTranslatedField}
        />
      )}
      {props.currentTab === constants.designSociety6 && (
        <DesignSociety6
          handleSubmit={props.handleSubmit}
          form={props.form}
          currentTab={props.currentTab}
          submitting={props.submitting}
          pristine={props.pristine}
          onChangeField={props.onChangeField}
        />
      )}
      {props.currentTab === constants.designTeepublic && (
        <DesignTeepublic
          handleSubmit={props.handleSubmit}
          form={props.form}
          currentTab={props.currentTab}
          submitting={props.submitting}
          pristine={props.pristine}
          onChangeField={props.onChangeField}
        />
      )}
      {props.currentTab === constants.designShirtee && (
        <DesignShirtee
          handleSubmit={props.handleSubmit}
          form={props.form}
          currentTab={props.currentTab}
          submitting={props.submitting}
          pristine={props.pristine}
          onChangeField={props.onChangeField}
          onChangeTranslatedField={props.onChangeTranslatedField}
        />
      )}
      {props.currentTab === constants.designZazzle && (
        <DesignZazzle
          handleSubmit={props.handleSubmit}
          form={props.form}
          currentTab={props.currentTab}
          submitting={props.submitting}
          pristine={props.pristine}
          onChangeField={props.onChangeField}
        />
      )}
      {props.currentTab === constants.designPrintful && (
        <DesignPrintful
          handleSubmit={props.handleSubmit}
          form={props.form}
          currentTab={props.currentTab}
          submitting={props.submitting}
          pristine={props.pristine}
          onChangeField={props.onChangeField}
        />
      )}
      {props.currentTab === constants.designMBA && (
        <DesignMBA
          handleSubmit={props.handleSubmit}
          form={props.form}
          currentTab={props.currentTab}
          submitting={props.submitting}
          pristine={props.pristine}
          onChangeField={props.onChangeField}
          onChangeTranslatedField={props.onChangeTranslatedField}
        />
      )}
      {props.currentTab === constants.designTeespring && (
        <DesignTeespring
          handleSubmit={props.handleSubmit}
          form={props.form}
          currentTab={props.currentTab}
          submitting={props.submitting}
          pristine={props.pristine}
          onChangeField={props.onChangeField}
        />
      )}
      {props.currentTab === constants.designDisplate && (
        <DesignDisplate
          handleSubmit={props.handleSubmit}
          form={props.form}
          currentTab={props.currentTab}
          submitting={props.submitting}
          pristine={props.pristine}
          onChangeField={props.onChangeField}
        />
      )}
    </>
  );
}

export default DesignSwitcher;
