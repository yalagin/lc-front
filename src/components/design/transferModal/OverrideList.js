import React, { useEffect, useState } from "react";
import _ from "lodash";
import { getListOfSelectedDesigns } from "../../../utils/designHelperFunctions";
import {
  arrayOfDescriptions,
  arrayOfMBAFields,
  arrayOfTitles,
  decodeTranslationObject,
  designMBA,
  HYDRAMEMBER,
  MBA, notTranslationFields, objectOfDesignFieldsForTransferring
} from "../../../utils/constants/constants";
import { designAndTheirLanguages } from "../../../utils/constants/translation";

const OverrideList = ({ values, setVisibleModal, handleOk, list, setList, transferAll }) => {
  const [checkList, setCheckList] = useState(false);
  useEffect(() => {
    setList(getListOfOverwrittenFields());
    setCheckList(true);
  }, []);

  useEffect(() => {
    if (checkList && _.isEmpty(list)) {
      setVisibleModal(false);
      handleOk();
    }
  }, [checkList]);

  function compareDestinationAndTranslatedValueIfNotSameAddToList(member, translation, field, design, mappedOverridableFields, destinationValue) {
    const translationValue = _.get(member, ["translations", translation, field]);
    if (!_.isEqual(destinationValue, translationValue) && !_.isEmpty(destinationValue)) {
      if (_.isEmpty(_.get(mappedOverridableFields, [design, translation]))) {
        _.set(mappedOverridableFields, [design, translation], []);
      }
      //below if will remove duplicates for each different value from  hydramember
      // possible counter, for how many designs will be rewritten
      if(!mappedOverridableFields[design][translation].includes(field))
      _.set(mappedOverridableFields, [design, translation], [
        ...mappedOverridableFields[design][translation],
        field
      ]);
    }
  }

  const checkField = (design, field, mappedOverridableFields) => {
    const members = values[HYDRAMEMBER];
    if (_.isArray(members)) {
      for (const member of members) {
        if (member.copyFromMaster || transferAll) {
          if (_.isEmpty(member.translations)) {
            return;
          }
          for (const translation in member.translations) {
            if (member.translations.hasOwnProperty(translation)) {
              if (_.has(designAndTheirLanguages, design)&& objectOfDesignFieldsForTransferring[design].includes(field)&&!notTranslationFields.includes(field)) {
                const destinationValue = _.get(member, _.compact([design, "translations", translation, field]));
                compareDestinationAndTranslatedValueIfNotSameAddToList(member, translation, field, design, mappedOverridableFields, destinationValue);
              }
              //for designs without translations
              if (translation === "en" && !_.has(designAndTheirLanguages, design)&& objectOfDesignFieldsForTransferring[design].includes(field)) {
                const destinationValue = _.get(member, _.compact([design, field]));
                compareDestinationAndTranslatedValueIfNotSameAddToList(member, translation, field, design, mappedOverridableFields, destinationValue);
              }
            }
          }
        }
      }
    }
  };

  const getListOfOverwrittenFields = () => {
    const arrayOfTransferDesigns = getListOfSelectedDesigns(values);
    const mappedOverridableFields = {};

    arrayOfTransferDesigns.forEach(design => {
      if (_.isObject(values["Title"])) {
        if (values["Title"].hasOwnProperty(design) && !values["Title"][design]) {
          return null;
        }
      }
      arrayOfTitles.forEach(title => checkField(design, title, mappedOverridableFields));
    });
    arrayOfTransferDesigns.forEach(design => {
      if (_.isObject(values["Description"])) {
        if (values["Description"].hasOwnProperty(design) && !values["Description"][design]) {
          return null;
        }
      }
      arrayOfDescriptions.forEach(
        description => checkField(design, description, mappedOverridableFields),
        mappedOverridableFields
      );
    });
    arrayOfTransferDesigns.forEach(design => {
      if (_.isObject(values["Tags"])) {
        if (values["Tags"].hasOwnProperty(design) && !values["Tags"][design]) {
          return null;
        }
      }
      checkField(design, "tags", mappedOverridableFields);
    });
    if (arrayOfTransferDesigns.includes(designMBA)) {
      arrayOfMBAFields.forEach(mbaField => {
        if (_.isObject(values[MBA])) {
          if (values[MBA].hasOwnProperty(mbaField) && !values[MBA][mbaField]) {
            return null;
          }
        }
        checkField(designMBA, mbaField, mappedOverridableFields);
      });
    }

    return mappedOverridableFields;
  };

  const getData = Object.keys(list).map(design => (
    <div key={design}>
      {_.startCase(design.slice(6))} - {console.log(list)}
      {Object.keys(list[design]).map((language, index) => (
        <React.Fragment key={language}>
          {index > 0 && ","} {decodeTranslationObject[language]} -{" "}
          {Object.keys(list[design][language]).map((field, index) => (
            <React.Fragment key={list[design][language][field]}>
              {" "}
              {index > 0 && ","} {_.startCase(list[design][language][field].replace(/[0-9]/g, ""))}{" "}
            </React.Fragment>
          ))}
        </React.Fragment>
      ))}{" "}
    </div>
  ));

  return <div>{getData}</div>;
};

export default OverrideList;
