import React, { useState } from 'react';
import { FormSpy } from 'react-final-form';
import { Button, Modal, Result } from "antd";
import OverrideList from './OverrideList';

const ModalWindowWIthFormSpy = ({
                                  handleOk,
                                  visible,
                                  setVisible,
                                  handleNotOverrideFieldsWithDifferentValues,
                                  transferAll,
                                }) => {
  const [list, setList] = useState({});

  return (
    visible && (
      <Modal
        visible={visible}
        onOk={handleOk}
        onCancel={() => setVisible(false)}
        footer={[
          <Button key="back" onClick={() => setVisible(false)}>
            Cancel
          </Button>,
          <Button
            key="not_overriding"
            type="primary"
            ghost
            onClick={() => handleNotOverrideFieldsWithDifferentValues(list)}
          >
            Override other fields
          </Button>,
          <Button key="submit" type="primary" onClick={handleOk}>
            Override all
          </Button>,
        ]}
      >
        <>
          <FormSpy subscription={{ values: true }}>
            {({ values }) => {
              return (
                <Result
                  status="warning"
                  title="You might override your fields"
                  subTitle={
                    <OverrideList
                      values={values}
                      setVisibleModal={setVisible}
                      handleOk={handleOk}
                      list={list}
                      setList={setList}
                      transferAll={transferAll}
                    />
                  }
                />
              );
            }}
          </FormSpy>
        </>
      </Modal>
    )
  );
};
export default ModalWindowWIthFormSpy;
