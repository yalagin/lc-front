import React, { useEffect, useRef, useState } from "react";
import {Button, Card, Col, Modal, Popover, Row, Space, Typography} from 'antd';
import {FormSpy, useField} from 'react-final-form';
import AddToDesignCreatedAndUpdated from './EditStateOfDesignForm/AddToDesignCreatedAndUpdated';
import {creation, listing} from '../../utils/constants/constants';
import ImageCarousel from './ImageCarousel/ImageCarousel';
import {uploadImageToS3AndCreateMediaObjectAndDesign} from '../../actions/mediaobject/create';
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import StyledBottomRowTemplateList from './bottomButtonsOfCarousel/StyledBottomRowTemplateList';
import {LeftOutlined, RightOutlined} from '@ant-design/icons';
import MyCustomSettingGearOutlined from '../input/MyCustomSettingGearOutlined';
import PopoverTitle from './popover/PopoverTitle';
import PopoverEditContent from './popover/PopoverEditContent';
import useWindowDimensions from '../../utils/hooks/useWindowDimensions';
import useProject from '../../utils/hooks/useProject';
import DesignSwitcher from './DesignSwitcher';
import ButtonWithoutFocus from '../input/button/ButtonWithoutFocus';
import LanguageAndTabsSwitcher from './LanguageAndTabsSwitcher/LanguageAndTabsSwitcher';
import Creation from './CreationMode/Creation';
import useSaveEditOfDesignStateOnUpdate from './EditStateOfDesignForm/useSaveEditOfDesignStateOnUpdate';
import _ from 'lodash';
import ColorPicker from './ColorPicker/ColorPicker';
import { resetDesignFormToInitalState } from "../../actions/design/list";

const DesignForm = ({handleSubmit, pristine, submitting, form, dirtyFieldsSinceLastSubmit}) => {
  const {Title} = Typography;
  const {
    input: {value: currentTab, onChange: setCurrentTab},
  } = useField('currentTab', {subscription: {value: true}});
  const [openModal, setOpenModal] = useState(false);
  const [onePicInSlider, setOnePicInSlider] = useState(false);
  const {project} = useProject();
  const {height} = useWindowDimensions();
  useSaveEditOfDesignStateOnUpdate();
  const dispatch = useDispatch();

  const resetForm = useSelector(state =>state.design.list.resetForm,shallowEqual);

  useEffect(() => {
   if(resetForm === true){
     form.reset();
     dispatch(resetDesignFormToInitalState());
   }

   if(resetForm === "DESIGN_LIST_SET_FAKE_DATA"){
     form.mutators.setFakeData(form);
     console.log('test');
     // dispatch(resetDesignFormToInitalState());
   }
  }, [resetForm]);

  //todo uncomment
  // useEffect(() => {
  //   window.onbeforeunload = function () {
  //     if (!_.isEmpty(dirtyFieldsSinceLastSubmit)) {
  //       return 'are you sure?';
  //     }
  //   };
  //   return () => {
  //     window.onbeforeunload = null;
  //   };
  // }, [dirtyFieldsSinceLastSubmit]);
  const [mode, setMode] = useState(listing);

  const uploadImage = async options => {
    const fmData = new FormData();
    fmData.append('file', options.file);
    // dispatch(createMediaObjectAndDesign(fmData, project));
    dispatch(uploadImageToS3AndCreateMediaObjectAndDesign(options.file, project));
  };

  const handleDeleteDesigns = () => {
    form.mutators.removeDesignsButtonHandler(form);
    form.mutators.ifEmptyDisable(form);
  };

  const handleRemoveDesigns = () => {
    form.mutators.removeDesignsButtonHandler(form);
    form.mutators.ifEmptyDisable(form);
  };

  const onChangeField = (field, value) => form.mutators.onChangeMainField(field, value);
  const onChangeTranslatedField = (field, value) =>
    form.mutators.onChangeTranslatedField(field, value);

  const {
    input: {value: selectedLanguageValue, onChange: selectedLanguageOnChange},
  } = useField('selectedLanguage', {subscription: {value: true}, initialValue: 'en'});

  useEffect(() => {
    document.getElementById("filedsetid").disabled = true;
    console.log('disable')
  }, []);


  const { input: {value: disabled}, } = useField('disabledAll', {subscription: {value: true}});
  return (
    <form >
      <fieldset id={'filedsetid'}>
      <Space size={'middle'} className={'modes-switcher'}>
        <ButtonWithoutFocus
          type={mode === listing ? 'primary' : ' '}
          onClick={() => setMode(listing)}
        >
          Listing
        </ButtonWithoutFocus>
        <ButtonWithoutFocus
          id={"creation-mode-btn"}
          type={mode === creation ? 'primary' : ' '}
          onClick={() => setMode(creation)}
        >
          Creation
        </ButtonWithoutFocus>
      </Space>
      {mode === listing && (
        <>
          <LanguageAndTabsSwitcher
            onClick={() => form.mutators.setFakeData(form)}
            value={selectedLanguageValue}
            onChange={e => {
              selectedLanguageOnChange(e);
              form.mutators.selectedLanguageOnChange(form);
            }}
            setCurrentTab={setCurrentTab}
            currentTab={currentTab}
          />
        </>
      )}
      <AddToDesignCreatedAndUpdated form={form} />
      {mode === creation && <br />}
      {mode === creation && <br />}
      <Row gutter={[16, 16]}>
        <Col xs={24} sm={24} md={24} lg={24} xl={12}>
          <Space align="baseline">
            <Popover
              placement="bottomLeft"
              title={<PopoverTitle title={'Edit'} />}
              content={
                <div
                  id="forth-step-WorkspaceWalkthroughSettings"
                >
                  {mode === listing && <PopoverEditContent title={'Edit'} />}
                  <div style={{marginTop: '1rem'}}>
                    <ColorPicker />
                  </div>
                </div>
              }
              trigger="click"
            >
              <MyCustomSettingGearOutlined id={'third-step-WorkspaceWalkthroughSettings'}
                                           className={'third-step-WorkspaceWalkthroughSettings'} />
            </Popover>
            <Title level={4}> Edit</Title>
          </Space>
          <div className={'forth-step-WorkspaceWalkthroughIntroduction'} id={"image-carousel"}>
            <Card>
              <ImageCarousel
                form={form}
                setOpenModal={setOpenModal}
                uploadImage={uploadImage}
                onePicInSlider={onePicInSlider}
                setOnePicInSlider={setOnePicInSlider}
                handleDeleteDesigns={handleDeleteDesigns}
                handleRemoveDesigns={handleRemoveDesigns}
                settings={{
                  dots: true,
                  arrows: true,
                  speed: 500,
                  slidesPerRow: onePicInSlider ? 1 : 3,
                  rows: onePicInSlider ? 1 : 2,
                  nextArrow: <RightOutlined />,
                  prevArrow: <LeftOutlined />,
                  adaptiveHeight: false,
                  fade: true,
                  responsive: [
                    {
                      breakpoint: 1600,
                      settings: {
                        slidesPerRow: onePicInSlider ? 1 : 2,
                        rows: onePicInSlider ? 1 : 2,
                      },
                    },
                    {
                      breakpoint: 1200,
                      settings: {
                        slidesPerRow: onePicInSlider ? 1 : 3,
                        rows: onePicInSlider ? 1 : 2,
                      },
                    },
                  ],
                }}
              />
            </Card>
          </div>
        </Col>
        {mode === listing && (
          <DesignSwitcher
            currentTab={currentTab}
            handleSubmit={handleSubmit}
            form={form}
            submitting={submitting}
            pristine={pristine}
            onChangeField={onChangeField}
            onChangeTranslatedField={onChangeTranslatedField}
          />
        )}
        {mode === creation && (
          <Creation
            onChangeField={onChangeField}
            handleSubmit={handleSubmit}
            form={form}
            submitting={submitting}
            pristine={pristine}
          />
        )}
      </Row>
      <Modal
        title="Template list"
        centered
        visible={openModal}
        onOk={() => setOpenModal(false)}
        onCancel={() => setOpenModal(false)}
        width={'90%'}
        heigt={'90%'}
        footer={[
          <StyledBottomRowTemplateList
            uploadImage={uploadImage}
            form={form}
            setOnePicInSlider={setOnePicInSlider}
            setOpenModal={setOpenModal}
            handleDeleteDesigns={handleDeleteDesigns}
            handleRemoveDesigns={handleRemoveDesigns}
          />,
          <Button
            key="back"
            size={'large'}
            style={{marginBottom: '1rem'}}
            onClick={() => setOpenModal(false)}
          >
            Return
          </Button>,
          <Button
            key="submit"
            size={'large'}
            type="primary"
            /*loading={loading}*/ onClick={() => handleSubmit()}
          >
            Save
          </Button>,
        ]}
      >
        <ImageCarousel
          form={form}
          setOpenModal={setOpenModal}
          uploadImage={uploadImage}
          onePicInSlider={onePicInSlider}
          hideButtons={true}
          settings={{
            dots: true,
            arrows: true,
            speed: 500,
            slidesPerRow: onePicInSlider ? 1 : 6,
            rows: onePicInSlider ? 1 : height > 840 ? 2 : 1,
            nextArrow: <RightOutlined />,
            prevArrow: <LeftOutlined />,
            adaptiveHeight: false,
            fade: true,
          }}
        />
      </Modal>
      </fieldset>
    </form>
  );
};

export default DesignForm;
