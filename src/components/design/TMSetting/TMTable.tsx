import React, { Dispatch, SetStateAction, useCallback, useEffect, useMemo, useState } from "react";
import { Button, message, Modal, Typography } from "antd";
import styled from 'styled-components';
import { useField } from 'react-final-form';
import { fetch2 } from '../../../utils/dataAccess';
import qs from 'qs';
import { useMutation, useQueries } from 'react-query';
import _ from 'lodash';
import { SortOrder } from 'antd/es/table/interface';
import ExtendableTable from '../../trademarks/Check/ExtendableTable';
import { WhiteHeaderTable } from '../../styles/TableStyles';
import { UseQueryOptions } from "react-query/types/react/types";
import { splitSentenceToWords } from "../../../utils/utilsFunctionsTyped";

const { Title } = Typography;

export const TMStyledHeader = styled(Title)`
  margin-top: 12px;
  color: #31394D;
  opacity: 50%;
  cursor: pointer;

  :hover {
    color: #7f67ff;
  }
`;

const TMTable = ({
                   selectedFieldValue,
                   isModalVisible,
                   setIsModalVisible
                 }: { selectedFieldValue: string[] , isModalVisible: boolean, setIsModalVisible: Dispatch<SetStateAction<boolean>> }) => {
  const { input: { value: selectedLanguageValue } } = useField('selectedLanguage', { subscription: { value: true } });
  const [data, setData] = useState<{ data: []; keyword: string }[]>([]);

  function fetchTrademark(selectedFieldValue: string) {
    return fetch2(
      '/check-for-trade-mark?' +
      qs.stringify(
        {
          classes: [9, 16, 18, 20, 21, 22, 24, 25],
          country: [selectedLanguageValue.toString().toUpperCase()],
        },
        {
          arrayFormat: 'brackets',
          skipNulls: true,
        }
      ),
      {
        method: 'POST',
        body: JSON.stringify({ msg: [selectedFieldValue] }),
      }
    );
  }

  const arrayOfCheckingWords =  useCallback(
    ()=>  _.uniqWith(_.flattenDeep(selectedFieldValue.map((value: string) => splitSentenceToWords(value))), _.isEqual)
      .filter(function(value) {return value !== "";})
    , []);

  useQueries(
    arrayOfCheckingWords().map((value: string) => ({
      queryKey: ['TMCheck',value],
      queryFn: ()=> fetchTrademark(value),
      // structuralSharing: false,
      onError: () => {
        message.error(`Request failed`);
      },
      onSuccess: (data:[]) => {
        setData(prevState => [...prevState, {data: data, keyword: value}]);
      },
    } as UseQueryOptions))
  );

  const columns = [
    {
      title: 'Keyword',
      dataIndex: 'keyword',
      sorter: (a: any, b: any) => {
        var nameA = a.keyword.toUpperCase(); // ignore upper and lowercase
        var nameB = b.keyword.toUpperCase(); // ignore upper and lowercase
        if (nameA < nameB) {
          return -1;
        }
        if (nameA > nameB) {
          return 1;
        }

        // names must be equal
        return 0;
      },
    },
    {
      title: 'Entries Found',
      dataIndex: 'data',
      render: (data: string | any[]) => data.length,
      defaultSortOrder: 'descend' as SortOrder,
      sorter: (a: any, b: any) => a.data.length - b.data.length,
    },
    {
      title: 'Track',
      render: (/*text, record, index*/) => <a>Add</a>,
    },
  ];


  return (<>
      <Modal width={'50%'} title="Trademark Check"
             visible={isModalVisible}
             onCancel={() => setIsModalVisible(false)}
             onOk={() => setIsModalVisible(false)}
             footer={[
               <Button key="back" onClick={() => setIsModalVisible(false)}>
                 Ok
               </Button>]}
      >
        {!_.isEmpty(data) && <WhiteHeaderTable
          // loading={isLoading}
          bordered
          dataSource={data}
          columns={columns}
          pagination={false}
          rowKey={({ keyword }: any) => keyword}
          expandable={{
            expandedRowRender: (record: any) => <ExtendableTable record={record.data} />,
          }}
        />}
        <br /><br />
        <div style={{ display: 'flex', justifyContent: " space-around" }}>{/*<Spin spinning={isLoading} />*/}</div>
      </Modal>
      {/*</Spin>*/}
    </>
  );
};

export default TMTable;
