import React, { useState } from 'react';
import styled from 'styled-components';
import { Button, notification, Popover, Select, Space, Tooltip, Typography } from "antd";
import { CloseOutlined, SwapOutlined } from '@ant-design/icons';
import Icons from '../../../assets/Icons/OldAssets';
import './index.less';
import { useField } from 'react-final-form';
import { decodeTranslationObject } from '../../../utils/constants/constants';
import useProfile from '../../../utils/hooks/useProfile';
import { useMutation } from 'react-query';
import qs from 'qs';
import _ from 'lodash';
import TMButton from "./TMButton";
import translationCall from "../../../utils/mutations/translationCall";

export const OkButtonHolder = styled.div`
  display: flex;
  justify-content: flex-end;
`;

export const TranslationHolder = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
`;

export const TranslationImage = styled.img`
  cursor: pointer;
  :hover{
    color: #7f67ff;
  }
`;

const TranslationSetting = ({ selectedField }:{selectedField:string}) => {
  const { Option } = Select;
  const { input: { value: selectedLanguageValue } } = useField('selectedLanguage', { subscription: { value: true } });
  const {
    input: {
      value: selectedFieldValue,
      onChange: selectedFieldOnChange
    }
  } = useField(selectedField, { subscription: { value: true } });
  const [isVisible, setIsVisible] = useState(false);
  const { input: { value: translateFrom,onChange:setTranslateFrom } } = useField('translateFrom', { subscription: { value: true } });
  const { profile } = useProfile();

  const translationMutation = useMutation(translationCall,{onError: (error:TypeError) => {
      notification.warning({ message: `translation is not working `, description: error.message });
    }})

  const popContent = () => {
    if (_.isEmpty(profile?.apiKey)) {
      return (
        <div>
          <div className="modal-tm-select-container">
            <div className="cross-content" onClick={() => setIsVisible(false)}>
              <CloseOutlined />
            </div>
            <div style={{ marginBottom: '0.5rem' }} />
            <div className="modal-select-content mt-2">
              please add a valid api key for <a href="https://www.deepl.com/pro">deepl</a> first at settings panel
            </div>
          </div>
        </div>
      );
    }


    return (
      <div>
        <div className="modal-tm-select-container">
          <div className="cross-content" onClick={() => setIsVisible(false)}>
            <CloseOutlined />
          </div>
          <div style={{ marginBottom: '0.5rem' }} />
          <div className="modal-select-content mt-2">
            <TranslationHolder>
              <div className="">
                <Select value={translateFrom} style={{ width: 110 }} onChange={setTranslateFrom}>
                  {Object.keys(decodeTranslationObject).map(function(keyName:string, keyIndex:number) {
                    // @ts-ignore
                    return <Option value={keyName}>{decodeTranslationObject[keyName] as string}</Option>;
                  })}
                </Select>
              </div>
              <SwapOutlined />
              {/* @ts-ignore */}
              <div className="">{decodeTranslationObject[selectedLanguageValue]}</div>
            </TranslationHolder>
            <br />
            <OkButtonHolder>
              <Button
                className="btn-tm mt-2"
                onClick={() => {
                  translationMutation.mutate({selectedLanguageValue,selectedFieldValue,translateFrom,profile}, {
                    onSuccess: (data, variables, context) => {
                      selectedFieldOnChange(data)
                    },
                  });
                  setIsVisible(false);
                }}
              >
                OK
              </Button>
            </OkButtonHolder>
          </div>
        </div>
      </div>
    );
  };

  if (_.isEmpty(profile?.apiKey)) return <Space size={'middle'} className="custom-setting-container">
   <TMButton selectedFieldValue={_.isArray(selectedFieldValue)? selectedFieldValue:[selectedFieldValue]}/>
    <Popover
      placement="bottomRight"
      className="tm-setting-content"
      content={popContent}
      trigger="click"
      style={{ width: 100 }}
      visible={isVisible}
      onVisibleChange={setIsVisible}
    >
      <Tooltip title="Please fist add deepl api key" color={'red'} key={"red"}  placement="bottomRight" >
        <TranslationImage src={Icons.ComHeaderTrans} alt="com-header-trans" className="cursor-pointer" />
      </Tooltip>
    </Popover>
  </Space>;

  return (
    <Space className="custom-setting-container">
      <TMButton selectedFieldValue={_.isArray(selectedFieldValue)? selectedFieldValue:[selectedFieldValue]}/>
      <Popover
        placement="bottomRight"
        className="tm-setting-content"
        content={popContent}
        trigger="click"
        style={{ width: 100 }}
        visible={isVisible}
        onVisibleChange={setIsVisible}
      >
        <TranslationImage src={Icons.ComHeaderTrans} alt="com-header-trans" className="cursor-pointer" />
      </Popover>
    </Space>
  );
};

export default TranslationSetting;
