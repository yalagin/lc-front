import React, { useState } from "react";
import { TitleProps } from "antd/lib/typography/Title";
import TMTable, { TMStyledHeader } from "./TMTable";

function TMButton({ selectedFieldValue, SelectedButton=TMStyledHeader }:{ SelectedButton?: React.FunctionComponent<TitleProps>,selectedFieldValue: string[]}) {
  const [isModalVisible, setIsModalVisible] = useState(false);

  return (<>
    <SelectedButton level={5} onClick={()=>setIsModalVisible(true)}> TM</SelectedButton>
      {isModalVisible && <TMTable  selectedFieldValue={selectedFieldValue}   isModalVisible={isModalVisible} setIsModalVisible={setIsModalVisible} />}
    </>
  );
}

export default TMButton;
