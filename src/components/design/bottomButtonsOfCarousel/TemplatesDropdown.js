import React from 'react';
import { useQuery } from 'react-query';
import { fetch2 } from '../../../utils/dataAccess';
import { Divider, List, Menu, Spin } from "antd";
import { HYDRAMEMBER } from '../../../utils/constants/constants';
import { DeleteOutlined } from "@ant-design/icons";
import _ from "lodash";

const TemplatesDropdown = ({ form }) => {
  const fetchUserTemplates = useQuery('templates', () =>
    fetch2('/templates?pagination=false&owner=' + localStorage.getItem('id'))
  );
  const fetchUserPredefinedTemplates = useQuery('PredefinedTemplates', () =>
    fetch2('/templates?pagination=false&exists[owner]=false')
  );

  function onChangeTemplate(menuItem) {
    form.mutators.selectTemplate(menuItem, form);
  }

  function handleRemoveTemplateName() {
    form.mutators.removeTemplateName(form);
  }

  return (
    <Spin spinning={fetchUserTemplates.isLoading}>
      <Menu style={{ background: '#FFE7DD', borderRadius: '8px' }}>
        {fetchUserTemplates.data &&
        fetchUserTemplates.data[HYDRAMEMBER] &&!!(fetchUserTemplates.data[HYDRAMEMBER]).length && <>
          <Divider orientation="left" >
            Your Templates
          </Divider>
          {fetchUserTemplates.data[HYDRAMEMBER].map((menuItem) => (
            <Menu.Item key={menuItem.id}>
              <div onClick={() => onChangeTemplate(menuItem)}>{menuItem.name}</div>
            </Menu.Item>
        ))}</>}
        { !!fetchUserPredefinedTemplates?.data &&
        <>
          <Divider orientation="left" >
            Predefined Templates
          </Divider>
          {fetchUserPredefinedTemplates.data &&
          fetchUserPredefinedTemplates.data[HYDRAMEMBER] &&
          fetchUserPredefinedTemplates.data[HYDRAMEMBER].map((menuItem) => (
            <Menu.Item key={menuItem.id}>
              <div onClick={() => onChangeTemplate(menuItem)}>{menuItem.name}</div>
            </Menu.Item>
          ))}
          </>}

       <> <Menu.Divider style={{
          height: "9px",
          margin: "-5px 0"
        }} />
          <Menu.Item onClick={() => handleRemoveTemplateName()} key="delete"><DeleteOutlined  />  Remove Template Connection</Menu.Item></>
      </Menu>
    </Spin>
  );
};

export default TemplatesDropdown;
