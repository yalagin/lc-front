import React from 'react';
import { Row, Space } from "antd";
import styled from 'styled-components';
import ControlButtons from './ControlButtons';

const StyledRow = styled(Row)`
  float: left;
  margin-left: 10px;

  img {
    cursor: pointer;
    height: 44px;
    width: 44px;
    margin-right: 0.5rem;
  }
`;
const StyledBottomRowTemplateList = (props) => <StyledRow justify="space-around" align="middle">
  <ControlButtons {...props} />
</StyledRow>;

export default StyledBottomRowTemplateList;
