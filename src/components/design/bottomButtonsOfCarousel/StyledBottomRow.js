import React from 'react';
import { Row } from 'antd';
import styled from 'styled-components';
import ControlButtons from './ControlButtons';

const StyledRow = styled(Row)`
  margin-top: 0.5rem;

  img {
    cursor: pointer;
    height: 44px;
    width: 44px;
  }
`;
const StyledBottomRow = (props) => <StyledRow justify="space-around" align="middle">
  <ControlButtons {...props} />
</StyledRow>;

export default StyledBottomRow;
