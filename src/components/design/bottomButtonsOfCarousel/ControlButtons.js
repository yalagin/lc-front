import React, { useRef, useState } from 'react';
import { Button, Dropdown, Popover, Tooltip, Upload } from 'antd';
import Icons from '../../../assets/Icons/OldAssets';
import UploadButtons from '../ImageCarousel/UploadButtons';
import useOnClickOutside from '../../../utils/hooks/useOnClickOutside';
import { MinusOutlined } from '@ant-design/icons';
import styled from 'styled-components';
import TemplatesDropdown from './TemplatesDropdown';

const TemplateChangerHolder = styled.div`
  &&&  .ant-menu {
    ;
  }
  .upload-template {
    background-color: #ffe7dd;
    cursor: pointer;
    justify-content: space-around;
    border-radius: 8px;
    align-items: center;
    display: flex;
    padding: 7px;
    width: 16vw;
    min-width: 60px;
    border: none;

    img {
      height: 20px;
      width: 14px;
    }

    .template-down-icon {
      width: 14px;
      height: 9px;
      margin-top: 4px;
    }

    .upload-img-content {
      img {
        width: 20px;
      }
    }

    .upload-action2-title {
      display: flex;
      align-items: center;
      justify-content: center;

      p {
        margin: 0;
        font-size: 20px;
        font-weight: bold;
      }
    }

    .ant-btn {
      border: none;
      background-color: #ffe7dd;
      height: fit-content;
    }

    .ant-btn:hover {
      background-color: #ffe7dd;
    }

    .select-container {
      background-color: #ffe7dd;

      .ant-select-selector {
        background-color: #ffe7dd;
        border: none;
        font-weight: bold;
      }

      .ant-select-arrow {
        font-size: 14px;
        font-weight: bold;
        color: #ff8408;
      }
    }
  
  }
`;


const ControlButtons = ({
                          uploadImage,
                          form,
                          setOnePicInSlider,
                          setOpenModal,
                          handleDeleteDesigns,
                          handleRemoveDesigns,
                        }) => {
  const [popoverVisible, setPopoverVisible] = useState(false);
  const ref = useRef();
  useOnClickOutside(ref, () => setPopoverVisible(false));

  return (
    <>
      <div className="upload-action-item">
          <Upload
            multiple
            customRequest={uploadImage}
            showUploadList={false}
            accept="image/*"
            onChange={() => setPopoverVisible(false)}
          >
            <Popover
              content={
                <div ref={ref}>
                  <UploadButtons setPopoverVisible={setPopoverVisible} />
                </div>
              }
              visible={popoverVisible}
            >
              <img
                src={Icons.UploadAction0Icon}
                alt="upload-action1"
                id={"upload-btn"}
                onClick={e => {
                  setPopoverVisible(true);
                  e.stopPropagation();
                }}
              />
            </Popover>
          </Upload>
      </div>
      <div className="upload-action-item template-mark">
        <Tooltip placement="top" title="Template Mark">
          <img
            src={Icons.UploadAction1Icon}
            alt="upload-action1"
            onClick={() => {
              form.mutators.markAdult(form);
              // form.mutators.populateMainFields(form);
            }}
          />
        </Tooltip>
      </div>
      <div className="upload-action-item single-view">
        <Tooltip placement="top" title="Single View">
          <img
            src={Icons.UploadAction3Icon}
            alt="upload-action3"
            onClick={() => {
              setOnePicInSlider(prevState => !prevState);
            }}
          />
        </Tooltip>
      </div>
      <TemplateChangerHolder className={'template-dropdown'}>
        <Dropdown overlay={<TemplatesDropdown form={form} />}>
          <div className="upload-action-item">
            <div className="upload-action-item upload-template">
              <div className="upload-img-content">
                <img src={Icons.UploadAction2Icon} alt="upload-action2" />
              </div>
              <div className="upload-action2-title h-100">
                {/*<Button>*/}
                <p>Template</p>
                {/*</Button>*/}
              </div>
              <img
                src={Icons.ChevronUploadDown}
                className="template-down-icon h-50"
                alt="upload-down"
              />
            </div>
          </div>
        </Dropdown>
      </TemplateChangerHolder>
      <div className="upload-action-item template-modal-list"  onClick={() => setOpenModal(prev => !prev)}>
        <Tooltip placement="top" title="Template List">
          <img src={Icons.UploadAction4Icon} alt="upload-action4" />
        </Tooltip>
      </div>
      <div className="upload-action-item remove-design-btn">
        <Tooltip placement="top" title="Remove design">
          {/*<img*/}
          {/*  src={Icons.DeleteIcon}*/}
          {/*  alt="upload-action-delete"*/}
          {/*  onClick={() => handleRemoveDesigns()}*/}
          {/*/>*/}
          <Button
            size={'large'}
            onClick={() => handleRemoveDesigns()}
            type="text"
            style={{borderRadius: '5px', backgroundColor: 'lightpink'}}
            icon={<MinusOutlined style={{color: 'lightcoral'}} />}
          />
        </Tooltip>
      </div>
    </>
  );
};

export default ControlButtons;
