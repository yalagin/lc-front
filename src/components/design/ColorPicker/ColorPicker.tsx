import React, { useEffect, useState } from "react";
import { Popover, Space } from "antd";
import { update } from "../../../actions/person/update";
import { ExclamationCircleOutlined } from "@ant-design/icons";
import { CircleFilled } from "../../styles/SwitchFieldStyles";
import useProfile from "../../../utils/hooks/useProfile";
import {SketchPicker} from 'react-color';
import { useDispatch } from "react-redux";
import { FlexDivCol } from "../../styles/ProfileStyles";

function ColorPicker() {
  const dispatch = useDispatch();
  const {profile} = useProfile();
  const [imageBackgroundColorForLightDesign, setImageBackgroundColorForLightDesign] = useState<string>(profile?.imageBackgroundColorLightDesign);
  const [imageBackgroundColorForDarkDesign, setImageBackgroundColorForDarkDesign] = useState<string>(profile?.imageBackgroundColorDarkDesign);
  useEffect(() => {
    setImageBackgroundColorForLightDesign(profile?.imageBackgroundColorLightDesign);
    setImageBackgroundColorForDarkDesign(profile?.imageBackgroundColorDarkDesign);
  }, [profile]);

  return (
    <FlexDivCol>
      <Popover
        placement="bottomRight"
        title={'Edit background color  for light design'}
        content={
          <Space direction={'vertical'} align={'center'} size={'large'}>
            <SketchPicker
              key={'SketchPicker'}
              disableAlpha
              color={imageBackgroundColorForLightDesign}
              onChangeComplete={color => {
                setImageBackgroundColorForLightDesign(color.hex);
                dispatch(update(profile, {imageBackgroundColorLightDesign: color.hex}));
              }}
            />
          </Space>
        }
        trigger="click"
      >
        <Space style={{cursor: 'pointer'}}>
          <ExclamationCircleOutlined style={{color: '#7f67ff'}} />
          Preview Background Color For Light Design
          <CircleFilled color={imageBackgroundColorForLightDesign} />
        </Space>
      </Popover>

      <Popover
        placement="bottomRight"
        title={'Edit background color for dark design'}
        content={
          <Space direction={'vertical'} align={'center'} size={'large'}>
            <SketchPicker
              key={'SketchPicker'}
              disableAlpha
              color={imageBackgroundColorForDarkDesign}
              onChangeComplete={color => {
                setImageBackgroundColorForDarkDesign(color.hex);
                dispatch(update(profile, {imageBackgroundColorDarkDesign: color.hex}));
              }}
            />
          </Space>
        }
        trigger="click"
      >
        <Space style={{cursor: 'pointer'}}>
          <ExclamationCircleOutlined style={{color: '#7f67ff'}} />
          Preview Background Color For Dark Design
          <CircleFilled color={imageBackgroundColorForDarkDesign} />
        </Space>
      </Popover>
    </FlexDivCol>
  );
}

export default ColorPicker;
