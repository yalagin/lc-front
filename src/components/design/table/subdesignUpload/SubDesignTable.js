import React, {useCallback, useEffect, useState} from 'react';
import {useInfiniteQuery, useMutation} from 'react-query';
import {fetch2} from '../../../../utils/dataAccess';
import qs from 'qs';
import { Alert, Button, message, Modal, Space, Spin, Tooltip } from "antd";
import {HYDRAMEMBER, uploadStatus} from '../../../../utils/constants/constants';
import useColumnStateForUpload from './useColumnStateForUpload';
import handleTableChange from '../tableChange/handleTableChange';
import {StyledDeleteForHeaderButton, TableStyled} from '../../../styles/TableStyles';
import {CloudUploadOutlined, DeleteOutlined, ExclamationCircleOutlined} from '@ant-design/icons';
import {useVT} from 'virtualizedtableforantd4';
import useWindowDimensions from '../../../../utils/hooks/useWindowDimensions';
import useProject from '../../../../utils/hooks/useProject';
import ResizableTitle from '../ResizableTitle';
import UploadedSelect from './select/UploadedSelect';
import TimeSelect from './select/TimeSelect';
import {failed, queue, uploaded, UploadStatusesEnum} from '../../../../utils/constants/upload';
import {DndProvider} from 'react-dnd';
import {HTML5Backend} from 'react-dnd-html5-backend';
import update from 'immutability-helper';
import DraggableBodyRow from './DraggableBodyRow';
import { useQueryClient } from 'react-query'

const SubDesignTable = ({design, url, setHeader, setSubHeader, setRightHeader,selectedUploadOption, setUploadOption}) => {
  const QueryClient = useQueryClient();
  const {project} = useProject();
  const [queryParams, setQueryParams] = useState({
    pagination: true,
    pageSize: 50,
    order: {sortingOrder: 'asc'},
    page: 1,
    isDeleted: false,
    'design.isUpload': true,
    'design.isEdit': false,
    'design.project': project ? project['@id'] : null,
    uploadOption:"Awaiting Upload",
    [uploadStatus]: [
      UploadStatusesEnum['queue'],
      UploadStatusesEnum['failed'],
      // UploadStatusesEnum['idle'],
    ],
  });
  const {columns : initialColumns} = useColumnStateForUpload(design)
  const [columns, setColumns] = useState(null);
  useEffect(() => {
   if(queryParams.uploadOption==='Awaiting Upload'){
     setColumns(initialColumns.map(value=>({...value, filters: false, filterDropdown: false,})))
   }else{
     setColumns(initialColumns);
   }
  }, [queryParams?.uploadOption,initialColumns]);

  useEffect(() => {
    if (project) {
      setQueryParams(prevState => ({...prevState, 'design.project': project['@id']}));
    }
  }, [project]);
  useEffect(() => {
    if (queryParams['design.project']) refetch();
  }, [queryParams['design.project']]);

  const [selectedRowKeys, setSelectedRowKeys] = useState([]);
  const [loading, setLoading] = useState(false);

  const {height} = useWindowDimensions();

  function fetchDesigns({pageParam = 0}) {
    if (!pageParam) {
      return fetch2(
        `/${url}?${qs.stringify(queryParams, {
          skipNulls: true,
        })}`
      );
    }
    return fetch2(pageParam);
  }

  const uploadAgainMutation = useMutation(
    () => {
      return fetch2('/design-action/upload-again/' + project['id'], {
        method: 'POST',
        headers: new Headers({'Content-Type': 'application/ld+json'}),
        body: JSON.stringify({}),
      });
    },
    {
      onSuccess: (data, variables, context) => {
        message.info(`Saved!`);
        refetch();
        QueryClient.invalidateQueries('uploadCounter')
      },
    }
  );

  const {data, error, isFetching, fetchNextPage, refetch, hasNextPage} = useInfiniteQuery(
    ['designs', design, queryParams],
    fetchDesigns,
    {
      keepPreviousData: true,
      // staleTime: 10000,
      refetchOnWindowFocus: false,
      getNextPageParam: lastPage => lastPage['hydra:view']['hydra:next'],
      enabled: !!project,
    }
  );

  const [dataSource, setDataSource] = useState([]);

  useEffect(() => {
    let paginatedData = [];
    data?.pages.forEach(page => {
      paginatedData = paginatedData.concat(page[HYDRAMEMBER]);
    });

    setDataSource(paginatedData);
  }, [data]);

  const [vt] = useVT(
    () => ({
      // debug: true,
      scroll: {y: height - 270},
      onScroll: ({isEnd}) => {
        if (isEnd ) {
          fetchNextPage();
        }
      },
    }),
    []
  );

  //headers set here
  useEffect(() => {
    if (dataSource.length) {
      setHeader(
        `( ${dataSource.length}  out of ${data?.pages[0]['hydra:totalItems'] ?? 0} )`
      );
    } else {
      setHeader(``);
    }
  }, [dataSource]);


  useEffect(() => {
    setRightHeader(
      <Space>
        <Button
          disabled={!selectedRowKeys.length > 0}
          onClick={() => mutation.mutate({[uploadStatus]: UploadStatusesEnum[uploaded]})}
        >Set Uploaded status</Button>
        <Button
          disabled={!selectedRowKeys.length > 0}
          onClick={() => mutation.mutate({[uploadStatus]: UploadStatusesEnum[failed]})}
        >Set Failed status</Button>
        <Tooltip placement="topRight" title="Also it removes canceled status">
          <Button
            disabled={!selectedRowKeys.length > 0}
            onClick={() =>
              mutation.mutate({[uploadStatus]: UploadStatusesEnum[queue], isDeleted: false})
            }
          >Set queue status</Button>
        </Tooltip>
        <UploadedSelect setQueryParams={setQueryParams}  selectedUploadOption={selectedUploadOption} setUploadOption={setUploadOption}/>
        <TimeSelect setQueryParams={setQueryParams} />
        <Tooltip placement="topRight" title="Queue again">
          <StyledDeleteForHeaderButton
            onClick={() => uploadAgainMutation.mutate(undefined, undefined)}
          >
            <CloudUploadOutlined style={{color: '#a38fff', fontSize: '1rem'}} />
          </StyledDeleteForHeaderButton>
        </Tooltip>
        <Tooltip placement="topRight" title="Cancel upload">
          <StyledDeleteForHeaderButton
            onClick={pressDeleteButton}
            disabled={!selectedRowKeys.length > 0}
          >
            <DeleteOutlined style={{color: '#ef6969', fontSize: '1rem'}} />
            {/*<Icon component={removeIcon} alt="remove-action1" />*/}
          </StyledDeleteForHeaderButton>
        </Tooltip>
      </Space>
    );

    return () => {
      setHeader(null);
      setSubHeader(null);
      setRightHeader(null);
    };

    // return setRightHeader('');
  }, [selectedRowKeys, queryParams,design,selectedUploadOption,setUploadOption]);

  const pressDeleteButton = () => {
    setLoading(true);
    const modal = Modal.confirm({
      title: "You're about to cancel designs",
      icon: <ExclamationCircleOutlined />,
      content: `Designs will be canceled from upload queue.`,
      okText: 'Yes',
      okType: 'danger',
      cancelText: 'No',
      onOk() {
        setLoading(false);
        mutation.mutate({isDeleted: true});
      },
      onCancel() {
        setLoading(false);
      },
    });
  };

  const changeStatusPromise = status =>
    Promise.all(
      selectedRowKeys.map(id =>
        fetch2(id, {
          method: 'PATCH',
          body: JSON.stringify(status),
          headers: {'Content-Type': 'application/merge-patch+json'},
        })
      )
    );
  const mutation = useMutation(changeStatusPromise,
    {
      onSuccess: (data, variables, context) => {
        message.info(`Saved!`);
        refetch();
        setSelectedRowKeys([]);
        QueryClient.invalidateQueries('uploadCounter')
      },
      onError: () =>{
        message.error('Something wrong');
        refetch();
        QueryClient.invalidateQueries('uploadCounter')
      }
    });


  useEffect(() => {
    if (selectedRowKeys.length > 0) {
      setSubHeader(<><Spin spinning={isFetching || loading || mutation.isLoading || uploadAgainMutation.isLoading}/>
        {`Selected ${selectedRowKeys.length} out of ${data?.pages[0]['hydra:totalItems']} designs`} </>
      );
    } else {
      setSubHeader( <Spin spinning={isFetching || loading || mutation.isLoading || uploadAgainMutation.isLoading}/>);
    }
  }, [selectedRowKeys,isFetching || loading || mutation.isLoading || uploadAgainMutation.isLoading]);


  const onSelectChange = selectedRowKeys => {
    console.log('selectedRowKeys changed: ', selectedRowKeys);
    setSelectedRowKeys(selectedRowKeys);
  };
  const rowSelection = {
    selectedRowKeys,
    onChange: onSelectChange,
    preserveSelectedRowKeys: true,
  };

  const editResource = useMutation(
    data => {
      return fetch2(data['@id'], {
        method: 'PATCH',
        headers: {'Content-Type': 'application/merge-patch+json'},
        body: JSON.stringify(data),
      });
    },
    {
      onSuccess: (data, variables, context) => {
        message.info(`Order saved!`);
      },
      onError: () => {
        message.error(`Order not saved!`);
      },
    }
  );

  const moveRow = useCallback(
    (dragIndex, hoverIndex) => {
      if (dragIndex !== hoverIndex) {
        const dragRow = dataSource[dragIndex];
        const hoverRow = dataSource[hoverIndex];
        dragRow.sortingOrder =
          dragIndex < hoverIndex ? ++hoverRow.sortingOrder : --hoverRow.sortingOrder;
        setDataSource(
          update(dataSource, {
            $splice: [
              [dragIndex, 1],
              [hoverIndex, 0, dragRow],
            ],
          })
        );
        editResource.mutate({'@id': dragRow['@id'], sortingOrder: dragRow.sortingOrder});
      }
    },
    [dataSource]
  );

  if (error) return <Alert message={error.message} type="error" showIcon closable />;
  if (mutation.error)
    return <Alert message={mutation.error.message} type="error" showIcon closable />;
  return (
    <DndProvider backend={HTML5Backend}>
      <TableStyled
        style={{marginTop: -15}}
        rowSelection={rowSelection}
        dataSource={dataSource}
        columns={columns}
        // loading={isFetching || loading || mutation.isLoading || uploadAgainMutation.isLoading}
        rowKey={record => record['@id']}
        pagination={false}
        onChange={(pagination, filters, sorter) =>
          handleTableChange(pagination, filters, sorter, setQueryParams)
        }
        components={{
          ...vt,
          header: {
            cell: ResizableTitle,
          },
          body: {
            row: DraggableBodyRow,
          },
        }}
        scroll={{y: height - 270}}
        height={height - 270}
        onRow={(record, index) => ({
          index,
          moveRow,
        })}
      />
    </DndProvider>
  );
};

export default SubDesignTable;
