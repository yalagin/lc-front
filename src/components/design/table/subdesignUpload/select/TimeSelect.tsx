import React, {SetStateAction, useEffect, useState} from 'react';
import {MinWidthSelect} from '../../../../dashboard/StatsInfo';
import {Select} from 'antd';
import _ from 'lodash';
import MyDate from '../../../../../utils/extendedDefaultClasses/MyDate';

const TimeSelect = ({setQueryParams}: {setQueryParams: SetStateAction<any>}) : React.ReactElement => {
  const timePeriod = [
    'All',
    'Today',
    'This week',
    'Last 15 minutes',
    'Last 30 minutes',
    'Last 1 hour',
    'Last 24 hours',
    'Last 7 days',
    'Last 30 days',
    'Last 90 days',
    'Last 1 year',
  ];


  const [selectedPeriod, setSelectedPeriod] = useState('All');
  useEffect(() => {
    switch (selectedPeriod) {
      case 'All':
        setQueryParams((prevState: any) => _.omit(prevState, 'createdAt[after]'));
        break;
      case 'Today':
        setQueryParams((prevState: any) => ({
          ...prevState,
          'createdAt[after]': new Date().toISOString().split('T')[0],
        }));
        break;
      case 'This week':
        setQueryParams((prevState: any) => ({
          ...prevState,
          'createdAt[after]': new MyDate().getFirstDayOfWeek().toISOString(),
        }));
        break;
      case 'Last 15 minutes':
        setQueryParams((prevState: any) => ({
          ...prevState,
          'createdAt[after]': new MyDate().getMinutesAgo(15).toISOString(),
        }));
        break;
      case 'Last 30 minutes':
        setQueryParams((prevState: any) => ({
          ...prevState,
          'createdAt[after]': new MyDate().getMinutesAgo(30).toISOString(),
        }));
        break;
      case 'Last 1 hour':
        setQueryParams((prevState: any) => ({
          ...prevState,
          'createdAt[after]': new MyDate().getMinutesAgo(60).toISOString(),
        }));
        break;
      case 'Last 24 hours':
        setQueryParams((prevState: any) => ({
          ...prevState,
          'createdAt[after]': new MyDate().getDaysAgo(1).toISOString(),
        }));
        break;
      case 'Last 7 days':
        setQueryParams((prevState: any) => ({
          ...prevState,
          'createdAt[after]': new MyDate().getDaysAgo(7).toISOString(),
        }));
        break;
      case 'Last 30 days':
        setQueryParams((prevState: any) => ({
          ...prevState,
          'createdAt[after]': new MyDate().getDaysAgo(30).toISOString(),
        }));
        break;
      case 'Last 90 days':
        setQueryParams((prevState: any) => ({
          ...prevState,
          'createdAt[after]': new MyDate().getDaysAgo(90).toISOString(),
        }));
        break;
      case 'Last 1 year':
        setQueryParams((prevState: any) => ({
          ...prevState,
          'createdAt[after]': new MyDate().getDaysAgo(365).toISOString(),
        }));
        break;
    }
  }, [selectedPeriod]);

  return (
    <MinWidthSelect
      className="select-container time-item"
      value={selectedPeriod}
      onChange={setSelectedPeriod as any}
    >
      {timePeriod.map((item, index) => (
        <Select.Option value={item} key={item}>
          {item}
        </Select.Option>
      ))}
    </MinWidthSelect>
  );
};

export default TimeSelect;
