import React, { Dispatch, SetStateAction, useEffect, useState } from "react";
import { Select, Tooltip } from 'antd';
import { MinWidthSelect } from '../../../../dashboard/StatsInfo';
import _ from 'lodash';
import { UploadStatusesEnum } from '../../../../../utils/constants/upload';
import { isDeleted, uploadStatus } from '../../../../../utils/constants/constants';

type UploadOptions = 'Awaiting Upload' | 'Uploaded' | 'Canceled' | 'All';
const uploadOptions = [/*'All',*/ 'Awaiting Upload', 'Uploaded', 'Canceled'];
const uploadOptionsDescription = {
  "All": ' All the designs whether they are uploaded or not and not canceled',
  'Awaiting Upload': 'Designs that are currently in the queue and the failed ones',
  "Uploaded": 'Successfully uploaded designs',
  "Canceled": 'Canceled designs',
};

const UploadedSelect = ({
                          setQueryParams,
                          selectedUploadOption,
                          setUploadOption
                        } : { setQueryParams: Dispatch<SetStateAction<any>>, selectedUploadOption: UploadOptions, setUploadOption: Dispatch<SetStateAction<UploadOptions>> }): React.ReactElement => {
  useEffect(() => {
    switch (selectedUploadOption) {
      case 'All':
        setQueryParams((prevState: any) => ({
          ..._.omit(prevState, uploadStatus),
          [isDeleted]: false,
          uploadOption: "All",
        }));
        break;
      case 'Awaiting Upload':
        setQueryParams((prevState: any) => ({
          ...prevState,
          [uploadStatus]: [
            UploadStatusesEnum['queue'],
            UploadStatusesEnum['failed'],
            // UploadStatusesEnum['idle'],
          ],
          [isDeleted]: false,
          uploadOption: "Awaiting Upload",
        }));
        break;
      case 'Uploaded':
        setQueryParams((prevState: any) => ({
          ...prevState,
          [uploadStatus]: [UploadStatusesEnum['uploaded'], UploadStatusesEnum['changed'],],
          [isDeleted]: false,
          uploadOption: "Uploaded",
        }));
        break;
      case 'Canceled':
        setQueryParams((prevState: any) => ({
          ...prevState,
          [uploadStatus]: [
            UploadStatusesEnum['queue'],
            UploadStatusesEnum['failed'],
            UploadStatusesEnum['idle'],
          ],
          [isDeleted]: true,
          uploadOption: "Canceled",
        }));
        break;
    }
  }, [selectedUploadOption]);


  return (
    <MinWidthSelect
      className="select-container time-item"
      value={selectedUploadOption}
      onChange={ (value: any)=>setUploadOption(value)}
    >
      {uploadOptions.map((item, index) => (

        <Select.Option value={item}>
          {/* @ts-ignore*/}
          <Tooltip title={uploadOptionsDescription[item]} placement="right" key={item}>
            <div style={{ width: "100%" }}>
              <div
                style={{
                  overflow: "hidden",
                  whiteSpace: "nowrap",
                  textOverflow: "ellipsis"
                }}
              >
                {item}
              </div>
            </div>
          </Tooltip>
        </Select.Option>
      ))}
    </MinWidthSelect>
  );
};

export default UploadedSelect;
