import React, {useEffect, useRef, useState} from 'react';
import {CheckCircleOutlined, CloseCircleOutlined} from '@ant-design/icons';
import {
  decodeTranslationObject,
  designMBA,
  designRedBubble,
  designShirtee,
  designSpreadshirtEu,
  getTypeOfField,
  image, Publication, uploadStatus
} from "../../../../utils/constants/constants";
import columnSearchProps from '../useColumnSearchProps';
import NonEditableTagGroup from '../../../input/NonEditableTagGroup';
import TableDesignImageContainer from '../../../mediaobject/image/TableDesignImage';
import _ from 'lodash';
import {
  getTitleForDesign,
  getTitleForUploadDesign,
  makeTitleStringLookPretty
} from "../../../../utils/designHelperFunctions";
import {ENTRYPOINT} from '../../../../config/entrypoint';
import {
  ForTreeFilteringInDesignTable,
  idle,
  objectOfDesignsWithNotTranslatedFields,
  objectOfDesignsWithTranslatedFields,
  UploadStatusesHelpObject,
  UploadStatusesNumberToString,
  UploadStatusesEnum,
  UploadStatusesForFilter, UploadStatusesForFilterUpload, updated, uploaded
} from "../../../../utils/constants/upload";
import { message, Tooltip, Typography } from "antd";
import ProductMarketplacesCol from "../columns/ProductMarketplacesCol";
import usePatchMutation from "../../../../utils/mutations/usePatchMutation";
import { UploadStatusColSubDesign } from "../columns/uploadStatusCol";
import { designAndTheirLanguages } from "../../../../utils/constants/translation";
const { Title ,Text} = Typography;

const useColumnStateForUpload = design => {
  const [resizingColumnIndex, _setResizingColumnIndex] = useState(0);
  const resizingColumnIndexRef = useRef(resizingColumnIndex);

  const [searchText, setSearchText] = useState('');
  const [searchedColumn, setSearchedColumn] = useState('');

  const [columnSettings, setColumnSettings] = useState([]);
  const [columns, updateColumns] = useState([]);

  useEffect(() => {
    //change initial order here

    if (ForTreeFilteringInDesignTable[design]) {
      const colSettings = [];

      colSettings.push({
        title: image,
        key: colSettings.length,
        visible: true,
        type: image,
        width: 200,
      });
      //selected languages from the upload modal
      if (
        design === designRedBubble ||
        design === designShirtee ||
        design === designSpreadshirtEu
        // || design === designMBA
      ) {
        colSettings.push({
          title: 'Languages',
          key: colSettings.length,
          visible: true,
          type: 'selectedLanguages',
          width: 200,
        });
      }
      //

      if (design in designAndTheirLanguages) {
        objectOfDesignsWithNotTranslatedFields[design].forEach(field => {
          colSettings.push({
            title: field,
            // parent: design,
            // treeSortable: _.toLower(design + field),
            key: colSettings.length,
            visible: true,
            type: getTypeOfField(field),
            width: 200,
          });
        });
        designAndTheirLanguages[design].forEach(lang => {
          objectOfDesignsWithTranslatedFields[design].forEach(field => {
            colSettings.push({
              title: field,
              parent: ['translations', lang],
              // treeSortable: _.toLower(design + 'translations' + lang + field),
              key: colSettings.length,
              visible: true,
              type: getTypeOfField(field),
              width: 200,
            });
          });
        });
      } else {
        ForTreeFilteringInDesignTable[design].map((field, index) => {
          colSettings.push({
            title: field,
            key: colSettings.length,
            visible: true,
            type: getTypeOfField(field),
            width: 200,
          });
        });
      }
      //
      // timeStamps.map((field, index) => {
      //   colSettings.push({
      //     title: field,
      //     key: colSettings.length,
      //     visible: true,
      //     type: getTypeOfField(field),
      //     width: 200,
      //   });
      // });


      setColumnSettings(colSettings);
    }
  }, []);

  const handleResize = index => (e, {size}) => {
    e.stopImmediatePropagation();
    e.stopPropagation();
    e.preventDefault();
    setColumnSettings(columns => {
      const nextColumns = [...columns];
      nextColumns[index] = {
        ...nextColumns[index],
        width: size.width,
      };
      // setServicesValueDebounced.current();
      return nextColumns;
    });
  };

  useEffect(() => {
    updateColumns(
      columnSettings
        .map((field, index) => {
          const {type, title, visible, parent, width} = field;
          if (visible === false) {
            return null;
          }
          let render = {};
          switch (type) {
            case 'bool':
              render = {
                render: bool =>
                  bool ? (
                    <CheckCircleOutlined style={{marginLeft: '2rem', color: '#0ea201'}} />
                  ) : (
                    <CloseCircleOutlined style={{marginLeft: '2rem', color: '#7f67ff'}} />
                  ),
                filters: [
                  {text: <CheckCircleOutlined style={{color: '#0ea201'}} />, value: true},
                  {text: <CloseCircleOutlined style={{color: '#7f67ff'}} />, value: false},
                ],
                filterMultiple: false,
                sorter: true,
              };
              break;
            case 'tags':
              render = {
                // width: 300,
                dataIndex: 'tags',
                ...columnSearchProps(
                  searchedColumn,
                  setSearchedColumn,
                  searchText,
                  setSearchText,
                  title
                ),
                render: tags => <NonEditableTagGroup tags={tags} />,
              };
              break;
            case 'time':
              render = {
                // width: 200,
                sorter: true,
                ...columnSearchProps(
                  searchedColumn,
                  setSearchedColumn,
                  searchText,
                  setSearchText,
                  title,
                  true
                ),
                render: updatedAt => new Date(updatedAt).toLocaleString(),
              };
              break;
            case 'file':
              render = {
                fixed: 'left',
                render: files =>
                  _.isArray(files)
                    ? files.map(file => new URL(file?.contentUrl, ENTRYPOINT))
                    : null,
              };
              break;
            case 'image':
              render = {
                fixed: 'left',
                render: image => (
                  <TableDesignImageContainer
                    key={image ? image.id : null}
                    image={image}
                    preview={true}
                  />
                ),
              };
              break;
              case uploadStatus:
              render = {
                render: (status, record) => <UploadStatusColSubDesign record={record} status={status} />,
                sorter: true,
                defaultSortOrder: 'descend',
                filters:
                  Object.keys(UploadStatusesForFilterUpload).map(value => {
                    return  {text: <Text keyboard>
                         <Tooltip title={UploadStatusesHelpObject[value] } key={value}>{value}</Tooltip>
                      </Text> , value: UploadStatusesEnum[value]}
                  })
                ,
              };
              break;
              case Publication:
                render = {
                  render: (text, record, index) => <ProductMarketplacesCol text={text} record={record} key={index} />,
                }
              break;
            default:
              render = {
                // width: 200,
                sorter: true,
                ...columnSearchProps(
                  searchedColumn,
                  setSearchedColumn,
                  searchText,
                  setSearchText,
                  title
                ),
              };
          }
          if (title === 'image') {
            render = {...render, dataIndex: ['design', 'image']};
          }
          if (parent) {
            render = {...render, dataIndex: [...parent, title]};
          }
          if (type === 'selectedLanguages') {
            render = {
              // width: 300,
              dataIndex: 'selectedLanguages',
              ...columnSearchProps(
                searchedColumn,
                setSearchedColumn,
                searchText,
                setSearchText,
                title
              ),
              render: tags => (
                <NonEditableTagGroup tags={ _.isArray(tags)?tags.map(tag => decodeTranslationObject[tag]):[]} />
              ),
            };
          }

          return {
            title: () => {
              const titleString = getTitleForUploadDesign(parent, title);
              if(titleString.indexOf("(") !== -1){
                const chars = titleString.split('(');
                return <>{chars[0]} <br/> <Text type="secondary" style={{fontSize:"11px"}}>({chars[1]}</Text></>
              }
              return <Text level={5}>{titleString}</Text>
            },
            dataIndex: title,
            onHeaderCell: column => ({
              width: column.width,
              onResize: handleResize(index),
            }),
            // key: key,
            minWidth: 300,
            width: width,
            ...render,
            // filters:false,
            // filterDropdown:false,
          };
        })
        .filter(filter => !!filter)
    );

  }, [columnSettings, searchedColumn, searchText]);

  return {columns, updateColumns, columnSettings, setColumnSettings};
};

export default useColumnStateForUpload;
