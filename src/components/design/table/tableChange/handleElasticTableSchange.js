import _ from 'lodash';
import { initialQueries, timeFormat } from '../../../../utils/constants/constants';

const handleElasticTableChange = (pagination, filters, sorter, queryParams, setQueryParams) => {
  if (_.isArray(sorter.field)) {
    sorter.field = sorter.field.join('.');
  }
  if (sorter?.order === 'ascend') {
    setQueryParams(prevState => ({ ...prevState, sort: { [sorter.field]: { order: 'asc' } } }));
  }
  if (sorter?.order === 'descend') {
    setQueryParams(prevState => ({ ...prevState, sort: { [sorter.field]: { order: 'desc' } } }));
  }

  if (!filters) {
    return;
  }

  const queries = _.clone(initialQueries);
  const clone = _.clone(filters);
  if (clone?.createdAt) {
    const initial = clone.createdAt;

    const createdAt = {
      field: 'createdAt',
      type: 'range',
      value: {
        gte: initial[0].format(timeFormat),
        lte: initial[1].format(timeFormat),
      },
    };
    queries.push(createdAt);
    delete clone.createdAt;
  }
  if (clone?.updatedAt) {
    const initial = clone.updatedAt;
    const updatedAt = {
      field: 'updatedAt',
      type: 'range',
      value: {
        gte: initial[0].format(timeFormat),
        lte: initial[1].format(timeFormat),
      },
    };
    queries.push(updatedAt);
    delete clone.updatedAt;
  }
  if (!_.isEmpty(clone.image)) {
    setQueryParams(prevState => ({ ...prevState, image: clone?.image?.pop() }));
    delete clone.image;
  }
  if (clone?.image === null) {
    setQueryParams(prevState => {
      delete prevState.image;
      return prevState;
    });
  }

  Object.getOwnPropertyNames(clone).forEach(prop => {
    if (clone[prop] && clone[prop].length ===1) {
      queries.push({field: prop, value: clone[prop][0], type: typeof clone[prop][0]});
    }else if (clone[prop] && clone[prop].length >1) {
      queries.push({field: prop, value: clone[prop], type: typeof clone[prop][0]});
    }
  });
  setQueryParams(prevState => ({ ...prevState, queries }));
};

export default handleElasticTableChange;
