import _ from 'lodash';

const handleTableChange = (pagination, filters, sorter, setQueryParams) => {
  if (sorter) {
    if (sorter?.order === 'ascend') {
      sorter.order = 'asc';
    }

    if (sorter?.order === 'descend') {
      sorter.order = 'desc';
    }

    setQueryParams(prevState => ({...prevState, order: {[sorter.field]: sorter.order}}));
  }

  if (filters) {


    const clone = _.clone(filters);


    //language searching split "translations.en.title" = value to translations.title = value and translations = en
    Object.getOwnPropertyNames(clone).forEach(prop => {
      const arrayOfFields = prop.split('.');
      const indexOfTranslations = arrayOfFields.indexOf('translations');
      if (indexOfTranslations !== -1) {
        const language = arrayOfFields.splice(indexOfTranslations + 1, 1);
        const propWithoutLanguage =  arrayOfFields.join('.');
        clone[propWithoutLanguage] = clone[prop];
        delete clone[prop];
        arrayOfFields.pop()
        clone[arrayOfFields.join('.')] = language;
      }
    });

    // const clone = filters.map(cloneValue=> (_.isArray(cloneValue)&&cloneValue.length===1) ?  cloneValue[0]:cloneValue  );
    if (clone.createdAt) {
      const initial = clone.createdAt;
      clone.createdAt = {
        before: initial[1].format('MM/DD/yyyy'),
        after: initial[0].format('MM/DD/yyyy'),
      };
    }
    if (clone.updatedAt) {
      const initial = clone.updatedAt;
      clone.updatedAt = {
        before: initial[1].format('MM/DD/yyyy'),
        after: initial[0].format('MM/DD/yyyy'),
      };
    }
    setQueryParams(prevState => ({...prevState, ...clone}));
  }

  if (pagination) {
    setQueryParams(prevState => ({...prevState, page: pagination.current}));
    if (pagination.pageSize) {
      setQueryParams(prevState => ({...prevState, pageSize: pagination.pageSize}));
    }
  }
};

export default handleTableChange;
