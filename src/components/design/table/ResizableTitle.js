import { Resizable } from "react-resizable";
import React from "react";

export default class ResizeableTitle extends React.Component {
  render() {
    const { onResize, width, onClick, ...restProps } = this.props;

    if (!width) {
      return <th {...restProps} />;
    }

    return (
      <Resizable
        width={width}
        height={0}
        onResizeStart={() => (this.resizing = true)}
        onResizeStop={() => {
          setTimeout(() => {
            this.resizing = false;
          });
        }}
        onResize={onResize}
        minConstraints={[150, 150]}
      >
        <th
          onClick={(...args) => {
            if (!this.resizing && onClick) {
              onClick(...args);
            }
          }}
          {...restProps}
        />
      </Resizable>
    );
  }
}

