import { TemplateSettingsMbaProduct } from "../../../../interfaces/templatesettingsmbaproduct";
import { TemplateSettingsMbaMarketPlace } from "../../../../interfaces/templatesettingsmbamarketplace";
import { Field, useField } from "react-final-form";
import MyCheckbox from "../../../input/MyFields/MyCheckbox";
import React from "react";
import styled from "styled-components";


const  FieldWithNoMarginBottom = styled(Field)`
  //&&& .ant-form-item{
     margin-bottom: 0;
  //}
`;

export const CentredDiv = styled.div`
  //background: rgba(235, 237, 244, 0.15);
  //padding: 0.3rem;
  align-items: center;
  justify-content: center;
  display: flex;
  //min-height: 42px;
  //min-height: 2.6rem;
  &&& .ant-form-item {
    margin-bottom: 0;
  }
`


//the twin form the src/components/template/mba/Marketplace.js
function ColRow({value:product, array,marketplace, prefix = ""}:{value:string,array:TemplateSettingsMbaProduct,marketplace:TemplateSettingsMbaMarketPlace,prefix?:string}) {
  const { input: { value: mbaProductPublished }, } = useField(`${prefix}mbaProductPublished[${array["@id"]}[${marketplace["@id"]}]`, { subscription: { value: true } });

  return (
    <CentredDiv>
      <FieldWithNoMarginBottom
        name={`${prefix}mbaProductCheckbox[${array['@id']}[${marketplace['@id']}]`}
        subscription={{value: true}}
        component={MyCheckbox}
        hasFeedback
        type="checkbox"
        disabled={mbaProductPublished}
      />
    </CentredDiv>
  );
}

export default ColRow;
