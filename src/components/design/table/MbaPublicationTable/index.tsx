import React from 'react';
import { PagedCollection } from '../../../../interfaces/Collection';
import { TemplateSettingsMbaProduct } from '../../../../interfaces/templatesettingsmbaproduct';
import { FormApi } from 'final-form';
import { TemplateSettingsMbaMarketPlace } from '../../../../interfaces/templatesettingsmbamarketplace';
import { DesignMba } from '../../../../interfaces/designmba';
import { Table } from 'antd';
import { ColumnType } from 'antd/lib/table/interface';
import ColRow from './ColRow';
import Product from "../../../template/mba/Product";

interface MbaPublicationTableInterface {
  products: PagedCollection<TemplateSettingsMbaProduct>;
  form: FormApi;
  designMba: DesignMba;
  marketplaces: PagedCollection<TemplateSettingsMbaMarketPlace>;
}

const firstCol = {
  title: 'Name',
  dataIndex: 'name',
  // key: 'name',
  width: 200
} as ColumnType<TemplateSettingsMbaProduct>;

function MbaPublicationTable({
                               products,
                               form,
                               marketplaces,
                               designMba
                             }: MbaPublicationTableInterface): React.ReactElement {
  // const [dataSource, setDataSource] = useState([]);

  const dataSource = products['hydra:member'].map((value) => ({
    ...value,
    key:value["@id"]
  }));

  const columns = marketplaces['hydra:member'].map(
    (marketplace): ColumnType<TemplateSettingsMbaProduct> => ({
      ...marketplace,
      // key: marketplace['@id'],
      title: marketplace.name,
      dataIndex: marketplace['@id'],
      render: (value, array) => {
        const marketPlaces = array?.priceForMarketplace?.map(value=>value?.marketPlace['@id']);
        return marketPlaces.includes( marketplace['@id']) ? <ColRow array={array} value={value} marketplace={marketplace} />: {props: {
            style: { background: "#f0f0f0" }
          }};
      },
      width: 50,
    })
  );

  return <Table<TemplateSettingsMbaProduct>
    pagination={false}
    dataSource={dataSource}
    columns={[firstCol, ...columns]}
    expandable={{
      expandedRowRender: (record,index) => <Product key={index}  product={record}  />,
      // rowExpandable: record => record.name !== 'Not Expandable',
    }}
  />;
}

export default MbaPublicationTable;
