import React, { useEffect, useState } from 'react';
import { Tooltip, Upload } from 'antd';
import _ from 'lodash';
import Icons from '../../../assets/Icons/OldAssets';
import { useMutation } from 'react-query';
import { UploadChangeParam, UploadFile } from 'antd/lib/upload/interface';
import useProject from '../../../utils/hooks/useProject';
import { useNavigate } from 'react-router-dom';
import { uploadToAwsPromiseAllMutation } from '../../../utils/mutations/uploadToAwsPromiseAllMutation';

const UploadButton = ({ setIsLoading }: { setIsLoading: any }) => {
  const [uploadList, setUploadList] = useState<UploadFile[]>([]);
  const { project } = useProject();
  const navigate = useNavigate();
  const mutationSave = useMutation(uploadToAwsPromiseAllMutation(project,'design',true));

  useEffect(() => {
    setIsLoading(mutationSave.isLoading);
  }, [mutationSave.isLoading]);

  useEffect(() => {
    if (!_.isEmpty(uploadList)) {
      mutationSave.mutateAsync(uploadList).then(data => {
        navigate('/dashboard/workspace',{
          state: { ids: data.map(design => design.id) },
        });
        setUploadList([]);
      });
    }
  }, [uploadList]);

  const uploadImage = () => {
    // dispatch(createMediaObjectAndDesign(fmData, project));
  };

  const onChange = (props: UploadChangeParam) =>
    setUploadList(prevState => {
      return _.isEqual(props.fileList, prevState) ? prevState : props.fileList;
    });

  return (
    <div id="hide_list_upload">
      <Upload multiple customRequest={uploadImage} onChange={onChange} accept="image/*">
        <Tooltip placement="top" title="Add Design">
          <img
            src={Icons.UploadAction0Icon}
            style={{ width: '35px', cursor: 'pointer' }}
            alt="upload-action1"
          />
        </Tooltip>
      </Upload>
    </div>
  );
};

export default UploadButton;

