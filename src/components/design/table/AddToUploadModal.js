import React, { useEffect,  useState } from "react";
import { notification, Spin, Tree, Modal } from "antd";
import {
  decodeTranslationObject,
  designMBA,
  designRedBubble, designs,
  designs as designsConstantArray,
  designSpreadshirtEu, uploadStatus
} from "../../../utils/constants/constants";
import { makeDesignsStringLookPretty } from "../../../utils/designHelperFunctions";
import _ from 'lodash';
import useProfile from "../../../utils/hooks/useProfile";
import { useMutation } from "react-query";
import { fetch2 } from "../../../utils/dataAccess";
import { update } from "../../../actions/person/update";
import { useDispatch } from "react-redux";
import { changed, queue, updated, uploaded, UploadStatusesEnum } from "../../../utils/constants/upload";
import { designAndTheirLanguages } from "../../../utils/constants/translation";
import { removeDisabledAndEmptyFields } from "../../../utils/utilsFunctions";

const AddToUploadModal = ({showUploadModal, setShowUploadModal, selectedRowKeys = null,designs}) => {
  const {profile} =  useProfile();
  const [treeData, setTreeData] = useState([]);
  const [checkedKeys, setCheckedKeys] = useState(profile?.uploadSettings);
  const [isModalFirstTimeOpened, setIsModalFirstTimeOpened] = useState(true);
  const [updateProfile, setUpdateProfile] = useState(false);
  const dispatch = useDispatch();

  useEffect(() => {
    if (updateProfile) {
      dispatch(update(profile, {uploadSettings: checkedKeys}));
      setUpdateProfile(false);
    }
  }, [checkedKeys,updateProfile]);

  useEffect(() => {
    // fix updating profile when page is loaded
    if(isModalFirstTimeOpened){
      setIsModalFirstTimeOpened(false)
    } else {
      setUpdateProfile(true);
    }
  }, [checkedKeys]);

  const mutationSave = useMutation(() => {
    let selectedDesigns;
    if (_.isNull(selectedRowKeys)) {
      selectedDesigns = designs;
    } else {
      selectedDesigns = designs.filter(member => selectedRowKeys.includes(member["id"]));
    }
    let selectedLaguages = {};
    checkedKeys.forEach(checkedDesignKey=>{
      if (checkedDesignKey.indexOf('-') > -1) {
        const splittedArray =  checkedDesignKey.split('-');
        if(_.isArray(selectedLaguages[splittedArray[0]])) {
          selectedLaguages = {
            ...selectedLaguages,
            [splittedArray[0]]: [...selectedLaguages[splittedArray[0]], splittedArray[1]],
          };
        } else {
          selectedLaguages = {
            ...selectedLaguages,
            [splittedArray[0]]: [splittedArray[1]],
          }
        }
      }
    })

    return Promise.all(
      selectedDesigns.map(async design => {
        const saveDesignObject = _.isNull(selectedRowKeys)?design:{};
        _.set(saveDesignObject,'isUpload',true);
        _.set(saveDesignObject,'isEdit',false);
        designs.forEach(function(designName) {
          if (saveDesignObject[designName]?.uploadStatus === UploadStatusesEnum[uploaded]) {
            _.set(saveDesignObject, [designName, uploadStatus], UploadStatusesEnum[changed]);
            if(designName===designMBA){
              _.set(saveDesignObject, [designName, uploadStatus], UploadStatusesEnum[updated]);
            }
          }
        });
        checkedKeys.forEach(key=> {
          _.set(saveDesignObject, [key, uploadStatus], UploadStatusesEnum[queue]);
        });
        _.forEach(selectedLaguages, function(value, key) {
          _.set(saveDesignObject,[key,'selectedLanguages'],value);
          _.set(saveDesignObject,[key,uploadStatus],UploadStatusesEnum[queue]);
        });
        removeDisabledAndEmptyFields(saveDesignObject);
        return   await fetch2(`/designs/${design['id']}`, {
          method: 'PATCH',
          body: JSON.stringify(saveDesignObject),
          headers: { 'Content-Type': 'application/merge-patch+json' },
        });
      })
    );
  },{onSuccess: (data, variables, context) => {
      notification.info({ message: `Sent to Upload Queue!` });
      setShowUploadModal(false);
    },onError: (data, variables, context) => {
      notification.error({ message: `Something went wrong!` });
    }});


  const handleOk = () => {
    // setShowUploadModal(false);
    mutationSave.mutate();
  };

  const handleCancel = () => {
    setShowUploadModal(false);
  };

  useEffect(() => {
    const data = [];
    designsConstantArray.forEach(design => {
      let listParent = {
        title:  makeDesignsStringLookPretty(design),
        key: design,
        children: [],
      };
      if (Object.keys(designAndTheirLanguages).includes(design) && design !== designMBA) {
        designAndTheirLanguages[design].forEach(lang => {
          listParent.children.push({
            title: <div >{ design===designRedBubble||design===designSpreadshirtEu? decodeTranslationObject[lang]:   _.toUpper(lang)}</div>,
            key: `${design}-${lang}`,
          });
        });
      }
      data.push(listParent);
    });

    setTreeData(data);
  }, []);


  const onSelect = (selectedKeys, info) => {
    console.log('selected', selectedKeys, info);
  };

  const onCheck = (checkedKeys, info) => {
    console.log('onCheck', checkedKeys, info);
    setCheckedKeys(checkedKeys);
  };

  return (
      <Modal title="Upload Queue" visible={showUploadModal} onOk={handleOk} onCancel={handleCancel}>
        <Spin spinning={mutationSave.isLoading}>
        <p>Select Platforms</p>
        <Tree
          checkable
          onSelect={onSelect}
          onCheck={onCheck}
          treeData={treeData}
          checkedKeys={checkedKeys}
          defaultCheckedKeys={['designSpreadshirtCom', 'designSpreadshirtEu', 'designSpreadshirtEu-en', 'designSpreadshirtEu-de', 'designRedBubble', 'designRedBubble-en', 'designRedBubble-de', 'designRedBubble-fr', 'designRedBubble-es', 'designSociety6', 'designTeepublic', 'designShirtee', 'designShirtee-en', 'designShirtee-de', 'designZazzle', 'designPrintful', 'designMBA', 'designTeespring', 'designDisplate']}
        />
        </Spin>
      </Modal>
  );
};

export default AddToUploadModal;
