import React, { useEffect, useState } from 'react';
import { Tree } from 'antd';
import {
  decodeTranslationObject,
  designs,
  keyAliasesForTreeData,
  ForTreeFilteringInDesignTable,
  uploadStatus
} from "../../../utils/constants/constants";
import _ from 'lodash';
import useProfile from "../../../utils/hooks/useProfile";
import {
  makeDesignsStringLookPretty,
  makeTitleStringLookPretty,
} from "../../../utils/designHelperFunctions";
import {
  designAndTheirLanguages,
  objectOfDesignsWithNotTranslatedFields,
  objectOfDesignsWithTranslatedFields
} from "../../../utils/constants/translation";

const MyTree = ({ columns, updateColumns,setUpdateProfile }) => {
  const {profile} =  useProfile();
  const [expandedKeys, setExpandedKeys] = useState(['Miscellaneous']);
  const [checkedKeys, setCheckedKeys] = useState(profile.colSettings?.filter(value => value.visible).map(value => value.treeSortable));
  const [autoExpandParent, setAutoExpandParent] = useState(true);
  const [treeData, setTreeData] = useState([]);
  const [isModalFirstTimeOpened, setIsModalFirstTimeOpened] = useState(true);

  useEffect(() => {
    let counter = 0;
    const data = [];

    let listParent = {
      title: "Listings",
      key: "Listings",
      children: [],
    };

    for (let [design, designFields] of Object.entries(ForTreeFilteringInDesignTable)) {
        let parent = {
          title: makeDesignsStringLookPretty(design),
          key: design,
          children: [],
        };
        if (design in designAndTheirLanguages) {
          designAndTheirLanguages[design].forEach(lang => {
            const arrayOfLanguageFields = [];
            objectOfDesignsWithTranslatedFields[design].forEach(field => {
              arrayOfLanguageFields.push({
                title: design ==="general"? makeTitleStringLookPretty(field,false):  makeTitleStringLookPretty(field),
                key:_.toLower(design+'translations'+lang+field)
              });
            });
            parent.children = [
              ...parent.children,
              {
                title: _.startCase(decodeTranslationObject[lang]),
                key:_.toLower(design + lang),
                children: arrayOfLanguageFields,
              },
            ];
          });

          const fitType = {
            title: "Fit Types",
            key: "fitTypes"+design,
            children: [],
          };

          designFields.forEach(field=>{
            if(objectOfDesignsWithNotTranslatedFields[design].includes(field)){
              if (/fit/.test(field)){
                fitType.children = [
                  {
                    title: makeTitleStringLookPretty(field),
                    key:_.toLower(design + field),
                  },
                  ...fitType.children,
                ];
              } else {
                parent.children = [
                  {
                    title: makeTitleStringLookPretty(field),
                    key:_.toLower(design + field),
                  },
                  ...parent.children,
                ];
              }

            }
          })

          if(!_.isEmpty(fitType.children)){
            parent.children = [
              fitType,
              ...parent.children,
            ];
          }

        } else {
          designFields.forEach(field => {
            parent.children = [
              ...parent.children,
              {
                title: makeTitleStringLookPretty(field),
                key:_.toLower( keyAliasesForTreeData[design]?keyAliasesForTreeData[design]+field:design+field),
              },
            ];
          });
        }
        if(design==='Miscellaneous'){
          data.push(parent);
        }else {
          listParent.children.push(parent);
        }
    }
    data.push(listParent);

    let uploadStatusParent = {
      title: "Upload Status",
      key: "Upload Status",
      children: [],
    };
    designs.forEach(design=>{
      let uploadStatusdesign = {
        title: makeDesignsStringLookPretty(design),
        key:_.toLower(design+uploadStatus),
      };
      uploadStatusParent.children.push(uploadStatusdesign)
    })

    data.push(uploadStatusParent);
    setTreeData(data);
  }, []);

  useEffect(() => {
    const col = _.clone(columns);

    col.map(eachCol => {
      eachCol.visible = checkedKeys.indexOf(eachCol.treeSortable) !== -1;
    });
    console.log(col);
    console.log(treeData);
    updateColumns(col);

    // fix updating profile when modal is open
    if(isModalFirstTimeOpened){
      setIsModalFirstTimeOpened(false)
    }else {
      setUpdateProfile(true);
    }

  }, [checkedKeys]);

  const onExpand = expandedKeysValue => {
    console.log('onExpand', expandedKeysValue);
    // if not set autoExpandParent to false, if children expanded, parent can not collapse.
    // or, you can remove all expanded children keys.

    setExpandedKeys(expandedKeysValue);
    setAutoExpandParent(false);
  };

  const onCheck = checkedKeysValue => {
    console.log('onCheck', checkedKeysValue);
    setCheckedKeys(checkedKeysValue);
  };

  const onDragEnter = info => {
    console.log(info);
    // expandedKeys 需要受控时设置
    // this.setState({
    //   expandedKeys: info.expandedKeys,
    // });
  };

  const onDrop = info => {
    const dropKey = info.node.key;
    const dragKey = info.dragNode.key;
    const dropPos = info.node.pos.split('-');
    const dropPosition = info.dropPosition - Number(dropPos[dropPos.length - 1]);

    const loop = (data, key, callback) => {
      for (let i = 0; i < data.length; i++) {
        if (data[i].key === key) {
          return callback(data[i], i, data);
        }
        if (data[i].children) {
          loop(data[i].children, key, callback);
        }
      }
    };
    const data = [...treeData];

    // Find dragObject
    let dragObj;
    loop(data, dragKey, (item, index, arr) => {
      arr.splice(index, 1);
      dragObj = item;
    });

    if (!info.dropToGap) {
      // Drop on the content
      loop(data, dropKey, item => {
        item.children = item.children || [];
        // where to insert 示例添加到头部，可以是随意位置
        item.children.unshift(dragObj);
      });
    } else if (
      (info.node.props.children || []).length > 0 && // Has children
      info.node.props.expanded && // Is expanded
      dropPosition === 1 // On the bottom gap
    ) {
      loop(data, dropKey, item => {
        item.children = item.children || [];
        // where to insert 示例添加到头部，可以是随意位置
        item.children.unshift(dragObj);
        // in previous version, we use item.children.push(dragObj) to insert the
        // item to the tail of the children
      });
    } else {
      let ar;
      let i;
      loop(data, dropKey, (item, index, arr) => {
        ar = arr;
        i = index;
      });
      if (dropPosition === -1) {
        ar.splice(i, 0, dragObj);
      } else {
        ar.splice(i + 1, 0, dragObj);
      }
    }

    setTreeData(data);
    // updateColumns(arrayMove(columns, dragKey, dropKey-1));
  };

  return (
    <Tree
      checkable
      onExpand={onExpand}
      expandedKeys={expandedKeys}
      autoExpandParent={autoExpandParent}
      onCheck={onCheck}
      checkedKeys={checkedKeys}
      // height={height - 400}
      // style={{height: height - 470}}
      treeData={treeData}
      className="draggable-tree"
      draggable
      blockNode
      onDragEnter={onDragEnter}
      onDrop={onDrop}
    />
  );
};

export default MyTree;
