import React, { useEffect, useMemo, useState } from "react";
import { Alert, Col, Image, Modal, Row, Space, Spin, Tooltip } from "antd";
import { fetch2 } from '../../../utils/dataAccess';
import { useNavigate } from 'react-router-dom';
import useWindowDimensions from '../../../utils/hooks/useWindowDimensions';
import { useInfiniteQuery, useMutation } from 'react-query';
import {
  StyledDeleteButton,
  StyledEditButton,
  StyledSearch,
  StyledUploadButton,
  TableStyled,
} from '../../styles/TableStyles';
import qs from 'qs';
import useColumnState from './useColumnState';
import TableFilterModal from './TableFilterModal';
import uploadIcon from '../../../assets/Icons/OldAssets/overview/upload.svg';
import removeIcon from '../../../assets/Icons/OldAssets/overview/delete.png';
import { useDispatch, useSelector } from 'react-redux';
import { reset as resetMedia } from '../../../actions/mediaobject/create';
import { reset as resetCreate } from '../../../actions/design/create';
import { ExclamationCircleOutlined } from '@ant-design/icons';
import { useVT } from 'virtualizedtableforantd4';
import { createdAt, HYDRAMEMBER } from "../../../utils/constants/constants";
import handleElasticTableChange from './tableChange/handleElasticTableSchange';
import useProject from '../../../utils/hooks/useProject';
import ResizableTitle from './ResizableTitle';
import AddToUploadModal from './AddToUploadModal';
import UploadButton from "./UploadButton";
import EditableRow from "./editable/EditableRow";
import EditableCell from "./editable/EditableCell";
import _ from "lodash";

const OverviewTable = ({ setHeader, setSubHeader }) => {
  const { project } = useProject();
  const [showUploadModal, setShowUploadModal] = useState(false);
  const [dataSource, setDataSource] = useState([]);

  //edit button
  const [selectedRowKeys, setSelectedRowKeys] = useState([]);
  const navigate = useNavigate();
  const [isLoading, setIsLoading] = useState(false);


  useEffect(() => {
    return () => {
      setHeader(null);
      setSubHeader(null);
    };
  }, []);

  useEffect(() => {
    if (project) {
      setQueryParams(prevState => ({
        ...prevState,
        project: {
          match_phrase_prefix: { project: project.id },
        },
      }));
      // refetch()
    }
  }, [project]);

  const { created, error: errorDesign, loading: loadingDesign } = useSelector(
    state => state.design.create
  );
  const { error: errorMedia, loading: loadingMedia } = useSelector(state => state.mediaobject.create);

  function fetchDesignsFromElasticSearch({ pageParam = 0 }) {
    if (!pageParam) {
      return fetch2(
        `/es/search?${qs.stringify(queryParams, {  skipNulls: true })}`
      );
    }
    return fetch2(pageParam);
  }

  const {data, error, isFetching, fetchNextPage, refetch, hasNextPage} = useInfiniteQuery(
    ['design'],
    fetchDesignsFromElasticSearch,
    {
      // keepPreviousData: true,
      // staleTime: 10000,
      getNextPageParam: lastPage => lastPage['hydra:view']['hydra:next'],
      enabled: false,
    }
  );

  useEffect(() => {
    let paginatedData = [];
    data?.pages.forEach(page => {
      paginatedData = paginatedData.concat(page[HYDRAMEMBER]);
    });
    setDataSource(paginatedData);
  }, [data]);

  //table
  const { height, width } = useWindowDimensions();
  const [vt, setVT] = useVT(
    () => ({
      debug: true,
      scroll: { y: height - 355 },
      onScroll: ({ isEnd }) => {
        if (isEnd) {
         fetchNextPage();
        }
      },
    }),
    []
  );

  useMemo(() => setVT({body: {
      row: EditableRow,
      cell: EditableCell
    }}), [setVT]);



  const [queryParams, setQueryParams] = useState({
    pageSize: 30,
    from: 0,
    sort: { [createdAt]: { order: 'desc' },["id"]: { order: 'desc' } },
    //this is just for reference (backend dont use it )
    queries: [{ field: 'isEdit', value: false, type: 'boolean' }],
    project: {
      match_phrase_prefix: { project: project?.id },
    },
  });

  const deleteSelectedDesignsPromise = ids =>
    Promise.all(
      ids.map(id=>
        fetch2(('/designs/'+id ), {
          method: 'DELETE',
          body: JSON.stringify({}),
        })
      )
    );

  const deleteMutation = useMutation(deleteSelectedDesignsPromise);
  const { columns, columnSettings, setColumnSettings, setUpdateProfile } = useColumnState();

  useEffect(() => {
    if (queryParams.project.match_phrase_prefix['project']) {
      refetch();
    }
  }, [queryParams]);

  const dispatch = useDispatch();

  useEffect(() => {
    if (loadingDesign === false) {
      setSelectedRowKeys([]);
    }
  }, [loadingDesign]);

  useEffect(() => {
    if (selectedRowKeys.length > 0) {
      setSubHeader(
        `Selected ${selectedRowKeys.length} out of ${data?.pages[0]['hydra:totalItems']} designs`
      );
    } else {
      setSubHeader('');
    }
  }, [selectedRowKeys]);


  useEffect(() => {
    if (deleteMutation.isSuccess) {
      setTimeout(function() {
        refetch();
      }, 700);
      setSelectedRowKeys([]);
    }
  }, [deleteMutation.isSuccess]);

  const pressEditButton = () => {
    navigate('/dashboard/workspace',{ state: { ids: selectedRowKeys }});
    // setVisible(false);
  };

  const pressDeleteButton = () => {
    const modal = Modal.confirm({
      title: "You're about to delete designs",
      icon: <ExclamationCircleOutlined />,
      content: `${selectedRowKeys.length} designs will be deleted`,
      okText: 'Yes',
      okType: 'danger',
      cancelText: 'No',
      onOk() {
        deleteMutation.mutate(selectedRowKeys);
      },
    });
  }
  const onSelectChange = selectedRowKeys => {
    console.log('selectedRowKeys changed: ', selectedRowKeys);
    setSelectedRowKeys(selectedRowKeys);
  };
  const rowSelection = {
    selectedRowKeys,
    onChange: onSelectChange,
    preserveSelectedRowKeys: true,
    // fixed:true,
    columnWidth: 32,
  };
  const hasSelected = selectedRowKeys.length > 0;

  const onSearch = value => {
    console.log(value);
    setQueryParams(prevState => ({ ...prevState, search_all: value }));
  };

  const onPressUploadButton = () => {
    setShowUploadModal(true);
    // isEditMutation.mutate({ isUpload: true, isEdit: false, isUploaded: false, ids: selectedRowKeys });
  };

  const handleSave = (row) => {
    const newData = [...dataSource];
    const index = newData.findIndex((item) => row.id === item.id);
    const item = newData[index];
    newData.splice(index, 1, { ...item, ...row });
    setDataSource(newData);
  };

  const columnsEditable = columns.map((col) => {
    if (!col.editable) {
      return col;
    }

    return {
      ...col,
      onCell: (record) => ({
        record,
        editable: col.editable,
        dataIndex: col.dataIndex,
        title: col.title,
        handleSave: handleSave,
      }),
    };
  });

  useEffect(() => {
    setHeader(<Spin spinning={isFetching || loadingDesign || loadingMedia || deleteMutation.isLoading || isLoading} />)
  }, [isFetching || loadingDesign || loadingMedia || deleteMutation.isLoading || isLoading]);


  return (
    <div>
      {error &&
      <Alert message={error.message} type="error" showIcon closable />}
      <>
        {errorDesign && (
          <>
            <Alert
              message={errorDesign}
              type="error"
              showIcon
              closable
              afterClose={() => {
                dispatch(resetCreate());
              }}
            />
          </>
        )}
        {loadingMedia && <Spin tip="Loading..." />}
        {errorMedia && (
          <>
            <Alert
              message={errorMedia}
              type="error"
              showIcon
              closable
              afterClose={() => {
                dispatch(resetMedia());
              }}
            />
          </>
        )}
      </>
      <Row gutter={[16, 16]} justify="space-around" align="middle" style={{ marginBottom: 16 }}>
        <Col xs={24} sm={24} md={24} lg={24} xl={12}>
          <StyledSearch
            size="large"
            bordered={false}
            allowClear
            placeholder={'Search'}
            onSearch={onSearch}
            enterButton
          />
        </Col>
        <Col span={3}>
          <TableFilterModal
            setUpdateProfile={setUpdateProfile}
            columns={columnSettings}
            updateColumns={setColumnSettings}
          />
        </Col>
        <Col xs={24} sm={24} md={21} lg={21} xl={9}>
          <Space size={[8, 16]} style={{ float: 'right' }}>

            <UploadButton setIsLoading={setIsLoading}/>

            <Tooltip placement="top" title="Add to Upload Queue">
              <StyledUploadButton disabled={!hasSelected} onClick={onPressUploadButton}>
                <img src={uploadIcon} alt="upload-action1" style={{ cursor: 'pointer' }} />
              </StyledUploadButton>
            </Tooltip>
            <StyledEditButton
              type="primary"
              onClick={pressEditButton}
              disabled={!hasSelected}
            >
              Edit
            </StyledEditButton>
            {/*<StyledEditButton*/}
            {/*  type="primary"*/}
            {/*  onClick={() => pressEditButton(false)}*/}
            {/*  disabled={!hasSelected}*/}
            {/*  loading={loading}*/}
            {/*>*/}
            {/*  Remove from edit*/}
            {/*</StyledEditButton>*/}
            <Tooltip placement="topRight" title="Delete design">
              <StyledDeleteButton  onClick={pressDeleteButton} disabled={!hasSelected}>
                <img src={removeIcon} alt="remove-action1" />
              </StyledDeleteButton>
            </Tooltip>
          </Space>
        </Col>
      </Row>
      <Image.PreviewGroup>
        <TableStyled
          bordered
          rowSelection={rowSelection}
          columns={columnsEditable}
          dataSource={dataSource}
          scroll={{
            y: width > 1200 ? height - 300 : width > 767 ? height - 350 : height - 400,
          }}
          height={
           width > 1200 ? height - 300 : width > 767 ? height - 350 : height - 400
          }
          pagination={false}
          // loading={isFetching || loadingDesign || loadingMedia || deleteMutation.isLoading || isLoading}
          rowKey={record => record['id']}
          onChange={(pagination, filters, sorter) =>
            handleElasticTableChange(pagination, filters, sorter, queryParams, setQueryParams)
          }
          components={{
            ...vt,
            header: {
              cell: ResizableTitle,
            },

          }}
        />
      </Image.PreviewGroup>
      <AddToUploadModal
        showUploadModal={showUploadModal}
        setShowUploadModal={setShowUploadModal}
        selectedRowKeys={selectedRowKeys}
        designs={dataSource}
      />
    </div>
  )
}

export default OverviewTable
