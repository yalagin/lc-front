import React from "react";
import {
  changed,
  idle,
  updated,
  uploaded,
  UploadStatusesEnum,
  UploadStatusesHelpObject,
  UploadStatusesNumberToString
} from "../../../../utils/constants/upload";
import { Tooltip, Typography } from "antd";
import _ from "lodash";
import { DesignMba } from "../../../../interfaces/designmba";
import { Design } from "../../../../interfaces/design";
const { Title ,Text} = Typography;


function Status(props: {status: UploadStatusesEnum}) {
  return (
    <Text keyboard>
      {props.status ? (
        <Tooltip title={UploadStatusesHelpObject[UploadStatusesNumberToString[props.status]]}>
          {UploadStatusesNumberToString[props.status]}
        </Tooltip>
      ) : (
        <Tooltip title={'select sub design in the workspace and fill up the fields and save it'}>
          sub design not created
        </Tooltip>
      )}
    </Text>
  );
}


export function UploadStatusColDesign({status, record}:{status:UploadStatusesEnum, record:Design}) {

  if(record.designMBA?.hasOwnProperty("mbaProductPublished") && !_.isEmpty(record.designMBA?.mbaProductPublished) ){
    return (
      <Text keyboard>
        {<Tooltip title={UploadStatusesHelpObject[UploadStatusesNumberToString[status]]} key={status}>
            {/*{status !== UploadStatusesEnum[uploaded] && status !== UploadStatusesEnum[idle]  && status !== UploadStatusesEnum[changed] && updated+' '}*/}
            {status !== UploadStatusesEnum[changed] && UploadStatusesNumberToString[status]}
            {status === UploadStatusesEnum[changed] && updated}
        </Tooltip> }
      </Text>
    );
  }

  return <Status status={status} />;
}


export function UploadStatusColSubDesign({
  status,
  record,
}: {
  status: UploadStatusesEnum;
  record: DesignMba;
}) {
  if (record?.hasOwnProperty('mbaProductPublished') && !_.isEmpty(record?.mbaProductPublished)) {
    return (
      <Text keyboard>
        {
          <Tooltip
            title={UploadStatusesHelpObject[UploadStatusesNumberToString[status]]}
            key={status}
          >
            {status !== UploadStatusesEnum[uploaded] &&
              status !== UploadStatusesEnum[idle] &&
              status !== UploadStatusesEnum[changed] &&
              updated+' '}
            {status !== UploadStatusesEnum[changed] && UploadStatusesNumberToString[status]}
            {status === UploadStatusesEnum[changed] && updated}
          </Tooltip>
        }
      </Text>
    );
  }
  return <Status status={status} />;
}
