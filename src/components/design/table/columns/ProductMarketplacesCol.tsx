import React, {useEffect, useState} from 'react';
import {DesignMba} from '../../../../interfaces/designmba';
import { useQuery, useQueryClient } from "react-query";
import {fetch2} from '../../../../utils/dataAccess';
import {HYDRAMEMBER} from '../../../../utils/constants/constants';
import { Avatar, Button, message, Modal, Space, Spin, Tooltip } from "antd";
import usePatchMutation from "../../../../utils/mutations/usePatchMutation";
import _ from 'lodash';
import {FlexDivCol} from '../../../styles/ProfileStyles';
import {TemplateSettingsMbaProduct} from '../../../../interfaces/templatesettingsmbaproduct';
import {TemplateSettingsMbaMarketPlace} from '../../../../interfaces/templatesettingsmbamarketplace';
import MbaPanel from '../../../template/mba/MbaPanel';
import { Form, FormRenderProps } from "react-final-form";
// @ts-ignore
import ReactCountryFlag from 'react-country-flag';
import { PagedCollection } from "../../../../interfaces/Collection";
import MbaPublicationTable from "../MbaPublicationTable";
import { mbaProducts } from "../../../../utils/urls";

interface CountryWithProducts {
  [key: string]: string[];
  'USA': ['shirt',"premium-shirt"]
}

function ProductMarketplacesCol({record}: {record: DesignMba}) {
  const [marketPlaceCounter, setMarketPlaceCounter] = useState<null|CountryWithProducts>(null);
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [designMba, setDesignMba] = useState<DesignMba>(record);
  const queryClient = useQueryClient();

  const {isLoading, error, data: products} = useQuery<PagedCollection<TemplateSettingsMbaProduct>>('mba_products', mbaProducts,{staleTime: 60 * 60 * 1000 });

  const {
    isLoading: isLoadingMarketplaces,
    error: errorMarketplaces,
    data: marketplaces,
  } = useQuery<PagedCollection<TemplateSettingsMbaMarketPlace>>('template_settings_mba_market_places', () =>
    fetch2('/template_settings_mba_market_places'),{staleTime: 60 * 60 * 1000 } // one hour of caching
  );

  const patchMutation = usePatchMutation();

  function onSubmit(values: DesignMba) {
    // @ts-ignore
    patchMutation.mutate({
      '@id': "/design_mbas/"+values?.id,
      mbaProductCheckbox: values.mbaProductCheckbox,
      mbaProductPrice: values.mbaProductPrice,
    }, {
      onSuccess: (data, variables, context) => {
        message.success(`Publication saved`);
        setDesignMba(values);
        setTimeout(()=>{
          queryClient.refetchQueries('design');
          queryClient.refetchQueries('designs');
        },2000)
      }
    });
  }

  useEffect(() => {
    if (
      designMba?.mbaProductCheckbox &&
      products &&
      products['hydra:totalItems'] &&
      marketplaces &&
      marketplaces['hydra:totalItems']
    ) {
      const objectOfMarketPlaces = {} as CountryWithProducts;
      Object.entries(designMba.mbaProductCheckbox).forEach(function (
        [product, marketplaceChecked],
        index
      ) {
        const productName = products[HYDRAMEMBER].filter(
          (data: TemplateSettingsMbaProduct) => data['@id'] === product
        )[0]?.name;
        Object.entries(marketplaceChecked).forEach(function ([marketplace, bool], index) {
          if (bool) {
            const country = marketplaces[HYDRAMEMBER].filter(
              (data: TemplateSettingsMbaMarketPlace) => data['@id'] === marketplace
            )[0]?.country;
            if (!Array.isArray(objectOfMarketPlaces[country])) objectOfMarketPlaces[country] = [];
            objectOfMarketPlaces[country].push(productName);
          }
        });
      });
      const objectOfMarketPlacesSorted = {} as CountryWithProducts;
      marketplaces[HYDRAMEMBER].forEach(function (marketplace) {
        if(!(marketplace.country in objectOfMarketPlaces)){
          objectOfMarketPlacesSorted[marketplace.country]=[];
        }else{
          objectOfMarketPlacesSorted[marketplace.country]=objectOfMarketPlaces[marketplace.country];
        }
      })
      setMarketPlaceCounter(objectOfMarketPlacesSorted);
    }
  }, [designMba, products, marketplaces]);

  return (
    <Space wrap style={{cursor: 'pointer'}} onClick={() => setIsModalVisible(true)}>
      {marketPlaceCounter&&
        Object.keys(marketPlaceCounter).map((keyName: string, i) => (
          <Tooltip title={marketPlaceCounter[keyName].join(', ')}>
            <FlexDivCol style={{alignItems: 'center',flexWrap: "wrap"}}>
              <Avatar src={<ReactCountryFlag style={{borderRadius: '40px'}} svg countryCode={keyName} />} />{' '}
              <div>{marketPlaceCounter[keyName].length}</div>
            </FlexDivCol>
          </Tooltip>
        ))}
      {_.isEmpty(marketPlaceCounter)&&!(isLoading||isLoadingMarketplaces||patchMutation.isLoading)
      && <Button onClick={()=>{setIsModalVisible(true)}}>Add products</Button>}
        <Form<DesignMba> onSubmit={onSubmit} initialValues={designMba} debug={process.env.REACT_APP_SPY?console.log:()=>{}}>
          {(props:FormRenderProps) => (
            <Modal
              title="Edit selected Mba Products and Marketplaces"
              visible={isModalVisible}
              onOk={e => {
                e.stopPropagation();
                setIsModalVisible(false);
                if(props.dirty) {
                  props.handleSubmit();
                  console.log('handleSubmit');
                }
              }}
              onCancel={e => {
                e.stopPropagation();
                setIsModalVisible(false)}}
              width={650}
            >
              <form onSubmit={props.handleSubmit}>
                {products && marketplaces && (<MbaPublicationTable products={products} form={props.form} designMba={designMba}  marketplaces={marketplaces} />)}
                {/*{products && (*/}
                {/*  <MbaPanel*/}
                {/*    data={products}*/}
                {/*    form={props.form}*/}
                {/*  />*/}
                {/*)}*/}
              </form>
            </Modal>
          )}
        </Form>
      <Spin spinning={isLoading||isLoadingMarketplaces||patchMutation.isLoading} />
    </Space>
  );
}

export default ProductMarketplacesCol;
