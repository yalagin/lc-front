import React, { forwardRef, useMemo } from "react";
import _ from 'lodash';
import { decodeTranslationObject } from "../../../../utils/constants/constants";
import {
  getTitleForDesign,
} from "../../../../utils/designHelperFunctions";

const Item = forwardRef(({id, ...props}, ref) => {
  const style = {
    cursor: 'grabbing',
    boxShadow:
      '0 0 0 calc(1px / var(--scale-x, 1)) rgba(63, 63, 68, 0.05), 0 1px calc(3px / var(--scale-x, 1)) 0 rgba(34, 33, 81, 0.15)',
    padding: 5,
    margin: 5,
    background: 'white',
  };

  return (
    <div style={{...style}} {...props} ref={ref}>
      {' '}
      {useMemo(() =>getTitleForDesign(id.parent,id.title),[id.parent,id.title])}
    </div>
  );
});
export default Item;
