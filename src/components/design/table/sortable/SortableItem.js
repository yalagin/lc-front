import React from 'react';
import { useSortable } from '@dnd-kit/sortable';
import { CSS } from '@dnd-kit/utilities';
import Item from './Item';

export function SortableItem(props) {
  const { attributes, listeners, setNodeRef, transform, transition } = useSortable({
    id: props.id
  });

  const style = {
    transform: CSS.Transform.toString(transform),
    transition,
    cursor:'grab',
    boxShadow: "0 0 0 calc(1px / var(--scale-x, 1)) rgba(63, 63, 68, 0.05), 0 1px calc(3px / var(--scale-x, 1)) 0 rgba(34, 33, 81, 0.15)",
    padding: 5,
    margin: 5,
    display: props.item.visible&& props.item.key!==0 ?"":"none"
  };

  return (
    <Item id={props.item} ref={setNodeRef} style={style} {...attributes} {...listeners}/>
  );
}
