import React, {useEffect, useRef, useState} from 'react';
import {
  designMBA,
  general,
  getTypeOfField,
  image,
  isPlaceholder,
  numberOfShowedFieldsInDesignTable,
  objectOfDesignFieldsForProcessingSubDesigns, Publication,
  tags, uploadStatus
} from "../../../utils/constants/constants";
import _ from 'lodash';
import {CheckCircleOutlined, CloseCircleOutlined} from '@ant-design/icons';
import NonEditableTagGroup from '../../input/NonEditableTagGroup';
import TableDesignImageContainer from '../../mediaobject/image/TableDesignImage';
import columnSearchProps from './useColumnSearchProps';
import useProfile from '../../../utils/hooks/useProfile';
import {update} from '../../../actions/person/update';
import {useDispatch} from 'react-redux';
import {
  getTitleForDesign,
} from "../../../utils/designHelperFunctions";
import { ENTRYPOINT } from "../../../config/entrypoint";
import {
  idle,
  UploadStatusesHelpObject,
  UploadStatusesNumberToString,
  UploadStatusesEnum, UploadStatusesForFilter, updated, uploaded
} from "../../../utils/constants/upload";
import { Tooltip, Typography } from "antd";
import ProductMarketplacesCol from "./columns/ProductMarketplacesCol";
import { UploadStatusColDesign } from "./columns/uploadStatusCol";
import {
  designAndTheirLanguages,
  objectOfDesignsWithNotTranslatedFields,
  objectOfDesignsWithTranslatedFields
} from "../../../utils/constants/translation";

const { Title ,Text} = Typography;

const useColumnState = () => {
  const [searchText, setSearchText] = useState('');
  const [searchedColumn, setSearchedColumn] = useState('');
  const [columnSettings, setColumnSettings] = useState([]);
  const [columns, updateColumns] = useState([]);
  const {profile} = useProfile();
  const dispatch = useDispatch();
  const [updateProfile, setUpdateProfile] = useState(false);
  const setServicesValueDebounced = useRef(_.debounce(() => setUpdateProfile(true), 1000));

  useEffect(() => {
    if (updateProfile) {
      dispatch(update(profile, {colSettings: columnSettings}));
      setUpdateProfile(false);
    }
  }, [columnSettings, updateProfile]);

  useEffect(() => {
    if (_.isEmpty(profile.colSettings)) {
      const colSettings = [];
      for (const [design, fields] of Object.entries(objectOfDesignFieldsForProcessingSubDesigns)) {
        if (design in designAndTheirLanguages) {
          objectOfDesignsWithNotTranslatedFields[design].forEach((field) => {
            colSettings.push({
              title: field,
              parent: design,
              treeSortable: _.toLower(design + field),
              key: colSettings.length,
              visible: colSettings.length < numberOfShowedFieldsInDesignTable,
              type: getTypeOfField(field),
              width: 200,
            });
          });
          designAndTheirLanguages[design].forEach(lang => {
                objectOfDesignsWithTranslatedFields[design].forEach((field) => {
                    colSettings.push({
                      title: field,
                      parent: [design, 'translations', lang],
                      treeSortable: _.toLower(design + 'translations' + lang + field),
                      key: colSettings.length,
                      visible: colSettings.length < numberOfShowedFieldsInDesignTable,
                      type: getTypeOfField(field),
                      width: 200,
                    });
                });
            });

        } else {
          if (_.isArray(fields)) {
            fields.forEach((field) => {
              colSettings.push({
                title: field,
                parent: design,
                treeSortable: _.toLower(design + field),
                key: colSettings.length,
                visible: colSettings.length < numberOfShowedFieldsInDesignTable,
                type: getTypeOfField(field),
                width: 200,
              });
            });
          }
        }
      }

      // for (const [key, value] of Object.entries(uploadStatus)) {
      //   if (_.isArray(value)) {
      //     value.forEach((field, index) => {
      //       colSettings.push({
      //         title: field,
      //         parent: key,
      //         treeSortable: _.toLower(key + field),
      //         key: colSettings.length,
      //         visible: colSettings.length < numberOfShowedFieldsInDesignTable,
      //         type: getTypeOfField(field),
      //         width: 200,
      //       });
      //     });
      //   }
      // }
      console.log(colSettings);
      if (!_.isEmpty(colSettings)) {
        setColumnSettings(colSettings);
        setUpdateProfile(true);
      }
    } else {
      setColumnSettings(profile.colSettings);
    }
    //change initial order here
  }, []);

  const handleResize = index => (e, {size}) => {
    e.stopImmediatePropagation();
    e.stopPropagation();
    e.preventDefault();
    setColumnSettings(columns => {
      const nextColumns = [...columns];
      nextColumns[index] = {
        ...nextColumns[index],
        width: size.width,
      };
      setServicesValueDebounced.current();
      return nextColumns;
    });
  };

  useEffect(() => {
    updateColumns(
      columnSettings
        .map((field, index) => {
          const {type, title, parent, visible, width} = field;
          if (visible === false) {
            return null;
          }
          let render = {};
          switch (type) {
            case 'bool':
              render = {
                render: bool =>
                  bool ? (
                    <CheckCircleOutlined style={{marginLeft: '2rem', color: '#0ea201'}} />
                  ) : (
                    <CloseCircleOutlined style={{marginLeft: '2rem', color: '#7f67ff'}} />
                  ),
                filters: [
                  {text: <CheckCircleOutlined style={{color: '#0ea201'}} />, value: true},
                  {text: <CloseCircleOutlined style={{color: '#7f67ff'}} />, value: false},
                ],
                filterMultiple: false,
                sorter: true,
              };
              break;
            case tags:
              render = {
                // dataIndex: 'tags',
                ...columnSearchProps(
                  searchedColumn,
                  setSearchedColumn,
                  searchText,
                  setSearchText,
                  title
                ),
                render: tags => <NonEditableTagGroup tags={tags} />,
              };
              break;
            case 'time':
              render = {
                sorter: true,
                ...columnSearchProps(
                  searchedColumn,
                  setSearchedColumn,
                  searchText,
                  setSearchText,
                  title,
                  true
                ),
                render: updatedAt => new Date(updatedAt).toLocaleString(),
              };
              break;
            case image:
              render = {
                fixed: 'left',
                render: image => (
                  <TableDesignImageContainer
                    key={image ? image.id : null}
                    image={image}
                    preview={true}
                  />
                ),
              };
              break;
            case 'file':
              render = {
                fixed: 'left',
                render: files => (_.isArray(files)? files.map(file=> new URL(file?.contentUrl, ENTRYPOINT)):null)
              };
              break;
            case isPlaceholder:
              render = {
                render: bool =>
                  !!!bool ? (
                    <CheckCircleOutlined style={{marginLeft: '2rem', color: '#0ea201'}} />
                  ) : (
                    <CloseCircleOutlined style={{marginLeft: '2rem', color: '#7f67ff'}} />
                  ),
                filters: [
                  {text: <CheckCircleOutlined style={{color: '#0ea201'}} />, value: true},
                  {text: <CloseCircleOutlined style={{color: '#7f67ff'}} />, value: false},
                ],
                filterMultiple: false,
                dataIndex: image,
              };
              break;
            case uploadStatus:
              render = {
                render: (status, record, index) =><UploadStatusColDesign  record={record} status={status}/>,
                // filterMultiple:false,
                sorter: true,
                defaultSortOrder: 'descend',
                filters:
                  Object.keys(UploadStatusesForFilter).map(value => {
                    return  {text: <Text keyboard>
                        {value?   <Tooltip title={UploadStatusesHelpObject[value] } key={value}>   {value}</Tooltip>:
                          <Tooltip title={"select sub design in the workspace and fill up the fields and save it"} key={1}>sub design not created</Tooltip> }
                      </Text> , value: UploadStatusesEnum[value]}
                  })
                ,
                editable: true,
              };
              break;
            case Publication:
              render = {
                // record.hasOwnProperty(designMBA) ? designMBA + '.' : ''
                render: (text, record, index) => <ProductMarketplacesCol text={text} record={ record.hasOwnProperty(designMBA) ? record.designMBA : {}} key={index} />,
              }
              break;
            default:
              render = {
                ...columnSearchProps(
                  searchedColumn,
                  setSearchedColumn,
                  searchText,
                  setSearchText,
                  title
                ),
              };
          }
          //general are first level nested data
          if(_.isArray(parent)){
            if(parent[0] === "general"){
              parent.shift();
            }
            render = {...render, dataIndex: [...parent, title]};
          } else {
            if(parent === "general"){
            render = {...render, dataIndex: title};
            } else {
              render = {...render, dataIndex: [parent,title]};
            }
          }

          return {
            title: () => {
              const titleString = getTitleForDesign(parent, title);
              if(titleString.indexOf("(") !== -1){
                const chars = titleString.split('(');
                return <>{chars[0]} <br/> <Text type="secondary" style={{fontSize:"11px"}}>({chars[1]}</Text></>
              }
              return <Title level={5}>{titleString}</Title>
            },
            onHeaderCell: column => ({
              width: column.width,
              onResize: handleResize(index),
            }),
            // key: key,
            minWidth: 300,
            width,
            ...render,
          };
        })
        .filter(filter => !!filter)
    );
  }, [columnSettings, searchedColumn, searchText]);

  return {columns, updateColumns, columnSettings, setColumnSettings, setUpdateProfile};
};

export default useColumnState;
