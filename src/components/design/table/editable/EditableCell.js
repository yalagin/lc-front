import React, { useContext, useEffect, useRef, useState } from 'react';
import { Form, message, Select, Tooltip } from 'antd';
import { EditableContext } from './EditableRow';
import {
  UploadStatusesForFilter,
  UploadStatusesHelpObject,
} from '../../../../utils/constants/upload';
import { useMutation } from 'react-query';
import { fetch2 } from '../../../../utils/dataAccess';
import _ from 'lodash';

const EditableCell =  React.forwardRef(({ title, editable, children, dataIndex, record, handleSave, ...restProps },ref) => {
  const [editing, setEditing] = useState(false);
  const inputRef = useRef(null);
  const form = useContext(EditableContext);

  const editTemplate = useMutation(
    data => {
      return fetch2('/designs/' + data['id'], {
        method: 'PATCH',
        headers: { 'Content-Type': 'application/merge-patch+json' },
        body: JSON.stringify(data),
      });
    },
    {
      onSuccess: (data, variables, context) => {
        message.info(`Saved!`);
        toggleEdit();
        handleSave(data);
      },
    }
  );

  useEffect(() => {
    if (editing) {
      inputRef.current.focus();
    }
  }, [editing]);

  const toggleEdit = () => {
    setEditing(!editing);
    form.setFieldsValue({
      [dataIndex]: record[dataIndex],
    });
  };

  const save = async () => {
    try {
      const values = await form.validateFields();
      const changedValue = _.get(values, dataIndex);
      const newVar = _.get(record, dataIndex);
      if (_.isUndefined(changedValue) || newVar === changedValue || editTemplate.isLoading) {
        return toggleEdit();
      }
      editTemplate.mutate({ id: record.id, ...values });
    } catch (errInfo) {
      console.log('Save failed:', errInfo);
    }
  };

  let childNode = children;

  if (editable) {
    childNode = editing ? (
      <Form.Item
        style={{
          margin: 0,
        }}
        name={dataIndex}
        // rules={[
        //   {
        //     required: true,
        //     message: `select value`,
        //   },
        // ]}
      >
        <Select
          ref={inputRef}
          defaultValue={_.get(record, dataIndex)}
          style={{ width: 120 }}
          onBlur={save}
          onSelect={save}
          loading={editTemplate.isLoading}
        >
          {Object.keys(UploadStatusesForFilter).map(value => {
            return (
              <Select.Option value={UploadStatusesForFilter[value]}>
                <Tooltip title={UploadStatusesHelpObject[value]} placement="right" key={value}>
                  <div style={{ width: '100%' }}>
                    <div
                      style={{
                        overflow: 'hidden',
                        whiteSpace: 'nowrap',
                        textOverflow: 'ellipsis',
                      }}
                    >
                      {value}
                    </div>
                  </div>
                </Tooltip>
              </Select.Option>
            );
          })}
        </Select>
      </Form.Item>
    ) : (
      <div
        className="editable-cell-value-wrap"
        style={{
          paddingRight: 24,
        }}
        onClick={toggleEdit}
      >
        {children}
      </div>
    );
  }

  return <td ref={ref}  {...restProps}>{childNode}</td>;
});

export default EditableCell;
