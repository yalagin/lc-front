import React from 'react';
import { Form } from 'antd';

export const EditableContext = React.createContext(null);

const EditableRow =  React.forwardRef(({ index,record, ...props },ref)=> {
  const [form] = Form.useForm();
  return (
    <Form form={form} component={false} initialValues={record}>
      <EditableContext.Provider value={form}>
        <tr ref={ref}  {...props} />
      </EditableContext.Provider>
    </Form>
  );
});

export default EditableRow;
