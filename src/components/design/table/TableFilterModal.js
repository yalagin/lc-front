import React, {useState} from 'react';
import {Button, Card, Col, Modal, Row} from 'antd';
import MyTree from './MyTree';
import {ControlOutlined} from '@ant-design/icons';
import SortColumns from './sortable/SortColumns';
import useWindowDimensions from '../../../utils/hooks/useWindowDimensions';
import {update} from '../../../actions/person/update';
import {useDispatch} from 'react-redux';
import useProfile from '../../../utils/hooks/useProfile';
import {useNavigate} from 'react-router-dom';
import ColorPicker from '../ColorPicker/ColorPicker';

const TableFilterModal = ({columns, updateColumns, setUpdateProfile}) => {
  const [isModalVisible, setIsModalVisible] = useState(false);
  const {height} = useWindowDimensions();
  const dispatch = useDispatch();
  const {profile} = useProfile();
  const navigate = useNavigate();

  const showModal = () => {
    setIsModalVisible(true);
  };
  const handleOk = () => {
    setIsModalVisible(false);
  };

  const handleResetFilters = () => {
    setIsModalVisible(false);
    dispatch(update(profile, {colSettings: null}));
    navigate(0);
  };
  const handleCancel = () => {
    setIsModalVisible(false);
  };

  return (
    <>
      <Button onClick={showModal}>
        <ControlOutlined />
        Filter & Sort
      </Button>
      <Modal
        title="Filters and rearrange columns"
        visible={isModalVisible}
        onOk={handleOk}
        onCancel={handleCancel}
        width={900}
        // height={height - 400}
        footer={[
          <div style={{marginTop: '-6px', float: 'left'}}>
            <ColorPicker />
          </div>,
          <Button key="link" type="primary" onClick={handleResetFilters}>
            Reset Filters
          </Button>,
          <Button key="link" type="primary" onClick={handleOk}>
            Ok
          </Button>,
        ]}
      >
        <Row gutter={[16, 16]} style={{maxHeight: height - 370}}>
          <Col span={10}>
            <Card
              style={{
                height: '50vh',
                margin: '0 auto',
                overflow: 'auto',
              }}
            >
              <MyTree
                columns={columns}
                updateColumns={updateColumns}
                setUpdateProfile={setUpdateProfile}
              />
            </Card>
          </Col>
          <Col span={14}>
            <Card
              style={{
                height: '50vh',
                margin: '0 auto',
                overflow: 'auto',
              }}
            >
              <SortColumns
                items={columns}
                setItems={updateColumns}
                setUpdateProfile={setUpdateProfile}
              />
            </Card>
          </Col>
        </Row>
      </Modal>
    </>
  );
};

export default TableFilterModal;
