import React from 'react';
import { Button, Col, DatePicker, Input, Row, Space } from 'antd';
import { CalendarFilled, SearchOutlined } from '@ant-design/icons';
import Highlighter from 'react-highlight-words';

const columnSearchProps = (searchedColumn, setSearchedColumn,searchText, setSearchText,dataIndex, timePicker = false) => {

  let searchInput ;

  const handleSearch = (selectedKeys, confirm, dataIndex) => {
    confirm();
    setSearchText(selectedKeys[0]);
    setSearchedColumn(dataIndex);
  };

  const handleReset = clearFilters => {
    clearFilters();
    setSearchText('');
    setSearchedColumn('');
  };

  const filter = {
    filterDropdown: ({ setSelectedKeys, selectedKeys, confirm, clearFilters }) => {
      if (!timePicker)
        return (
          <div style={{ padding: 8 }}>
            <Input
              ref={node=>searchInput=node}
              placeholder={`Search ${dataIndex}`}
              value={selectedKeys[0]}
              onChange={e => setSelectedKeys(e.target.value ? [e.target.value] : [])}
              onPressEnter={() => handleSearch(selectedKeys, confirm, dataIndex)}
              style={{ width: 188, marginBottom: 8, display: 'block' }}
            />
            <Space>
              <Button
                type="primary"
                onClick={() => handleSearch(selectedKeys, confirm, dataIndex)}
                icon={<SearchOutlined />}
                size="small"
                style={{ width: 90 }}
              >
                Search
              </Button>
              <Button onClick={() => handleReset(clearFilters)} size="small" style={{ width: 90 }}>
                Reset
              </Button>
              <Button
                type="link"
                size="small"
                onClick={() => {
                  confirm({ closeDropdown: false });
                  setSearchText(selectedKeys[0]);
                  setSearchedColumn(dataIndex);
                }}
              >
                Filter
              </Button>
            </Space>
          </div>
        );
      if (timePicker)
        return (
          <div style={{ padding: 8 }}>
            <Row gutter={16}>
              <Col span={24} style={{ width: 75 }}>
                <DatePicker.RangePicker
                  autoFocus={true}
                  value={selectedKeys}
                  onChange={(date, dateString) => {
                    setSelectedKeys(date);
                  }}
                  // format={format}
                  style={{ marginBottom: 8 }}
                />
              </Col>
              <Col span={24}>
                <Button
                  type="primary"
                  role="search"
                  onClick={() => handleSearch(selectedKeys, confirm, dataIndex)}
                  // style={{ width: btnWidth }}
                  icon={<SearchOutlined />}
                  size="small"
                  style={{ width: 90 }}
                >
                  {/*{label[0]}*/}Submit
                </Button>
                <Button
                  role="reset"
                  // style={{ width: btnWidth, marginLeft }}
                  onClick={() => {
                    handleReset(clearFilters);
                  }}
                  type="link"
                  size="small"
                  style={{ width: 90 }}
                >
                  {/*{label[1]}*/}
                  {`Reset ${dataIndex} search`}
                </Button>
              </Col>
            </Row>
          </div>
        );
    },
    filterIcon: filtered =>
      !timePicker ? (
        <SearchOutlined style={{ color: filtered ? '#7f67ff' : undefined }} />
      ) : (
        <CalendarFilled style={{ color: filtered ? '#7f67ff' : undefined }} />
      ),
    onFilterDropdownVisibleChange: visible => {
      if (visible) {
        setTimeout(() => (!timePicker ? searchInput?.select() : null), 100);
      }
    },
  };

  if(dataIndex === searchedColumn) {
    return {
      ... filter,
      render: text =>
          <Highlighter
            highlightStyle={{backgroundColor: '#ffc069', padding: 0}}
            searchWords={[searchText]}
            autoEscape
            textToHighlight={text ? text.toString() : ''}
          />
    };
  } else{
    return {...filter, render: text => <div className={'text-col'} >{text}</div>};
  }
};

export default columnSearchProps;
