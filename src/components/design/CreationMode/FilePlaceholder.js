import React, { useMemo } from 'react';
import { Button, Dropdown, Menu, Typography } from 'antd';
import { DownOutlined } from '@ant-design/icons';
import Delete from '../../../assets/Icons/ReactSvg/Delete';
import Download from '../../../assets/Icons/ReactSvg/Download';
import { IconSmall, MyButton, SmallButton } from '../../styles/CreationFiles';

const { Text } = Typography;

const FilePlaceholder = ({ file, handleDeleteFile, icon, downloadFile }) => {
  //
  // function downloadFileByLink() {
  //   return fetch2(
  //     file.contentUrl,
  //   );
  // }
  //  todo cors with backend
  // const downloadMutation = useMutation(downloadFileByLink, {
  //   onSuccess: (data, variables, context) => {
  //     const url = window.URL.createObjectURL(new Blob([data]));
  //     const link = document.createElement('a');
  //     link.href = url;
  //     link.setAttribute('download', file.originalName); //or any other extension
  //     document.body.appendChild(link);
  //     link.click();
  //   }, onError: (error, variables, context) => {
  //     notification.error({ message: error.message });
  //   },
  // });

  const menu = (
    <Menu>
      <Menu.Item
        // onClick={downloadMutation.mutate}
      >
        <a target="_blank" href={downloadFile} download>
          <IconSmall size={'small'} component={Download} />Download
        </a>
      </Menu.Item>
      <Menu.Item danger onClick={() => handleDeleteFile(file)}>
        {' '}
        <IconSmall size={'small'} component={Delete} />
        Delete{' '}
      </Menu.Item>
    </Menu>
  );

  function getRandomColor() {
    var letters = '0123456789ABCDEF';
    var color = '#';
    for (var i = 0; i < 6; i++) {
      color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
  }

  const memoizedValue = useMemo(() => getRandomColor(), [icon]);

  return (
    <Button.Group>
      {' '}
      <SmallButton ghost size={'large'} color={memoizedValue} icon={icon} />
      <MyButton size={'large'}>
        {' '}
        <Text type="secondary">{file.originalName} </Text>
        <Dropdown arrow overlay={menu} placement="bottomRight">
          <DownOutlined />
        </Dropdown>
      </MyButton>
    </Button.Group>
  );
};

export default FilePlaceholder;
