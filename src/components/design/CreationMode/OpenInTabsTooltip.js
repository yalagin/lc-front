import React from "react";
import round from "./../../../assets/round.png"


const OpenInTabsTooltip = () => {
  return (
    <div>
      in order to open more than one tab you should allow popup windows for our site please click on that icon in url bar and allow it
      <img  src={round} alt="in the url bar" />
    </div>
  );
};

export default OpenInTabsTooltip;
