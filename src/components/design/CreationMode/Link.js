import React, { useState } from "react";
import Rectangle from '../../../assets/Icons/ReactSvg/Rectangle';
import Icon from '@ant-design/icons';
import { Divider, Typography } from 'antd';
import { useSortable } from '@dnd-kit/sortable';
import { CSS } from '@dnd-kit/utilities';
import Delete from "../../../assets/Icons/ReactSvg/Delete";
import IconToTheRightAndShowOnHover from "../../styles/IconToTheRightAndShowOnHover";

const { Text, Link: HyperLink } = Typography;

const Link = (props) => {
  const [hovered, setHovered] = useState(false);
  const {
    attributes,
    listeners,
    setNodeRef,
    transform,
    transition,
  } = useSortable({ id: props.id });
  const style = {
    transform: CSS.Transform.toString(transform),
    transition,
  };


  return (
    <div ref={setNodeRef}  style={style} {...attributes} {...listeners} onMouseEnter={()=>setHovered(true)} onMouseLeave={()=>setHovered(false)}>
      <Icon component={Rectangle} />{' '}
      <HyperLink href={props.link} target="_blank"  ellipsis={{ rows: 1, expandable: false, symbol: 'more' }}>
        {props.link.length > 55 ? props.link.substring(0, 55)+"...":props.link}
      </HyperLink>
      <IconToTheRightAndShowOnHover component={Delete} hovered={hovered} onClick={()=>props.handleClose(props.link)} />
      <Divider style={{ height: "100%", margin: "10px 0" }} />
    </div>
  );
};

export default Link;
