import React from 'react';
import {
  closestCenter,
  DndContext,
  KeyboardSensor,
  PointerSensor,
  useSensor,
  useSensors,
} from '@dnd-kit/core';
import {
  arrayMove,
  SortableContext,
  sortableKeyboardCoordinates,
  verticalListSortingStrategy,
} from '@dnd-kit/sortable';
import File from "./File";

const FileList = ({onChange, value}) => {
  const sensors = useSensors(
    useSensor(PointerSensor),
    useSensor(KeyboardSensor, {
      coordinateGetter: sortableKeyboardCoordinates,
    })
  );

  const handleDeleteFile = removedFile => {
    onChange(value.filter(file => file !== removedFile));
  };

  return (
    <DndContext
      sensors={sensors}
      collisionDetection={closestCenter}
      onDragEnd={handleDragEnd}>
      <SortableContext items={value.map(item => item.originalName)} strategy={verticalListSortingStrategy}>
        {value && value.map((file, index) => (
          <File key={file.id} file={file} handleDeleteFile={handleDeleteFile} id={file.originalName}  />
        ))}
      </SortableContext>
    </DndContext>
  );

  function handleDragEnd(event) {
    const {active, over} = event;
    if (active.id !== over.id) {
      const oldIndex = value.findIndex(item => item.originalName === active.id);
      const newIndex = value.findIndex(item => item.originalName === over.id);
      onChange(arrayMove(value, oldIndex, newIndex));
    }
  }
};

export default FileList;
