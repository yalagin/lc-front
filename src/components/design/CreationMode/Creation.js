import React, { useEffect, useState } from "react";
import { Field } from 'react-final-form';
import { OnChange } from 'react-final-form-listeners';
import { fieldDisabledText } from '../../input/fieldDisabledText';
import { Button, Col, Divider, Dropdown, Input, Menu, message, notification, Space, Typography } from "antd";
import SubmitAndTransferButtons from '../../input/button/SubmitAndTransferButtons';
import EditableLinks from './EditableLinks';
import { CloseOutlined, CloudUploadOutlined, DownOutlined, PlusOutlined } from '@ant-design/icons';
import DashedButton from '../../input/button/DashedButton';
import IconToTheRightAndShowOnHover from '../../styles/IconToTheRightAndShowOnHover';
import DropArea, {
  DropAreaPlaceHolder,
  DropAreaPlaceHolderOfUploadIcon,
  DropAreaPlaceHolderOfUploadText, WorkingFiles
} from "../../styles/DropArea";
import { useMutation } from 'react-query';
import FileList from './FileList';
import { uploadToAwsPromiseAllMutation } from "../../../utils/mutations/uploadToAwsPromiseAllMutation";
import _ from "lodash";
import useProject from "../../../utils/hooks/useProject";

const Creation = ({
                    handleSubmit,
                    form,
                    onChangeField,
                    submitting,
                    pristine
                  }) => {
  const {Title,Text} = Typography;
  const { TextArea } = Input;
  const [isDropAreaOpen, setIsDropAreaOpen] = useState(false);

  const [uploadList, setUploadList] = useState([]);
  const {project} = useProject();
  const mutationSave = useMutation(uploadToAwsPromiseAllMutation(project,"workingFile"), {
    onSuccess: (data, variables, context) => {
      setUploadList([]);
      console.log(data);
      message.info(`uploaded !`);
      form.mutators.onChangeWorkingFiles(data);
      setIsDropAreaOpen(false);
    },
    onError: (error, variables, context) => {
      notification.error({ message: error.message });
    },
  });
  useEffect(() => {
    if (!_.isEmpty(uploadList)) {
      mutationSave.mutate(uploadList);
      setUploadList([]);
      console.log('test')
    }
  }, [uploadList]);
  const onChange = (props) =>
    setUploadList(prevState => {
      return _.isEqual(props.fileList, prevState) ? prevState : props.fileList;
    });


  const props = {
    multiple: true,
    showUploadList: false,
    customRequest: ()=>{},
    onChange
  };

  return (
    <>
      {!isDropAreaOpen && (
        <Col xs={24} sm={24} md={24} lg={24} xl={12}>
          <Title level={5} style={{marginTop: '2.1rem'}}>
            Text
          </Title>
          <div className={"text-filed-creation-mode"}>
            <Field name={'textdisabled'} subscription={{value: true}}>
              {({input, meta}) => (
                <>
                  <Field
                    name={`text`}
                    disabled={input.value}
                    subscription={{value: true}}
                    allowNull={true}
                  >
                    {props => (
                      <>
                        <OnChange name={props.input.name}>
                          {value => {
                            if (!input.value) {
                              onChangeField(props.input.name, value);
                            }
                          }}
                        </OnChange>
                        <TextArea
                          autoSize
                          // maxLength={2000}
                          showCount
                          rows={1}
                          size="large"
                          disabled={props.disabled}
                          value={props.input.value}
                          onChange={props.input.onChange}
                        />
                      </>
                    )}
                  </Field>
                  {input.value && fieldDisabledText}
                </>
              )}
            </Field>
          </div>
          <br />
          <br />
          <Title level={5}>Description</Title>
          <div className={"description-filed-creation-mode"}>
            <Field name={'descriptiondisabled'} subscription={{value: true}}>
              {({input, meta}) => (
                <>
                  <Field
                    name={`description`}
                    disabled={input.value}
                    subscription={{value: true}}
                    allowNull={true}
                  >
                    {props => (
                      <>
                        <OnChange name={props.input.name}>
                          {value => {
                            if (!input.value) {
                              onChangeField(props.input.name, value);
                            }
                          }}
                        </OnChange>
                        <TextArea
                          // autoSize
                          maxLength={2000}
                          showCount
                          rows={6}
                          size="large"
                          disabled={props.disabled}
                          value={props.input.value}
                          onChange={props.input.onChange}
                        />
                      </>
                    )}
                  </Field>
                  {input.value && fieldDisabledText}
                </>
              )}
            </Field>
          </div>
          <br />
          <br />
          <Title level={5}>Inspiration</Title>
          <Field name={'inspirationLinksdisabled'} subscription={{value: true}}>
            {({input, meta}) => (
              <div className={"inspiration-filed-creation-mode"}>
                <Field
                  name={`inspirationLinks`}
                  // component={Tags}
                  disabled={input.value}
                  subscription={{value: true}}
                  allowNull={true}
                  initialValue={[]}
                >
                  {props => (
                    <>
                      <OnChange name={props.input.name}>
                        {value => {
                          if (!input.value) {
                            onChangeField(props.input.name, value);
                          }
                        }}
                      </OnChange>
                      <EditableLinks disabled={input.value} input={props.input} />
                    </>
                  )}
                </Field>
                {input.value && fieldDisabledText}
              </div>
            )}
          </Field>
          <br />
          <br />
          <Title level={5}>Working Files</Title>
          <Field name={'workingFilesdisabled'} subscription={{value: true}}>
            {({input, meta}) => (
              <WorkingFiles className={"workingFiles-filed-creation-mode"}>
                <Field
                  name={`workingFiles`}
                  // component={Tags}
                  disabled={input.value}
                  subscription={{value: true}}
                  allowNull={true}
                >
                  {props => (
                    <>
                      <OnChange name={props.input.name}>
                        {value => {
                          if (!input.value) {
                            onChangeField(props.input.name, value);
                          }
                        }}
                      </OnChange>
                      <FileList
                        value={props.input.value ? props.input.value : []}
                        onChange={props.input.onChange}
                      />
                    </>
                  )}
                </Field>
                <DashedButton
                  disabled={input.value}
                  size={'large'}
                  onClick={() => setIsDropAreaOpen(true)}
                >
                  <PlusOutlined style={{color: '#748AA1'}} />
                </DashedButton>
                {input.value && (
                  <div style={{display: 'flex', alignItems: 'flex-end'}}>{fieldDisabledText}</div>
                )}
              </WorkingFiles>
            )}
          </Field>
          <br />
          <Divider />
          <SubmitAndTransferButtons
            form={form}
            handleSubmit={handleSubmit}
            pristine={pristine}
            submitting={submitting}
          />
        </Col>
      )}

      {isDropAreaOpen &&
        <Col xs={24} sm={24} md={24} lg={24} xl={12}>
          <IconToTheRightAndShowOnHover
            component={CloseOutlined}
            hovered={true}
            onClick={() => setIsDropAreaOpen(false)}
          />
          <DropArea {...props}>
            <DropAreaPlaceHolder>
              <DropAreaPlaceHolderOfUploadIcon>
                <CloudUploadOutlined />
              </DropAreaPlaceHolderOfUploadIcon>
              <DropAreaPlaceHolderOfUploadText>
                <Text strong> Drop your files here </Text>
                <Text type="secondary"> Supported formats: .psd, .ai, .pdf, etc... </Text>
              </DropAreaPlaceHolderOfUploadText>
            </DropAreaPlaceHolder>
          </DropArea>
        </Col>
      }
    </>
  );
};

export default Creation;
