import React, { useState } from 'react';
import { Col, Divider, Input, Row, Tooltip } from 'antd';
import Icon, { PlusOutlined,InfoCircleOutlined } from '@ant-design/icons';
import RoundButton from '../../input/button/RoundButton';
import OpenInTabs from '../../../assets/Icons/ReactSvg/OpenInTabs';
import Links from './Links';
import OpenInTabsTooltip from './OpenInTabsTooltip';
import _ from "lodash";
import { validURL } from "../../../utils/utilsFunctions";
import { SearchStyled } from "../../styles/CreationFiles";

const { Search } = Input;

const EditableLinks = ({ input: { onChange, value }, label, disabled, ...rest }) => {
  const [inputValue, setInputValue] = useState('');
  const [inputError, setInputError] = useState(false);
  const handleInputChange = e => {
    setInputValue(e.target.value);
    // setInputError(false);
  };

  const handleInputConfirm = e => {
    e.preventDefault();
    if(_.isEmpty(inputValue)){
      return setInputError({ text: "value is empty", color: '#FFC338' });
    }

    if (!_.isEmpty(value)&& value.indexOf(inputValue) !== -1) {
      setInputValue('');
      return setInputError({ text: "value is the same", color: '#FFC338' });
    }

    if (!validURL(inputValue)) {
      setInputValue('');
      return setInputError({ text: "value is not valid url", color: '#fd0000' });
    }
    if(!value){
      value =[];
    }
    onChange([...value, inputValue]);
    setInputError(false);
    setInputValue('');
  };


  const openLinks = () => {
    if (Array.isArray(value)) {
      value.forEach(function(url) {
        let link = document.createElement('a');
        link.href = url;
        link.target = '_blank';
        link.click();
      });
    }
  }

  return (
    <>
      <Row>
        <Col span={17}>
          <SearchStyled
            error={inputError}
            type="text"
            suffix={inputError &&
              <Tooltip title={inputError.text}>
                <InfoCircleOutlined style={{ color: inputError.color }} />
              </Tooltip>
            }
            value={inputValue}
            onChange={handleInputChange}
            onBlur={handleInputConfirm}
            onPressEnter={handleInputConfirm}
            onSearch={() => handleInputConfirm(document.createEvent('Event'))}
            disabled={disabled}
            placeholder={'Paste your reference link here'}
            size="large"
            enterButton={<PlusOutlined />}
          />
        </Col>
        <Col span={2}><Divider type="vertical" style={{ height: "100%", marginLeft: "2rem" }} /></Col>
        <Col span={2}>
          <Tooltip placement="bottomRight" title={OpenInTabsTooltip}>
            <RoundButton onClick={openLinks}>
              <Icon component={OpenInTabs} /> {' '} Open all in tabs
            </RoundButton>
          </Tooltip>
        </Col>
      </Row>
      <br />
      <Links onChange={onChange} value={Array.isArray(value) ? value : []} />
    </>
  );
};

export default EditableLinks;
