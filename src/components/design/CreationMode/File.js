import React from 'react';
import {useSortable} from '@dnd-kit/sortable';
import {CSS} from '@dnd-kit/utilities';
import FilePlaceholder from './FilePlaceholder';
import {ENTRYPOINT} from '../../../config/entrypoint';
import _ from "lodash";

const File = props => {
  const {attributes, listeners, setNodeRef, transform, transition} = useSortable(
    {id: props.id});
  const style = {
    transform: CSS.Transform.toString(transform),
    transition,
  };

  return (
    <div ref={setNodeRef} style={style} {...attributes} {...listeners}>
      <FilePlaceholder
        downloadFile={ENTRYPOINT + props.file.contentUrl}
        handleDeleteFile={props.handleDeleteFile}
        file={props.file}
        icon={_.capitalize(_.split(props.file.originalName, '.').pop())}
      />
    </div>
  );
};

export default File;
