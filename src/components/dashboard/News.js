import React from 'react';
import { Avatar, Card, Col, Row } from "antd";
import { DashOutlined, HeartFilled, UserOutlined, WechatFilled } from '@ant-design/icons';
import styled from 'styled-components';

const { Meta } = Card;

const StyledCard = styled(Card)`
  width: 240px;
  border-radius: 40px;
  filter: drop-shadow(13.766px 19.66px 69px rgba(28, 33, 54, 0.071));
  &&& > .ant-card-actions {
    border-radius: 40px;
  }
  &&& > .ant-card-cover img {
    border-top-left-radius: 40px;
    border-top-right-radius: 40px;
  }
`;

const News = () => {
  return (
  <div className="site-card-wrapper">
    <Row gutter={16} style={{maxWidth: "50%"}}>
      <Col>
        <StyledCard
        hoverable
        cover={<img alt="example" src="https://gw.alipayobjects.com/zos/rmsportal/JiqGstEfoWAOHiTxclqi.png" />}
      >
        <Meta title='The word "coffee" ' />
      </StyledCard>
      </Col>
      <Col>
        <StyledCard
        // title={<><Avatar size="small" icon={<UserOutlined />} /> Yvonne Schultz <DashOutlined
        //   style={{ float: 'right', margin: "0.3rem" }} /></>}
        hoverable
        cover={<img alt="example" src="https://gw.alipayobjects.com/zos/rmsportal/JiqGstEfoWAOHiTxclqi.png" />}
      >
        <Meta title='The word "coffee" ' />
      </StyledCard>
      </Col>
      <Col>
        <StyledCard
        // title={<><Avatar size="small" icon={<UserOutlined />} /> Yvonne Schultz <DashOutlined
        //   style={{ float: 'right', margin: "0.3rem" }} /></>}
        hoverable
        cover={<img alt="example" src="https://gw.alipayobjects.com/zos/rmsportal/JiqGstEfoWAOHiTxclqi.png" />}
      >
        <Meta title='The word "coffee" ' />
      </StyledCard>
      </Col>
    </Row>
  </div>

  );
}

export default News;
