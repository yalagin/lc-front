import React from "react";
import { Liquid } from "@ant-design/charts";
import { useQuery } from "react-query";
import { fetch2 } from "../../../utils/dataAccess";
import { Spin } from "antd";
import { StyledBorder } from "../StatsInfo";

const StorageCapacity = () => {

  const getStorageLimit = useQuery('get_storage_limit', () =>
    fetch2('/get_storage_limit')
  );
  return (
    <Spin  spinning={getStorageLimit.isLoading}>
      <StyledBorder className={'forth-step-dashboard'}>
        <div>Storage is {getStorageLimit.data?.storageCapacity} Gb
        </div>
        <div> out of {getStorageLimit.data?.storageLimit} Gb</div>
        <br />
         <Liquid wave={{ length: 128 }}
                  percent={(getStorageLimit.data?.storageCapacity / getStorageLimit.data?.storageLimit)}
                  outline={{
                    border: 4,
                    distance: 8,
                  }}
                  height={200}
                  width={200}
          />
      </StyledBorder>
    </Spin>
  );
};

export default StorageCapacity;
