import React, { useState } from 'react';
import { useQuery } from 'react-query';
import { fetch2 } from '../../../utils/dataAccess';
import { Select, Spin, Typography } from 'antd';
import { MinWidthSelect, StyledBorder } from '../StatsInfo';
import { designsWithFixedMba } from '../../../utils/constants/constants';
import _ from 'lodash';
import useProject from '../../../utils/hooks/useProject';
import qs from 'qs';
import { convertToUrl } from '../../../utils/utilsFunctions';

const UploadQueue = () => {
  const { project } = useProject();
  const [queryParams, setQueryParams] = useState({
    pagination: true,
    pageSize: 0,
    isUpload: true,
    isUploaded: false,
    'design.isUpload': true,
    'design.project': project ? project['@id'] : null,
  });

  const [selectedDesign, setSelectedDesign] = useState('design_spreadshirt_coms');

  const designQuery = useQuery(selectedDesign, () =>
    fetch2(
      `/${selectedDesign}?${qs.stringify(queryParams, {
        
        skipNulls: true,
      })}`
    )
  );

  return (
    <div className={'six-step-dashboard'} >
    <Spin spinning={designQuery.isLoading}>
      <StyledBorder>
        <Typography.Title level={3}>Upload Queue </Typography.Title>
        <MinWidthSelect
          value={selectedDesign}
          defaultValue={'design_spreadshirt_coms'}
          onChange={setSelectedDesign}
        >
          {/*<Select.Option value={null}>All</Select.Option>*/}
          {designsWithFixedMba.map((item, index) => (
            <Select.Option value={convertToUrl(item)} key={item}>
              {_.startCase(item.slice(6))}
            </Select.Option>
          ))}
        </MinWidthSelect>
        <br />
        <Typography.Title style={{ color: '#7f67ff' }} level={3}>
          Total: {designQuery.data && designQuery.data['hydra:totalItems']}
        </Typography.Title>
      </StyledBorder>
    </Spin>
    </div>
  );
};

export default UploadQueue;
