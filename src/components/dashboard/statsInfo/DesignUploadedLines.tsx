import React, {useEffect, useMemo, useState} from 'react';
import {notification, Select, Spin, Typography} from 'antd';
import {DivCol} from '../../styles/TemplateStyles';
import {designs} from '../../../utils/constants/constants';
import {Line} from '@ant-design/charts';
import {DivSpaceBetween, LineHolder, MinWidthSelect} from '../StatsInfo';
import {useQuery} from 'react-query';
import {fetch2} from '../../../utils/dataAccess';
import useProject from '../../../utils/hooks/useProject';
import qs from 'qs';
import _ from 'lodash';
import {
  formatISO,
  startOfMonth,
  startOfISOWeek,
  startOfYear,
  subDays,
  subMinutes,
  subMonths,
  subYears,
} from 'date-fns';
// import ReactJson from 'react-json-view';

export type TimePeriod = 'Day' | 'Week' | 'Month' | 'Year';

const DesignUploadedLines = () => {
  const timePeriod = useMemo<TimePeriod[]>(() => [/*'Day',*/ 'Week', 'Month', 'Year'], []);
  const [selectedDesign, setSelectedDesign] = useState(null);
  const {project} = useProject();
  const [selectedPeriod, setSelectedPeriod] = useState<TimePeriod>('Week');
  const [currentItem, setCurrentItem] = useState('Today');
  const [currentItemCounter, setCurrentItemCounter] = useState(0);
  const [lastItem, setLastItem] = useState('Yesterday');
  const [lastItemCounter, setLastItemCounter] = useState(0);
  const [data, setData] = useState([]);
  const config = {
    xField: 'xField',
    yField: 'doc_count',
    smooth: true,
    seriesField: 'chartKey',
    color: ['#7F67FF', 'lightgrey'],
  };
  const today = new Date();
  const todayMidnight = new Date(new Date().setHours(0, 0, 0, 0));
  const yesterdayMidnight = subDays(todayMidnight, 1);
  const aWeekAgo = subDays(today, 7);

  const getQueryForTimePeriod = (timePeriod: TimePeriod) => {
    let queryData = {};
    switch (timePeriod) {
      case 'Day':
        queryData = {
          interval: 'hour',
          currentItem: 'Today',
          lastItem: 'Yesterday',
          createdAtCurrent: {
            after: todayMidnight.toISOString(),
          },
          createdAtPrevious: {
            after: yesterdayMidnight.toISOString(),
            before: subMinutes(todayMidnight, 1).toISOString(),
          },
        };
        break;
      case 'Week':
        queryData = {
          interval: 'day',
          currentItem: 'This week',
          lastItem: 'Last week',
          createdAtCurrent: {
            after: formatISO(startOfISOWeek(new Date()), {representation: 'date'}),
          },
          createdAtPrevious: {
            after: formatISO(startOfISOWeek(aWeekAgo), {representation: 'date'}),
            before: formatISO(subDays(startOfISOWeek(new Date()),1), {representation: 'date'}),
          },
        };
        break;
      case 'Month':
        queryData = {
          interval: 'day',
          currentItem: 'This month',
          lastItem: 'Last month',
          createdAtCurrent: {
            after: formatISO(startOfMonth(today), {representation: 'date'}),
          },
          createdAtPrevious: {
            after: formatISO(startOfMonth(subMonths(today, 1)), {representation: 'date'}),
            before: formatISO(subDays(startOfMonth(today), 1), {representation: 'date'}),
          },
        };
        break;
      case 'Year':
        queryData = {
          interval: 'month',
          currentItem: 'This year',
          lastItem: 'Last year',
          createdAtCurrent: {
            after: today.getFullYear(),
          },
          createdAtPrevious: {
            after: subYears(today, 1).getFullYear(),
            before: formatISO(subMinutes(startOfYear(today), 1), {representation: 'date'}),
          },
        };
        break;
    }
    return queryData;
  };

  const [query, setQuery] = useState({
    interval: 'day',
    currentItem: 'This week',
    lastItem: 'Last week',
    createdAtCurrent: {
      after: formatISO(startOfISOWeek(new Date()), {representation: 'date'}),
    },
    createdAtPrevious: {
      after: formatISO(startOfISOWeek(aWeekAgo), {representation: 'date'}),
      before: formatISO(subDays(startOfISOWeek(new Date()),1), {representation: 'date'}),
    },
    design: selectedDesign,
    project: {match_phrase_prefix: {project: project?.id}},
  });

  useEffect(() => {
    // @ts-ignore
    setQuery({
      ...getQueryForTimePeriod(selectedPeriod),
      design: selectedDesign,
      project: {match_phrase_prefix: {project: project?.id}},
    });
  }, [selectedPeriod]);

  const getAggregate = useQuery(
    ['/es/dashboard-graph', query],
    () => fetch2(`/es/dashboard-graph?${qs.stringify(query, {skipNulls: true})}`),
    {
      enabled: !_.isEmpty(project?.id),
      onError: (error: Error) => {
        notification.warning({
          message: `something goes wrong with design, please try again later`,
          description: error.message,
        });
      },
      onSuccess: response => {
        setCurrentItemCounter(response.currentItem);
        setLastItemCounter(response.lastItem);
        if (selectedPeriod === 'Day') {
          response.value.forEach(
            (aggregate: {xField: string; key_as_string: string | number | Date}) =>
              (aggregate.xField = new Date(aggregate.key_as_string)
                .toLocaleTimeString()
                .replace(/:\d+ /, ' ')
                .slice(0, -3))
          );
          setCurrentItem('Today');
          setLastItem('Yesterday');
        }
        if (selectedPeriod === 'Week') {
          response.value.forEach(
            (aggregate: {xField: string; key_as_string: string | number | Date}) =>
              (aggregate.xField = new Date(aggregate.key_as_string).toLocaleDateString('en', {
                weekday: 'long',
              }))
          );
          setCurrentItem('This Week');
          setLastItem('Last Week');
        }
        if (selectedPeriod === 'Month') {
          response.value.forEach(
            (aggregate: {xField: string; key_as_string: string | number | Date}) =>
              (aggregate.xField = new Date(aggregate.key_as_string).getUTCDate().toString())
          );
          setCurrentItem('This Month');
          setLastItem('Last Month');
        }
        if (selectedPeriod === 'Year') {
          response.value.forEach(
            (aggregate: {xField: string; key_as_string: string | number | Date}) =>
              (aggregate.xField = new Date(aggregate.key_as_string).toLocaleDateString('en', {
                month: 'long',
              }))
          );
          setCurrentItem('This Year');
          setLastItem('Last Year');
        }

        setData(response.value);
      },
    }
  );

  useEffect(() => {
    setQuery({...query, design: selectedDesign});
  }, [selectedDesign]);

  return (
    <Spin spinning={getAggregate.isLoading}>
      <LineHolder className={'third-step-dashboard'}>
        <Typography.Title level={3}>Designs Uploaded </Typography.Title>
        <DivSpaceBetween>
          <DivCol>
            <div>{currentItem}</div>
            <Typography.Title style={{color: '#7f67ff'}} level={3}>
              {currentItemCounter}
            </Typography.Title>
          </DivCol>
          <DivCol>
            <div>{lastItem}</div>
            <Typography.Title type={'secondary'} level={3}>
              {' '}
              {lastItemCounter}
            </Typography.Title>
          </DivCol>
          {/*@ts-ignore*/}
          <MinWidthSelect value={selectedDesign} defaultValue="" onChange={setSelectedDesign}>
            <Select.Option value={null}>All</Select.Option>
            {designs.map(item => (
              <Select.Option value={item} key={item}>
                {_.startCase(item.slice(6))}
              </Select.Option>
            ))}
          </MinWidthSelect>
          <Select
            className="select-container time-item"
            value={selectedPeriod}
            onChange={setSelectedPeriod}
          >
            {timePeriod.map((item, index) => (
              <Select.Option value={item} key={index}>
                {item}
              </Select.Option>
            ))}
          </Select>
        </DivSpaceBetween>
        <Line data={data} {...config} />
        {/*<ReactJson src={data} />*/}
      </LineHolder>
    </Spin>
  );
};

export default DesignUploadedLines;
