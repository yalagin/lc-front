import React from "react";
import { useMutation, useQuery } from "react-query";
import { fetch2 } from "../../../utils/dataAccess";
import { notification, Spin } from "antd";
import { StyledBorder } from "../StatsInfo";
import { Gauge, Liquid } from "@ant-design/charts";
import useProfile from "../../../utils/hooks/useProfile";
import qs from "qs";
import _ from "lodash";

const TranslationCharCounter = () => {
  const { profile } = useProfile();

  const translationCall = async () => {
    const response = await fetch(
      (profile?.apiKey.slice(-2) === 'fx'
        ? 'https://api-free.deepl.com/v2/usage?'
        : 'https://api.deepl.com/v2/usage?') +
      qs.stringify(
        {
          auth_key: profile?.apiKey,
        },
        { arrayFormat: 'repeat', skipNulls: true }
      )
    );
    return await response.json();
  }


  const getTranslationLimit = useQuery('get_translation_limit', translationCall, {
    onError: (error, variables, context) => {
      notification.warning({ message: `something goes wrong with translation, please check api key`, description: error.message });
    },
    enabled : !_.isEmpty(profile?.apiKey)
  });


  if (_.isEmpty(profile?.apiKey)) return <></>;

  var config = {
    percent: getTranslationLimit.data?.character_count / getTranslationLimit.data?.character_limit,
    range: {
      ticks: [0, 1 / 3, 2 / 3, 1],
      color: ['#30BF78', '#FAAD14', '#F4664A'],
    },
    axis: {
      label: {
        formatter: function formatter(v) {
          return Number(v) * 100;
        },
      },
      subTickLine: { count: 3 },
    },
    indicator: {
      pointer: { style: { stroke: '#D0D0D0' } },
      pin: { style: { stroke: '#D0D0D0' } },
    },
    statistic: {
      content: {
        style: {
          fontSize: '16px',
          lineHeight: '16px',
        },
      },
    },
  };


  return (
    <Spin  spinning={getTranslationLimit.isLoading}>
      <StyledBorder  className={'five-step-dashboard'}>
        <div>translated {getTranslationLimit.data?.character_count}</div>
        <div> out of {getTranslationLimit.data?.character_limit} characters</div>
        <br />
        <Gauge
          {...config}
          height={200}
          width={200}
        />
      </StyledBorder>
    </Spin>
  );
};

export default TranslationCharCounter;
