import React, { useState } from "react";
import { List, Typography } from "antd";
import { useQuery } from "react-query";
import { fetch2 } from "../../utils/dataAccess";
import qs from "qs";
import { HYDRAMEMBER } from "../../utils/constants/constants";
import _ from "lodash";

const ChangeLog = () => {
  const [queryParams, setQueryParams] = useState({});
  const designQuery = useQuery("ChangeLog", () =>
    fetch2(
      `/changelogs?${qs.stringify(queryParams, {
        
        skipNulls: true,
      })}`
    )
  );
  return (
    <div className={'second-step-dashboard'} style={{float:'right', width:'50%', height: "400px"}}><Typography.Title level={5} >Patch notes</Typography.Title>
    <List
      loading={designQuery.isLoading}
      scroll={{ y: 240 }}
      style={{height: "380px", overflow:'auto'}}
      header={''}
      dataSource={_.isEmpty(designQuery.data)?[]:designQuery.data[HYDRAMEMBER]}
      renderItem={item => (
        <List.Item>
          <List.Item.Meta
            title={item.name}
            description={item.text}
          />
        </List.Item>
      )}
    /></div>
  );
};

export default ChangeLog;
