import React from 'react';
import styled from 'styled-components';
import { DivRow } from '../styles/TemplateStyles';
import { Select, Space } from 'antd';
import StorageCapacity from './statsInfo/StorageCapacity';
import DesignUploadedLines from './statsInfo/DesignUploadedLines';
import UploadQueue from "./statsInfo/UploadQueue";
import TranslationCharCounter from "./statsInfo/TranslationCharCounter";

export const StyledBorder = styled.div`
  filter: drop-shadow(13.766px 19.66px 69px rgba(28, 33, 54, 0.071));
  border-radius: 40px;
  margin: 1rem;
  padding: 2rem;
  background: #ffffff;
  margin-top: 3rem;
  display: flex;
  width: fit-content;
  height: fit-content;
  flex-direction: column;
  justify-content: center;
  align-items: center;
`;

export const LineHolder = styled(StyledBorder)`
  align-items: normal;
  justify-content: normal;
  width: auto;
  flex: 0 0 100%;
  & > div:first-child {
    width: 100%;
  }
`;

export const MinWidthSelect = styled(Select)`
  &&& > .ant-select-selector {
    width: 100%;
    min-width: 100px;
  }
`;
export const DivSpaceBetween = styled(Space)`
  justify-content: space-between;
`;

export const DivFlexWrap = styled(Space)`
  flex-wrap:  wrap;
  display: flex;
`;

const StatsInfo = () =>
    <DivRow >
      <StorageCapacity  />
      <TranslationCharCounter />
      <UploadQueue />
    </DivRow>

export default StatsInfo;
