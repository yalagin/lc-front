import React from "react";
import { Form } from "react-final-form";
import TrackingFields from "./TrackingFields";
import { FORM_ERROR } from "final-form";
import _ from "lodash";
import { classification, country, region, trademarkFilter } from "../../../utils/constants/constants";
import useProject from "../../../utils/hooks/useProject";

const TrackingForm = ({mutate,setQueryParams}) => {
  const {project} =useProject();

  const onSubmit = async (values,form) => {
    console.log(values);
    if(!_.isEmpty(values.country)) {
      values.country = _.without(values.country, country);
    } else {
      values.country = trademarkFilter[region];
    }
    if(!_.isEmpty(values.classes)) {
      values.classes =_.without(values.classes, "classes")
    } else {
      values.classes = trademarkFilter[classification];
    }
    values.project = project["@id"];

    try {
      await mutate(values);
    } catch (e){
      return { [FORM_ERROR]: 'Failed' }
    }
    form.change('keyword',null);
  };

  return (
    <Form
      debug={process.env.REACT_APP_SPY && console.log}
      onSubmit={onSubmit}
      // validate={validate}
      component={TrackingFields}
      setQueryParams={setQueryParams}
      validate={(values) => {
        const errors = {};
        if (!values.keyword) {
          errors.keyword = "Required";
        }
        return errors;
      }}
    />
  );
};

export default TrackingForm;
