import React, {useEffect} from 'react';
import {Tree} from 'antd';
import {useField} from 'react-final-form';
import { classification, country, region, trademarkFilter } from "../../../utils/constants/constants";
import _ from "lodash";

const RegionClassificationTree = ({setQueryParams}) => {
  const countryField = useField(country, {subscription: {value: true}});
  const classificationField = useField("classes", {subscription: {value: true}});
  const countryData = [
    {
      title: "Region",
      key: country,
      children: trademarkFilter[region].map(child => ({
        title: child,
        key: child,
      })),
    },
  ];
  const classificationData = [
    {
      title: "Classification",
      key: "classes",
      children: trademarkFilter[classification].map(child => ({
        title: child,
        key: child,
      })),
    },
  ];
  useEffect(() => {
    const value =countryField?.input?.value;
    if(!_.isEmpty(value)) {
      setQueryParams(prevState => ({...prevState,country: _.without(value, country) }))
    } else {
      setQueryParams(prevState => ({ ...prevState, country:trademarkFilter[region] }))
    }
  }, [countryField?.input?.value]);

  useEffect(() => {
    const value =classificationField?.input?.value;
    if(!_.isEmpty(value)) {
      setQueryParams(prevState => ({...prevState,classes:  _.without(value, "classes")}))
    } else {
      setQueryParams(prevState => ({...prevState,classes:trademarkFilter[classification]}))
    }

  }, [classificationField?.input?.value]);

  return (
    <>
      <Tree checkable onCheck={countryField.input.onChange} treeData={countryData} />
      <Tree checkable onCheck={classificationField.input.onChange} treeData={classificationData} />
    </>
  );
};

export default RegionClassificationTree;
