import React from "react";
import { useMutation, useQuery, useQueryClient } from "react-query";
import { fetch2 } from "../../../utils/dataAccess";
import { message, notification, Space, Spin } from "antd";
import { WhiteHeaderTable } from "../../styles/TableStyles";
import ExtendableTable from "../Check/ExtendableTable";
import TrackingForm from "./TrackingForm";
import { PagedCollection } from "../../../interfaces/Collection";
import { TrackTrademark } from "../../../interfaces/tracktrademark";
import useCheckForTrademark from "../../../utils/mutations/useCheckForTrademark";
import useTrackTrademarkMutation from "../../../utils/mutations/useTrackTrademarkMutation";
import usePatchMutation from "../../../utils/mutations/usePatchMutation";
import useProject from "../../../utils/hooks/useProject";

function Track() {
  const {project } = useProject();
  const fetchUserTrackedTrademarks = useQuery<PagedCollection<TrackTrademark>,Error>('trademark_tracks', () =>
    fetch2('/trademark_tracks?pagination=false&project=' + project["@id"])
  );
  const checkForTrademark = useCheckForTrademark();
  const trackTrademarkMutation = useTrackTrademarkMutation();
  const patchMutation = usePatchMutation();

  const deleteTrademarkToTracking = useMutation(
    async (data:TrackTrademark) =>
      await fetch2(data["@id"], {
        method: 'DELETE',
      }),
    {
      onSuccess: (data, variables, context) => {
        message.success(`Tracking Deleted!`);
        fetchUserTrackedTrademarks.refetch();
      },
    }
  );

  const columns = [
    {
      title: 'Keyword',
      dataIndex: ['keyword','keyword'],
      sorter: (a: any, b: any) => {
        var nameA = a.keyword.keyword.toUpperCase(); // ignore upper and lowercase
        var nameB = b.keyword.keyword.toUpperCase(); // ignore upper and lowercase
        if (nameA < nameB) {
          return -1;
        }
        if (nameA > nameB) {
          return 1;
        }

        // names must be equal
        return 0;
      },
    },
    {
      title: 'Territory',
      dataIndex: ['keyword','country'],
      render: (data: string[] | any[]) => data?.toString(),
    },
    {
      title: 'Classification',
      dataIndex: ['keyword','classes'],
      render: (data: string[] | any[]) => data?.toString(),
    },
    {
      title: 'New Entries',
      dataIndex: 'newEntries',
      // render: (data: null | any[]) => data?.length,
      // defaultSortOrder: 'descend' as SortOrder,
      sorter: true,
    },
    {
      title: 'Entries',
      // dataIndex: ["keyword","data"],
      // key: 'entries',
      render: (data: undefined | number, record: TrackTrademark) => record.keyword?.data?.length,
      sorter: (a: any, b: any) => a.keyword?.data.length - b.keyword?.data.length,
    },
    {
      title: 'Date',
      dataIndex: 'createdAt',
      sorter: (a: any, b: any) => {
        const dateA = new Date(a.date_prop).getTime();
        const dateB = new Date(b.date_prop).getTime();
        return dateA < dateB ? 1 : -1; // ? -1 : 1 for ascending/increasing order
      },
      render: (data: string) => new Date(data).toLocaleString(),
    },
    {
      title: 'Action',
      key: 'action',
      render: (text: string, record: TrackTrademark) => (
        <Space size="middle">
          <a onClick={() => patchMutation.mutate({["@id"]:record['@id'],newEntries:0},{onSuccess:()=>{
            message.success(`New Entries set to 0 for `+record?.keyword?.keyword);
              fetchUserTrackedTrademarks.refetch();
          }})}>Approve</a>
          <a onClick={() => deleteTrademarkToTracking.mutate(record)}>Delete</a>
        </Space>
      ),
    },
  ];

  const saveToTrackingForm = async (object: {
    keyword: {keyword: string; classes: number[]; country: string[]};
  }) => {
    await trackTrademarkMutation.mutateAsync(
          object,
          {
            // @ts-ignore
            onError: (error: Error) => {
              message.error(error.toString());
            },
            onSuccess: () =>{
              fetchUserTrackedTrademarks.refetch();
            }
          });
  };

  return (
    <>
      <Spin spinning={fetchUserTrackedTrademarks.isLoading||checkForTrademark.isLoading} />
      <TrackingForm setQueryParams={checkForTrademark.setQueryParams} mutate={saveToTrackingForm}/>
      <br/>
      <WhiteHeaderTable
        bordered
        dataSource={fetchUserTrackedTrademarks.data?fetchUserTrackedTrademarks.data['hydra:member']:[]}
        columns={columns}
        pagination={false}
        rowKey={({keyword}: any) => keyword}
        expandable={{
          expandedRowRender: (record: any) => <ExtendableTable all={false} record={record.keyword.data} />,
          rowExpandable: (record: any) => !!record.keyword?.data?.length,
        }}
      />
    </>
  );
}

export default Track;
