import React from "react";
import { Field, FormSpy } from "react-final-form";
import { Button, Col, Space, Typography } from "antd";
import styled from "styled-components";
import { EnterOutlined, PlusOutlined } from "@ant-design/icons";
import MyTextField from "../../input/MyFields/MyTextField";
import HandleEnterKey from "../../input/HandleKey/HandleEnterKey";
import { DivRow } from "../../styles/TemplateStyles";
import FilterModal from "./FilterModal";

const BigField = styled(Field)`
   margin-bottom: 0;
  width: 500px;
  .ant-input {
    line-height: 2.2;
  }
`;

const GrayColor = styled(Button)`
  color: #31394D;
  background: #EBEDF4;
  border-color: #EBEDF4;
  height: 3.15rem;
  margin-bottom: 1.5rem;
`;



const TrackingFields =  ({ submitError, handleSubmit,setQueryParams }) => {
  const wrapperCol = { span: 10 }
  return (
    <form onSubmit={handleSubmit}>
      <Typography.Title level={5}>Add Keywords</Typography.Title>
      <Space style={{marginTop:"1rem"}}>
      <BigField
        size={'large'}
        // wrapperCol={wrapperCol}
        // label={'Email'}
        name="keyword"
        component={MyTextField}
        placeholder={"Keywords & Phrases"}
        hasFeedback
        allowNull={true}
        parse={x => x}
      />
        <FilterModal  setQueryParams={setQueryParams}/>
        <GrayColor  size={'large'} onClick={handleSubmit} type={'primary'}>
        <PlusOutlined/>
      </GrayColor>
        </Space>
      <HandleEnterKey handleOnEnterKey={handleSubmit} />
    </form>
  );
};


export default TrackingFields;
