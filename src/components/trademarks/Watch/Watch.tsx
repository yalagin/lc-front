import React from 'react';
import {useQuery} from 'react-query';
import {PagedCollection} from '../../../interfaces/Collection';
import {TrackTrademark} from '../../../interfaces/tracktrademark';
import {fetch2} from '../../../utils/dataAccess';
import { message, Space, Spin } from "antd";
import {WhiteHeaderTable} from '../../styles/TableStyles';
import ExtendableTable from '../Check/ExtendableTable';
import useProject from '../../../utils/hooks/useProject';
import usePatchMutation from "../../../utils/mutations/usePatchMutation";

function Watch() {
  const {project} = useProject();

  const fetchUserTrackedTrademarks = useQuery<PagedCollection<TrackTrademark>, Error>(
    'trademark_watches',
    () => fetch2('/trademark_watches?pagination=false&project=' + project['@id'])
  );
  const patchMutation = usePatchMutation();

  const columns = [
    {
      title: 'Keyword',
      dataIndex: ['keyword', 'keyword'],
      sorter: (a: any, b: any) => {
        var nameA = a.keyword.keyword.toUpperCase(); // ignore upper and lowercase
        var nameB = b.keyword.keyword.toUpperCase(); // ignore upper and lowercase
        if (nameA < nameB) {
          return -1;
        }
        if (nameA > nameB) {
          return 1;
        }

        // names must be equal
        return 0;
      },
      // render: (data: string) => console.log(data),
    },
    {
      title: 'Territory',
      dataIndex: ['keyword', 'country'],
      render: (data: string[] | any[]) => data?.toString(),
    },
    {
      title: 'Classification',
      dataIndex: ['keyword', 'classes'],
      render: (data: string[] | any[]) => data?.toString(),
    },
    {
      title: 'New Entries',
      dataIndex: 'newEntries',
      sorter: true,
    },
    {
      title: 'Entries',
      // dataIndex: 'keyword.data',
      // key: 'entries',
      render: (data: undefined | number, record: TrackTrademark) => record.keyword?.data?.length,
      sorter: (a: any, b: any) => a.keyword?.data.length - b.keyword?.data.length,
    },
    {
      title: 'Date',
      dataIndex: 'createdAt',
      sorter: (a: any, b: any) => {
        const dateA = new Date(a.date_prop).getTime();
        const dateB = new Date(b.date_prop).getTime();
        return dateA < dateB ? 1 : -1; // ? -1 : 1 for ascending/increasing order
      },
      render: (data: string) => new Date(data).toLocaleString(),
    },
    {
      title: 'Action',
      key: 'action',
      render: (text: string, record: TrackTrademark) => (
        <Space size="middle">
          <a onClick={() => patchMutation.mutate({["@id"]:record['@id'],newEntries:0},{onSuccess:()=>{
              message.success(`New Entries set to 0 for `+record?.keyword?.keyword);
              fetchUserTrackedTrademarks.refetch();
            }})}>Approve</a>
        </Space>
      ),
    },
  ];

  return (
    <>
      <Spin spinning={fetchUserTrackedTrademarks.isLoading} />
      <br />
      <WhiteHeaderTable<TrackTrademark | any>
        bordered
        dataSource={
          fetchUserTrackedTrademarks.data ? fetchUserTrackedTrademarks.data['hydra:member'] : []
        }
        columns={columns}
        pagination={false}
        rowKey={({keyword}: any) => keyword}
        expandable={{
          expandedRowRender: (record: any) => (
            <ExtendableTable all={false} record={record.keyword.data} />
          ),
          rowExpandable: (record: any) => !!record.keyword?.data?.length,
        }}
      />
    </>
  );
}

export default Watch;
