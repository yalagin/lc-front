import React, {useState} from 'react';
import AsinForm from './Search/AsinForm';
import {message, Spin, Typography} from 'antd';
import _ from 'lodash';
import ExtendableTable from './ExtendableTable';
import {WhiteHeaderTable} from '../../styles/TableStyles';
import {SortOrder} from 'antd/es/table/interface';
import useCheckForTrademark from '../../../utils/mutations/useCheckForTrademark';
import useTrackTrademarkMutation from '../../../utils/mutations/useTrackTrademarkMutation';
import {splitSentenceToWords} from '../../../utils/utilsFunctionsTyped';
import useProject from "../../../utils/hooks/useProject";
import { TableProps } from "antd/lib/table/Table";
import { ColumnsType, CompareFn } from "antd/lib/table/interface";

interface DataKeyword {
  data: [];
  keyword: string;
}

const Check = () => {
  const {Title} = Typography;
  const [data, setData] = useState<DataKeyword[]>([]);
  const checkForTrademark = useCheckForTrademark();
  const trackTrademarkMutation = useTrackTrademarkMutation();
  const { project } = useProject();

  const columns: ColumnsType<DataKeyword> = [
    {
      title: 'Keyword',
      dataIndex: 'keyword',
      sorter: (a, b) => {
        var nameA = a.keyword.toUpperCase(); // ignore upper and lowercase
        var nameB = b.keyword.toUpperCase(); // ignore upper and lowercase
        if (nameA < nameB) {
          return -1;
        }
        if (nameA > nameB) {
          return 1;
        }

        // names must be equal
        return 0;
      } ,
    },
    {
      title: 'Entries Found',
      dataIndex: 'data',
      render: (data) => data.length,
      defaultSortOrder: 'descend' as SortOrder,
      sorter: (a, b) => a.data.length - b.data.length,
    },
    {
      title: 'Track',
      render: (text, record) => (
        <Typography.Link onClick={() => trackTrademarkMutation.mutate({keyword:record.keyword,...checkForTrademark.queryParams,project:project["@id"]})}>
          Add
        </Typography.Link>
      ),
    },
  ];

  const mutationCall = (value: {asin: string; classification: []; region: []}) => {
    if (_.isEmpty(value?.asin)) return null;
    setData([]);
    asyncMutation(splitSentenceToWords(value?.asin), 0);
  };

  const asyncMutation = (array: string[], index: number) => {
    checkForTrademark.mutate(array[index], {
      onError: (data, variables, context) => {
        message.error(`Request failed`);
      },
      onSuccess: (data, variables, context) => {
        setData(prevState => [...prevState, {data: data, keyword: array[index].toString()}]);
        if (index < array.length - 1) {
          asyncMutation(array, ++index);
        }
      },
    });
  };


  return (
    <>
      <Spin spinning={checkForTrademark.isLoading} />
      <AsinForm mutate={mutationCall} setQueryParams={checkForTrademark.setQueryParams} />
      <Title  style={{margin: '1rem 0'}} level={5}>
        {' '}
        Found Trademarks
      </Title>
      <WhiteHeaderTable<DataKeyword|any>
        // loading={isLoading}
        bordered
        dataSource={data}
        columns={columns}
        pagination={false}
        rowKey={({keyword}: any) => keyword}
        expandable={{
          expandedRowRender: (record: any) => <ExtendableTable record={record.data} />,
          rowExpandable: (record: any) => !!record?.data?.length,
        }}
      />
    </>
  );
};

export default Check;
