import React from "react";
import { WhiteHeaderTable } from "../../styles/TableStyles";

type AppProps = {
  record: {};
  all?: boolean
};

const ExtendableTable = ({record, all = true}:AppProps) => {
  const columns = all? [
    {title: 'Registry', dataIndex: 'Registry', key: 'Registry'},
    {
      title: 'SerialNumber',
      dataIndex: 'SerialNumber',
      key: 'SerialNumber',
      render: (text: {} | null | undefined, row: {Registry: any; UrlId: any}, index: any) => {
        switch (row.Registry) {
          case 'USPTO':
            return (
              <a
                target="_blank"
                rel="noreferrer noopener"
                href={`https://tsdr.uspto.gov/#caseNumber=${text}&caseType=SERIAL_NO&searchType=statusSearch`}
              >
                {text}
              </a>
            );
          case 'DPMA':
            return (
              <a
                target="_blank"
                rel="noreferrer noopener"
                href={`https://register.dpma.de/DPMAregister/marke/register/${text}/DE`}
              >
                {text}
              </a>
            );
          case 'WIPO':
            return (
              <a
                target="_blank"
                rel="noreferrer noopener"
                href={`https://www3.wipo.int/branddb/en/showData.jsp?ID=${text}`}
              >
                {text}
              </a>
            );
          case 'EUIPO':
            return (
              <a
                target="_blank"
                rel="noreferrer noopener"
                href={`https://euipo.europa.eu/eSearch/#details/trademarks/0${text}`}
              >
                {text}
              </a>
            );
          default:
            return (
              <a
                target="_blank"
                rel="noreferrer noopener"
                href={`https://www.tmdn.org/tmview/#/tmview/detail/${row.UrlId}`}
              >
                {text}
              </a>
            );
        }
      },
    },
    {
      title: 'Trademark',
      dataIndex: 'Trademark',
      key: 'Trademark',
    },
    {
      title: 'Classification',
      dataIndex: 'Classification',
      key: 'Classification',
      // render: text => <Text style={{width:"fit-content"}} ellipsis={{ tooltip: text, }}>{text}</Text>,
      // width:"300px"
    },
    {
      title: 'Registration Date',
      dataIndex: 'RegistrationDate',
      key: 'RegistrationDate',
    },
    {
      title: 'Type',
      dataIndex: 'Type',
      key: 'Type',
    },
    {
      title: 'Status',
      dataIndex: 'Status',
      key: 'Status',
    },
    // {
    //   title: 'Track',
    //   dataIndex: '',
    //   key: 'x',
    //   render: () => <a>Add</a>,
    // },
  ]:[{
    title: 'SerialNumber',
    dataIndex: 'SerialNumber',
    key: 'SerialNumber',
    render: (text: {} | null | undefined, row: {Registry: any; UrlId: any}, index: any) => {
      switch (row.Registry) {
        case 'USPTO':
          return (
            <a
              target="_blank"
              rel="noreferrer noopener"
              href={`https://tsdr.uspto.gov/#caseNumber=${text}&caseType=SERIAL_NO&searchType=statusSearch`}
            >
              {text}
            </a>
          );
        case 'DPMA':
          return (
            <a
              target="_blank"
              rel="noreferrer noopener"
              href={`https://register.dpma.de/DPMAregister/marke/register/${text}/DE`}
            >
              {text}
            </a>
          );
        case 'WIPO':
          return (
            <a
              target="_blank"
              rel="noreferrer noopener"
              href={`https://www3.wipo.int/branddb/en/showData.jsp?ID=${text}`}
            >
              {text}
            </a>
          );
        case 'EUIPO':
          return (
            <a
              target="_blank"
              rel="noreferrer noopener"
              href={`https://euipo.europa.eu/eSearch/#details/trademarks/0${text}`}
            >
              {text}
            </a>
          );
        default:
          return (
            // <a
            //   target="_blank"
            //   rel="noreferrer noopener"
            //   href={`https://www.tmdn.org/tmview/#/tmview/detail/${row.UrlId}`}
            // >
              text
            // </a>
          );
      }
    },
  },
    {
      title: 'Status',
      dataIndex: 'Status',
      key: 'Status',
    },];

  return (
    // @ts-ignore
    <WhiteHeaderTable
      bordered
      dataSource={record}
      columns={columns}
      pagination={false}
    />
  );
};

export default ExtendableTable;
