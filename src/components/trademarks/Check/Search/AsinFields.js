import React from "react";
import { Field, FormSpy } from "react-final-form";
import MyTextField from "../../../input/MyFields/MyTextField";
import { Button, Col, Space } from "antd";
import HandleEnterKey from "../../../input/HandleKey/HandleEnterKey";
import styled from "styled-components";
import { SearchOutlined } from "@ant-design/icons";
import FilterModal from "./FilterModal";

const BigField = styled(Field)`
  width: 500px;
  .ant-input {
  line-height: 2.2;
}
  
`;

const GrayColor = styled(Button)`
  color: #31394D;
  background: #EBEDF4;
  border-color: #EBEDF4;
  height: 3.15rem;
  margin-bottom: 1.5rem;
`;

const AsinFields =  ({ submitError, handleSubmit, setQueryParams }) => {
  const wrapperCol = { span: 10 };
  return (
    <form onSubmit={handleSubmit}>
      <Space style={{marginTop:"1rem"}}>
      <BigField
        size={'large'}
        // wrapperCol={wrapperCol}
        // label={'Email'}
        name="asin"
        component={MyTextField}
        placeholder={"Keywords & Phrases"}
        hasFeedback
        allowNull={true}
        parse={x => x}
      />
      <GrayColor  size={'large'} onClick={handleSubmit} type={'primary'}>
        <SearchOutlined />
      </GrayColor>
        </Space>
      <HandleEnterKey handleOnEnterKey={handleSubmit} />
      <FilterModal  setQueryParams={setQueryParams}/>
    </form>
  );
};


export default AsinFields;
