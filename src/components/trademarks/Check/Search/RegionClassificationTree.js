import React, {useEffect} from 'react';
import {Tree} from 'antd';
import {useField} from 'react-final-form';
import { classification,region, trademarkFilter } from "../../../../utils/constants/constants";
import _ from "lodash";

const RegionClassificationTree = ({setQueryParams}) => {
  const regionField = useField(region, {subscription: {value: true}});
  const classificationField = useField(classification, {subscription: {value: true}});
  const regionData = [
    {
      title: "Region",
      key: region,
      children: trademarkFilter[region].map(child => ({
        title: child,
        key: child,
      })),
    },
  ];
  const classificationData = [
    {
      title: "Classification",
      key: classification,
      children: trademarkFilter[classification].map(child => ({
        title: child,
        key: child,
      })),
    },
  ];
  useEffect(() => {
    const value =regionField?.input?.value;
    if(!_.isEmpty(value)) {
      setQueryParams(prevState => ({...prevState,country: _.without(value, region) }))
    } else {
      setQueryParams(prevState => ({ ...prevState, country:trademarkFilter[region] }))
    }
  }, [regionField?.input?.value]);

  useEffect(() => {
    const value =classificationField?.input?.value;
    if(!_.isEmpty(value)) {
      setQueryParams(prevState => ({...prevState,classes:  _.without(value, classification)}))
    } else {
      setQueryParams(prevState => ({...prevState,classes:trademarkFilter[classification]}))
    }

  }, [classificationField?.input?.value]);

  return (
    <>
      <Tree checkable onCheck={regionField.input.onChange} treeData={regionData} />
      <Tree checkable onCheck={classificationField.input.onChange} treeData={classificationData} />
    </>
  );
};

export default RegionClassificationTree;
