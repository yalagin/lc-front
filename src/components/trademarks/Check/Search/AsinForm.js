import React from "react";
import { Form } from "react-final-form";
import ResetPasswordFields from "../../../auth/resetPassword/ResetPasswordFields";
import AsinFields from "./AsinFields";

const AsinForm = ({mutate,setQueryParams}) => {

  const onSubmit = async (values,form) => {
    console.log(values);
    // form.restart();
    mutate(values)
  };

  return (
    <Form
      onSubmit={onSubmit}
      debug={process.env.REACT_APP_SPY && console.log}
      // validate={validate}
      component={AsinFields}
      setQueryParams={setQueryParams}
      validate={(values) => {
        const errors = {};
        if (!values.asin) {
          errors.asin = "Required";
        }
        return errors;
      }}
    />
  );
};

export default AsinForm;
