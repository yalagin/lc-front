import React, { Dispatch, SetStateAction, useState } from "react";
import { Modal,Button } from "antd";
import RegionClassificationTree from "./RegionClassificationTree";

const FilterModal = ({setQueryParams}:{setQueryParams:Dispatch<SetStateAction<{classes:number[],country:string[]}>>}) => {
  const [isModalVisible, setIsModalVisible] = useState(false);

  const showModal = () => {
    setIsModalVisible(true);
  };

  const handleOk = () => {
    setIsModalVisible(false);
  };

  const handleCancel = () => {
    setIsModalVisible(false);
  };

  return (
    <>
      <Button style={{float:'right', margin: "1rem"}} size={'large'} type="primary" onClick={showModal}>
        Filter
      </Button>
      <Modal title="Trademark Filter" visible={isModalVisible} onOk={handleOk} onCancel={handleCancel}>
        <RegionClassificationTree setQueryParams={setQueryParams}/>
      </Modal>
    </>
  );
};

export default FilterModal;
