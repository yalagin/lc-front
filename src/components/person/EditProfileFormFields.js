import React from 'react';
import { Button, Col, Form, Row, Typography } from 'antd';
import { Field } from 'react-final-form';
import MyTextField from '../input/MyFields/MyTextField';
import MyPassword from "../input/MyFields/MyPassword";

const { Title,Text } = Typography;

const EditProfileFormFields = ({ submitError, handleSubmit }) => {
  return (
    <form onSubmit={handleSubmit}>
      <Title level={4}>Address</Title>
      <Row gutter={[16, 16]}>
       <Col span={24} lg={{span: 8}}>
          <Field
            size={'large'}
            labelCol={{ span: 24 }}
            label={'Company Name'}
            name="company"
            component={MyTextField}
            hasFeedback
            allowNull={true}
            parse={x => x}
          />
        </Col>
       <Col span={24} lg={{span: 8}}>
          <Field
            size={'large'}
            labelCol={{ span: 24 }}
            label={'Contact Number'}
            name="telephone"
            component={MyTextField}
            hasFeedback
            allowNull={true}
            parse={x => x}
          />
        </Col>
       <Col span={24} lg={{span: 8}}>
          <Field
            size={'large'}
            labelCol={{ span: 24 }}
            label={'Address'}
            name="street"
            placeholder="Address"
            hasFeedback
            component={MyTextField}
            allowNull={true}
            parse={x => x}
          />
        </Col>
       <Col span={24} lg={{span: 8}}>
          <Field
            size={'large'}
            labelCol={{ span: 24 }}
            label={'City'}
            name="city"
            component={MyTextField}
            hasFeedback
            allowNull={true}
            parse={x => x}
          />
        </Col>
       <Col span={24} lg={{span: 8}}>
          <Field
            size={'large'}
            labelCol={{ span: 24 }}
            label={'State'}
            name="region"
            hasFeedback
            component={MyTextField}
            allowNull={true}
            parse={x => x}
          />
        </Col>
       <Col span={24} lg={{span: 8}}>
          <Field
            size={'large'}
            labelCol={{ span: 24 }}
            label={'Country'}
            name="country"
            component={MyTextField}
            hasFeedback
            allowNull={true}
            parse={x => x}
          />
        </Col>
       <Col span={24} lg={{span: 8}}>
          <Field
            size={'large'}
            labelCol={{ span: 24 }}
            label={'Postal code'}
            name="zip"
            component={MyTextField}
            hasFeedback
            allowNull={true}
            parse={x => x}
          />
        </Col>
        <Col span={24} lg={{span: 8}}>
          <Field
            size={'large'}
            labelCol={{ span: 24 }}
            label={'Api Key for translations'}
            name="apiKey"
            component={MyPassword}
            hasFeedback
            allowNull={true}
            parse={x => x}
          />
        </Col>
      </Row>
      {submitError && <Text type="danger">submitError</Text>}
      <Button onClick={handleSubmit} size={'large'} type={'primary'}>
        Save Changes
      </Button>
    </form>
  );
};

export default EditProfileFormFields;
