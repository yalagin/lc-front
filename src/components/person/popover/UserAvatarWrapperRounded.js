import styled from "styled-components";
import { Space } from "antd";

 const UserAvatarWrapperRounded = styled(Space)`
  border-radius: 22px;
  cursor: pointer;
  padding: 0.2rem;
  border: 1px solid rgba(72, 94, 144, 0.16);
  box-shadow: 0 2px 0 rgb(0 0 0 / 2%);
  transition: all 0.3s cubic-bezier(0.645, 0.045, 0.355, 1);
  -webkit-user-select: none;
  user-select: none;
  touch-action: manipulation;

  :hover, :focus {
   //background: #f6f5f8;
  border-color: #a38fff;
   color: #a38fff;
   //box-shadow: 0 1px 2px -2px rgb(0 0 0 / 16%), 0 3px 6px 0 rgb(0 0 0 / 12%), 0 5px 12px 4px rgb(0 0 0 / 9%);
  }


  & > *:first-child {
   margin-left: 0.5rem;
  }

  & > *:last-child {
   margin-right: 0.5rem;
  }
 `;
export default UserAvatarWrapperRounded;
