import { Avatar, Image } from "antd";
import {DownOutlined} from '@ant-design/icons';
import React, { useEffect, useState } from "react";
import useProfile from '../../../utils/hooks/useProfile';
import UserAvatarWrapperRounded from './UserAvatarWrapperRounded';
import { ENTRYPOINT } from "../../../config/entrypoint";

const UserAvatarWrapper = () => {
  const {profile} = useProfile();
  const [profileImage, setProfileImage] = useState(null);
  useEffect(() => {
    const image = profile;
    setProfileImage(profile?.image?.contentUrl);
  }, [profile]);

  return (
    <UserAvatarWrapperRounded align="center">
      <Avatar
        style={{marginRight: 0}}
        src={<Image
          preview={false}
          style={{ cursor: 'pointer' }}
          src={profileImage?new URL(profileImage, ENTRYPOINT):"https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png"}
          fallback="https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png" />}
      />
      <div style={{color: '#7f67ff'}}>
        {' '}
        {profile?.givenName} {profile?.familyName}
      </div>
      <DownOutlined />
    </UserAvatarWrapperRounded>
  );
};
export default UserAvatarWrapper
