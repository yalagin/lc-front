import React from "react";
import EditProfileFormFields from "./EditProfileFormFields";
import { Form } from "react-final-form";
import useProfile from "../../utils/hooks/useProfile";
import { useMutation } from "react-query";
import { fetch2 } from "../../utils/dataAccess";
import { message } from "antd";
import { list as personList } from "../../actions/person/list";
import { useDispatch } from "react-redux";

const EditProfileForm = () => {
  const { profile } = useProfile();
  const dispatch = useDispatch();

  const mutationSaveProfile = useMutation(async data => await fetch2(data['@id'], {
    method: 'PUT',
    body: JSON.stringify(data)
  }), {
    onSuccess: (data, variables, context) => {
      message.info(`Saved!`);
      dispatch(personList('people?user=' + localStorage.getItem('id')));
    }
  });

  const onSubmit = async values => {
    console.log(values);
    try {
      await mutationSaveProfile.mutateAsync(values)
    } catch (error) {
      return error.errors
    }
  };

  return (
    <Form
      onSubmit={onSubmit}
      // validate={validate}
      initialValues={profile}
      component={EditProfileFormFields}
    />
  );
};

export default EditProfileForm;
