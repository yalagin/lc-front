import React, {useState} from 'react';
import { Button, message, Modal, notification, Tooltip } from "antd";
import UserAvatarWrapperRounded from '../person/popover/UserAvatarWrapperRounded';
import {BugOutlined} from '@ant-design/icons';
import {Field, Form} from 'react-final-form';
import MyTextField from '../input/MyFields/MyTextField';
import MyTextAreaField from '../input/MyFields/MyTextAreaField';
import { useMutation } from "react-query";
import { fetch2 } from "../../utils/dataAccess";

interface BugReport  {subject?:string,about?:string}

function ReportBugs() {
  const [isModalVisible, setIsModalVisible] = useState<boolean>(false);

  const save = useMutation(
    (data:BugReport) =>
      fetch2('/send-bug-report', {
        method: 'POST',
        headers: new Headers({'Content-Type': 'application/ld+json'}),
        body: JSON.stringify(data),
      }),
    {
      onSuccess: (data, variables, context) => {
        notification.info({message: `Sent!`});
        setIsModalVisible(false);
      },
      onError: () =>{
        message.error('Something wrong');
        setIsModalVisible(false);
      }
    }
  );


  const onSubmit = (values:BugReport) => {
    save.mutate(values);
    setIsModalVisible(false);
  };

  return (
    <>
      <Tooltip placement="bottom" title={'Report bugs'}>
        <UserAvatarWrapperRounded
          onClick={() => {
            setIsModalVisible(true);
          }}
          style={{marginRight: '1rem'}}
        >
          <BugOutlined />
        </UserAvatarWrapperRounded>
      </Tooltip>
      <Form
        onSubmit={onSubmit}
        validate={values => {
          const  errors:BugReport = {
            about: undefined,
            subject: undefined
          }
          if (!values.subject) {
            errors.subject = 'Required'
          }
          if (!values.about) {
            errors.about = 'Required'
          }
          return errors
        }}
        render={({handleSubmit, form, submitting, pristine, values}) => (
          <Modal
            title="Submit bugs"
            visible={isModalVisible}
            onOk={()=>setIsModalVisible(false)}
            onCancel={()=>setIsModalVisible(false)}
            footer={[
              <Button type="primary" loading={save.isLoading} onClick={handleSubmit} disabled={submitting || pristine}>
              Submit
            </Button>
            ]}
          >
            <form onSubmit={handleSubmit}>
              <div>
                <label>Title</label>
                <Field
                  name="subject"
                  type="text"
                  placeholder="Title"
                  hasFeedback
                  defaultValue={'Bug report'}
                  component={MyTextField}
                />
              </div>
              <div>
                <label>Bugs</label>
                <Field
                  name="about"
                  type="text"
                  placeholder="Something wrong?"
                  hasFeedback
                  component={MyTextAreaField}
                />
              </div>
            </form>
          </Modal>
        )}
      />
    </>
  );
}

export default ReportBugs;
