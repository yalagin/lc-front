import React, {useRef} from 'react';
import {Col, Row} from 'antd';
import {CustomButton, MySwitchFieldInput} from '../styles/SwitchFieldStyles';

const MySwitchField = ({label, disabled, input}) => {
  const ref = useRef(null);
  return (
    <CustomButton onClick={() => ref.current.click()} disabled={disabled}>
      <Row justify="space-around" align="middle">
        <Col>
          <label style={{margin: '0.5rem'}}>{label}</label>
        </Col>
        <Col>
          <MySwitchFieldInput
            id={input.name}
            style={{margin: '0.5rem'}}
            name={input.name}
            disabled={disabled}
            type="checkbox"
            checked={input?.checked||input?.value}
            {...input}
            ref={ref}
            onClick={() => ref.current.click()}
          />
        </Col>
      </Row>
    </CustomButton>
  );
};

export default MySwitchField;
