import React, { useCallback, useEffect } from 'react';

const HandleEnterKey = ({ handleOnEnterKey , keyCode = 13 }) => {
  const escFunction = useCallback((event) => {
    if (event.keyCode === keyCode) {
      handleOnEnterKey();
    }
  }, [handleOnEnterKey]);

  useEffect(() => {
    document.addEventListener('keydown', escFunction, false);

    return () => {
      document.removeEventListener('keydown', escFunction, false);
    };
  }, []);

  return (
        <></>
  );
};
export default HandleEnterKey;
