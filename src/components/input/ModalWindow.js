import React, { useState } from "react";
import {Modal} from 'antd';
import { FormSpy } from "react-final-form";

const ModalWindow = ({ handleOk, visible, setVisible,title,...props}) => {
  return visible &&
    <Modal
      title={title}
      visible={visible}
      onOk={handleOk}
      onCancel={() => setVisible(false)}
    >
      <FormSpy
        subscription={{ values: true }}
      >
        {props.children}
      </FormSpy>
    </Modal>;
}
export default ModalWindow;
