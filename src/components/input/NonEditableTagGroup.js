import React from 'react';
import {Input, Tag} from "antd";
import _ from "lodash"

const NonEditableTagGroup = ({tags}) => {
    const forMap = (tag, index) => {
        let color;
        if (index < 10) {
            color = "#FF9F9F";
        } else if (index < 15) {
            color = "rgba(171,168,255,0.76)";
        } else if (index < 20) {
            color = "rgba(255,168,249,0.76)";
        }  else if (index < 25) {
            color = "#91d5ff";
        } else {
            color = "#F9C371C2";
        }
        const tagElem = (
            <Tag
                color={color}
            >
                {tag}
            </Tag>
        );
        return (
            <span key={tag} style={{ display: 'inline-block' }}>
        {tagElem}
      </span>
        );
    };

    const tagChild = _.isArray(tags)&& tags.map(forMap);
    return (
        <>
            <div style={{ marginBottom: 10  }}>
                    {tagChild}
            </div >
        </>
    );
};

export default NonEditableTagGroup;
