import { Form } from "antd";
const FormItem = Form.Item;

const makeField = Component => ({ input, meta, children, hasFeedback, label, ...rest }) => {
  const hasError = meta.touched && meta.invalid;
  const formItemLayout = {
    labelCol:{span: 24},
  };

  return (
    <FormItem
      {...formItemLayout}
      label={label}
      validateStatus={hasError ? "error" : "success"}
      hasFeedback={hasFeedback && hasError}
      help={hasError && meta.error||meta.submitFailed && meta.submitError}
    >
      <Component {...input} {...rest} children={children} />
    </FormItem>
  );
};

export default makeField;
