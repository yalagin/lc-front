import React from "react";
import { Form, Input, Popover, Space, Typography } from "antd";
import { SketchPicker } from "react-color";
import { ExclamationCircleOutlined } from "@ant-design/icons";
import { CircleFilled } from "../../styles/SwitchFieldStyles";

const FormItem = Form.Item;
const { Title } = Typography;

const MyColorPicker =  ({
                          input,
                          meta,
                          children,
                          hasFeedback,
                          label,
                          labelCol,
                          wrapperCol,
                          placeholder,
                          size,
                          ...rest
                        }) => {
  const hasError = meta.touched && meta.invalid && !meta.dirtySinceLastSubmit;
  return (
    <FormItem
      label={label}
      validateStatus={hasError ? "error" : "success"}
      hasFeedback={hasFeedback && hasError}
      help={(hasError && meta.error) || (hasError && meta.submitError)}
      labelCol={labelCol}
      wrapperCol={wrapperCol}
      {...rest}
    >
      <Popover
        placement="topLeft"
        title={'Edit background color'}
        content={
          <Space direction={'vertical'} align={'center'} size={'large'}>
            <SketchPicker
              key={'SketchPicker'}
              disableAlpha
              color={input.value}
              onChangeComplete={color => {
                input.onChange(color.hex);
                // dispatch(update(profile, { myColor: color.hex }));
              }}
            />
          </Space>
        }
        trigger="click"
      >
        <Space align={'baseline'} style={{cursor: 'pointer'}}>
          <ExclamationCircleOutlined style={{color: '#7f67ff'}} />
          <Title level={5}>Background Color</Title>
          <CircleFilled color={input.value} />
        </Space>
      </Popover>
    </FormItem>
  );
};

export default MyColorPicker;
