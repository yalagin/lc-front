import React from 'react';
import { Form, Select } from 'antd';

const FormItem = Form.Item;

const MySelect = ({
                    input,
                    meta,
                    children,
                    hasFeedback,
                    label,
                    labelCol,
                    wrapperCol,
                    placeholder,
                    size,
                    filterOption,
                    optionFilterProp,
                    showSearch,
                    ...rest
                  }) => {
  const hasError = meta.touched && meta.invalid && !meta.dirtySinceLastSubmit;
  // console.log(meta,meta.touched , meta.invalid , !meta.dirtySinceLastSubmit,meta.error,meta.submitError);
  return (
    <FormItem
      label={label}
      validateStatus={hasError ? "error" : "success"}
      hasFeedback={hasFeedback && hasError}
      help={(hasError && meta.error) || (hasError && meta.submitError)}
      labelCol={labelCol}
      wrapperCol={wrapperCol}
      {...rest}
    >
      <Select dropdownRender={rest.dropdownRender}
          listHeight={rest.listHeight} listItemHeight={rest.listHeight} tokenSeparators={rest.tokenSeparators}
        {...input} value={input.value || undefined} onChange={input.onChange} optionFilterProp={optionFilterProp}  mode={rest.mode}
        filterOption={filterOption} showSearch={showSearch} size={size} allowClear placeholder={placeholder} disabled={rest.disabled}
      >
        {children}
      </Select>
    </FormItem>
  );
};

export default MySelect;
