import React from 'react';
import { Form, Input } from 'antd';

const FormItem = Form.Item;

const MyPassword = ({
                      input,
                      meta,
                      children,
                      hasFeedback,
                      label,
                      labelCol,
                      wrapperCol,
                      size,
                      ...rest
                    }) => {
  const hasError = meta.touched && meta.invalid && !meta.dirtySinceLastSubmit;
  return (
    <FormItem
      label={label}
      validateStatus={hasError ? "error" : "success"}
      hasFeedback={hasFeedback && hasError}
      help={(hasError && meta.error) || (hasError && meta.submitError)}
      labelCol={labelCol}
      wrapperCol={wrapperCol}
    >
      <Input.Password {...input} size={size} allowClear />
      {children}
    </FormItem>
  );
};

export default MyPassword;
