import React from 'react';
import { Form, Switch } from 'antd';

const FormItem = Form.Item;

const MySwitch = ({
                    input,
                    meta,
                    children,
                    hasFeedback,
                    label,
                    labelCol,
                    wrapperCol,
                    placeholder,
                    size,
                    ...rest
                  }) => {
  const hasError = meta.touched && meta.invalid && !meta.dirtySinceLastSubmit;
  return (
    <FormItem
      label={label}
      validateStatus={hasError ? "error" : "success"}
      hasFeedback={hasFeedback && hasError}
      help={(hasError && meta.error) || (hasError && meta.submitError)}
      labelCol={labelCol}
      wrapperCol={wrapperCol}
      {...rest}
    >
      <Switch
        {...input} size={size} allowClear placeholder={placeholder} disabled={rest.disabled}
      >
        {children}
      </Switch>
    </FormItem>
  );
};

export default MySwitch;
