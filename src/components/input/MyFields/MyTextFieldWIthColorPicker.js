import React from 'react';
import { Button, Dropdown, Form, Input, Space } from "antd";
import { SketchPicker } from "react-color";

const FormItem = Form.Item;

const MyTextFieldWIthColorPicker = ({
                       input,
                       meta,
                       children,
                       hasFeedback,
                       label,
                       labelCol,
                       wrapperCol,
                       placeholder,
                        size,
                       ...rest
                     }) => {
  const hasError = meta?.touched && meta?.invalid && !meta?.dirtySinceLastSubmit;


  const overlay = (
      <Space direction={'vertical'} align={'center'} size={'large'}>
        <SketchPicker
          key={'SketchPicker'}
          disableAlpha
          color={input.value}
          onChangeComplete={color => {
            input.onChange(color.hex);
          }}
        />
      </Space>
  );

  return (
    <FormItem
      label={label}
      validateStatus={hasError ? "error" : "success"}
      hasFeedback={hasFeedback && hasError}
      help={(hasError && meta?.error) || (hasError && meta?.submitError)}
      labelCol={labelCol}
      wrapperCol={wrapperCol}
      {...rest}
    >
      <Input {...input} size={size} allowClear placeholder={placeholder}
             prefix={
        <Dropdown trigger={["click"]} overlay={overlay}>
          <Button style={{ background: input.value,    width: "23px" }} size={'small'}>  &#32; &#32;  </Button>
        </Dropdown>
      } />
    </FormItem>
  );
};


export default MyTextFieldWIthColorPicker;
