import React from 'react';
import { Form, InputNumber, Slider } from "antd";

const FormItem = Form.Item;

const MyPercentageField = ({
                       input,
                       meta,
                       children,
                       hasFeedback,
                       label,
                       labelCol,
                       wrapperCol,
                       placeholder,
                        size,
                       ...rest
                     }) => {
  const hasError = meta?.touched && meta?.invalid && !meta?.dirtySinceLastSubmit;
  return (
    <FormItem
      label={label}
      validateStatus={hasError ? "error" : "success"}
      hasFeedback={hasFeedback && hasError}
      help={(hasError && meta?.error) || (hasError && meta?.submitError)}
      labelCol={labelCol}
      wrapperCol={wrapperCol}
      layout={'horizontal'}
      {...rest}
    >
      {/*<Slider   {...input} defaultValue={100}  />*/}
      <InputNumber
        // defaultValue={"100%"}
        min={0}
        max={100}
        formatter={value => `${value}%`}
        parser={value => value.replace('%', '')}
        {...input}
        size={size}
        placeholder={placeholder}
        allowClear
      />
    </FormItem>
  );
};

export default MyPercentageField;
