import React from 'react';
import { Form, Input } from 'antd';

const FormItem = Form.Item;

const MyTextField = ({
                       input,
                       meta,
                       children,
                       hasFeedback,
                       label,
                       labelCol,
                       wrapperCol,
                       placeholder,
                        size,
                       ...rest
                     }) => {
  const hasError = meta?.touched && meta?.invalid && !meta?.dirtySinceLastSubmit;
  return (
    <FormItem
      label={label}
      validateStatus={hasError ? "error" : "success"}
      hasFeedback={hasFeedback && hasError}
      help={(hasError && meta?.error) || (hasError && meta?.submitError)}
      labelCol={labelCol}
      wrapperCol={wrapperCol}
      {...rest}
    >
      <Input {...input} disabled={rest.disabled}  size={size} allowClear placeholder={placeholder} />
    </FormItem>
  );
};

export default MyTextField;
