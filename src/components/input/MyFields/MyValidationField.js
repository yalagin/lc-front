import React from 'react';
import {Form} from 'antd';
import styled from "styled-components";

const StyledFormItem = styled(Form.Item) `
 &&& .ant-form-item-control-input {
    min-height: 0;
  }
`;


const MyValidationField = ({
  input,
  meta,
  children,
  hasFeedback,
  label,
  labelCol,
  wrapperCol,
  placeholder,
  size,
  ...rest
}) => {
  const hasError = meta.touched && meta.invalid && !meta.dirtySinceLastSubmit;
  return (
    <StyledFormItem
      label={label}
      validateStatus={hasError ? 'error' : meta.error?'warning':'success'}
      hasFeedback={hasFeedback && hasError}
      help={ meta.error|| (hasError && meta.submitError)}
      labelCol={labelCol}
      wrapperCol={wrapperCol}
      style={{marginBottom:"-1rem"}}
      {...rest}
    />
  );
};

export default MyValidationField;
