import React, { useEffect, useRef, useState } from "react";
import { Form, Input, Mentions, Tag } from "antd";
import styled from "styled-components";
import { useForm } from 'react-final-form'
const { Option } = Mentions;

const StyledMentions = styled(Mentions)`
textarea{
  padding: 6.5px 11px;
  font-size: 16px;
}
`;
const StyledCharCount = styled.div`
  float: right;
  color: rgba(0, 0, 0, 0.45);
  white-space: nowrap;
  content: attr(data-count);
  pointer-events: none;
`;
const StyledCharCountLeft = styled(StyledCharCount)`
  float: left;
`;

const MyTextAreaWithMentionsField = ({
                       input,
                       meta,
                       children,
                       hasFeedback,
                       label,
                       labelCol,
                       wrapperCol,
                       placeholder,
                       size,
                       atData,
                       maxLength,
                       showCount,
                       ...rest
                     }) => {

  const [prefix, setPrefix] = useState("#");
  const [data, setData] = useState( {
    '#':['Christmas', 'New year'],
    '@':  ["Standard t-shirt"                 ,
      "Premium t-shirt"                       ,
      "V-neck t-shirt"                        ,
      "Tank top"                              ,
      "Long sleeve t-shirt"                   ,
      "Raglan"                                ,
      "Sweatshirt"                            ,
      "Pullover hoodie"                       ,
      "Zip hoodie"                            ,
      "PopSockets grip"                       ,
      "iPhone cases"                          ,
      "Samsung Galaxy cases"                  ,
      "Tote bag"                              ,
      "Throw pillows"],
  });
  const form = useForm();
  const [dataCount, setDataCount] = useState("0".concat(!!maxLength ? " / ".concat(maxLength) : ''));
  const availablePrefixes = ['@', "#"];
  const hasError = meta.touched && meta.invalid && !meta.dirtySinceLastSubmit;
  const FormItem = Form.Item;

  // const ref = useRef(null);

  useEffect(() => {
    if (showCount &&  input?.value) {
     setDataCount("".concat(input.value.length).concat(!!maxLength ? " / ".concat(maxLength) : ''));
    }else {
      setDataCount("0".concat(!!maxLength ? " / ".concat(maxLength) : ''));
    }
  }, [showCount, input?.value]);

  useEffect(() => {
    setData(prevState => ({...prevState,'#':atData}))
  }, [atData]);

  const onSearch = (_, prefix) => {
    setPrefix(prefix)
  };

  function onSelect(option) {
    console.log('select', option);
    // setTimeout(form.mutators.removeSpecialChars(input.name,availablePrefixes), 1000);
    form.mutators.removeSpecialChars(input.name,availablePrefixes)
   }

   // const onclick = e => {
   //   const target = e.target;
   //   const keyboardEvent = new KeyboardEvent("keypress", {
   //     bubbles : true,
   //     cancelable : true,
   //     key : "Enter",
   //     shiftKey : false,
   //     keyCode : 13
   //   });
   //   target.dispatchEvent(keyboardEvent);
   // }


  return (
    <FormItem
      label={label}
      validateStatus={hasError ? 'error' : 'success'}
      hasFeedback={hasFeedback && hasError}
      help={(hasError && meta.error) || (hasError && meta.submitError)}
      labelCol={labelCol}
      wrapperCol={wrapperCol}
      {...rest}
    >
      <StyledMentions
        // ref={ref}
        onSelect={onSelect}
        onSearch={onSearch}
        prefix={availablePrefixes}
        {...input}
        size={size}
        allowClear
        placeholder={placeholder}
        {...rest}
        maxLength={maxLength}
        // onClick={onclick}
      >
        {(data[prefix] || []).map(value => (
          <Option key={value} value={value}>
            {value}
          </Option>
        ))}
      </StyledMentions>
      {maxLength && <StyledCharCount> Type “#” for adding from tags, {dataCount}</StyledCharCount>}
    </FormItem>
  );
};

export default MyTextAreaWithMentionsField;
