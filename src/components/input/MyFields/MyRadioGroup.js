import React from 'react';
import { Form, Radio } from 'antd';

const FormItem = Form.Item;

const MyRadioGroup = ({
                    input,
                    meta,
                    children,
                    hasFeedback,
                    label,
                    labelCol,
                    wrapperCol,
                    placeholder,
                    size,
                    defaultValue,
                        value,
                    ...rest
                  }) => {
  const hasError = meta.touched && meta.invalid && !meta.dirtySinceLastSubmit;
  return (
    <FormItem
      label={label}
      validateStatus={hasError ? "error" : "success"}
      hasFeedback={hasFeedback && hasError}
      help={(hasError && meta.error) || (hasError && meta.submitError)}
      labelCol={labelCol}
      wrapperCol={wrapperCol}
      {...rest}
    >
      <Radio.Group
        {...input} defaultValue={defaultValue}  size={size} allowClear placeholder={placeholder} disabled={rest.disabled}
      >
        {children}
      </Radio.Group>
    </FormItem>
  );
};

export default MyRadioGroup;
