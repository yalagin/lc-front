import React from 'react';
import { Form ,Checkbox} from 'antd';

const FormItem = Form.Item;

const MyCheckbox = ({
                      input,
                      meta,
                      children,
                      hasFeedback,
                      label,
                      labelCol,
                      wrapperCol,
                      placeholder,
                      size,
                      disabled,
                      ...rest
                    }) => {
  const hasError = meta.touched && meta.invalid && !meta.dirtySinceLastSubmit;
  return (
    <FormItem
      label={label}
      validateStatus={hasError ? "error" : "success"}
      hasFeedback={hasFeedback && hasError}
      help={(hasError && meta.error) || (hasError && meta.submitError)}
      labelCol={labelCol}
      wrapperCol={wrapperCol}
      {...rest}
    >
      <Checkbox
        {...input} size={size} allowClear placeholder={placeholder} disabled={disabled}
      >
        {children}
      </Checkbox>
    </FormItem>
  );
};

export default MyCheckbox;
