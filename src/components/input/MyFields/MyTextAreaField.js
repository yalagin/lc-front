import React from 'react';
import { Form, Input } from 'antd';

const FormItem = Form.Item;

const MyTextAreaField = ({
                       input,
                       meta,
                       children,
                       hasFeedback,
                       label,
                       labelCol,
                       wrapperCol,
                       placeholder,
                        size,
                       ...rest
                     }) => {
  const hasError = meta.touched && meta.invalid && !meta.dirtySinceLastSubmit;
  return (
    <FormItem
      label={label}
      validateStatus={hasError ? "error" : "success"}
      hasFeedback={hasFeedback && hasError}
      help={(hasError && meta.error) || (hasError && meta.submitError)}
      labelCol={labelCol}
      wrapperCol={wrapperCol}
      {...rest}
    >
      <Input.TextArea {...input} size={size} allowClear placeholder={placeholder}  {...rest} />
    </FormItem>
  );
};

export default MyTextAreaField;
