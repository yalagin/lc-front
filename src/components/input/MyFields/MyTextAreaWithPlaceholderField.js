import React from 'react';
import { Form, Select, Tag,Input } from "antd";

const FormItem = Form.Item;

const MyTextAreaWithPlaceholderField = ({
                       input,
                       meta,
                       children,
                       hasFeedback,
                       label,
                       labelCol,
                       wrapperCol,
                       placeholder,
                        size,
                       ...rest
                     }) => {
  const hasError = meta.touched && meta.invalid && !meta.dirtySinceLastSubmit;



  const addTag = e => {
    const txt = e.target.innerHTML;
    input.onChange( `${input.value} <${txt}> `);
  }

  return (
    <FormItem
      label={label}
      validateStatus={hasError ? "error" : "success"}
      hasFeedback={hasFeedback && hasError}
      help={(hasError && meta.error) || (hasError && meta.submitError)}
      labelCol={labelCol}
      wrapperCol={wrapperCol}
      {...rest}
    >
      {/*<Tag onClick={addTag} color="magenta">Template</Tag>*/}
      {/*<div contentEditable="true"  style={{width:"100%"}}   className="ant-select-selector" onChange={onChange}   placeholder={placeholder}  {...rest} >{value}</div>*/}
      <Input.TextArea  {...input} style={{ width: '100%' }} />
    </FormItem>
  );
};

export default MyTextAreaWithPlaceholderField;
