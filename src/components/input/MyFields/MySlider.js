import React from "react";
import { Slider, Form, Input } from "antd";

const FormItem = Form.Item;

const MySlider = ({
                           input,
                           meta,
                           children,
                           hasFeedback,
                           label,
                           labelCol,
                           wrapperCol,
                           placeholder,
                           size,
                           ...rest
                         }) => {
  const hasError = meta?.touched && meta?.invalid && !meta?.dirtySinceLastSubmit;
  return (
    <FormItem
      label={label}
      validateStatus={hasError ? "error" : "success"}
      hasFeedback={hasFeedback && hasError}
      help={(hasError && meta?.error) || (hasError && meta?.submitError)}
      labelCol={labelCol}
      wrapperCol={wrapperCol}
      {...rest}
    >
      <Slider {...input} disabled={rest.disabled}   />
    </FormItem>
  );
};

export default MySlider;
