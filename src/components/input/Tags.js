import EditableTagGroup from "./EditableTagGroup";

const Tags = ({input: {onChange, value}, label,disabled, ...rest}) => (
    <EditableTagGroup
        onChange={onChange}
        value={Array.isArray(value) ? value : []}
        rest={rest}
        disabled={disabled}
        {...rest}
    />
)
export default Tags;
