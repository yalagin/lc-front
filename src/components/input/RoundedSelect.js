import React from "react";
import styled from "styled-components";
import { Select } from "antd";

const RoundedSelect = styled(Select)`
  float: ${props => (props.right ? 'right' : "")};
  &&& > .ant-select-selector {
    border-radius: 40px;
    //border-radius: 40px !important;
    min-width: 80px;  
  }
`;

export default RoundedSelect;
