import React from "react";
import styled from "styled-components";
import { Badge } from "antd";

const StyledBadge=styled(Badge)`
  cursor:pointer;
  display:  ${props => props.hidden===true ? "none": ""};
  
  &>sup{
    width: 44px;
    margin: 0.3rem;
    transition: all 0.3s;
    background: ${props => props.touched ?"#0BB533": "#7987a1"};
    border-color:${props => props.selected ? "#a38fff":''};
    border-right-width: ${props => props.selected ?"3px !important":""};
    box-shadow: ${props => props.selected ? "0 0 0px 4px rgb(127 103 255)":""};
    //:hover{
    //  border: 3px solid #7f67ff;
    //}
  }
`;

export default StyledBadge;
