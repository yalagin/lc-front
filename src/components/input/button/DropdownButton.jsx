import styled from "styled-components";
import {Dropdown} from "antd";

export const DropdownButton = styled(Dropdown.Button)`
button{
  width: 100%;
}
`