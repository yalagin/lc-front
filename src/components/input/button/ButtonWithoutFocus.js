import React from "react";
import styled from "styled-components";
import { Button } from "antd";


const ButtonWithoutFocus = styled(Button)`
  :focus {
    background: #7f67ff;
    border-color: #7f67ff;
  }
`;

export default ButtonWithoutFocus;
