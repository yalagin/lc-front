import React from "react";
import styled from "styled-components";
import ButtonWithoutFocus from "./ButtonWithoutFocus";


const BigButton = styled(ButtonWithoutFocus)`
  width: 100%;
`;

export default BigButton;
