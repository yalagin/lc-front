import React from "react";
import styled from "styled-components";
import { Button } from "antd";

const DashedButton = styled(Button)`
  border: 1px dashed #d9d9d9;
  margin: 0.3rem;
`;

export default DashedButton;
