import React, { useState } from "react";
import {Col, Row, message, Menu} from "antd";
import BigButton from "./BigButton";
import ModalWindowWIthFormSpy from "../../design/transferModal/ModalWindowWIthFormSpy";
import {DropdownButton} from "./DropdownButton";
import { FormSpy } from 'react-final-form';
import AddToUploadModal from "../../design/table/AddToUploadModal";
import {HYDRAMEMBER} from "../../../utils/constants/constants";

const SubmitAndTransferButtons = ({handleSubmit,submitting,pristine,form,}) => {
  const [openModal, setOpenModal] = useState(false);
  const [transferAll, setTransferAll] = useState(false);
  const [showUploadModal, setShowUploadModal] = useState(false);
  const handleOk = () =>{
    setOpenModal(false);
    form.mutators.transfer(form,null,transferAll);
    message.success('fields are transferred, all fields in sub-designs are overridden!', 7)
  }

  const handleNotOverrideFieldsWithDifferentValues = (list) => {
    setOpenModal(false);
    form.mutators.transfer(form,list,transferAll);
    message.success('fields are transferred, fields are partly overridden!', 7)
  }

    function handleMenuClick(e) {
        setShowUploadModal(true)
    }

    const menu = (
        <Menu    style={{width:'100%'}}  onClick={handleMenuClick}>
            <Menu.Item key="1" style={{width:'100%'}} >    Save to Queue    </Menu.Item>
        </Menu>
    );

  return (
    <Row gutter={[16, 16]}>
      <Col xs={24} sm={24} md={12} lg={12} xl={12} xxl={8}>
        <BigButton size={"large"} type={"primary"} className={'transfer-selected-designs-btn'} onClick={(e) => {
          e.target.focus();
          return setOpenModal(true);
        }}>Transfer selected designs</BigButton>
      </Col>
      <Col xs={24} sm={24} md={12} lg={12} xl={12} xxl={8}>
        <BigButton size={"large"} type={"primary"}  className={'transfer-all-designs-btn'}  onClick={(e) => {
          e.target.focus();
          setTransferAll(true);
          return setOpenModal(true);
        }}>Transfer all designs</BigButton>
      </Col>
      <Col xs={24} sm={24} md={24} lg={24}  xl={24} xxl={8}>
        <DropdownButton style={{width:'100%'}} overlay={menu} className={'save-design-btn'}   size={"large"} type={"primary"} /*disabled={submitting || pristine}*/ onClick={(e) => {
          e.target.focus();
          return handleSubmit();
      }} >Save</DropdownButton>
      </Col>
      <ModalWindowWIthFormSpy
          visible={openModal}
          setVisible={setOpenModal}
          handleOk={handleOk}
          handleNotOverrideFieldsWithDifferentValues={handleNotOverrideFieldsWithDifferentValues}
          transferAll={transferAll}
      />
        {showUploadModal && <>
            <FormSpy subscription={{ values: true }}>
                {({ values }) => {
                    return (
                        <AddToUploadModal
                            showUploadModal={showUploadModal}
                            setShowUploadModal={setShowUploadModal}
                            designs={values[HYDRAMEMBER]}
                        />
                    );
                }}
            </FormSpy>
        </>
        }
    </Row>
  );
};

export default SubmitAndTransferButtons;
