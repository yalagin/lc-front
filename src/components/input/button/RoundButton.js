import React from "react";
import styled from "styled-components";
import { Button } from "antd";

const RoundButton = styled(Button)`
  border-radius: 100px;
  margin-top: 5px;
`;

export default RoundButton;
