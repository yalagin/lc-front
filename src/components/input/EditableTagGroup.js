import React, { useEffect, useState } from "react";
import { Tag, Input, Col } from "antd";
import {
  DndContext,
  closestCenter,
  KeyboardSensor,
  PointerSensor,
  useSensor,
  useSensors,
} from '@dnd-kit/core';
import {
  arrayMove,
  SortableContext,
  sortableKeyboardCoordinates,
  rectSortingStrategy, useSortable, horizontalListSortingStrategy
} from "@dnd-kit/sortable";

import { CSS } from "@dnd-kit/utilities";
import { useKeyPress } from "../../utils/hooks/useKeyPress";
import styled from "styled-components";

const StyledTag= styled(Tag)`
  &&& .ant-tag {
    transition: none;
  }
  transition: none;
`;

const ForMap = ({ tag, index ,color,maxLength,handleClose}) => {
  const {
    attributes,
    listeners,
    setNodeRef,
    transform,
    transition,
  } = useSortable({id: tag});

  const style = {
    transform: CSS.Transform.toString(transform),
    transition,
  };

  let tagColor;
  if (color === null) {

    if (index < 10) {
      tagColor = "#FF9F9F";
    } else if (index < 15) {
      tagColor = "rgba(171,168,255,0.76)";
    } else if (index < 20) {
      tagColor = "rgba(255,168,249,0.76)";
    } else if (index < 25) {
      tagColor = "#91d5ff";
    } else {
      tagColor = "#F9C371C2";
    }
  } else {
    tagColor = color;
  }

  return (
    <StyledTag
      closable
      color={ maxLength && index >= maxLength?'#d9d9d9':tagColor}
      ref={setNodeRef}
      {...attributes}
      {...listeners}
      style={{ margin: "4px" ,cursor:'grab',...style  }}
      onClose={e => {
        e.preventDefault();
        handleClose(tag);
      }}
    >
      {tag}
    </StyledTag>

  );
};

const EditableTagGroup = ({ value, onChange, disabled = false, color = null, maxLength = null ,wrapperCol = null,limitOfTags=999}) => {
  const [inputValue, setInputValue] = useState("");
  const comma = useKeyPress(',');

  const sensors = useSensors(
    useSensor(PointerSensor),
    useSensor(KeyboardSensor, {
      coordinateGetter: sortableKeyboardCoordinates,
    })
  );
  const handleClose = removedTag => {
    onChange(value.filter(tag => tag !== removedTag));
  };
  const handleInputChange = e => {
    setInputValue(e.target.value);
  };
  const handleInputConfirm = (e) => {
    e.preventDefault()
    if (inputValue && value.indexOf(inputValue) === -1 &&value.length<limitOfTags) {
      // onChange([...value, ...inputValue.split(',').map(tag=>tag.trim())]);
      onChange([...value, inputValue]);
    }
    setInputValue("");
  };

  useEffect(() => {
   if(comma===true){
     if (inputValue && value.indexOf(inputValue) === -1 &&value.length<limitOfTags) {
       onChange([...value, inputValue]);
     }

   }else {
     setInputValue("");
   }
  }, [comma]);


  const onDragEnd = (result) => {
    const {active, over} = result;
    if (active.id !== over.id) {
      const oldIndex = value.indexOf(active.id);
      const newIndex = value.indexOf(over.id);
      onChange( arrayMove(value, oldIndex, newIndex));
    }
  };

  return (
    <DndContext sensors={sensors} collisionDetection={closestCenter} onDragEnd={onDragEnd}>
      <div style={{padding: 16}} className={'added-tags'}>
        <SortableContext items={value} strategy={rectSortingStrategy}>
          {value.map((tag, index) => (
            <ForMap
              key={tag}
              tag={tag}
              index={index}
              color={color}
              maxLength={maxLength}
              handleClose={handleClose}
            />
          ))}
        </SortableContext>
      </div>
      <Col {...wrapperCol}>
        <Input
          type="text"
          value={inputValue}
          onChange={handleInputChange}
          onBlur={handleInputConfirm}
          onPressEnter={handleInputConfirm}
          disabled={disabled}
          placeholder={'Press enter to add new tag'}
          size="large"
        />
      </Col>
    </DndContext>
  );
};

export default EditableTagGroup;
