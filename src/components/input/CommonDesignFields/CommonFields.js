import React, { useState } from "react";
import { Col, Row, Typography } from "antd";
import StyledBadge from "../StyledBadge";
import { Field, useField, useForm } from "react-final-form";
import MyTextAreaWithMentionsField from "../MyFields/MyTextAreaWithMentionsField";
import { fieldDisabledText } from "../fieldDisabledText";
import Tags from "../Tags";
import { OnChange } from "react-final-form-listeners";
import TitleField from "./TitleField";
import DescriptionField from "./DescriptionField";

const CommonFields = ({prefix=""}) => {
  const {Title} = Typography;
  const form = useForm();

  const [selectedTitle, setSelectedTitle] = useState(`${prefix}title26`);
  const { input: {value: valueOfSelectedTitle}, } = useField(selectedTitle, {subscription: {value: true}});

  const { input: {value: title26}, } = useField(`${prefix}title26`, {subscription: {value: true}});
  const { input: {value: title40}, } = useField(`${prefix}title40`, {subscription: {value: true}});
  const { input: {value: title50}, } = useField(`${prefix}title50`, {subscription: {value: true}});
  const { input: {value: title60}, } = useField(`${prefix}title60`, {subscription: {value: true}});
  const { input: {value: title100},} = useField(`${prefix}title100`, {subscription: {value: true}});

  const [selectedDescription, setSelectedDescription] = useState(`${prefix}description140`);
  const { input: {value: valueOfSelectedDescription}, } = useField(selectedDescription, {subscription: {value: true}});
  const { input: {value: description140}, } = useField(`${prefix}description140`, {subscription: {value: true}});
  const { input: {value: description200}, } = useField(`${prefix}description200`, {subscription: {value: true}});
  const { input: {value: description250}, } = useField(`${prefix}description250`, {subscription: {value: true}});
  const { input: {value: description2000},} = useField(`${prefix}description2000`, {subscription: {value: true}});

  const tags = useField(`${prefix}tags`, {subscription: {value: true}});

  return (
    <>
      <Title level={5}>
        <Row>
          <Col span={1}>Title</Col>
          <Col span={23}>
            <StyledBadge
              style={{marginLeft: '1rem'}}
              count={26}
              onClick={() => {
                setSelectedTitle(`${prefix}title26`);
                if (!title26) form.change(`${prefix}title26`, valueOfSelectedTitle);
              }}
              selected={selectedTitle === `${prefix}title26`}
              touched={!!title26}
            />
            <StyledBadge
              count={40}
              onClick={() => {
                setSelectedTitle(`${prefix}title40`);
                if (!title40) form.change(`${prefix}title40`, valueOfSelectedTitle);
              }}
              selected={selectedTitle === `${prefix}title40`}
              touched={!!title40}
            />
            <StyledBadge
              count={50}
              onClick={() => {
                setSelectedTitle(`${prefix}title50`);
                if (!title50) form.change(`${prefix}title50`, valueOfSelectedTitle);
              }}
              selected={selectedTitle === `${prefix}title50`}
              touched={!!title50}
            />
            <StyledBadge
              count={60}
              onClick={() => {
                setSelectedTitle(`${prefix}title60`);
                if (!title60) form.change(`${prefix}title60`, valueOfSelectedTitle);
              }}
              selected={selectedTitle === `${prefix}title60`}
              touched={!!title60}
            />
            <StyledBadge
              count={100}
              overflowCount={999}
              onClick={() => {
                setSelectedTitle(`${prefix}title100`);
                if (!title100) form.change(`${prefix}title100`, valueOfSelectedTitle);
              }}
              selected={selectedTitle === `${prefix}title100`}
              touched={!!title100}
            />
          </Col>
        </Row>
      </Title>
      <TitleField
        selectedTitle={selectedTitle}
        tags={tags}
        name={`${prefix}title26`}
      />
      <TitleField
        selectedTitle={selectedTitle}
        tags={tags}
        name={`${prefix}title40`}
      />
      <TitleField
        selectedTitle={selectedTitle}
        tags={tags}
        name={`${prefix}title50`}
      />
      <TitleField
        selectedTitle={selectedTitle}
        tags={tags}
        name={`${prefix}title60`}
      />
      <TitleField
        selectedTitle={selectedTitle}
        tags={tags}
        name={`${prefix}title100`}
      />
      <Title level={5}>
        <Row align="middle" gutter={[0, 24]}>
          <Col span={2}>Description</Col>
          <Col span={21}>
            <StyledBadge
              count={140}
              overflowCount={999}
              onClick={() => {
                setSelectedDescription(`${prefix}description140`);
                if (!description140) form.change(`${prefix}description140`, valueOfSelectedDescription);
              }}
              selected={selectedDescription === `${prefix}description140`}
              touched={!!description140}
            />
            <StyledBadge
              count={200}
              onClick={() => {
                setSelectedDescription(`${prefix}description200`);
                if (!description200) form.change(`${prefix}description200`, valueOfSelectedDescription);
              }}
              selected={selectedDescription === `${prefix}description200`}
              touched={!!description200}
              overflowCount={999}
            />
            <StyledBadge
              count={250}
              onClick={() => {
                setSelectedDescription(`${prefix}description250`);
                if (!description250) form.change(`${prefix}description250`, valueOfSelectedDescription);
              }}
              selected={selectedDescription === `${prefix}description250`}
              touched={!!description250}
              overflowCount={999}
            />
            <StyledBadge
              count={2000}
              onClick={() => {
                setSelectedDescription(`${prefix}description2000`);
                if (!description2000) form.change(`${prefix}description2000`, valueOfSelectedDescription);
              }}
              selected={selectedDescription === `${prefix}description2000`}
              touched={!!description2000}
              overflowCount={2001}
            />
          </Col>
        </Row>
      </Title>
      <DescriptionField
        selectedDescription={selectedDescription}
        tags={tags}
        name={`${prefix}description140`}
      />
      <DescriptionField
        selectedDescription={selectedDescription}
        tags={tags}
        name={`${prefix}description200`}
      />
      <DescriptionField
        selectedDescription={selectedDescription}
        tags={tags}
        name={`${prefix}description250`}
      />
      <DescriptionField
        selectedDescription={selectedDescription}
        tags={tags}
        name={`${prefix}description2000`}
      />
      <div>
        <Title level={5}>
          <Row align="middle" gutter={[16, 16]}>
            <Col xs={20} sm={20} md={3} lg={2} xl={2} xxl={2} flex={'auto'}>
              Tags
            </Col>
          </Row>
        </Title>
        <Field name={'tagsdisabled'} subscription={{value: true}}>
          {({input, meta}) => (
            <>
              <Field
                name={`${prefix}tags`}
                // component={Tags}
                disabled={input.value}
                subscription={{value: true}}
                allowNull={true}
              >
                {props => (
                  <>
                    <Tags disabled={input.value} input={props.input} wrapperCol={{ span: 9 }} />
                  </>
                )}
              </Field>
              <OnChange name={`${prefix}tags`}>
                {value => {
                  form.mutators.onChangeTranslatedField(`${prefix}tags`, value);
                }}
              </OnChange>
              {input.value && fieldDisabledText}
            </>
          )}
        </Field>
      </div>
      <br />
      <br />
      <div>
        <Title level={5}>Bulletpoints</Title>
        <Field name={'bulletpointsdisabled'} subscription={{value: true}}>
          {({input, meta}) => (
            <>
              <Field
                name={`${prefix}bulletpoints`}
                component={MyTextAreaWithMentionsField}
                atData={tags.input.value}
                disabled={input.value}
                maxLength={255}
                subscription={{value: true}}
                allowNull={true}
                autoSize
                showCount
                rows={1}
                size="large"
                placeholder={'Bulletpoint goes here'}
                wrapperCol={{ span: 15 }}
              />
              <OnChange name={`${prefix}bulletpoints`}>
                {value => {
                  form.mutators.onChangeTranslatedField(`${prefix}bulletpoints`, value);
                }}
              </OnChange>
              {input.value && fieldDisabledText}
            </>
          )}
        </Field>
      </div>
      <div style={{marginBottom: '2.9rem'}} />
      <div>
        <Field name={'bulletpoints2disabled'} subscription={{value: true}}>
          {({input, meta}) => (
            <>
              <Field
                name={`${prefix}bulletpoints2`}
                component={MyTextAreaWithMentionsField}
                atData={tags.input.value}
                disabled={input.value}
                maxLength={255}
                subscription={{value: true}}
                allowNull={true}
                autoSize
                showCount
                rows={1}
                size="large"
                placeholder={'Bulletpoint goes here'}
                wrapperCol={{ span: 15 }}
              />
              <OnChange name={`${prefix}bulletpoints2`}>
                {value => {
                  form.mutators.onChangeTranslatedField(`${prefix}bulletpoints2`, value);
                }}
              </OnChange>
              {input.value && fieldDisabledText}
            </>
          )}
        </Field>
      </div>
    </>
  );
};

export default CommonFields;
