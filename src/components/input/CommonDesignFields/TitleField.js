import React from "react";
import { Field,useForm } from "react-final-form";
import MyTextAreaWithMentionsField from "../MyFields/MyTextAreaWithMentionsField";
import { OnChange } from "react-final-form-listeners";

const name = `title26`;
const TitleField = ({selectedTitle,tags,name}) => {
  const form = useForm();
  return (
    <div style={{display: selectedTitle === name ? '' : 'none'}}>
      <Field
        name={name}
        subscription={{ value: true }}
        allowNull={true}
        component={MyTextAreaWithMentionsField}
        atData={tags.input.value}
        wrapperCol={{ span: 12 }}
        autoSize
        maxLength={parseInt(name.match( /\d+/g)[0])}
        showCount
        rows={1}
        size="large"
      />
      <OnChange name={name}>
        {value => {
          form.mutators.onChangeTranslatedField(name, value);
        }}
      </OnChange>
    </div>
  );
};

export default TitleField;
