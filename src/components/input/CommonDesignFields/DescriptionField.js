import React from "react";
import { Field,useForm } from "react-final-form";
import MyTextAreaWithMentionsField from "../MyFields/MyTextAreaWithMentionsField";
import { OnChange } from "react-final-form-listeners";

const name = `description2000`;
const DescriptionField = ({selectedDescription,tags,name}) => {
  const form = useForm();
  return (
    <div style={{display: selectedDescription === name ? '' : 'none'}}>
      <Field
        name={name}
        component={MyTextAreaWithMentionsField}
        atData={tags.input.value}
        maxLength={parseInt(name.match( /\d+/g)[0])}
        subscription={{value: true}}
        allowNull={true}
        showCount
        rows={3}
        size="large"
        wrapperCol={{ span: 15 }}
      />
      <OnChange name={name}>
        {value => {
          form.mutators.onChangeTranslatedField(name, value);
        }}
      </OnChange>
    </div>
  );
};

export default DescriptionField;
