import { Typography } from "antd";
import React from "react";

export const fieldDisabledText = < Typography.Text type="danger">field is disabled, because designs don't have the same value</Typography.Text>;
