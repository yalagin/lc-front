import React from "react";
import styled from "styled-components";
import { SettingOutlined } from "@ant-design/icons";


const MyCustomSettingGearOutlined = styled(SettingOutlined)`
  //margin: 5px;
  //cursor: pointer;
  color: #7987a1;
  font-size: 1.4rem;
  :hover{
    color: #7f67ff;
  }
`;

export default MyCustomSettingGearOutlined;
