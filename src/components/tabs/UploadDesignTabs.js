import React, {useState} from 'react';
import { Badge, message, Tabs, Tooltip } from "antd";
import { designs, uploadStatus, urlsForDesigns } from "../../utils/constants/constants";
import _ from 'lodash';
import './DesignTabs.less';
import {useQueries} from 'react-query';
import {designRoutes, UploadStatusesEnum} from '../../utils/constants/upload';
import {fetch2} from '../../utils/dataAccess';
import qs from 'qs';
import useProject from '../../utils/hooks/useProject';
import useProfile from "../../utils/hooks/useProfile";
import SubDesignTable from "../design/table/subdesignUpload/SubDesignTable";

const UploadDesignTabs = ({setCurrentTab,isShowCounter,tableProps}) => {
  const {TabPane} = Tabs;
  const [designCounter, setDesignCounter] = useState({});
  const {project} = useProject();
  const { profile } = useProfile();

  useQueries(
    Object.entries(designRoutes).map(([key, value]) => ({
      queryKey: ['uploadCounter', value],
      queryFn: () =>
        fetch2(
          value +
            '?' +
            qs.stringify(
              {
                pageSize: 0,
                isDeleted: false,
                'design.isUpload': true,
                'design.isEdit': false,
                'design.project': project ? project['@id'] : null,
                uploadOption: 'Awaiting Upload',
                [uploadStatus]: [
                  UploadStatusesEnum['queue'],
                  UploadStatusesEnum['failed'],
                  // UploadStatusesEnum['idle'],
                ],
              },
              {
                arrayFormat: 'brackets',
                skipNulls: true,
              }
            )
        ),
      onError: () => {
        message.error(`Request failed`);
      },
      onSuccess: data => {
        setDesignCounter(prevState => ({...prevState, [key]: data['hydra:totalItems']}));
      },
    }))
  );

  return (
    <div id={'designTabs'}>
      <Tabs defaultActiveKey="1" onChange={setCurrentTab} centered={true}>
        {/*<TabPane tab={general} key={general} />*/}
        {profile?.editableDesignsForWorkspace &&
        Object.keys(profile?.editableDesignsForWorkspace)
          .map(design =>profile?.editableDesignsForWorkspace[design]&& (
          <TabPane
            tab={
              <>
                {_.startCase(design.slice(6))}
                { isShowCounter && !!designCounter[design] &&
                  <Tooltip
                    placement="topRight"
                    title={
                      designCounter[design] +
                      ' designs waiting for upload at ' +
                      _.startCase(design.slice(6))
                    }
                  >
                    <Badge color={'#7f67ff'} offset={[10, -10]} count={designCounter[design]}>
                      &nbsp;&nbsp;
                    </Badge>
                  </Tooltip>
                }
              </>
            }
            key={design}><SubDesignTable design={design} url={urlsForDesigns[design]} {...tableProps} /></TabPane>
        ))}
      </Tabs>
    </div>
  );
};

export default UploadDesignTabs;
