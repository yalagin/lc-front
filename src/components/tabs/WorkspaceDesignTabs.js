import React from "react";
import { Tabs } from "antd";
import { designs, general } from "../../utils/constants/constants";
import _ from "lodash";
import { useField } from "react-final-form";
import "./DesignTabs.less"

const WorkspaceDesignTabs = ({ setCurrentTab,currentTab }) => {

  const { input: { value: valueOfEditSettings } } = useField("Edit", { subscription: { value: true } });
  // const { input: {value: selectedLanguageValue}} = useField('selectedLanguage', {subscription: {value: true}});

  const { TabPane } = Tabs;
  return (
    <div id={'designTabs'}  className={'second-step-WorkspaceWalkthroughIntroduction first-step-WorkspaceWalkthroughSettings'}>
    <Tabs activeKey={currentTab} onChange={setCurrentTab} centered={true}>
      <TabPane tab={general} key={general} />
      {!valueOfEditSettings && designs.map(design => <TabPane tab={_.startCase(design.slice(6))} key={design} />)}
      {valueOfEditSettings && Object.keys(valueOfEditSettings).map(
        (key) =>
          valueOfEditSettings[key] && <TabPane tab={_.startCase(key.slice(6))} key={key} />
      )}
    </Tabs>
    </div>
  );
};

export default WorkspaceDesignTabs;
