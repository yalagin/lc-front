import React from 'react';
import { Button, notification, Typography } from "antd";
import { Field } from 'react-final-form';
import MyPassword from '../../input/MyFields/MyPassword';
import PasswordStrengthBar from 'react-password-strength-bar';
import { useMutation } from 'react-query';
import { fetch2 } from '../../../utils/dataAccess';
import { FORM_ERROR } from "final-form";


const { Text } = Typography;

const ResetPasswordFields = ({ submitError, handleSubmit }) => {
  const mutationSendResetPasswordLinkToEmail = useMutation(
    async () =>
      await fetch2('/users/send-token-to-email', {
        method: 'POST',
        body: JSON.stringify({}),
      }),
    {
      onSuccess: (data, variables, context) => {
        notification.info({ message: `Link sent!`, description:'Please check your email' });
      },
      onError: (data, variables, context) => {
        notification.warning({ message: `Link not sent!`, description:data?.errors[FORM_ERROR]});
      },
    }
  );

  const wrapperCol = { span:24, lg:{span: 8} };
  const labelCol = { span: 24 };
  return (
    <form onSubmit={handleSubmit}>
      <Field
        size={'large'}
        labelCol={labelCol}
        wrapperCol={wrapperCol}
        label={'Current Password'}
        name="oldPassword"
        component={MyPassword}
        hasFeedback
        allowNull={true}
        parse={x => x}
      />
      <Field
        size={'large'}
        labelCol={labelCol}
        wrapperCol={wrapperCol}
        label={'New Password'}
        name="newPassword"
        component={MyPassword}
        hasFeedback
        allowNull={true}
      >
        {props => (
          <MyPassword {...props}>
            <PasswordStrengthBar password={props.input.value} style={{ marginBottom: '-1rem' }} />
          </MyPassword>
        )}
      </Field>
      <Field
        size={'large'}
        labelCol={labelCol}
        wrapperCol={wrapperCol}
        label={'Confirm New Password'}
        name="newRetypedPassword"
        placeholder="New Password"
        hasFeedback
        component={MyPassword}
        allowNull={true}
        parse={x => x}
      />
      {submitError && <Text type="danger">submitError</Text>}
      <Button onClick={handleSubmit} size={'large'} type={'primary'}>
        Save Changes
      </Button>
      <Button
        onClick={mutationSendResetPasswordLinkToEmail.mutate}
        size={'large'}
        style={{ marginLeft: '1rem' }}
      >
        Send reset password link to email
      </Button>
    </form>
  );
};

export default ResetPasswordFields;
