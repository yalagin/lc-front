import React from 'react';
import {Form} from 'react-final-form';
import {useMutation} from 'react-query';
import { message, notification } from "antd";
import ResetPasswordFields from './ResetPasswordFields';
import useProfile from "../../../utils/hooks/useProfile";
import { fetch2 } from "../../../utils/dataAccess";

const ResetPasswordForm = () => {
  const {profile} = useProfile();

  const mutationSaveProfile = useMutation(
    async data =>
      await fetch2(profile['user']+"/reset-password", {
        method: 'POST',
        body: JSON.stringify(data),
      }),
    {
      onSuccess: (data, variables, context) => {
        notification.info({message: `Saved!`});
      },
    }
  );

  const onSubmit = async (values,form) => {
    console.log(values);
    try {
      await mutationSaveProfile.mutateAsync(values);
    } catch (error) {
      return error.errors;
    }
    form.restart();
  };

  return (
    <Form
      onSubmit={onSubmit}
      // validate={validate}
      component={ResetPasswordFields}
    />
  );
};

export default ResetPasswordForm;
