import React from 'react';
import { Button } from 'antd';
import { Field } from 'react-final-form';
import MyTextField from '../../input/MyFields/MyTextField';
import HandleEnterKey from '../../input/HandleKey/HandleEnterKey';

const ForgotPasswordFields = ({ submitError, handleSubmit }) => {
  const wrapperCol = { span: 8 };
  const labelCol = { span: 24 };
  return (
    <form onSubmit={handleSubmit}>
      <Field
        size={'large'}
        labelCol={labelCol}
        // wrapperCol={wrapperCol}
        label={'Email'}
        name="email"
        component={MyTextField}
        hasFeedback
        allowNull={true}
        parse={x => x}
        rules={['email']}
      />
      <Button onClick={handleSubmit} type={'primary'}>
        Send reset password link to email
      </Button>
      <HandleEnterKey handleOnEnterKey={handleSubmit} />
    </form>
  );
};


export default ForgotPasswordFields;
