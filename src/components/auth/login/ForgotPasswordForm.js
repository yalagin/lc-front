import React from 'react';
import { useMutation } from 'react-query';
import { fetch2 } from '../../../utils/dataAccess';
import { notification } from 'antd';
import { Form } from 'react-final-form';
import ForgotPasswordFields from './ForgotPasswordFields';
import { FORM_ERROR } from "final-form";

const ForgotPasswordForm = () => {
  const mutationSendResetPasswordLinkToEmail = useMutation(
    async (data) =>
      await fetch2('/users/send-token-to-email', {
        method: 'POST',
        body: JSON.stringify(data),
      }),
    {
      onSuccess: (data, variables, context) => {
        notification.info({ message: `Link sent!`, description: 'Please check your email' });
      },
      onError: (data, variables, context) => {
        notification.warning({ message: `Link not sent!`, description: data?.errors[FORM_ERROR] });
      },
    }
  );

  const onSubmit = async (values, form) => {
    console.log(values);
    try {
      await mutationSendResetPasswordLinkToEmail.mutateAsync(values);
    } catch (error) {
      return error.errors;
    }
    form.restart();
  };

  return (
    <Form
      onSubmit={onSubmit}
      // validate={validate}
      component={ForgotPasswordFields}
    />
  );
};

export default ForgotPasswordForm;
