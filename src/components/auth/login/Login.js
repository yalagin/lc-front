import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link, Navigate } from "react-router-dom";
import Form from './LoginForm';
import { login, reset } from '../../../actions/user/login';
import { list } from '../../../actions/person/list';
import {Alert, Button, Spin} from "antd";

class Login extends Component {

    render() {
        if (localStorage.getItem('token'))
            return (
                <Navigate
                    to={`/dashboard`}
                />
            );
        // if (this.props.loggedIn) {
        //     this.props.list("https://localhost/people?page=1&user="+this.props.loggedIn.token);
        //     return (
        //         <Redirect
        //             to={`edit/${encodeURIComponent(this.props.created['@id'])}`}
        //         />
        //     );
        // }

        return (
            <div>


                {this.props.loading && (
                    <Spin tip="Loading..."/>
                )}
                {this.props.error && (
                    <Alert message={this.props.error} type="error" showIcon />
                )}
                <br/>
                <Form onSubmit={this.props.login} values={this.props.item} onClickForgetPasswordButton={this.props.onClickForgetPasswordButton} />
            </div>
        );
    }
}

const mapStateToProps = state => {
    const { loggedIn, error, loading } = state.user.login;
    return { loggedIn, error, loading };
};

const mapDispatchToProps = dispatch => ({
    login: values => dispatch(login(values)),
    list: values => dispatch(list(values)),
    reset: () => dispatch(reset())
});

export default connect(mapStateToProps, mapDispatchToProps)(Login);
