import React, { Component } from 'react';
import { Field, reduxForm } from 'redux-form';
import { TextField } from 'redux-form-antd';
import { Button, Typography } from 'antd';
import HandleEnterKey from '../../input/HandleKey/HandleEnterKey';

const { Link } = Typography;

const required = value => (value || typeof value === 'number' ? undefined : 'Required');
const email = value =>
  value && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value)
    ? 'Invalid email address'
    : undefined;

const Form = (props)=> {
    return (
      <form className={'form-login'} onSubmit={props.handleSubmit}>
        <Field
          component={TextField}
          name="email"
          type="email"
          placeholder="email"
          required={true}
          validate={[required, email]}
        />
        <Field
          component={TextField}
          name="password"
          type="password"
          placeholder="password"
          required={true}
          validate={[required]}
        />

        <Button type="primary" onClick={props.handleSubmit}>
          Submit
        </Button>
        <Button style={{ marginLeft: "1rem" }} onClick={props.onClickForgetPasswordButton}>
          Forgot password?
        </Button>
        <HandleEnterKey handleOnEnterKey={props.handleSubmit} />
      </form>
    );
}

export default reduxForm({
  form: 'user-login',
  enableReinitialize: true,
  keepDirtyOnReinitialize: true
})(Form);
