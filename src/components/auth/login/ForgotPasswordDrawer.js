import { Drawer, Typography } from 'antd';
import React, { useEffect, useState } from 'react';
import Login from './Login';
import { useLocation } from 'react-router-dom';
import ForgotPasswordForm from "./ForgotPasswordForm";

const ForgotPasswordDrawer = () => {
  const { state } = useLocation();
  const [visible, setVisible] = useState(false);
  useEffect(() => {
    if (state && state.forgotButton) {
      setVisible(true);
    }
  }, [state]);

  return (
    <>
      <a onClick={() => setVisible(true)}>ForgetPassword</a>
      <Drawer
        title="Forgot Password?"
        width={600}
        onClose={() => setVisible(false)}
        visible={visible}
        bodyStyle={{}}
        placement={'right'}
        key={'top'}

      >

        {visible && <ForgotPasswordForm/>}
        {/*<div>*/}
        {/*  <Link*/}
        {/*    onClick={() => {*/}
        {/*      history.push({*/}
        {/*        pathname: '/',*/}
        {/*        state: { login: true },*/}
        {/*      });*/}
        {/*      setVisible(false);*/}
        {/*    }}*/}
        {/*  >*/}
        {/*    Login*/}
        {/*  </Link>*/}
        {/*</div>*/}
      </Drawer>
    </>
  );

};

export default ForgotPasswordDrawer;
