import { Drawer, Typography } from 'antd';
import React, { useEffect, useState } from 'react';
import Login from './Login';
import { useNavigate, useLocation } from 'react-router-dom';

const { Link } = Typography;


const LoginDrawerForm = () => {
  const { state } = useLocation();
  const navigate = useNavigate();
  const [visible, setVisible] = useState(false);
  useEffect(() => {
    if (state && state.login) {
      setVisible(true);
    }
  }, [state]);


  const onClickForgetPasswordButton = () => {
    navigate("/",{
      state: { forgotButton: true },
    });
    setVisible(false);
  }

  return (
    <>
      <a onClick={() => setVisible(true)}>Login</a>
      <Drawer
        title="Login"
        width={600}
        onClose={() => setVisible(false)}
        visible={visible}
        bodyStyle={{}}
        placement={'right'}
        key={'top'}
      >
        <div>
          <Link
            onClick={() => {
              navigate("/",{
                state: { create: true },
              });
              setVisible(false);
            }}
          >
            Dont have an account?
          </Link>
        </div>
        {visible && <Login onClickForgetPasswordButton={onClickForgetPasswordButton} />}
      </Drawer>
    </>
  );

};

export default LoginDrawerForm;
