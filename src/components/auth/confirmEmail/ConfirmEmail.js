import React, { useEffect, useState } from 'react';
import { useNavigate, useLocation } from "react-router-dom";
import qs from 'qs';
import { Button, notification, Result } from 'antd';
import { useMutation } from 'react-query';
import { fetch2 } from '../../../utils/dataAccess';
import { FORM_ERROR } from 'final-form';

const ConfirmEmail = () => {
  const [isSuccess, setIsSuccess] = useState(false);
  const [isError, setIsError] = useState(false);

  const location = useLocation();
  const navigate = useNavigate();

  const mutationValidateEmail = useMutation(
    async confirmationToken => {
      // return fetch2(`/confirm-user/confirmationToken`, {
      await fetch2(`/confirm-user/${confirmationToken}`, {
        method: 'POST',
        body: JSON.stringify({ confirmationToken: confirmationToken }),
      });
    },
    {
      onSuccess: (data, variables, context) => {
        notification.info({ message: `Email validated!` });
        setIsSuccess(true);
      },
      onError: (data, variables, context) => {
        setIsError(data.errors[FORM_ERROR]);
      },
    }
  );

  useEffect(() => {
    const params = qs.parse(location?.search, { ignoreQueryPrefix: true });

    if (params?.confirmationToken) {
      mutationValidateEmail.mutate(params.confirmationToken);
    }
  }, [location?.search]);

  return (
    <>
      {isSuccess && (
        <Result
          status="success"
          title="Successfully Validated Email!"
          subTitle="Now you can login and upload your designs"
          extra={[
            <Button
              type="primary"
              key="console"
              onClick={() => {
                setIsSuccess(false);
                navigate('/',{
                  state: { login: true },
                });
              }}
            >
              Login
            </Button>,
            <Button onClick={() => setIsSuccess(false)} key="buy">
              Close
            </Button>,
          ]}
        />
      )}
      {isError && (
        <Result
          status="403"
          title={isError}
          extra={[
            <Button type="primary" onClick={() => setIsError(false)} key="buy">
              Close
            </Button>,
          ]}
        />
      )}
    </>
  );
};

export default ConfirmEmail;
