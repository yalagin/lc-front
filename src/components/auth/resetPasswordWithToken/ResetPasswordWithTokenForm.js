import React from 'react';
import {Form} from 'react-final-form';
import {useMutation} from 'react-query';
import {notification} from 'antd';
import ResetPasswordWithTokenFields from './ResetPasswordWithTokenFields';
import { fetch2, MIME_TYPE } from "../../../utils/dataAccess";
import {ENTRYPOINT} from '../../../config/entrypoint';

const ResetPasswordWithTokenForm = ({props: {token, id}, handleCancel}) => {
  const mutationSaveProfile = useMutation(
    async data => {
      localStorage.clear();
      await fetch2(`/users/${id}/reset-password-with-token`, {
        method: 'POST',
        body: JSON.stringify({...data, token}),
      });
    },
    {
      onSuccess: (data, variables, context) => {
        notification.info({message: `Password saved!`});
        handleCancel();
      },
    }
  );

  const onSubmit = async (values, form) => {
    console.log(values);
    try {
      await mutationSaveProfile.mutateAsync(values);
    } catch (error) {
      console.log(error.errors);
      return error.errors;
    }
    // form.restart();
    handleCancel();
  };

  return (
    <Form
      onSubmit={onSubmit}
      // validate={validate}
      component={ResetPasswordWithTokenFields}
    />
  );
};

export default ResetPasswordWithTokenForm;
