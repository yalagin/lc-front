import React, { useEffect, useState } from 'react';
import { Modal } from 'antd';
import qs from 'qs';
import { useLocation } from 'react-router-dom';
import ResetPasswordWithTokenForm from './ResetPasswordWithTokenForm';

const ResetPasswordModal = () => {
  const [isModalVisible, setIsModalVisible] = useState(false);
  const location = useLocation();
  useEffect(() => {
    const params = qs.parse(location?.search, { ignoreQueryPrefix: true });

    if (params?.token && params?.id) {
      setIsModalVisible(params);
    }
  }, [location?.search]);

  const showModal = () => {
    setIsModalVisible(true);
  };

  const handleOk = () => {
    // setIsModalVisible(false);
    document.getElementById('password-reset-form')
      .dispatchEvent(new Event('submit', { cancelable: true, bubbles:true }))
  };

  const handleCancel = () => {
    setIsModalVisible(false);
  };

  return (
    <>
      <Modal
        title="Reset Password"
        visible={isModalVisible}
        onOk={handleOk}
        onCancel={handleCancel}
      >
        {isModalVisible && (
          <ResetPasswordWithTokenForm props={isModalVisible} handleCancel={handleCancel} />
        )}
      </Modal>
    </>
  );
};

export default ResetPasswordModal;
