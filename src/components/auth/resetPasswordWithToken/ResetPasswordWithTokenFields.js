import React from 'react';
import { Field, useField } from "react-final-form";
import MyPassword from '../../input/MyFields/MyPassword';
import PasswordStrengthBar from 'react-password-strength-bar';
import HandleEnterKey from "../../input/HandleKey/HandleEnterKey";
import { Typography } from "antd";
const {Text} = Typography;

const ResetPasswordWithTokenFields = ({ submitError, handleSubmit }) => {
  const wrapperCol = { span: 8 };
  const labelCol = { span: 24 };
  const token = useField('token')

  return (
    <form id={'password-reset-form'} onSubmit={handleSubmit}>
      <Field
        size={'large'}
        labelCol={labelCol}
        // wrapperCol={wrapperCol}
        label={'New Password'}
        name="newPassword"
        component={MyPassword}
        hasFeedback
        allowNull={true}
      >
        {props => (
          <MyPassword {...props}>
            <PasswordStrengthBar password={props.input.value} style={{marginBottom: '-1rem'}} />
          </MyPassword>
        )}
      </Field>
      <Field
        size={'large'}
        labelCol={labelCol}
        // wrapperCol={wrapperCol}
        label={'Confirm New Password'}
        name="newRetypedPassword"
        placeholder="New Password"
        hasFeedback
        component={MyPassword}
        allowNull={true}
        parse={x => x}
      />
      {token.meta.submitError && <Text type="danger">{token.meta.submitError}</Text>}
      {submitError && <Text type="danger">submitError</Text>}
      <HandleEnterKey handleOnEnterKey={handleSubmit} />
    </form>
  );
};

export default ResetPasswordWithTokenFields;
