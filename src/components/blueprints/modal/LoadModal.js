import React, {useState} from 'react';
import { Button, List, message, Modal, Spin, Typography } from "antd";
import BigButton from '../../input/button/BigButton';
import { useMutation } from "react-query";
import { fetch2 } from "../../../utils/dataAccess";
import _ from 'lodash';

const LoadModal = ({data,form,fetchUserBlueprints}) => {
  const [isModalVisible, setIsModalVisible] = useState(false);
  const {Text} = Typography;
  const showModal = () => {
    setIsModalVisible(true);
  };

  const DeleteBlueprint = useMutation(blueprint => fetch2(blueprint["@id"], {
    method: 'DELETE',
    body: JSON.stringify(blueprint),
  }), {
    onSuccess: (data, variables, context) => {
      message.info(`Deleted!`);
      fetchUserBlueprints.refetch();
    }
  })

  const editBlueprint = useMutation(data => {
    return fetch2(data.item['@id'], {
      method: 'PUT',
      headers: new Headers({'Content-Type': 'application/ld+json'}),
      body: JSON.stringify({...data.item, name:data.name}),
    });}, {
    onSuccess: (data, variables, context) => {
      message.info(`Saved!`);
      fetchUserBlueprints.refetch();
    }
  })


  // const handleOk = () => {
  //   setIsModalVisible(false);
  //   handleSubmit()
  // };

  const handleCancel = () => {
    setIsModalVisible(false);
  };

  const load = (item) =>{
    item.selectedLanguage='en'
    form.change('',item);
    handleCancel();
    form.mutators.selectedLanguageOnChange(form);
  }

  return (
    <>
      <BigButton disabled={_.isEmpty(data)} size={'large'} onClick={showModal}>
        Load
      </BigButton>
      <Modal
        centered
        title="Load blueprint"
        visible={isModalVisible}
        onCancel={handleCancel}
        footer={[
          <Button key="back" onClick={handleCancel}>
            Cancel
          </Button>,
        ]}
      >
        <Spin spinning={editBlueprint.isLoading}>
        <List
          itemLayout="horizontal"
          dataSource={data}
          renderItem={item => (
            <List.Item   actions={[<a onClick={()=>DeleteBlueprint.mutate(item)} key="list-loadmore-edit">Delete</a>,/* <a key="list-loadmore-more">More</a>,*/ <a onClick={()=>load(item)} key="list-loadmore-more">Load</a>]} >
              <List.Item.Meta
                title={<Text editable={{ onChange: (name)=> {

                    editBlueprint.mutate({ name, item });
                  } }}>{item.name}</Text>}
              />
            </List.Item>
          )}
        />
        </Spin>
      </Modal>
    </>
  );
};

export default LoadModal;
