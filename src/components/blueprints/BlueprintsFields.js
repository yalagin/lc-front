import React, { useState } from "react";
import { Col, Row, Spin, Typography } from "antd";
import StyledBadge from "../input/StyledBadge";
import { Field, FormSpy, useField } from "react-final-form";
import { fieldDisabledText } from "../input/fieldDisabledText";
import Tags from "../input/Tags";
import MyTextAreaField from "../input/MyFields/MyTextAreaField";
import { HYDRAMEMBER } from "../../utils/constants/constants";
import SaveNewModal from './modal/SaveNewModal';
import LoadModal from './modal/LoadModal';
import SaveExistedModal from "./modal/SaveExistedModal";
import MyTextAreaWithMentionsField from "../input/MyFields/MyTextAreaWithMentionsField";
import CommonFields from "../input/CommonDesignFields/CommonFields";
import LanguageAndTabsSwitcher from "../design/LanguageAndTabsSwitcher/LanguageAndTabsSwitcher";
import LanguageSelector from "../design/LanguageAndTabsSwitcher/LanguageSelector";

const BlueprintsFields = ({ handleSubmit, pristine, submitting, form, fetchUserBlueprints }) => {
  const id = useField('id', { subscription: { value: true } })
  const {
    input: { value: selectedLanguageValue, onChange: selectedLanguageOnChange },
  } = useField('selectedLanguage', { subscription: { value: true }, initialValue: 'en' });

  return (
    <div>
      {process.env.REACT_APP_SPY && (
        //just for dev purposes we need to see whats inside form
        <FormSpy subscription={{values: true}}>
          {({values}) => {
            return <>{console.log(values)}</>;
          }}
        </FormSpy>
      )}
      {/*<br />*/}
      <LanguageSelector
        right
        style={{float:'right'}}
        value={selectedLanguageValue}
        onChange={e => {
          selectedLanguageOnChange(e);
          form.mutators.selectedLanguageOnChange(form);
        }}
      />
      <CommonFields/>
      {fetchUserBlueprints.data ? (
        <Row gutter={[16, 16]}>
          <Col xs={24} sm={24} md={12} lg={12} xl={12} xxl={8}>
            {!id.input.value && (
              <SaveNewModal
                data={fetchUserBlueprints.data[HYDRAMEMBER]}
                handleSubmit={handleSubmit}
                fetchUserBlueprints={fetchUserBlueprints}
                form={form}
              />
            )}
            {id.input.value && (
              <SaveExistedModal
                data={fetchUserBlueprints.data[HYDRAMEMBER]}
                handleSubmit={handleSubmit}
                fetchUserBlueprints={fetchUserBlueprints}
                form={form}
              />
            )}
          </Col>
          <Col xs={24} sm={24} md={12} lg={12} xl={12} xxl={8}>
            <LoadModal
              data={fetchUserBlueprints.data[HYDRAMEMBER]}
              form={form}
              fetchUserBlueprints={fetchUserBlueprints}
            />
          </Col>
        </Row>
      ) : (
        <Spin />
      )}
    </div>
  );
}

export default BlueprintsFields;
