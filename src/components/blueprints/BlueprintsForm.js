import React from 'react';
import {Form} from 'react-final-form';
import arrayMutators from 'final-form-arrays';
import BlueprintsFields from './BlueprintsFields';
import {useMutation, useQuery} from 'react-query';
import {fetch2} from '../../utils/dataAccess';
import {message} from 'antd';
import _ from 'lodash';
import {
  translations
} from "../../utils/constants/constants";
import { objectOfDesignsWithTranslatedFields } from "../../utils/constants/translation";

const BlueprintsForm = () => {
  const fetchUserBlueprints = useQuery('blueprints', () =>
    fetch2('/blueprints?pagination=false&owner=' + localStorage.getItem('id'))
  );

  const saveBlueprint = useMutation(
    blueprint =>
      fetch2('/blueprints', {
        method: 'POST',
        body: JSON.stringify(blueprint),
      }),
    {
      onSuccess: (data, variables, context) => {
        message.info(`Saved!`);
        fetchUserBlueprints.refetch();
      },
    }
  );

  const editBlueprint = useMutation(
    blueprint =>
      fetch2(blueprint['@id'], {
        method: 'PUT',
        headers: new Headers({'Content-Type': 'application/ld+json'}),
        body: JSON.stringify(blueprint),
      }),
    {
      onSuccess: (data, variables, context) => {
        message.info(`Saved!`);
        fetchUserBlueprints.refetch();
      },
    }
  );

  function makeEmptyStingNullable(obj) {
    _.forOwn(obj, function(value, key) {

      if (_.isObject(value)) {
        makeEmptyStingNullable(value);
      }
      if (
        !Number.isInteger(value) &&
        !_.isBoolean(value) &&
        _.isEmpty(value) &&
        key !== 'translations'
      ) {
        return _.set(obj, key, null);
      }
    });
  }

  function onSubmit(values, form) {
    makeEmptyStingNullable(values);
    if (values['@id']) {
      editBlueprint.mutate(values);
    } else {
      saveBlueprint.mutate(values);
    }
    form.restart();
  }

  return (
    <Form
      subscription={{submitting: true}}
      onSubmit={onSubmit}
      mutators={{
        ...arrayMutators,
        removeSpecialChars: ([name, availablePrefixes], state, tools) => {
          tools.changeValue(state, name, prev => {
            for (let char of availablePrefixes) {
              prev = prev.split(char).join('');
            }
            return prev;
          });
        },
        selectedLanguageOnChange: ([], state, tools) => {
          const {
            formState: {values},
          } = state;
          const selectedLanguage = _.get(values, 'selectedLanguage');

          //we rewrite already translated fields
          objectOfDesignsWithTranslatedFields["general"].forEach(field => {
            _.set(values, field, _.get(values, [translations, selectedLanguage, field]));
          });
          //we replace already translated fields for subDesigns
          _.forOwn(objectOfDesignsWithTranslatedFields, function(value, key) {
            value.forEach(field => {
              _.set(values, [key, field], _.get(values, [
                key,
                "translations",
                selectedLanguage,
                field
              ]));
            });
          });
        },
        //this is for setting the same hydra:member but with translations
        // fields onChange general or sub designs field
        onChangeTranslatedField: ([field, value], state, {changeValue}) => {
          const {
            formState: {values},
          } = state;

          const selectedLanguage = _.get(values, 'selectedLanguage');
          if(_.isEmpty(values.translations )){
            values.translations={}
          }

          changeValue(state, translations, translations => {
            console.log(translations)
            _.set(translations, _.compact([selectedLanguage, field]), value);
            _.set(
              translations,
              _.compact([ selectedLanguage, 'locale']),
              selectedLanguage
            );
            console.log(translations)
            return translations;
          });
        },
      }}
      component={BlueprintsFields}
      fetchUserBlueprints={fetchUserBlueprints}
    />
  );
};

export default BlueprintsForm;
