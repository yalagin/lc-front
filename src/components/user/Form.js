import React from 'react';
import { Field, reduxForm } from 'redux-form';
import {TextField} from "redux-form-antd";
import {Button} from "antd";
import HandleEnterKey from "../input/HandleKey/HandleEnterKey";


const passwordsMatch = (value, allValues) =>
    value !== allValues.password ? 'Passwords don\'t match' : undefined;

const Form = props => {
    return (
      <form className={'form-signin'} onSubmit={props.handleSubmit}>
        <Field
          component={TextField}
          name="first_name"
          type="text"
          placeholder="Name"
        />
        <Field
          component={TextField}
          name="last_name"
          type="text"
          placeholder="Family name"
        />
        <Field
          component={TextField}
          name="street"
          type="text"
          placeholder="street"
        />
        <Field
            component={TextField}
          name="city"
          type="text"
          placeholder="city"
        />
        <Field
            component={TextField}
          name="zip"
          type="text"
          placeholder="zip"
        />
        <Field
            component={TextField}
          name="country"
          type="text"
          placeholder="country"
        />
        <Field
          component={TextField}
          name="email"
          type="email"
          placeholder="email"
          required={true}
        />
        <Field
          component={TextField}
          name="password"
          type="password"
          placeholder="password"
          required={true}
        />
          <Field
              component={TextField}
              name="r_password"
              type="password"
              placeholder="Repeat password"
              validate={passwordsMatch}
          />
        <Button type="primary" className="btn btn-success" onClick={props.handleSubmit}>
          Submit
        </Button>
          <HandleEnterKey handleOnEnterKey={props.handleSubmit}/>
      </form>
    );
}

export default reduxForm({
  form: 'user-create',
  enableReinitialize: true,
  keepDirtyOnReinitialize: true
})(Form);
