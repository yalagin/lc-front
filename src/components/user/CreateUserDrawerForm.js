import { Drawer, Typography } from 'antd';
import React, { useEffect, useState } from 'react';
import Create from './Create';
import { useNavigate, useLocation } from 'react-router-dom';

const { Link } = Typography;


const CreateUserDrawerForm = () => {
  const { state } = useLocation();
  const navigate = useNavigate();
  const [visible, setVisible] = useState(false);
  useEffect(() => {
    if (state && state.create) {
      setVisible(true);
    }
  }, [state]);
  return (
    <>
      <a onClick={() => setVisible(true)}>New account</a>
      <Drawer
        title="Create a new account"
        width={600}
        onClose={() => setVisible(false)}
        visible={visible}
        bodyStyle={{ paddingBottom: 80 }}
      >
        <div style={{ marginBottom: '1rem ' }}>
          <Link
            onClick={() => {
              navigate('/',{
                state: { login: true },
              });
              setVisible(false);
            }}
          >
            Already has an account?
          </Link>
        </div>
        {visible && <Create />}
      </Drawer>
    </>
  );

}

export default CreateUserDrawerForm;
