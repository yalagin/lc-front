import React, { Component } from 'react';
import { connect } from 'react-redux';
import Form from './Form';
import { create, reset } from '../../actions/user/create';
import {Alert, Button, Spin} from "antd";

class Create extends Component {


  componentWillUnmount() {
    this.props.reset();
  }

  render() {
    // if (this.props.created)
    //   return (
    //     <Redirect
    //       to={`users/show/${encodeURIComponent(this.props.created['@id'])}`}
    //     />
    //   );

    return (
      <div>

        {this.props.loading && (
            <Spin tip="Loading..." style={{margin:"1rem"}}/>
        )}
        {this.props.error && (
            <Alert message={this.props.error} type="error" showIcon closable style={{margin:"1rem"}}/>
        )}

        {this.props.created && (
            <Alert message={"created!"} type="success" showIcon closable style={{margin:"1rem"}}/>
        )}

        <Form onSubmit={this.props.create} values={this.props.item} />
      </div>
    );
  }
}

const mapStateToProps = state => {
  const { created, error, loading } = state.user.create;
  return { created, error, loading };
};

const mapDispatchToProps = dispatch => ({
  create: values => dispatch(create(values)),
  reset: () => dispatch(reset())
});

export default connect(mapStateToProps, mapDispatchToProps)(Create);
