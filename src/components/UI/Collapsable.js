import { Collapse } from 'antd';
import { MinusOutlined, PlusOutlined} from '@ant-design/icons';
import styled from "styled-components";

const { Panel } = Collapse;

const text = `
  A dog is a type of domesticated animal.
  Known for its loyalty and faithfulness,
  it can be found as a welcome guest in many households across the world.
`;


const StyledCollapse = styled(Collapse)`
  &&& .site-collapse-custom-panel {
    margin-bottom: 24px;
    overflow: hidden;
    background: #f7f7f7;
    border: 0px;
    border-radius: 2px;
  }
  &&&   .ant-collapse-item{
    margin-bottom: 1rem;
  }
`

const Collapsable = () => {
  return (
    <StyledCollapse
      bordered={false}
      defaultActiveKey={['1']}
      expandIcon={({ isActive }) => isActive?<MinusOutlined />:<PlusOutlined />}
      className="site-collapse-custom-collapse"
      expandIconPosition={'right'}
    >
      <Panel header="This is panel header 1" key="1" className="site-collapse-custom-panel">
        <p>{text}</p>
      </Panel>
      <Panel header="This is panel header 2" key="2" className="site-collapse-custom-panel">
        <p>{text}</p>
      </Panel>
      <Panel header="This is panel header 3" key="3" className="site-collapse-custom-panel">
        <p>{text}</p>
      </Panel>
    </StyledCollapse>
  );
};

export default Collapsable;
