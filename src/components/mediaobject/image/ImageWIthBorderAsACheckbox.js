import React, { useCallback, useEffect, useMemo, useRef, useState } from "react";
import { Field, useField } from 'react-final-form';
import Icons from '../../../assets/Icons/OldAssets';
import ImageBorder from './ImageBorder';
import { StyledAdultImage, StyledTemplate } from '../../styles/ImageStyles';
import styled from 'styled-components';
import { MinusCircleOutlined, SyncOutlined } from '@ant-design/icons';
import { useMutation } from 'react-query';
import { message, Spin, Typography, Upload } from "antd";
import { fetch2, MIME_TYPE_PATCH } from '../../../utils/dataAccess';
import { HYDRAMEMBER } from '../../../utils/constants/constants';
import { uploadToAwsMutation } from "../../../utils/mutations/uploadToAwsMutation";
import useProfile from "../../../utils/hooks/useProfile";
import FastAverageColor from 'fast-average-color';

export const StyledSyncOutlined = styled(SyncOutlined)`
  font-size: 1.2rem;
  cursor: pointer;
  color: ${props => (props.isDark && props.isImage ? 'white' : 'black')};
  display: ${props => (props.hovered ? '' : 'none')};

  :hover {
    filter: drop-shadow(0px 0px 3px #a38fff);
    color: #a38fff;
    transition: border-color 0.3s;
  }
`;

export const StyledRemoveOutlined = styled(MinusCircleOutlined)`
  font-size: 1.2rem;
  z-index: 2;
  color: ${props => props.isDark && props.isImage ? 'white' : 'black'};
  display: ${props => props.hovered ? '' : 'none'};
  cursor: pointer;
  position: absolute;
  bottom: 7px;
  right: 10px;

  :hover {
    color: #a38fff;
    transition: border-color 0.3s;
    filter: drop-shadow(0px 0px 3px #a38fff);
  }
`;

export const StyledAbsoluteUpload = styled(Upload)`
  position: absolute;
  bottom: -14px;
  left: 10px;
  width: 40px;
  height: 40px;
  z-index: 2;
`;

const ImageWIthBorderAsACheckbox = ({ name,  form, index }) => {
  const ref = useRef(null);
  const designField = useField(name, { subscription: { value: true } });
  const [hovered, setHovered] = useState(false);
  const {profile} = useProfile();
  const [imageBackgroundColorForLightDesign, setImageBackgroundColorForLightDesign] = useState(profile?.imageBackgroundColorLightDesign);
  const [imageBackgroundColorForDarkDesign, setImageBackgroundColorForDarkDesign] = useState(profile?.imageBackgroundColorDarkDesign);
  useEffect(() => {
    setImageBackgroundColorForLightDesign(profile?.imageBackgroundColorLightDesign);
    setImageBackgroundColorForDarkDesign(profile?.imageBackgroundColorDarkDesign);
  }, [profile]);
  const [isDarkImage, setIsDarkImage] = useState(true);

  useEffect( () => {
    const fac = new FastAverageColor();
     setTimeout( ()=>
    {
      fac.getColorAsync(designField.input.value?.image?.contentUrl)
        .then(color => {
          setIsDarkImage(color.isDark);
          console.log(color.isDark);
        })
        .catch(e => {
          console.log(e);
          // setIsDarkImage(true);
        });
    }
  , 3000);
  }, [designField.input.value?.image?.contentUrl]);

  const mutationIsEdit = () => {
    form.mutators.remove(HYDRAMEMBER, index);
  };

  const designMutation = useMutation((data) => fetch2(designField.input.value['@id'], {
    method: 'PATCH',
    headers: { "Content-Type": MIME_TYPE_PATCH },
    body: JSON.stringify({
      image: data['@id']
    }),
  }), {
    onSuccess: (data, variables, context) => {
      // Boom baby!
      message.success('Image added to design')
      designField.input.onChange(data);
    }
  });

  const imageMutation = useMutation(uploadToAwsMutation(), {
    onSuccess: (data, variables, context) => {
      message.success('image uploaded')
      designMutation.mutate(data)
    },
    onError: () => {
      message.error(`An error happened!`)
    },
  });

  const deleteImage = useMutation((image) => fetch2(image['@id'], {
    method: 'DELETE',
  }));


  const uploadImage = async options => {
    // if (design.image) {
    //   deleteImage.mutate(design.image)
    //   imageMutation.mutate(options.file)
    // } else {
    //   imageMutation.mutate(options.file)
    // }
    imageMutation.mutate(options.file)
  };
  const { Text } = Typography;

  return (
    <div
      style={{ position: 'relative' }}
      onMouseOver={() => setHovered(true)}
      onMouseLeave={() => setHovered(false)}
    >
      <Field subscription={{ value: true }} name={`${name}.isAdult`}>
        {props => props.input.value ? <StyledAdultImage src={Icons.UploadLabelIcon} alt="card-mark" /> : ''}
      </Field>
      <Field subscription={{ value: true }} name={`${name}.templateName`}>
        {props => props.input.value && <StyledTemplate>
          <div className={'centred'}><Text className={'box'}
                                           ellipsis={{ tooltip: props.input.value }}>{props.input.value}</Text></div>
        </StyledTemplate>}
      </Field>
      <Field subscription={{ value: true }} name={`${name}.copyFromMaster`} type={'checkbox'}>
        {props => (
          <>
            <input
              style={{ display: 'none' }}
              name={name}
              type="checkbox"
              checked={props.input.value}
              {...props.input}
              ref={ref}
            />
            <Spin tip="Updating..." spinning={imageMutation.isLoading || designMutation.isLoading}>
              <ImageBorder
                imageCarouselBackgroundColor={isDarkImage?imageBackgroundColorForDarkDesign:imageBackgroundColorForLightDesign}
                value={designField.input.value.image}
                checkedValue={props.input.checked}
                checkBoxRef={ref}
                form={form}
                name={name}
              />
            </Spin>
          </>
        )}
      </Field>
      <StyledAbsoluteUpload showUploadList={false} customRequest={uploadImage} accept="image/*">
        <StyledSyncOutlined
          isDark={!isDarkImage}
          isImage={!!designField.input.value.image}
          hovered={hovered}
        />
      </StyledAbsoluteUpload>
      <StyledRemoveOutlined
        isDark={!isDarkImage}
        isImage={!!designField.input.value.image}
        hovered={hovered}
        onClick={mutationIsEdit}
      />
    </div>
  );
};
export default ImageWIthBorderAsACheckbox;
