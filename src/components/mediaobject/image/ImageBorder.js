import React from 'react';
import Image from './Image';
import { StyledBorder } from '../../styles/ImageStyles';
import { isMacintosh } from '../../../utils/utilsFunctions';

const ImageBorder = ({
                       value,
                       checkedValue = false,
                       preview = false,
                       imageCarouselBackgroundColor,
                       name,
                       form,
                     }) => (
  <StyledBorder
    imageCarouselBackgroundColor={imageCarouselBackgroundColor}
    onClick={e => {
      form.mutators.clickOnDesignImage(
        name,
        form,
        isMacintosh() ? e.altKey : e.ctrlKey,
        e.shiftKey
      );
    }}
    checkedValue={checkedValue}
  >
    <div>
      <Image value={value} preview={preview} />
    </div>
  </StyledBorder>
);
export default ImageBorder;
