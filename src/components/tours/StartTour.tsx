import React, { Dispatch, SetStateAction, useEffect, useState } from "react";
import {Button, Modal, Tooltip, Typography} from 'antd';
import UserAvatarWrapperRounded from '../person/popover/UserAvatarWrapperRounded';
import questions from '../../assets/questions.png';
import {
  stepsDashboard, stepsTemplates, stepsTrademarks, WorkspaceWalkthroughCreation,
  WorkspaceWalkthroughDesignUpload,
  WorkspaceWalkthroughIntroduction, WorkspaceWalkthroughListings, WorkspaceWalkthroughLoadListing,
  WorkspaceWalkthroughSettings, WorkspaceWalkthroughTransfer
} from "./steps";
import { useNavigate,useLocation } from "react-router-dom";
import {
  uploadTestImageToS3AndCreateMediaObjectAndDesign
} from "../../actions/mediaobject/create";
import { useDispatch } from "react-redux";
import useProject from "../../utils/hooks/useProject";
import { resetDesignForm, setFakeData } from "../../actions/design/list";
import Joyride, { ACTIONS, BeaconRenderProps, CallBackProps, EVENTS, STATUS, Step, StoreHelpers } from "react-joyride";
import styled from "styled-components";

const Beacon = styled.span`
  //display: none;
`;


function StartTour() {
  const [isTourActivated, setIsTourActivated] = useState<boolean>(false);
  const [isShowModal, setIsShowModal] = useState<boolean>(false);
  const [isRun, setIsRun] = useState<boolean>(false);
  const [steps, setSteps] = useState<Step[]>([]);
  const [currentStep, setCurrentStep] = useState<number>(0);
  const [key, setKey] = useState(0);
  const {project} = useProject();
  const dispatch = useDispatch();
  const location = useLocation();
  let myHelpers: null|StoreHelpers ;

  const [name, setName] = useState<null|string>(null);

  const uploadTestImage = () => {
    dispatch(uploadTestImageToS3AndCreateMediaObjectAndDesign(process.env.REACT_APP_TOUR_IMAGES+"1.png",project));
    dispatch(uploadTestImageToS3AndCreateMediaObjectAndDesign(process.env.REACT_APP_TOUR_IMAGES+"2.png",project));
    dispatch(uploadTestImageToS3AndCreateMediaObjectAndDesign(process.env.REACT_APP_TOUR_IMAGES+"3.png",project));
  };

  const addFakeData = () =>{
    dispatch(setFakeData());
  }

  useEffect(() => {
    if(isRun){
      setIsRun(false);
      setCurrentStep(0);
    }
  }, [location.pathname]);


  useEffect(() => {
    if(isTourActivated){
      setName(null);
      console.log(`Tour is opened at ${location.pathname}.`);
      switch (location.pathname) {
        case '/dashboard/templates':
          setIsTourActivated(false);
          setSteps(stepsTemplates);
          setCurrentStep(0);
          setIsRun(true);
          setKey(prevState => ++prevState);
          break;
        case '/dashboard/workspace':
          setIsTourActivated(false);
          setIsShowModal(true);
          break;
        case '/dashboard/trademarks':
          setIsTourActivated(false);
          setSteps(stepsTrademarks);
          setCurrentStep(0);
          setKey(prevState => ++prevState);
          setIsRun(true);
          break;
        case '/dashboard':
          setIsTourActivated(false)
          setSteps(stepsDashboard);
          setKey(prevState => ++prevState);
          setCurrentStep(0);
          setIsRun(true);
          break;
        default:
          setIsTourActivated(false);
          console.log(`Tour is opened at ${location.pathname}.`);
      }
    }
  }, [isTourActivated,location]);

  function  startWorkspaceTour(steps:Step[]) {
    setIsShowModal(false);
    dispatch(resetDesignForm());
    setIsTourActivated(false);
    setSteps(steps);
    setKey(prevState => ++prevState);
    // await new Promise(resolve => setTimeout(resolve, 500));
    setCurrentStep(0);
    setIsRun(true);
  }

  const onClickEvent = (step:number)=> function() {
    setTimeout(() => {
      setCurrentStep(step);
    }, 300);
  };


  const handleJoyrideCallback = (data:CallBackProps) => {
    const { status, type,index,action } = data;
    const finishedStatuses :string[] = [STATUS.FINISHED, STATUS.SKIPPED];
    const startedEvents :string[] = [EVENTS.TOUR_START];


    if (([EVENTS.STEP_AFTER, EVENTS.TARGET_NOT_FOUND] as string[]).includes(type)) {
      const nextIndex = index + (action === ACTIONS.PREV ? -1 : 1);
      setCurrentStep(nextIndex);
      console.log(nextIndex,name);
      if (name === 'WorkspaceWalkthroughListings' && nextIndex === 11) {
        addFakeData();
      }
    }

    if (finishedStatuses.includes(status)) {
      setIsRun(false);
      setKey(prevState => ++prevState);
      if(name==="WorkspaceWalkthroughSettings") {
        // todo:removeEventListeners
      }
      if(name==="WorkspaceWalkthroughDesignUpload") {
        // todo:removeEventListeners
      }
      if(name==="WorkspaceWalkthroughCreation") {
        document.getElementById('creation-mode-btn')?.removeEventListener(
          'click',
          onClickEvent(1),
          false);

        document.getElementById('title40-general-badge')?.removeEventListener(
          'click',
          onClickEvent(6),
          false
        )
        document.getElementById('title50-general-badge')?.removeEventListener(
          'click',
          onClickEvent(8),
          false
        )

        document
          .getElementsByClassName('first-step-WorkspaceWalkthroughSettings')
          ?.item(0)
          ?.children?.item(0)
          ?.children?.item(0)
          ?.children?.item(0)
          ?.children?.item(0)
          ?.children?.item(1)
          ?.removeEventListener(
            'click',
            onClickEvent(14),
            false
          );

      }

      if(name==="WorkspaceWalkthroughLoadListing") {
        document.getElementById('load-listing-btn')?.removeEventListener(
          'click',
          onClickEvent(1),
          false
        );
      }

    }

    if (startedEvents.includes(type)) {

      if(name==="WorkspaceWalkthroughSettings") {
        document.getElementById('third-step-WorkspaceWalkthroughSettings')?.addEventListener(
          'click',
          function() {
            // setCurrentStep(4);
            setTimeout(() => {
              setCurrentStep(4);
              document.getElementById('forth-step-WorkspaceWalkthroughSettings')?.addEventListener(
                'click',
                ()=> {
                  // setCurrentStep(step);
                  // console.log(step);
                  setTimeout(() => {
                    // setCurrentStep(step);
                    if(
                      !(document.querySelector("input[name='Edit.designSpreadshirtCom']") as HTMLInputElement )?.checked&&
                      !(document.querySelector("input[name='Edit.designSpreadshirtEu']") as HTMLInputElement )?.checked

                    ){
                      setCurrentStep(5);
                    }
                  }, 300);
                },
                false
              );
            }, 300);
          },
          false
        );
        document.getElementById('gear-general-title')?.addEventListener(
          'click',
          function() {
            setTimeout(() => {
              setCurrentStep(8);
              (document.querySelector("input[name='Title.designDisplate']") as HTMLInputElement )?.addEventListener(
                'click',
                ()=> {
                  // setCurrentStep(step);
                  // console.log(step);
                  setTimeout(() => {
                    // setCurrentStep(step);
                    if(
                      !(document.querySelector("input[name='Title.designDisplate']") as HTMLInputElement )?.checked){
                      setCurrentStep(9);
                    }
                  }, 300);
                },
                false
              );
            }, 300);
          },
          false
        );
      }

      if(name==="WorkspaceWalkthroughDesignUpload") {
        document.getElementById('upload-design-button')?.addEventListener(
            'click',
            function (e) {
              // e.stopImmediatePropagation();
              e.stopPropagation();
              uploadTestImage();
              setTimeout(() => {
                setCurrentStep(2);
              }, 1000);
            },
            false
          );
        document.getElementById('upload-btn')?.addEventListener(
          'click',
          function() {
            // setCurrentStep(4);
            setTimeout(() => {
              setCurrentStep(4);
              document.getElementById('add-design-here')?.addEventListener(
                'click',
                onClickEvent(5),
                false
              );
            }, 300);
          },
          false
        );
      }

      if(name==="WorkspaceWalkthroughCreation") {
        uploadTestImage();
        document.getElementById('creation-mode-btn')?.addEventListener(
          'click',
          onClickEvent(1),
          false);
      }

      if(name==="WorkspaceWalkthroughListings") {
        uploadTestImage();
        document.getElementById('creation-mode-btn')?.addEventListener(
          'click',
          onClickEvent(1),
          false);


          document.getElementById('title26-general')?.addEventListener(
            'keypress',
            onClickEvent(5),
            false
          );

        document.getElementById('title40-general-badge')?.addEventListener(
          'click',
          onClickEvent(6),
          false
        );
        document.getElementById('title50-general-badge')?.addEventListener(
          'click',
          onClickEvent(8),
          false
        );

        document
          .getElementsByClassName('first-step-WorkspaceWalkthroughSettings')
          ?.item(0)
          ?.children?.item(0)
          ?.children?.item(0)
          ?.children?.item(0)
          ?.children?.item(0)
          ?.children?.item(1)
          ?.addEventListener(
            'click',
            onClickEvent(14),
            false
          );

      }

      if(name==="WorkspaceWalkthroughLoadListing") {
        document.getElementById('load-listing-btn')?.addEventListener(
          'click',
          onClickEvent(1),
          false
        );
      }
    }
  }

  return (
    <>
      <Joyride
        //THIS WILL MAKE BEACON DISAPPEAR
        beaconComponent={Beacon as React.ElementType<BeaconRenderProps>}
        key={key}
        continuous={true}
        run={isRun}
        showProgress={true}
        showSkipButton={true}
        scrollToFirstStep={true}
        scrollOffset={300}
        steps={steps}
        stepIndex={currentStep}
        debug={!!process.env.REACT_APP_SPY}
        callback={handleJoyrideCallback}
        getHelpers={(helpers: StoreHelpers) => {
          myHelpers = helpers;
        }}
        floaterProps={{
          styles: {
            wrapper: {
              zIndex: 2000,
            },
          },
        }}
        styles={{
          buttonBack: {
            color: '#7f67ff',
          },
          buttonNext: {
            backgroundColor: '#7f67ff',
            border: 'none',
            borderRadius: 5,
          },
          options: {
            zIndex: 2000,
          },
        }}
      />
      <Tooltip placement="bottom" title={'Help!'}>

        <UserAvatarWrapperRounded
          onClick={() => {
            setIsTourActivated(true);
          }}
          style={{marginRight: '1rem'}}
        >
          <img className="logo-img" src={questions} alt="logo" style={{maxHeight: '14px'}} />
        </UserAvatarWrapperRounded>
      </Tooltip>
      <Modal
        title={
          <>
            Start walkthrough.
            <Typography.Text type={'danger'}> It will reset workspace.</Typography.Text>
          </>
        }
        visible={isShowModal}
        onOk={() => setIsShowModal(false)}
        onCancel={() => setIsShowModal(false)}
        footer={[
          <Button
            type="primary"
            onClick={() => {
              setIsShowModal(false);
            }}
          >
            Cancel
          </Button>,
        ]}
      >
        <ul>
          <li key={1}>
            <Typography.Link
              onClick={() => {
                // startWorkspaceTour(WorkspaceWalkthroughIntroduction);
                addFakeData();
              }}
            >
              Workspace Introduction
            </Typography.Link>
          </li>
          <li key={2}>
            <Typography.Link
              onClick={() => {
                setName('WorkspaceWalkthroughSettings');
                startWorkspaceTour(WorkspaceWalkthroughSettings());
              }}
            >
              Make Your Settings
            </Typography.Link>
          </li>
          <li key={3}>
            <Typography.Link
              onClick={() => {
                setName('WorkspaceWalkthroughDesignUpload');
                startWorkspaceTour(
                  WorkspaceWalkthroughDesignUpload()
                );
              }}
            >
              Upload and Manage Designs
            </Typography.Link>
          </li>
          <li key={4}>
            <Typography.Link
              onClick={() => {
                setName('WorkspaceWalkthroughCreation');
                startWorkspaceTour(WorkspaceWalkthroughCreation());
              }}
            >
              Creation Mode
            </Typography.Link>
          </li>
          <li key={5}>
            <Typography.Link
              onClick={() => {
                setName('WorkspaceWalkthroughListings');
                startWorkspaceTour(
                  WorkspaceWalkthroughListings()
                );
              }}
            >
              Write Your Listing
            </Typography.Link>
          </li>
          <li key={6}>
            <Typography.Link
              onClick={() => {
                setName('WorkspaceWalkthroughLoadListing');
                startWorkspaceTour(WorkspaceWalkthroughLoadListing());
              }}
            >
              Load a Listing
            </Typography.Link>
          </li>
          <li key={7}>
            <Typography.Link
              onClick={() => {
                startWorkspaceTour(WorkspaceWalkthroughTransfer());
              }}
            >
              Transfer Your Listings
            </Typography.Link>
          </li>
        </ul>
      </Modal>
    </>
  );
}

export default StartTour;
