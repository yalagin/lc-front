import React, {  Dispatch } from "react";
import { Typography } from "antd";
import { Step } from "react-joyride";

export const stepsDashboard = [
  {
    spotlightClicks: true,
    hideCloseButton: true,
    disableBeacon: true,
    title: "Welcome to our LazyCloud!",
    content:
      " You're currently on your Dashboard which is supposed to give you the most important updates and statistics regarding the project.",
    placement: "center",
    target: "body",
    showProgress: false
  },
  {
    spotlightClicks: true,
    hideCloseButton: true,
    //disableBeacon: true,
    target: ".second-step-dashboard",
    content:
      "Here you can see the latest patch notes. Try to keep an eye on them so you can try our new implementations"
  },
  {
    spotlightClicks: true,
    hideCloseButton: true,
    //disableBeacon: true,
    target: ".third-step-dashboard",
    content:
      "This area displays some data regarding your uploads to the LazyCloud. By choosing options from the dropdowns on the right you can filter your view."
  },
  {
    spotlightClicks: true,
    hideCloseButton: true,
    //disableBeacon: true,
    target: ".forth-step-dashboard",
    content: "How much storage is currently used can be found here."
  },
  {
    spotlightClicks: true,
    hideCloseButton: true,
    //disableBeacon: true,
    target: ".five-step-dashboard",
    content:
      "We also request the already used and available translation characters from your DeepL Account."
  },
  {
    spotlightClicks: true,
    hideCloseButton: true,
    //disableBeacon: true,
    target: ".six-step-dashboard",
    content:
      "Here you see how many designs are currently waiting for their upload in the Upload Queue. You can filter by platforms as well."
  }
] as Step[];

export const WorkspaceWalkthroughIntroduction = [
  {
    spotlightClicks: true,
    hideCloseButton: true,
    disableBeacon: true,
    content:
      "This is your workspace. The place where you upload your designs to the cloud, write listings or prepare working on both of them.",
    placement: "center",
    target: "body",
    title: "Workspace Walkthrough Introduction"
  },
  {
    spotlightClicks: true,
    hideCloseButton: true,
    //disableBeacon: true,
    target: ".ant-tabs-nav-list  .ant-tabs-tab:first-child",
    content:
      "The general tab is one of the most powerful areas. Its purpose is to prevent you from repetetive steps you'd do for every platform you want to upload to."
  },
  {
    spotlightClicks: true,
    hideCloseButton: true,
    //disableBeacon: true,
    target: ".general-fields",
    content: "On the general tab you can write your listings for multiple platforms..."
  },
  {
    spotlightClicks: true,
    hideCloseButton: true,
    //disableBeacon: true,
    target: ".forth-step-WorkspaceWalkthroughIntroduction",
    content: "Upload designs to the workspace to assign listings to them"
  },
  {
    spotlightClicks: true,
    hideCloseButton: true,
    //disableBeacon: true,
    target: ".five-step-WorkspaceWalkthroughIntroduction",
    content:
      "And switch between languages to write your listing in another language or translate them to desired languages."
  }
] as Step[];

export const WorkspaceWalkthroughSettings = () =>
  [
    {
      spotlightClicks: true,
      hideCloseButton: true,
      disableBeacon: true,
      placement: "center",
      target: "body",
      title: "Workspace Walkthrough Settings",
      content:
        "In this walkthrough guide we'll cover the most important settings of the workspace. You can customize the most areas to your needs and you should do so before starting to work with it on a daily basis."
    },
    {
      spotlightClicks: true,
      hideCloseButton: true,
      //disableBeacon: true,
      target: ".first-step-WorkspaceWalkthroughSettings",
      content:
        "This is the platform tab bar. Here you can select the desired tab to edit your listing for a specific platform. At the moment the general tab is selected."
    },
    {
      spotlightClicks: true,
      hideCloseButton: true,
      //disableBeacon: true,
      content:
        "Please take note of these two platforms. Let's say you don't want them to appear in the platform tab because you don't upload your designs on these platforms. What you want to do is the following...",
      target:
        ".first-step-WorkspaceWalkthroughSettings .ant-tabs-tab:nth-of-type(2) .ant-tabs-tab:nth-of-type(3)"
    },
    {
      spotlightClicks: true,
      hideCloseButton: true,
      //disableBeacon: true,
      target: ".third-step-WorkspaceWalkthroughSettings",
      content: "Click on the settings icon of the whole Edit area.",
      disableOverlayClose: true,
      hideFooter: true
      //spotlightClicks: true,
    },
    {
      spotlightClicks: true,
      hideCloseButton: true,
      // //disableBeacon: true,
      target: "#forth-step-WorkspaceWalkthroughSettings",
      // disableOverlayClose: true,
      hideFooter: true,
      //spotlightClicks: true,
      content:
        "Here you can see the workspace settings where you can enable or disable platforms as well as set your preferred colors for display purposes. Please disable Spreadshirt COM and Spreadshirt EU."
    },
    {
      spotlightClicks: true,
      hideCloseButton: true,
      // //disableBeacon: true,
      hideBackButton: true,
      target: "#designTabs",
      content:
        "As you can see in the platform bar the two platforms you disabled are gone now. This means that if you're about to upload some designs now and write listings for them, the listing won't be written for these platforms. You're just editing and writing listing for what's enabled up here."
    },
    {
      spotlightClicks: true,
      hideCloseButton: true,
      //disableBeacon: true,
      target: "#title-selector",
      content:
        "When talking about writing listings... These numbered lables represent the upper character boundaries of the platforms. It shows the limits for the titles, descriptions and the amount of tags the different platforms allow. For example, Displate doesn't accept titles with more than 26 characters."
    },
    {
      spotlightClicks: true,
      hideCloseButton: true,
      //disableBeacon: true,
      target: "#gear-general-title",
      content:
        "If you don't want to write a general title for a specific platform, you can exclude it in the title settings. Please click on the settings icon.",
      disableOverlayClose: true,
      hideFooter: true
      //spotlightClicks: true,

    },
    {
      spotlightClicks: true,
      hideCloseButton: true,
      //disableBeacon: true,
      target: ".edit-fields",
      content:
        " Disable Displate to get rid of its title label. This doesn't mean that you can't write titles for Displate anymore. You've just excluded it from the general tab listing. You're still able to visit the Displate tab and write an individual title for your designs.",
      // disableOverlayClose: true,
      hideFooter: true
      //spotlightClicks: true,
    },
    {
      spotlightClicks: true,
      hideCloseButton: true,
      // //disableBeacon: true,
      hideBackButton: true,
      // placement:'center',
      target: "#title-selector",
      content: "As you can see the label for the 26 character limit is gone."
    },
    {
      spotlightClicks: true,
      hideCloseButton: true,
      //disableBeacon: true,
      target: ".ten-step-WorkspaceWalkthroughSettings",
      content: "Whatever title you start to write here will only affect the platforms that are enabled to receive your general title."
    },
    {
      spotlightClicks: true,
      hideCloseButton: true,
      //disableBeacon: true,
      target: ".five-step-WorkspaceWalkthroughIntroduction",
      content:
        "Before starting to work on your listings, you should always check which language is selected."
    }
  ] as Step[];

export const WorkspaceWalkthroughDesignUpload = () =>
  [
    {
      spotlightClicks: true,
      disableBeacon: true,
      hideCloseButton: true,
      // placement: 'center',
      title: "Workspace Walkthrough Design Upload",
      content:
        "The preview box offers two buttons to add images or image placeholders if it's empty. Alternatively, you can drag & drop your images into this area.",
      target: "#image-carousel"
    },
    {
      spotlightClicks: true,
      hideCloseButton: true,
      //disableBeacon: true,
      content: "Click on Upload Design to open your file explorer. But this time we are going to add some test images for you.",
      target: "#upload-design-button",
      disableOverlayClose: true,
      hideFooter: true
      //spotlightClicks: true,
    },
    {
      spotlightClicks: true,
      hideCloseButton: true,
      //spotlightClicks: true,
      //disableBeacon: true,
      content:
        "As you can see there are some test images now. The preview box gives you the possibility to select the images you want to write or edit the listing for. Shortcuts like CTRL+Click or Shift+Click work as well.",
      target: ".ant-carousel"
      // position: 'right',
      // padding: 0,
    },
    {
      spotlightClicks: true,
      hideCloseButton: true,
      disableOverlayClose: true,
      hideFooter: true,
      //disableBeacon: true,
      content: "With a click on the + icon you can open another upload area. Click it to continue.",
      //spotlightClicks: true,
      target: "#upload-btn"
    },
    {
      spotlightClicks: true,
      hideCloseButton: true,
      //disableBeacon: true,
      content:
        "This way you can upload images although there are already some in the preview box. Let's add some placeholders. Placeholders are supposed to exist as long as you replace them by an image. Their purpose is to give you the possibility to write listings for images that doesn't even exist yet. Go ahead and add three placeholders.",
      target: "#add-design-here",
      //spotlightClicks: true,
      hideFooter: true

    },
    {
      spotlightClicks: true,
      hideCloseButton: true,
      //disableBeacon: true,
      content:
        "Now, the placeholders appeared in the preview box. These are considered to be images like the others. So you can select them and edit their listing data too.",
      target: ".ant-carousel"
    },
    {
      spotlightClicks: true,
      hideCloseButton: true,
      //disableBeacon: true,
      content:
        "By the button in the top right corner of the preview box you can select or deselect all the images loaded in the preview box. This concerns all the images you uploaded here, not just the ones that are visible on the first page of the preview box.",
      target: ".select-all"
    },
    {
      spotlightClicks: true,
      hideCloseButton: true,
      //disableBeacon: true,
      content:
        "This button marks an image to be an adult image. It ensures that the proper presets are made, e.g. the removement of the Youth fit type at MBA.",
      target: ".template-mark"
      //spotlightClicks: true,
    },
    {
      spotlightClicks: true,
      hideCloseButton: true,
      //disableBeacon: true,
      content:
        "This button toggles between the view you currently see and a single image view. You need to enlarge an image to better see some parts of it? You can either double-click the images to get a preview window of them or click on this single image view button to enlarge them within the preview box.",
      target: ".single-view"
      //spotlightClicks: true,
    },
    {
      spotlightClicks: true,
      hideCloseButton: true,
      //disableBeacon: true,
      content:
        "The template dropdown gives you the possibility to assign a template to an image. Templates can be created on the Template page and contain information about enabled products, colors, prices and many other settings. The idea is to set them up once and save time when it comes to your daily workflow where you just want to write listings, assign product settings and leave that repetitive stuff behind as fast as you can.",
      target: ".template-dropdown"
      //spotlightClicks: true,
    },
    {
      spotlightClicks: true,
      hideCloseButton: true,
      //disableBeacon: true,
      content:
        "The button to the right of the template dropdown is kinda the opposite of the single image view. This button gives you an overlay modal that contains a bigger preview box. This is helpful if you uploaded a lot of images and want to select all the designs of a niche. It simply gives you more overview about what's uploaded to the workspace and what's selected.",
      target: ".template-modal-list"
      //spotlightClicks: true,
    },
    {
      spotlightClicks: true,
      hideCloseButton: true,
      //disableBeacon: true,
      content:
        "We end this walkthrough guid with a small challenge. Select all the images by your preferred shortcut or button...",
      target: ".ant-carousel"
      //spotlightClicks: true,
    },
    {
      spotlightClicks: true,
      hideCloseButton: true,
      //disableBeacon: true,
      content: "And click on the remove button on the bottom right of the preview box.",
      target: ".remove-design-btn"
      //spotlightClicks: true,
    }
  ] as Step[];

export const WorkspaceWalkthroughCreation = () =>
  [
    {
      spotlightClicks: true,
      //spotlightClicks: true,
      hideCloseButton: true,
      disableBeacon: true,
      showProgress: true,
      hideFooter: true,
      title: "Workspace Walkthrough Creation",
      content:
        "The workspace divides into two modes. One is the listing mode which offers fields and settings that are " +
        "relevant for the uploads to the print on demand platforms. The other mode is called the Creation mode. Its purpose " +
        "is to offer some background information about an image, such as working files, resources or instructions for your " +
        "designers and VAs. Click on Creation to continue.",
      target: ".modes-switcher"
    },
    {
      spotlightClicks: true,
      hideCloseButton: true,
      //disableBeacon: true,
      content:
        "At first the Creation mode doesn't look too different from the listing mode. There's a preview box as well so you can select the designs you want to write or edit the creation mode data for. Basically, you can write to its fields whatever you want but they were meant to be filled with the following...",
      target: "#image-carousel"
    },
    {
      spotlightClicks: true,
      hideCloseButton: true,
      //disableBeacon: true,
      content:
        "The text area is meant to be filled with whatever words, phrases or quotes shall be written on the image. For example, if you create a placeholder for your designer and you want to tell him which slogan you want to see on the design.",
      target: ".text-filed-creation-mode"
    },
    {
      spotlightClicks: true,
      hideCloseButton: true,
      //disableBeacon: true,
      content:
        "The description areas purpose is to contain any descriptive text about your image. For example, if you add instructions to a placeholder for your designer, by which he knows how the design should look then.",
      target: ".description-filed-creation-mode"
    },
    {
      spotlightClicks: true,
      hideCloseButton: true,
      //disableBeacon: true,
      content:
        "Inspiration is the place you can add URLs to. Whenever you find some inspiring sites or images that relate to your design idea, you can paste them here. If you added more than one link, the \"Open in all tabs\" button offers to open them all at once.",
      target: ".inspiration-filed-creation-mode"
    },
    {
      spotlightClicks: true,
      hideCloseButton: true,
      //disableBeacon: true,
      content:
        "In the working files area you can upload the files it took to create your final result. This may be some vector files of design elements, inspirational files, sketches or whatever. Be aware of your selection in the preview box. If there's multiple images selected, the working files will be assigned to each of them. This can be helpful if you uploaded a design series which was made out of the same illustration file but just with different colors or minor changes.",
      target: ".workingFiles-filed-creation-mode"
    }
  ] as Step[];

export const WorkspaceWalkthroughListings = () =>
  [
    {
      spotlightClicks: true,
      hideCloseButton: true,
      //spotlightClicks: true,
      placement: "center",
      target: "body",
      title: "Workspace Walkthrough Listings",
      content:
        "Writing listings is a repetitive task. Some users love to write individual descriptions and tags for each design. Some think that one description per niche is enough. Anyways, our Workspace offers possibilities for both of these use cases."
    },
    {
      spotlightClicks: true,
      hideCloseButton: true,
      //disableBeacon: true,
      target: ".first-step-WorkspaceWalkthroughSettings",
      //spotlightClicks: true,
      // action: node => {spotlightClicks: true,hideCloseButton: true,//disableBeacon: true,
      //   node?.dispatchEvent(new Event('click'));
      // },
      content:
        "The general tab is the most powerful area because it offers the possibility to write generic listings for multiple selected designs and for every platform you want to upload to."
    },
    {
      spotlightClicks: true,
      hideCloseButton: true,
      //spotlightClicks: true,
      //disableBeacon: true,
      target: "#image-carousel",
      // resizeObservables: ['#image-carousel'],
      content:
        " When talking about selected designs... Here's your preview box. There, you can upload your designs and select the designs you want to write or edit the listing for. Regarding the selection shortcuts like CTRL+Click and Shift+Click do work as well."
    },
    {
      spotlightClicks: true,
      hideCloseButton: true,
      //spotlightClicks: true,
      //disableBeacon: true,
      target: "#title-target",
      content:
        " As you already might have noticed there's multiple numbers above each input field. These represent the upper character boundaries of your enabled platforms. If you select a specific number, like 26, you're about to edit the title for all platforms with a title character limit of 26. Starting from the lowest one and continueing through the highest one will make you write a generic title using all of space and potential of each platform's title."
    },
    {
      spotlightClicks: true,

      hideCloseButton: true,
      //spotlightClicks: true,
      hideFooter: true,
      //disableBeacon: true,
      target: ".ten-step-WorkspaceWalkthroughSettings",
      // disableActions: true,
      // action: () => {spotlightClicks: true,hideCloseButton: true,//disableBeacon: true,
      //   document.getElementById('title26-general-badge')?.click();
      //   document.getElementById('title26-general')?.addEventListener(
      //     'keypress',
      //     function (e) {spotlightClicks: true,hideCloseButton: true,//disableBeacon: true,
      //       setDisabledActions(false);
      //     },
      //     false
      //   );
      // },
      content:
        "Let's try it out. The 26 character area is selected and you can now write something to the input field. If you've made your input, go ahead and see how this helps you writing the titles for platforms with a higher upper character boundary."
    },
    {
      spotlightClicks: true,
      hideCloseButton: true,
      hideFooter:true,
      //disableBeacon: true,
      target: "#title40-general-badge",
      // disableActions: true,
      // action: node => {
      //   document.getElementById('title40-general-badge')?.addEventListener(
      //     'click',
      //     function (e) {
      //       setTimeout(() => {
      //         setCurrentStep(6);
      //         setDisabledActions(false);
      //       }, 300);
      //     },
      //     false
      //   );
      // },
      content:
        "Well done, you wrote your title for the 26 area. Now, click on 40 to write your title for the next platforms."
    },
    {
      spotlightClicks: true,
      hideCloseButton: true,
      //disableBeacon: true,
      target: ".ten-step-WorkspaceWalkthroughSettings",
      // disableActions: true,
      // action: () => {spotlightClicks: true,hideCloseButton: true,//disableBeacon: true,
      //   document.getElementById('title40-general')?.addEventListener(
      //     'keypress',
      //     function (e) {spotlightClicks: true,hideCloseButton: true,//disableBeacon: true,
      //       setDisabledActions(false);
      //     },
      //     false
      //   );
      // },
      // hideFooter:true,
      content: "As you can see, the title for the 40 area inherited what you wrote in the title to its left. You can now add some words and make use of the new available space to progress with your generic title."
    },
    {
      spotlightClicks: true,
      hideCloseButton: true,
      hideFooter:true,
      target: "#title50-general-badge",
      // disableActions: true,
      // action: node => {spotlightClicks: true,hideCloseButton: true,//disableBeacon: true,
      //   document.getElementById('title50-general-badge')?.addEventListener(
      //     'click',
      //     function (e) {spotlightClicks: true,hideCloseButton: true,//disableBeacon: true,
      //       setTimeout(() => {spotlightClicks: true,hideCloseButton: true,//disableBeacon: true,
      //         setCurrentStep(8);
      //         setDisabledActions(false);
      //       }, 300);
      //     },
      //     false
      //   );
      // },
      content: 'Once again, choose the next title area and see which input will appear in here.',
    },
    {
      spotlightClicks: true,
      hideCloseButton: true,
      //disableBeacon: true,
      target: ".ten-step-WorkspaceWalkthroughSettings",
      content:
        "And here it is, I think you got the idea. The inheritation only takes place if the next title area you select is empty. If all the areas are filled, they work completely independent and you can switch between them without worrying to overwrite something."
    },
    {
      spotlightClicks: true,
      hideCloseButton: true,
      //disableBeacon: true,
      target: ".example-tags-general",
      position: "top",
      content:
        "While Title and Description work the same with their character limited areas, the Tags area is slightly different. The colored and numbered rectangles represent the limited amount of tags of your enabled platforms."
    },
    {
      spotlightClicks: true,
      hideCloseButton: true,
      //disableBeacon: true,
      target: ".tags-input",
      position: "top",
      content:
        "If you type in your tags, these will be colored like the enabled rectangles. If your tags become colored like the second rectangle, you know that from this color onwords the tags won't be used for the platforms with a lower limit anymore. Add as much tags as you want and click on continue."
    },
    {
      spotlightClicks: true,
      hideCloseButton: true,
      //disableBeacon: true,
      target: ".added-tags",
      resizeObservables: [".added-tags"],
      // action: () => addFakeData(),
      position: "top",
      content:
        "We added some further tags to give you an impression of it. Platforms that take 15 tags will use the orange and purple ones from here. Platforms that take 20 tags will use the orange, purple and pink ones from here, and so on. It also works in a generic way so you want to use the most matching and meaningful tags for your niche at first. If you're not happy with the order you typed them in, feel free to rearrange them by drag & drop the tags to your desired place."
    },
    {
      spotlightClicks: true,
      hideCloseButton: true,
      //disableBeacon: true,
      target: ".mba-general",
      content:
        "The general tab also offers an area for MBA. You don't need to fill it here because you could alternatively switch to the MBA tab too. Anyways, some users might find this helpful to see their descriptions and tags from above and use parts of it when writing the MBA related things."
    },
    {
      spotlightClicks: true,
      hideCloseButton: true,
      //disableBeacon: true,
      target: ".first-step-WorkspaceWalkthroughSettings .ant-tabs-tab:nth-of-type(2)",
      hideFooter:true,
      disableActions: true,
      content:
        "As mentioned, you don't need the general tab to write a listing. You can visit a platform tab to see your transferred listing from general tab or to write a listing for this platform from scratch. Click on Spreadshirt COM to switch to its tab."
    },
    {
      spotlightClicks: true,
      hideCloseButton: true,
      //disableBeacon: true,
      target: ".DesignSpreadshirtCom",
      content:
        "As you can see here there's just the fields and their proper limits that are relevant for this platform." +
        " The reason why there's no title or tags yet, is because we didn't transfer them when we were filling the fields " +
        "on the general tab. Transferring will be explained in the next walkthrough guide.\n" +
        "Important to notice here is that you could write a listing for Spreadshirt COM independently from general tab. " +
        "If you don't want to transfer stuff from the general tab to Spreadshirt COM and want to write a listing for this platform from scratch," +
        " disable the transferring options for this platform in the general tab. The settings are also part of another walkthrough guide.",
    }
  ] as Step[];

export const WorkspaceWalkthroughLoadListing = () =>
  [
    {
      spotlightClicks: true,
      hideCloseButton: true,
      hideFooter: true,
      target: "#load-listing-btn",
      disableBeacon: true,
      content:
        "In the case that you don't want to write a listing from scratch, you can click on the load listing button which offers you some possibilities to load an already existing listing. Click on it to continue."
    },
    {
      spotlightClicks: true,
      hideCloseButton: true,
      //disableBeacon: true,
      target: '.load-listing-tabs .ant-tabs-tab:nth-of-type(1)',
      // position: 'left',
      content:
        "Let's say you already wrote a listing for English but you want to upload the images to a german marketplace as well. You already switched the language of the Workspace to be German and clicked on the load listing button. So this Other Languages area will offer you to load your listing From English because it already exists. This will load the same listing you wrote in English to your german area of the Workspace. You can now translate it or upload the English listing to german marketplaces."
    },
    {
      spotlightClicks: true,
      hideCloseButton: true,
      //disableBeacon: true,
      target: ".load-listing-tabs .ant-tabs-tab:nth-of-type(2)",
      // position: 'left',
      content:
        "The storage tab offers you to load a listing from the ones you've already written in the past. By typing in your search term into a search bar you'll be able to find listings in your storage that relate to this word(s). This is super useful if you're about to upload designs for this year's christmas season and want to just use the listings from last year."
    },
    {
      spotlightClicks: true,
      hideCloseButton: true,
      //disableBeacon: true,
      target: ".load-listing-tabs .ant-tabs-tab:nth-of-type(3)",
      // position: 'left',
      content:
        "Blueprints can be written on the Template page. Its purpose is to be some kind of a cloze text. It can be loaded and filled by you. What's useful here, is that you can type in your tags and keywords at first, load a blueprint version for titles and descriptions and fill in the blank spots with a hash sign #. If you do so this hash sign results in giving you a dropdown menu that offers you the keywords you've written before."
    }
  ] as Step[];

export const WorkspaceWalkthroughTransfer = () =>
  [
    {
      spotlightClicks: true,
      hideCloseButton: true,
      disableBeacon: true,
      target: ".general-fields",
      content:
        "If your general tab was filled with your listings and you managed to get all the character limit areas filled, the listing can be transferred to all the platforms."
    },
    {
      spotlightClicks: true,
      hideCloseButton: true,
      //disableBeacon: true,
      target: ".gear-general",
      // highlightedtargets: [
      //   '.gear-general',
      //   '.gear-general-tags',
      //   '.gear-general-desc',
      //   '.gear-general-title',
      // ],
      // position: 'left',
      content:
        "Therefore, make sure that each input area has the right settings. You can disable the title to be transferred for a platform such as Teepublic but keep the transfer for description and tags enabled for this platform. This might be useful if you want to use the same description you already use for other platforms but you want to write an individual title for a specific platform that differs from the titles of the others."
    },
    {
      spotlightClicks: true,
      hideCloseButton: true,
      //disableBeacon: true,
      target: ".transfer-selected-designs-btn",
      content:
        "If you set everything up properly and filled your listings, you can either transfer the selected images..."
    },
    {
      spotlightClicks: true,
      hideCloseButton: true,
      //disableBeacon: true,
      target: ".transfer-all-designs-btn",
      content: "or transfer all the images, no matter which are selected."
    },
    {
      spotlightClicks: true,
      hideCloseButton: true,
      //disableBeacon: true,
      target: ".save-design-btn",
      content:
        "The save button will save your entries and offers an additional option to not just save but also instantly pushing the uploaded designs and written listings to the Upload Queue. Make sure transfer to all your wanted platforms before saving. If you mistakenly saved bevore transferring, you can go to the overview, select the images again and bring them back to the Workspace by clicking on the Edit button of the Overview page."
    }
  ] as Step[];

export const stepsTemplates = [
  {
    spotlightClicks: true,
    hideCloseButton: true,
    disableBeacon: true,
    placement: "center",
    target: "body",
    showProgress: false,
    title: "Welcome to templates page!",
    content: (
      <Typography.Text>
        Here you can select templates and then import it in workspace tab.
        <br />
        <br /> Basically, here you can select default import settings for different platform
      </Typography.Text>
    )
  }
] as Step[];
export const stepsTrademarks = [
  {
    spotlightClicks: true,
    hideCloseButton: true,
    disableBeacon: true,
    placement: "center",
    showProgress: false,
    target: "body",
    title: "Welcome to trademark page!",
    content:
      "Here you can check trademark for markets. Dont forget to use filter if you need particular Region or  Classification"
  },
  {
    spotlightClicks: true,
    hideCloseButton: true,
    //disableBeacon: true,
    target: '.ant-tabs-nav-list .ant-tabs-tab:nth-of-type(1)',
    // position: 'left',
    content: "In the check tab you can apply a filter for which classes and regions you want to check specific words and phrases. You can also track the results for a desired word or phrase so you will be notified about news in the future."
  },{
    spotlightClicks: true,
    hideCloseButton: true,
    //disableBeacon: true,
    target: '.ant-tabs-nav-list .ant-tabs-tab:nth-of-type(2)',
    // position: 'left',
    content: "In the check tab you can apply a filter for which classes and regions you want to check specific words and phrases. You can also track the results for a desired word or phrase so you will be notified about news in the futureThe track tab shows your list of tracked words and phrases. It also displays if there are new entries since the last time you checked the results of your tracked entries."
  },{
    spotlightClicks: true,
    hideCloseButton: true,
    //disableBeacon: true,
    target: '.ant-tabs-nav-list .ant-tabs-tab:nth-of-type(3)',
    // position: 'left',
    content: " Within “Watch” you can find notifications about any used words and phrases of your listings. We assume that at the time of uploading them to the cloud you carefully checked the trademarks for it. From that point onwords we notify you whenever there’s news about results of a specific word and phrase."
  },
] as Step[];
