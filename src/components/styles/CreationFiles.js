import styled from "styled-components";
import { Button, Input } from "antd";
import Icon from "@ant-design/icons";

export const MyButton = styled(Button)`
  border-left-width: 0;
  border-top-right-radius: 13px;
  border-bottom-right-radius: 13px;
`;

export const SmallButton = styled(Button)`
  &&& {
    background: ${props => (props.color)} !important;
    border-top-color: rgb(217, 217, 217);
    border-top-style: solid;
    border-top-width: 1px;
    border-left-color: rgb(217, 217, 217);
    border-left-style: solid;
    border-left-width: 1px;
    border-bottom-color: rgb(217, 217, 217);
    border-bottom-style: solid;
    border-bottom-width: 1px;
    border-right-color: rgb(217, 217, 217);
    border-right-style: solid;
    border-right-width: 0;
    border-top-left-radius: 13px;
    border-bottom-left-radius: 13px;
  }
`;


export const IconSmall = styled(Icon)`
  & > svg {
    height: 12px;
    width: 17px;
  }
`;


const { Search } = Input;
export const SearchStyled = styled(Search)`
   .ant-input {
       //color: ${props => (!props.error ? 'black' : '' + props.error.color)};
  }
`;

