import styled from "styled-components";

export const StyledCard = styled.div`
  border: 1px solid #eaeaea;
  box-sizing: border-box;
  box-shadow: 0px 1px 17px rgba(0, 0, 0, 0.06);
  display: flex;
  margin: 1rem 0;
`;

export const ProductHeader = styled.div`
  background: #F6F7FA;
  min-width: 240px;
  height: 41px;
  align-items: center;
  justify-content: center;
  display: flex;
`;
export const SelectMarketplacePricing = styled.div`
  height: 41px;
  //align-items: center;
  //justify-content: center;
  //display: flex;
  padding: 1rem;
`;
export const ProductInformationHolder = styled.div`
  flex-grow: 4
`;
export const MarketplaceHolder = styled.div`
  display: flex;
  flex-direction: row;
  //border: 1px solid #EBEDF4;
  box-sizing: border-box;
  margin: 1rem;
  flex-wrap: wrap;
`;

export const DownCentredDiv = styled.div`
  padding: 0.3rem;
  background: #F6F7FA;
  align-items: center;
  justify-content: center;
  display: flex;
  min-height: 42px;
  //min-height: 2.6rem;
  &&& .ant-form-item {
    margin-bottom: 0;
  }
`

export const DownTitleCentredDiv = styled(DownCentredDiv)`
  //height: 42px
`

export const CentredDiv = styled.div`
  background: rgba(235, 237, 244, 0.15);
  padding: 0.3rem;
  align-items: center;
  justify-content: center;
  display: flex;
  min-height: 42px;
  //min-height: 2.6rem;
  &&& .ant-form-item {
    margin-bottom: 0;
  }
`

export const MarketplaceTitleHolder = styled.div`
  border: 1px solid #ebedf4;
  box-sizing: border-box;
`

export const MarginLeft = styled.div`
  margin: 0 1rem;
`
export const ColorHolder = styled.div`
  display: flex;
  flex-direction: column;
  border: 1px solid #EBEDF4;
  box-sizing: border-box;
  margin: 1rem;
  padding: 1rem;
  //flex-wrap: wrap;
`;

export const ImageContainer = styled.div`
  height: 14rem;
  margin: 1rem;
  align-items: center;
  justify-content: center;
  display: flex;

  > img {
    height: inherit;
    width: unset;
  }
`;

export const DivRow = styled.div`
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
`

export const DivCol = styled.div`
  display: flex;
  flex-wrap: wrap;
  flex-direction: column;
`

