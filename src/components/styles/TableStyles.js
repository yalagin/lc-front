import styled from 'styled-components';
import { Button, Input, Table } from 'antd';

export const TableStyled = styled(Table)`
  //.ant-select-dropdown-placement-bottomLeft {
  //  top: -140px !important;
  //}
  //.ant-table [vt] > table > .ant-table-tbody > tr > td {
  //  padding: 8px;
  //}
  //https://www.npmjs.com/package/virtualizedtableforantd4
  // size={'small'} 
  //.ant-table [vt] > table > .ant-table-tbody > tr > td {
  //  padding: 8px;
  //}

  .react-resizable {
    position: relative;
    background-clip: padding-box;
  }
  
  &&& .ant-table-body {
    height: ${props => props.height+'px' || "auto"};
  }
  &&& .ant-table-tbody > tr > td {
    border-bottom: none;
    transition: background 0.3s;
  }

  .react-resizable-handle {
    position: absolute;
    right: -5px;
    bottom: 0;
    z-index: 1;
    width: 10px;
    height: 100%;
    cursor: col-resize;
    //background:red;
  }
  .ant-table-thead > tr > th {
    webkit-user-select: none; /* Safari */
    -moz-user-select: none; /* Firefox */
    -ms-user-select: none; /* IE10+/Edge */
    user-select: none; /* Standard */ 
  }

  &&& tr.drop-over-downward td {
    border-bottom: 2px dashed #1890ff;
  }

  &&& tr.drop-over-upward td {
    border-top: 2px dashed #1890ff;
  }

  &&& .editable-cell {
    position: relative;
  }

  &&& .editable-cell-value-wrap {
    padding: 5px 12px;
    cursor: pointer;
  }

  &&& .editable-row:hover .editable-cell-value-wrap {
    padding: 4px 11px;
    border: 1px solid #d9d9d9;
    border-radius: 2px;
  }

  &&& [data-theme='dark'] .editable-row:hover .editable-cell-value-wrap {
    border: 1px solid #434343;
  }
`;


export const StyledSearch = styled(Input.Search)`
  border-radius: 45px;
  background-color: #F6F7FB;
  padding: 5px 10px;
  width: 100%;
  border: 1px solid #F6F7FB;
  :hover, :focus {
    border: 1px solid #a38fff;
  }
  &&& input{
    background-color: #F6F7FB;
  }
  &&& .ant-btn-primary {
    color: #8c8c8c;
    background: #F6F7FB;
    border-color: #F6F7FB;
    text-shadow: none;
    box-shadow: none;
  }
`;

export const StyledEditButton = styled(Button)`
  width: 114px;
  background-color: #7F67FF;
  color: white;
  letter-spacing: 0;
  display: flex;
  align-items: center;
  justify-content: center;
  height: 35px;
  border-radius: 5px;
  opacity: 0.83;
`;

export const StyledUploadButton = styled(Button)`
  transition: all 0.3s cubic-bezier(0.645, 0.045, 0.355, 1);
  background: #C7F7E7;
  width: 35px;
  height: 35px;
  display: flex;
  align-items: center;
  justify-content: center;
  border-radius: 5px;
  opacity: 0.83;
  border: 1px solid transparent;

  :hover, :focus {
    color: unset;
    background: #C7F7E7;
    border-color: #a38fff;
  }

  img {
    width: 17px;
    height: 17px;
  }
`;

export const StyledDeleteButton = styled(Button)`
  transition: all 0.3s cubic-bezier(0.645, 0.045, 0.355, 1);
  background: #FAD9D9;
  width: 35px;
  height: 35px;
  display: flex;
  align-items: center;
  justify-content: center;
  border-radius: 5px;
  opacity: 0.83;
  border: 1px solid transparent;

  :hover, :focus {
    color: unset;
    background: #FAD9D9;
    border-color: #a38fff;
  }

  img {
    width: 17px;
    height: 17px;
  }
`;
export const StyledDeleteForHeaderButton = styled(Button)`
  transition: all 0.3s cubic-bezier(0.645, 0.045, 0.355, 1);
  box-shadow: 0 2px 0 rgb(0 0 0 / 2%);
  background: #FAD9D9;
  //width: 35px;
  height: 35px;
  //display: flex;
  //align-items: center;
  //justify-content: center;
  border-radius: 5px;
  opacity: 0.83;
  border: 1px solid transparent;
  width: 48px;

  :hover, :focus {
    color: unset;
    background: #f6baba;
    border-color: #a38fff;
    //box-shadow: 0 1px 2px -2px rgb(0 0 0 / 16%), 0 3px 6px 0 rgb(0 0 0 / 12%), 0 5px 12px 4px rgb(0 0 0 / 9%);
  }

  //img {
  //  width: 17px;
  //  height: 17px;
  //}
`;
export const StyledShuffleForHeaderButton = styled(StyledDeleteForHeaderButton)`
  background: #d9dffa;
  width: 50px;
  :hover, :focus {
    background: #c0ccf5;
  }
`;
export const WhiteHeaderTable = styled(Table)`
  .ant-table-thead > tr > th {

    background: white;
  }

  .ant-table-tbody > tr > td {
    border-bottom: 1px solid white;
    transition: background 0.3s;
  }

  .ant-table-tbody > tr:last-child > td {
    border-bottom: 1px solid #CFD8DC;
    transition: background 0.3s;
  }

  table {
    line-height: 1rem;
  }

`;
