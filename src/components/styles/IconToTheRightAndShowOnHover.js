import React from 'react';
import styled from 'styled-components';
import Icon from '@ant-design/icons';

const IconToTheRightAndShowOnHover = styled(Icon)`
  float: right;
  display: ${props => (props.hovered ? '' : 'none')};
  cursor: pointer;
  :hover{
    color:#7f67ff
  }
`;

export default IconToTheRightAndShowOnHover;

