import styled from 'styled-components';
import {Carousel, Image as antdImage, Row, Upload} from 'antd';

export const StyledAdultImage = styled.img`
  position: absolute;
  top: 6px;
  left: 6px;
  width: 40px;
  height: 40px;
  z-index: 1;
`;

export const StyledTemplate = styled.div`
  position: absolute;
  bottom: 15px;
  width: 100%;
  z-index: 1;
  div {
    display: flex;
    align-items: center;
    justify-content: center;
  }
  .box {
    padding: 0.3rem;
    background-color: #ffffff;
    border-radius: 5px;
    width: 55%;
    text-align: center;
  }
`;

export const StyledBorder = styled.div`
  border: ${props => (props.checkedValue ? ' 3px solid #7f67ff' : '')};
  box-shadow: 0px 1px 5px #e5e9f2;
  border-radius: 5px;
  background: ${props => props.imageCarouselBackgroundColor};
  display: flex;
  align-items: center;
  justify-content: center;

  :hover {
    cursor: pointer;
    box-shadow: 0 1px 2px -2px rgb(0 0 0 / 16%), 0 3px 6px 0 rgb(0 0 0 / 12%),
      0 5px 12px 4px rgb(0 0 0 / 9%);
  }

  & > .ant-image {
    position: unset;
    display: unset;
  }

  @supports (aspect-ratio: 5 / 6) {
    aspect-ratio: 5 / 6;
    margin: 0.2rem;
    padding: 0.2rem;
  }

  @supports not (aspect-ratio: 5 / 6) {
    padding-top: 120%;
    margin: 0.2rem;
    position: relative;
    width: 98%;
  }
`;

export const MyImage = styled.img`
  @supports (aspect-ratio: 5 / 6) {
    //aspect-ratio: 5 / 6;
    width: 100%;
    height: auto;
  }
  @supports not (aspect-ratio: 5 / 6) {
    width: 98%;
    position: absolute;
    top: 0;
    height: 98%;
    margin: 0.2rem;
  }
`;

export const MyTableImage = styled(antdImage)`
  @supports (aspect-ratio: 5 / 6) {
    aspect-ratio: 5 / 6;
    width: 100%;
    height: 100%;
  }
  @supports not (aspect-ratio: 5 / 6) {
    position: absolute;
    top: 0;
    width: 100%;
    height: 100%;
  }
`;
export const StyledTableBorder = styled.div`
  & > .ant-image {
    position: unset;
    display: unset;
  }
  background: ${props => props.imageBackgroundColor};

  @supports not (aspect-ratio: 5 / 6) {
    padding-top: 120%;
    position: relative;
    width: 98%;
  }
`;

export const GreyHeader = styled.h3`
  color: #90a1b4;
  font-size: 1.53125rem;
`;

export const StyledUpload = styled(Upload)`
  .ant-upload-select-text {
    width: 100%;
  }
`;

export const StyledSpace = styled(Row)`
  margin-bottom: 0.5rem;
`;

export const StyledAddMore = styled.div`
  width: 100%;
  height: 500px;
  background-color: #f5f6fa;
  padding: 2rem;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  margin-bottom: 0.5rem;
  //margin-top: 7rem;
  > div {
    width: 100px;
    height: 100px;
  }
`;
export const ImageCarousel = styled(Carousel)`
  padding-bottom: 50px;
  margin-right: 0.5rem;

  &&& .slick-arrow {
    color: #7f67ff !important;
    font-size: 22px;
  }
`;
