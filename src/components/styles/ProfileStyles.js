import styled from "styled-components";
import Icon from "@ant-design/icons";
import { Card } from "antd";

export const SettingsContainer = styled.div`
  display: flex;
  align-items: center;
  flex-direction: column;
`;

export const GoogleIcon = styled(Icon)`
  & > svg {
    max-height: 9rem;
  }
`;

export const SettingsCard = styled(Card)`
  width: 90%;
  margin: 1rem;
`;

export const FlexDivCol = styled.div`
  display: flex;
  flex-direction: column;
`;

export const FlexDivRow = styled.div`
  display: flex;
  flex-direction: row;
`;
