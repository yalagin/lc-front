import React from "react";
import styled from "styled-components";
import Icon, { CloudUploadOutlined } from "@ant-design/icons";
import { Typography, Upload } from "antd";
const {   Dragger } = Upload;

const DropArea =styled(Dragger)`
&&&{
  min-height: 500px;
  background: #F7F8FE;
  height: 95%;
  margin-top: 38px;
  margin-bottom: 38px;
  border-radius: 5px;
  border: 1px dashed #5F80EC;
  box-sizing: border-box;
  display: flex;
  justify-content: space-evenly;
  cursor: pointer;

  :hover {
    border-color: #a38fff;
    transition: border-color 0.3s;
  }
}
`;

export default DropArea;

export const DropAreaPlaceHolder = styled.div`
  display: flex;
  justify-content: center;
`;
export const DropAreaPlaceHolderOfUploadIcon = styled.div`
  font-size: 72px;
  display: flex;
  flex-direction: column;
  justify-content: center;
`;
export const DropAreaPlaceHolderOfUploadText = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  margin-left: 1rem;
  align-items: flex-start
`;

export const WorkingFiles = styled.div`
  display: flex;
  flex-wrap: wrap;
  & > div {
    margin: 0.3rem;
  }   
`;
