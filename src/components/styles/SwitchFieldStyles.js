import styled from "styled-components";

export  const CustomButton = styled.div`
  border: 1px solid lightgrey;
  text-align: center;
  :hover {
    border-color: ${props => props.disabled? "none":" #a38fff"};
    border-right-width: ${props => props.disabled? "none":"1px !important"}
  }

  border-radius: 2px;
  cursor: ${props => props.disabled?  "not-allowed":"pointer"};;
`;

export const MySwitchFieldInput = styled.input`
  width: 1.3em;
  float: right;
  height: 1.4em;
  background-color: white;
  border-radius: 50%;
  vertical-align: middle;
  border: 1px solid #ddd;
  -webkit-appearance: none;
  //outline: none;
  cursor: ${props => props.disabled?  "not-allowed":"pointer"};;
  font-size: 16px;
  & > input[type='checkbox'] {
    font-size: 14px;
  }
  :hover {
    border-color: ${props => props.disabled? "none":" #a38fff"};
    border-right-width: ${props => props.disabled? "none":"1px !important"}
  }
  :checked {
    background-size: 90%;
    background-image: url("data:image/svg+xml,%3csvg xmlns='http://www.w3.org/2000/svg' viewBox='-1 -1 8 8'%3e%3cpath fill='%23fff' d='M6.564.75l-3.59 3.612-1.538-1.55L0 4.26 2.974 7.25 8 2.193z'/%3e%3c/svg%3e");
    background-color: #7f67ff;
  }
`;

export const CircleFilled = styled.div`
  background: ${props => props.color || "green"};
  width: 13px;
  height: 13px;
  border-radius: 50%;
  cursor: pointer;
  border: 1px solid #000000;
`;
