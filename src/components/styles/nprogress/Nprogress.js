import React, { useEffect } from "react";
import NProgress from 'nprogress'
import  "./Fancyprogress.less";
import 'nprogress/nprogress.css'

const Nprogress = ({isLoading}) => {
  useEffect(() => {
   if(isLoading===true){
     NProgress.start()
   }

    if(isLoading===false){
      NProgress.done()
    }
  }, [isLoading]);

return <></>;
};

export default Nprogress;
