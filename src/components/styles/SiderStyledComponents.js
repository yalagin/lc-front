import styled from 'styled-components';
import { Layout, PageHeader } from 'antd';
import { DoubleLeftOutlined, DoubleRightOutlined } from "@ant-design/icons";

const { Sider, Content, Footer } = Layout;

export const SiderStyledComponents = styled(Content)`
  margin: 16px 0;
  padding: 1rem 2rem 2rem;
  min-height: 100vh;
  box-shadow: 0px 1px 4px #e5e9f2;
  border-radius: 5px;
`;


export const CustomFooter = styled(Footer)`
  padding: 0;
  text-align: center;
`;
export const CustomSider = styled(Sider)`
  margin: 1rem;
  box-shadow: 0px 1px 4px #e5e9f2;
  border-radius: 5px;
  height: 95vh;
`;
export const CustomHeader = styled(PageHeader)`
  box-shadow: 0px 1px 4px #e5e9f2;
  border-radius: 5px;
  .ant-page-header-heading-extra{
    display: flex;
  }
`;
export const DoubleRightOutlinedStyled = styled(DoubleRightOutlined)`
  &&& {
    color: white !important;
    vertical-align: -0.025em;
  }
  svg{
    margin-left: -0.3rem;
  }
`;
export const DoubleLeftOutlinedStyled = styled(DoubleLeftOutlined)`
  &&&  {
    color: white !important;
    vertical-align: -0.025em;
  }
  svg{
    margin-left: -0.3rem;
  }
`;
