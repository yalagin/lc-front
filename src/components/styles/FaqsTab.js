import styled from "styled-components";
import {Tabs} from "antd";

const FaqsTab = styled(Tabs)`
  .ant-tabs-nav {
    background: rgba(243, 242, 242, 0.47);
    border-radius: 9px;
    overflow: hidden;
    width: 200px;
    margin-right: 1.5rem;
    //margin-top: 1rem;
  }

  .ant-tabs-ink-bar {
    display: none;
  }

  .ant-tabs-tab-active {
    background: #7f67ff;
    color: white;
  }


  .ant-tabs-tab.ant-tabs-tab-active .ant-tabs-tab-btn {
    color: white;
    text-shadow: 0 0 0.25px currentColor;
  }

  &&& .ant-tabs-tab {
    margin: 0;
  }

`

export default FaqsTab;