import styled from "styled-components";

export const ColorCheckBox = styled.div<{colorUrl: string | URL | undefined,checked?:boolean}>`
  margin: 1rem;
  height: 20px;
  width: 20px;
  background-image: ${props => `url(${props.colorUrl})`};
  background-color: ${props => props.color};
  background-size: cover;
  // border:  ${props =>props.checked? "1px solid #7f67ff":"0.2px solid #ebedf4"};
  background-repeat: repeat;
  cursor: pointer;
  background-blend-mode: multiply;
  box-shadow: ${props => props.checked ? "0 0 0px 4px rgb(127 103 255)":""};
  transition: all 0.3s linear;
  -webkit-user-select: none;
  -moz-user-select: none;
  -ms-user-select: none;
  user-select: none;
`;


export const ColorCheckBoxInSelected = styled(ColorCheckBox)`
  margin: auto;
`;
