import React from 'react';
import {ExclamationCircleOutlined} from '@ant-design/icons';
import { Field, useField } from "react-final-form";
import MyTextField from '../../input/MyFields/MyTextField';
import { Col, Radio, Row } from "antd";
import MySwitch from '../../input/MyFields/MySwitch';
import WhenFieldChanges from '../../input/MyFields/WhenFieldChanges';
import MyRadioGroup from "../../input/MyFields/MyRadioGroup";

const SpreadshirtComTemplate = ({wrapperCol}) => {
  const spreadshirtComIsShop = useField('spreadshirtComIsShop', { subscription: { value: true },type:'checkbox' })

  return (
    <>
      <ExclamationCircleOutlined style={{color: '#7f67ff'}} /> Design Template
      <br />
      <br />
      <Field
        subscription={{value: true}}
        wrapperCol={wrapperCol}
        name={ 'spreadshirtComSelectDesignTemplate'}
        component={MyTextField}
        hasFeedback
        placeholder={'Add template'}
        size={'large'}
      />
      <Row gutter={[16, 16]}>
        <Col span={4}>
          <ExclamationCircleOutlined style={{color: '#7f67ff'}} /> Marketplce:
        </Col>
        <Col span={24}>
          <Field
            subscription={{value: true}}
            name={ 'spreadshirtComIsMarketplace'}
            component={MySwitch}
            initialValue={null}
            hasFeedback
            allowNull
            type={'checkbox'}
            placeholder={'Add template'}
            size={'large'}
          />
        </Col>
        <Col span={24}>
          <ExclamationCircleOutlined style={{color: '#7f67ff'}} /> Shop:
        </Col>
        <Col span={4}>
          <MySwitch {...spreadshirtComIsShop} />
        </Col>
        <WhenFieldChanges
          field={ 'spreadshirtComIsShop'}
          becomes={false}
          set={ 'spreadshirtComShop'}
          to={undefined}
        />
        <Col span={20}>
          <Field
            subscription={{value: true}}
            // wrapperCol={wrapperCol}
            name={ 'spreadshirtComShop'}
            component={MyTextField}
            initialValue={null}
            hasFeedback
            allowNull
            placeholder={'(optional)'}
            size={'large'}
            disabled={!spreadshirtComIsShop.input.checked}
            wrapperCol={wrapperCol}
          />
        </Col>
      </Row>
    </>
  );
};

export default SpreadshirtComTemplate;
