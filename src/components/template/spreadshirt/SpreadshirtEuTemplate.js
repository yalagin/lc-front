import React from 'react';
import {ExclamationCircleOutlined} from '@ant-design/icons';
import { Field, useField } from "react-final-form";
import MyTextField from '../../input/MyFields/MyTextField';
import { Col, Radio, Row } from "antd";
import MySwitch from '../../input/MyFields/MySwitch';
import WhenFieldChanges from '../../input/MyFields/WhenFieldChanges';
import { designSpreadshirtEu } from "../../../utils/constants/constants";
import { OnChange } from "react-final-form-listeners";
import { fieldDisabledText } from "../../input/fieldDisabledText";
import MyRadioGroup from "../../input/MyFields/MyRadioGroup";

const SpreadshirtEuTemplate = ({wrapperCol}) => {
  const spreadshirtEuIsShop = useField('spreadshirtEuIsShop', {subscription: {value: true},type:"checkbox"});

  return (
    <>
      <ExclamationCircleOutlined style={{color: '#7f67ff'}} /> Design Template
      <br />
      <br />
      <Field
        subscription={{value: true}}
        wrapperCol={wrapperCol}
        name={'spreadshirtEuSelectDesignTemplate'}
        component={MyTextField}
        initialValue={null}
        hasFeedback
        allowNull
        placeholder={'Add template'}
        size={'large'}
      />
      <Row gutter={[16, 16]}>
        <Col span={4}>
          <ExclamationCircleOutlined style={{color: '#7f67ff'}} /> Marketplce:
        </Col>
        <Col span={24}>
          <Field
            subscription={{value: true}}
            name={ 'spreadshirtEuIsMarketplace'}
            component={MySwitch}
            initialValue={null}
            hasFeedback
            allowNull
            placeholder={'Add template'}
            size={'large'}
            type={"checkbox"}
          />
        </Col>
        <WhenFieldChanges
          field={ 'spreadshirtEuIsShop'}
          becomes={false}
          set={ 'spreadshirtEuShop'}
          to={undefined}
        />
        <Col span={24}>
          <ExclamationCircleOutlined style={{color: '#7f67ff'}} /> Shop:
        </Col>
        <Col span={4}>
          <MySwitch {...spreadshirtEuIsShop} />
        </Col>
        <Col span={20}>
          <Field
            subscription={{value: true}}
            // wrapperCol={wrapperCol}
            name={'spreadshirtEuShop'}
            component={MyTextField}
            initialValue={null}
            disabled={!spreadshirtEuIsShop.input.checked}
            hasFeedback
            allowNull
            placeholder={'(optional)'}
            size={'large'}
            wrapperCol={wrapperCol}
          />
        </Col>
        <Col span={4}>
          <ExclamationCircleOutlined style={{color: '#7f67ff'}} /> Targetmarket:
        </Col>
        <Col span={24}>
          <Field
            subscription={{value: true}}
            name={'spreadshirtEuTargetmarket'}
            component={MyRadioGroup}
            initialValue={null}
            hasFeedback
            allowNull
            size={'large'}
            // type={'radio'}
          >
            <Radio value={'DE'}>DE</Radio>
            <Radio value={'UK'}>UK</Radio>
          </Field>
        </Col>
      </Row>
    </>
  );
};

export default SpreadshirtEuTemplate;
