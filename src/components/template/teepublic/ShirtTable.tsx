import React, { useState } from 'react';
import { Radio, Select, Space, Spin, Table } from "antd";
import {
  HYDRAMEMBER,
  teepublicShirtPrintingIsEnabled,
  teepublicShirtSelection
} from "../../../utils/constants/constants";
import { TemplateSettingsColor } from '../../../interfaces/templatesettingscolor';
import { useQuery } from 'react-query';
import { fetch2 } from '../../../utils/dataAccess';
import qs from 'qs';
import { ColumnType } from 'antd/lib/table/interface';
import { ENTRYPOINT } from '../../../config/entrypoint';
import { ColorCheckBoxInSelected } from '../../styles/ColorCheckBox';
import { Field } from "react-final-form";
import MySelect from "../../input/MyFields/MySelect";
import MySwitch from "../../input/MyFields/MySwitch";
import { TemplateTeepublicShirtName } from "../../../interfaces/templateteepublicshirtname";

interface Props {
  prefix: string
}

const ShirtTable = ({prefix}:Props) => {

  const [queryParams, setQueryParams] = useState({
    pagination: false,
  });

  const shirtNamesQuery = useQuery('template_teepublic_shirt_names', () =>
    fetch2(
      `/template_teepublic_shirt_names?${qs.stringify(queryParams, {
        arrayFormat: 'brackets',
        skipNulls: true,
      })}`
    )
  );

  const columns = [
    {
      title: 'Item',
      dataIndex: 'name',
      key: 'name',
      width: "50%",
    },
    {
      title: 'Enable',
      render: (text, record, index) =>
          <Field
            style={{ marginBottom: "unset", width:'100%'}}
            name={`${prefix}${teepublicShirtPrintingIsEnabled}[${record["@id"]}]`}
            subscription={{ value: true }}
            component={MySwitch}
            hasFeedback
            type="checkbox"
            defaultValue={true}
          />
      ,
    },
    {
      title: 'Default Color',
      dataIndex: 'colors',
      width: "40%",
      render: (colors: TemplateSettingsColor[], record, index) => (
      <>
        <Field
          name={`${prefix}${teepublicShirtSelection}[${record["@id"]}].color`}
          subscription={{value: true}}
          component={MySelect}
          style={{ marginBottom: "unset", width:'100%'}}
          hasFeedback
          showSearch
          placeholder="Select a Color"
          optionFilterProp="children"
          filterOption={(input: string, option: {children: {props: {children: string[]}}}) => {
            return option?.children?.props.children[1].toLowerCase().indexOf(input.toLowerCase()) >= 0;
          }
          }
        >
        {colors.map(color => (
          <Select.Option key={color['@id']} value={color['@id']}>
            <Space>
              <ColorCheckBoxInSelected
                color={color.color}
                colorUrl={
                  color.image ? new URL(color.image.contentUrl, ENTRYPOINT) : color.colorUrl
                }
              />
              {color.name}
            </Space>
          </Select.Option>
        ))}
        </Field>
      </>
      ),
    },
  ] as ColumnType<TemplateTeepublicShirtName>[];

  return (
    <Spin spinning={shirtNamesQuery.isLoading} >{ shirtNamesQuery?.data &&
    <Table
      dataSource={shirtNamesQuery?.data[HYDRAMEMBER]}
      columns={columns}
      pagination={false}
      components={{
        header: {
          cell: () => <></>
        }
      }}
    />
    }</Spin>
  )
}

export default ShirtTable;
