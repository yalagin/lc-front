import React, { useState } from "react";
import { Tag, Input, Typography } from "antd";
import {
  DndContext,
  closestCenter,
  KeyboardSensor,
  PointerSensor,
  useSensor,
  useSensors,
} from '@dnd-kit/core';
import {
  arrayMove,
  SortableContext,
  sortableKeyboardCoordinates,
  rectSortingStrategy, useSortable, horizontalListSortingStrategy
} from "@dnd-kit/sortable";

import { CSS } from "@dnd-kit/utilities";
import { HYDRAMEMBER } from "../../../utils/constants/constants";
import styled from "styled-components";
const { Text, Link } = Typography;

const StyledTag = styled(Tag)`
  background: #FFFFFF;
  box-shadow: 0px 0px 2px rgba(0, 0, 0, 0.25);
  border-radius: 18px;
  transition:none; 
  margin: 4px ;
  cursor:grab;
  width: 100px;
`;


//color is id
//colorObject is object
const ForMap = ({ color, handleClose,colorObject}) => {
  const {
    attributes,
    listeners,
    setNodeRef,
    transform,
    transition,
  } = useSortable({id: color});

  const style = {
    transform: CSS.Transform.toString(transform),
    transition,
  };

  return (
    <StyledTag
      closable
      ref={setNodeRef}
      {...attributes}
      {...listeners}
      style={{ ...style  }}
      onClose={e => {
        e.preventDefault();
        handleClose(color);
      }}
    >
      <Text style={{width:"70px" }} ellipsis={{ tooltip: colorObject.name}}>{colorObject&&colorObject.name}</Text>
    </StyledTag>
  );
};

const EditableColorGroup = ({value, onChange,colors}) => {
  const sensors = useSensors(
    useSensor(PointerSensor),
    useSensor(KeyboardSensor, {
      coordinateGetter: sortableKeyboardCoordinates,
    })
  );
  const handleClose = removedTag => {
    onChange(value.filter(tag => tag !== removedTag));
  };

  const onDragEnd = result => {
    const {active, over} = result;
    if (active.id !== over.id) {
      const oldIndex = value.indexOf(active.id);
      const newIndex = value.indexOf(over.id);
      onChange(arrayMove(value, oldIndex, newIndex));
    }
  };

  // color={colors[HYDRAMEMBER].find(hydraColor => hydraColor['@id'] === color)}

  return (
    <DndContext sensors={sensors} collisionDetection={closestCenter} onDragEnd={onDragEnd}>
      <div style={{padding: 16}}>
        <SortableContext items={value} strategy={rectSortingStrategy}>
          { value.map((color, index) => (
            <ForMap key={color} color={color} index={index} handleClose={handleClose} colorObject={colors[HYDRAMEMBER].find(hydraColor => hydraColor['@id'] === color)} />
          ))}
        </SortableContext>
      </div>
    </DndContext>
  );
};

export default EditableColorGroup;
