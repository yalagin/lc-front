import React, { useState } from 'react';
import styled from 'styled-components';
import { Button, Col, Divider, Row, Space, Table  } from 'antd';
import { HYDRAMEMBER, teepublicColors } from "../../../utils/constants/constants";
import _ from 'lodash';
import Color from './Color';
import { useField } from 'react-final-form';
import ShirtTable from "./ShirtTable";
import { TemplateSettingsColor } from "../../../interfaces/templatesettingscolor";
import { ColSize } from "antd/lib/grid/col";
import { FormApi } from "final-form";
import ProductTable from "./ProductTable";

export const GreyButton = styled(Button)`
  background: #ebedf4;
`;

export const ColorHolder = styled.div`
  display: flex;
  flex-direction: row;
  border: 1px solid #EBEDF4;
  box-sizing: border-box;
  margin: 1rem;
  flex-wrap: wrap;
`;

export const ColorsTitle = styled.div`
  margin-left: 1rem;
  margin-top: 0.5rem;
  margin-bottom: -1rem;
`;

interface Props {
  colors:{[HYDRAMEMBER]:TemplateSettingsColor[]},
  prefix?:string,
  wrapperCol:ColSize,
  form:FormApi
}


const Teepublic = ({ colors, wrapperCol, form, prefix="" }:Props ) => {
  const [clickAllSwitcher, setClickAllSwitcher] = useState(true);
  const {
    input: {
      value: teepublicSelectedColors,
      onChange: teepublicSelectedColorsOnChange
    }
  } = useField(prefix+teepublicColors, { subscription: { value: true } })

  const clickAll = () => {
    form.batch(() => {
      form.change(prefix+teepublicColors, []);
      if (clickAllSwitcher) {
        colors[HYDRAMEMBER].forEach((color) => {
          form.change(prefix+teepublicColors, colors[HYDRAMEMBER].map(color => color['@id']));
        })
      }
      setClickAllSwitcher(prevState => !prevState);
    })
  }

  const clickLight = () => {
    form.change(prefix+teepublicColors, colors[HYDRAMEMBER].filter(color => color.isLight).map(color => color['@id']));
  }

  const clickDark = () => {
    form.change(prefix+teepublicColors, colors[HYDRAMEMBER].filter(color => color.isDark).map(color => color['@id']));
  }


  return (
    colors &&
    !_.isEmpty(colors[HYDRAMEMBER]) && (
      <>
        <Row gutter={[52, 32]}>
          <Col xl={wrapperCol}>
            {/*<Divider />*/}
            <ProductTable prefix={prefix}/>
          </Col>
          <Col xl={wrapperCol}>
            <Space style={{marginLeft:"1rem"}}>
              <GreyButton onClick={clickAll}>All/None</GreyButton>
              <GreyButton onClick={clickLight}>Light</GreyButton>
              <GreyButton onClick={clickDark}>Dark</GreyButton>
            </Space>
          <ColorHolder >
            <ColorsTitle>Choose colors</ColorsTitle>
            <Divider />
            {colors[HYDRAMEMBER].map(color => (
              <Color prefix={prefix} color={color} key={color['@id']} />
            ))}
          </ColorHolder>
          </Col>
        </Row>
      </>
    )
  );
};

export default Teepublic;
