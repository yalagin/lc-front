import React, { useState } from "react";
import { useQuery } from "react-query";
import { fetch2 } from "../../../utils/dataAccess";
import qs from "qs";
import {
  APPAREL,
  HYDRAMEMBER, PILLOWS, PINS,
  teepublicShirtPrintingIsEnabled,
  teepublicShirtSelection, TOTES, WALL_ART
} from "../../../utils/constants/constants";
import { Field, useField } from "react-final-form";
import { Table, Space, Spin } from "antd";
import MySwitch from "../../input/MyFields/MySwitch";
import { TemplateSettingsColor } from "../../../interfaces/templatesettingscolor";
import { ColumnType } from "antd/lib/table/interface";
import { TemplateTeepublicProductName } from "../../../interfaces/templateteepublicproductname";
import _ from "lodash";
import MyTextFieldWIthColorPicker from "../../input/MyFields/MyTextFieldWIthColorPicker";
import MyPercentageField from "../../input/MyFields/MyPersentagetField";
import ShirtTable from "./ShirtTable";
import { OnChange } from "react-final-form-listeners";

interface Props {
  colors?: TemplateSettingsColor[],
  prefix: string
}


const ProductTable = ({prefix}:Props) => {
  const [queryParams, setQueryParams] = useState({ pagination: false});
  const {
    input: {
      value: teepublicShirtPrintingIsEnabledValue,
      onChange: teepublicShirtPrintingIsEnabledOnChange
    }
  } = useField(prefix+teepublicShirtPrintingIsEnabled, { subscription: { value: true } })

  const productNamesQuery = useQuery('template_teepublic_product_names', () =>
    fetch2(
      `/template_teepublic_product_names?${qs.stringify(queryParams, {
        arrayFormat: 'brackets',
        skipNulls: true,
      })}`
    )
  );


  // @ts-ignore
  const columns = [
    {
      title: 'Name',
      dataIndex: 'name',
      key: 'name',
      width: '50%',
      render: (text, record, index) => (
        <>
          {text === APPAREL && (
            <OnChange name={`${prefix}isTeepublicProductEnabled[${record['@id']}]`}>
              {
                (value: any) => {
                  const copy = {...teepublicShirtPrintingIsEnabledValue}
                  Object.keys(copy).forEach(
                    (key: any) => (copy[key] = value)
                  )
                  teepublicShirtPrintingIsEnabledOnChange(
                    copy
                  );
                }
              }
            </OnChange>
          )}
          {_.capitalize(text)}
        </>
      ),
    },
    {
      title: 'Enable',
      render: (text, record, index) => (
        <Field
          style={{marginBottom: 'unset'}}
          name={`${prefix}isTeepublicProductEnabled[${record['@id']}]`}
          subscription={{value: true}}
          component={MySwitch}
          hasFeedback
          type="checkbox"
          defaultValue={true}
        />
      ),
    },
    {
      title: 'Color/Scale',
      dataIndex: 'colors',
      width: '40%',
      render: (colors: TemplateSettingsColor[], record, index) => {
        return (
          <>
            {' '}
            {!record.products && record.name !== APPAREL && (
              <Field
                // wrapperCol={{ span: 12 }}
                // style={{ marginBottom: "unset", width:'100%'}}
                // label={'Hash Color'}
                name={`${prefix}teepublicProduct[${record['@id']}][color]`}
                placeholder=" Hash Color"
                hasFeedback
                component={MyTextFieldWIthColorPicker}
                allowNull={true}
                parse={x => x}
              />
            )}
            { record.name === APPAREL && (
              <Field
                // name={`${prefix}${teepublicShirtSelection}[${record["@id"]}].scale`}
                name={`${prefix}${teepublicShirtSelection}.scale`}
                subscription={{ value: true }}
                component={MyPercentageField}
                label={"Scale"}
                // wrapperCol={{ span: 1 }}
                style={{ marginBottom: "unset", width:'100%'}}
                // labelCol={{ span: 12 }}
                hasFeedback
              />
            )}
            { (record.name === WALL_ART
            || record.name === PILLOWS
            || record.name === TOTES
            || record.name === PINS)
            && (
              <Field
                name={`${prefix}teepublicProduct[${record['@id']}][scale]`}
                subscription={{ value: true }}
                component={MyPercentageField}
                label={"Scale"}
                // wrapperCol={{ span: 1 }}
                style={{ marginBottom: "unset", width:'100%'}}
                // labelCol={{ span: 12 }}
                hasFeedback
              />
            )}
          </>
        );
      },
    },
  ] as ColumnType<TemplateTeepublicProductName>[];

  return  <> { productNamesQuery?.data &&
  <Table
    dataSource={productNamesQuery?.data[HYDRAMEMBER]}
    bordered
    columns={columns}
    pagination={false}
    expandable={{
      defaultExpandAllRows: true,
      rowExpandable: record => !!record.products||record.name==APPAREL,
      expandedRowRender: record => <>
        { record.name==APPAREL && <ShirtTable prefix={prefix}  /> }
        {record.products?.map(product => {
          return <>
            {!!record.products&& <Space>
            <Field
              name={`${prefix}teepublicProduct[${record["@id"]}][${product}].scale`}
              subscription={{ value: true }}
              component={MyPercentageField}
              label={product + ". Scale"}
              wrapperCol={{ span: 10 }}
              // labelCol={{ span: 12 }}
              hasFeedback
            />
            <Field
              wrapperCol={{ span: 24 }}
              // label={'Hash Color'}
              name={`${prefix}teepublicProduct[${record["@id"]}][${product}].color`}
              placeholder=" Hash Color"
              hasFeedback
              component={MyTextFieldWIthColorPicker}
              allowNull={true}
              parse={x => x}
            />
          </Space>}</>;
        })}
      </>
    }}
    rowKey={record => record["@id"]}
  />
}<Spin spinning={productNamesQuery.isLoading}  /></>;
};

export default ProductTable;
