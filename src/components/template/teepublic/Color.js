import React from 'react';
import { Tooltip } from 'antd';
import MyColorSelectorInput from '../mba/input/MyColorSelectorInput';
import { Field } from 'react-final-form';

const Color = ({ color, prefix="" }) => {
  return (
    <Tooltip placement="bottomLeft" title={color.name}>
      <Field
        subscription={{ value: true }}
        component={MyColorSelectorInput}
        name={prefix+`teepublicColors`}
        // component="input"
        value={color["@id"]}
        hasFeedback
        type="checkbox"
        color={color.color}
        colorUrl={color.colorUrl}
      />
    </Tooltip>
  );
};

export default Color;
