import React, {useState} from 'react';
import { Button, Divider, List, message, Modal, Spin, Typography } from "antd";
import BigButton from '../../input/button/BigButton';
import { useMutation, useQuery } from "react-query";
import { fetch2 } from "../../../utils/dataAccess";
import _ from 'lodash';
import { HYDRAMEMBER } from "../../../utils/constants/constants";

const LoadModal = ({data,form,fetchUserTemplates}) => {
  const [isModalVisible, setIsModalVisible] = useState(false);
  const {Text} = Typography;
  const showModal = () => {
    setIsModalVisible(true);
  };

  const DeleteTemplate = useMutation(template => fetch2(template["@id"], {
    method: 'DELETE',
    body: JSON.stringify(template),
  }), {
    onSuccess: (data, variables, context) => {
      message.info(`Deleted!`);
      fetchUserTemplates.refetch();
    }
  })

  const fetchUserPredefinedTemplates = useQuery('PredefinedTemplates', () =>
    fetch2('/templates?pagination=false&exists[owner]=false')
  );

  const editTemplate = useMutation(data => {
    return fetch2(data.item['@id'], {
      method: 'PUT',
      headers: new Headers({'Content-Type': 'application/ld+json'}),
      body: JSON.stringify({...data.item, name:data.name}),
    });}, {
    onSuccess: (data, variables, context) => {
      message.info(`Saved!`);
      fetchUserTemplates.refetch();
    }
  })


  // const handleOk = () => {
  //   setIsModalVisible(false);
  //   handleSubmit()
  // };

  const handleCancel = () => {
    setIsModalVisible(false);
  };

  const load = (item) =>{
    form.change('',item);
    handleCancel()
  }

  const loadPredefinedTemplates = (item) =>{

    delete item["id"];
    delete item["@id"];

    form.change('',item);
    handleCancel()
  }

  return (
    <>
      <BigButton size={'large'} onClick={showModal}>
        Load
      </BigButton>
      <Modal
        centered
        title="Load template"
        visible={isModalVisible}
        onCancel={handleCancel}
        footer={[
          <Button key="back" onClick={handleCancel}>
            Cancel
          </Button>,
        ]}
      >
        <Spin spinning={editTemplate.isLoading}>

            <Divider orientation="left" >
              Your Templates
            </Divider>
        <List
          itemLayout="horizontal"
          dataSource={data}
          renderItem={item => (
            <List.Item   actions={[<a onClick={()=>DeleteTemplate.mutate(item)} key="list-loadmore-edit">Delete</a>,/* <a key="list-loadmore-more">More</a>,*/ <a onClick={()=>load(item)} key="list-loadmore-more">Load</a>]} >
              <List.Item.Meta
                title={<Text editable={{ onChange: (name)=> {
                    editTemplate.mutate({ name, item });
                  } }}>{item.name}</Text>}
              />
            </List.Item>
          )}
        />

          { !!fetchUserPredefinedTemplates?.data &&
          <>
            <Divider orientation="left" >
              Predefined Templates
            </Divider>
            <List
            itemLayout="horizontal"
            dataSource={fetchUserPredefinedTemplates?.data[HYDRAMEMBER]}
            renderItem={item => (
              <List.Item   actions={[/*<a onClick={()=>DeleteTemplate.mutate(item)} key="list-loadmore-edit">Delete</a>, <a key="list-loadmore-more">More</a>,*/ <a onClick={()=>loadPredefinedTemplates(item)} key="list-loadmore-more">Load</a>]} >
                <List.Item.Meta
                  title={<Text>{item.name}</Text>}
                />
              </List.Item>
            )}
          /></>}
        </Spin>
      </Modal>
    </>
  );
};

export default LoadModal;
