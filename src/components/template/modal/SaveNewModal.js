import React, {useState} from 'react';
import { Button, Col, List, message, Modal, Spin, Typography, Row } from "antd";
import MyTextField from '../../input/MyFields/MyTextField';
import {Field, useField} from 'react-final-form';
import BigButton from '../../input/button/BigButton';
import { useMutation } from "react-query";
import { fetch2 } from "../../../utils/dataAccess";
import styled from "styled-components";

const StyledRow = styled(Row)`
   display: flex;
   flex-flow: row wrap;
   justify-content: space-around;
   align-items: center;
`;

const SaveNewModal = ({handleSubmit, data,fetchUserTemplates,form}) => {
  const [isModalVisible, setIsModalVisible] = useState(false);

  const DeleteTemplate = useMutation(template => fetch2(template["@id"], {
    method: 'DELETE',
    body: JSON.stringify(template),
  }), {
    onSuccess: (data, variables, context) => {
      message.info(`Deleted!`);
      fetchUserTemplates.refetch();
    }
  })
  const editTemplate = useMutation(data => {
    return fetch2(data.item['@id'], {
      method: 'PUT',
      headers: new Headers({'Content-Type': 'application/ld+json'}),
      body: JSON.stringify({...data.item, name:data.name}),
    });}, {
    onSuccess: (data, variables, context) => {
      message.info(`Saved!`);
      fetchUserTemplates.refetch();
    }
  })

  const showModal = () => {
    setIsModalVisible(true);
  };

  const handleOk = (isNew) => {
    if(isNew){
      form.change('@id',null);
    }
    setIsModalVisible(false);
    handleSubmit();
  };

  const handleCancel = () => {
    setIsModalVisible(false);
  };

  const required = value => (value ? undefined : 'Required');
  const id = useField('id', {subscription: {value: true}});

  function saveAsAnotherTemplate(item) {
    form.change('@id',item['@id']);
    form.change('id',item['id']);
    form.change('name',item['name']);
    setIsModalVisible(false);
    handleSubmit();
  }

  return (
    <>
      <BigButton size={'large'} type="primary" onClick={showModal}>
        Save
      </BigButton>
      <Modal
        centered
        title="Save template"
        visible={isModalVisible}
        okText={'Save'}
        onOk={handleOk}
        onCancel={handleCancel}
        footer={[

        ]}
      >
        <StyledRow>< Field
          labelCol={{span: 24}}
          wrapperCol={{span: 24}}
          label={"Template Name :"}
          name="name"
          subscription={{value: true}}
          validate={required}
          hasFeedback
          component={MyTextField}
          allowNull={true}
        />
          <Col span={4}><Button style={{marginTop:"1rem"}} key="submit" type="primary" onClick={handleOk}>
            Save
          </Button></Col></StyledRow>
        <Spin spinning={editTemplate.isLoading}>
          <List
            itemLayout="horizontal"
            dataSource={data}
            renderItem={item => (
              <List.Item
                actions={[
                  <a onClick={() => DeleteTemplate.mutate(item)} key="list-loadmore-edit">
                    Delete
                  </a>,
                  <a
                    onClick={() => saveAsAnotherTemplate(item)}
                  >
                    Replace
                  </a>,
                ]}
              >
                <List.Item.Meta title={<Typography.Text  editable={{ onChange: (name)=> {
                    editTemplate.mutate({ name, item });
                  } }} >{item.name}</Typography.Text>} />
              </List.Item>
            )}
          />
        </Spin>
      </Modal>
    </>
  )
}

export default SaveNewModal;
