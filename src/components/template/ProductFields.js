import React from 'react';
import { Col, Collapse, Divider, Radio, Row, Select, Spin, Typography } from "antd";
import { ExclamationCircleOutlined } from '@ant-design/icons';
import { HYDRAMEMBER } from '../../utils/constants/constants';
import { Field, useField } from 'react-final-form';
import MbaPanel from './mba/MbaPanel';
import Teepublic from './teepublic/Teepublic';
import MyTextField from '../input/MyFields/MyTextField';
import MyColorPicker from '../input/MyFields/MyColorPicker';
import SaveNewModal from './modal/SaveNewModal';
import LoadModal from './modal/LoadModal';
import SaveExistedModal from './modal/SaveExistedModal';
import SpreadshirtEuTemplate from './spreadshirt/SpreadshirtEuTemplate';
import SpreadshirtComTemplate from './spreadshirt/SpreadshirtComTemplate';
import MyRadioGroup from "../input/MyFields/MyRadioGroup";
import MySelect from "../input/MyFields/MySelect";

const { Panel } = Collapse;
const { Title, Text } = Typography;
const { Option } = Select;

const ProductFields = ({
                         handleSubmit,
                         form,
                         fetchUserTemplates,
                         dataColor,
                         products,
                         saveTemplate
                       }) => {
  const wrapperCol = { span: 12 };
  const id = useField('id', { subscription: { value: true } })
  // const name = useField('name', { subscription: { value: true } }) it somehow make field slow


  return (
    <form onSubmit={handleSubmit}>
      <Spin tip="Loading..." spinning={saveTemplate.isLoading}>
        {id.input.value && <Typography.Title style={{ textAlign: "center" }} level={2}>Edit template</Typography.Title>}
        <Collapse
          ghost
          expandIconPosition={'right'}
          // defaultActiveKey={"Teepublic"}
          // defaultActiveKey={designs.map(design => design.slice(6))}
        >
          <Panel
            header={
              <Divider style={{ margin: '0' }} orientation="left">
                MBA
              </Divider>
            }
            key="MBA"
          >
            <Title level={5}>Select Products</Title>
            <Text type="secondary">
              We’ll publish your design on the selected products and marketplaces.
            </Text>
            <MbaPanel data={products} form={form} />
          </Panel>

          <Panel
            header={
              <Divider style={{ margin: '0' }} orientation="left">
                Teepublic
              </Divider>
            }
            key="Teepublic"
          >
            <Title level={5}>Product colors</Title>
            <Text type="secondary">
              Activate the background colors that you want to make available for your enabled
              products.
            </Text>
            <br />
            <br />
            {dataColor && <Teepublic wrapperCol={wrapperCol} colors={dataColor} form={form} />}
          </Panel>

          <Panel
            header={
              <Divider style={{ margin: '0' }} orientation="left">
                Spreadshirt EU
              </Divider>
            }
            key="SpreadshirtEu"
          >
            <SpreadshirtEuTemplate wrapperCol={wrapperCol} />
          </Panel>
          <Panel
            header={
              <Divider style={{ margin: '0' }} orientation="left">
                Spreadshirt COM
              </Divider>
            }
            key="SpreadshirtCom"
          >
            <SpreadshirtComTemplate wrapperCol={wrapperCol} />
          </Panel>
          <Panel
            header={
              <Divider style={{ margin: '0' }} orientation="left">
                Redbubble
              </Divider>
            }
            key="RedBubble"
          >
            <ExclamationCircleOutlined style={{ color: '#7f67ff' }} /> Duplication URL
            <br />
            <br />
            <Field
              subscription={{ value: true }}
              size={'large'}
              wrapperCol={wrapperCol}
              name="redBubbleDuplicationURL"
              component={MyTextField}
              hasFeedback
              allowNull={true}
              parse={x => x}
            />
          </Panel>

          <Panel
            header={
              <Divider style={{ margin: '0' }} orientation="left">
                Displate
              </Divider>
            }
            key="Displate"
          >
            <Field
              subscription={{ value: true }}
              name="displateColorPicker"
              component={MyColorPicker}
              hasFeedback
            />
          </Panel>

          <Panel
            header={
              <Divider style={{ margin: '0' }} orientation="left">
                Zazzle
              </Divider>
            }
            key="Zazzle"
          >
            <ExclamationCircleOutlined style={{ color: '#7f67ff' }} /> Design Template
            <br />
            <br />
            <Field
              subscription={{ value: true }}
              wrapperCol={wrapperCol}
              name="zazzleSelectDesignTemplate"
              component={MyTextField}
              hasFeedback
              placeholder={'Add template'}
              size={'large'}
            />
            <br />
            <br />
            <ExclamationCircleOutlined style={{ color: '#7f67ff' }} /> Store
            <br />
            <br />
            <Field
              subscription={{ value: true }}
              size={'large'}
              wrapperCol={wrapperCol}
              name="zazzleStore"
              component={MyTextField}
              hasFeedback
              allowNull={true}
              parse={x => x}
            />
          </Panel>
          <Panel
            header={
              <Divider style={{ margin: '0' }} orientation="left">
                Society6
              </Divider>
            }
            key="Society6"
          >
            <ExclamationCircleOutlined style={{ color: '#7f67ff' }} /> Colors
            <br />
            <br />
            <Field
              subscription={{ value: true }}
              size={'large'}
              wrapperCol={wrapperCol}
              name="society6Colors"
              component={MyTextField}
              hasFeedback
              allowNull={true}
              parse={x => x}
            />
            <br />
            <br />
          </Panel>
          <Panel
            header={
              <Divider style={{ margin: '0' }} orientation="left">
                Teespring
              </Divider>
            }
            key="Teespring"
          >
            <ExclamationCircleOutlined style={{ color: '#7f67ff' }} /> Duplication URL
            <br />
            <br />
            <Field
              subscription={{ value: true }}
              size={'large'}
              wrapperCol={wrapperCol}
              name="teespringDuplicationURL"
              component={MyTextField}
              hasFeedback
              allowNull={true}
              parse={x => x}
            />
          <ExclamationCircleOutlined style={{ color: '#7f67ff' }} /> Shop
          <br />
          <br />
          <Field
            subscription={{ value: true }}
            size={'large'}
            wrapperCol={wrapperCol}
            name="teespringShop"
            component={MyTextField}
            hasFeedback
            allowNull={true}
            parse={x => x}
          />
        </Panel>
          <Panel
            header={
              <Divider style={{ margin: '0' }} orientation="left">
                Printful
              </Divider>
            }
            key="Printful"
          >
            <ExclamationCircleOutlined style={{ color: '#7f67ff' }} /> Colors
            <br />
            <br />
            <Field
              subscription={{ value: true }}
              size={'large'}
              wrapperCol={wrapperCol}
              name="printfulColors"
              component={MyTextField}
              hasFeedback
              allowNull={true}
              parse={x => x}
            />
            <ExclamationCircleOutlined style={{ color: '#7f67ff' }} /> Store Name
            <br />
            <br />
            <Field
              subscription={{ value: true }}
              size={'large'}
              wrapperCol={wrapperCol}
              name="printfulStoreName"
              component={MyTextField}
              hasFeedback
              allowNull={true}
              parse={x => x}
            />
            <br />
            <br />
            <ExclamationCircleOutlined style={{ color: '#7f67ff' }} /> Product Name
            <br />
            <br />
            <Field
              subscription={{ value: true }}
              size={'large'}
              wrapperCol={wrapperCol}
              name="printfulProductName"
              component={MyTextField}
              hasFeedback
              allowNull={true}
              parse={x => x}
            />
          </Panel>
          <Panel
            header={
              <Divider style={{ margin: '0' }} orientation="left">
                Shirtee
              </Divider>
            }
            key="Shirtee"
          >
            <ExclamationCircleOutlined style={{ color: '#7f67ff' }} /> Design Template
            <br />
            <br />
            <Field
              subscription={{ value: true }}
              wrapperCol={wrapperCol}
              name="shirteeSelectDesignTemplate"
              component={MyTextField}
              hasFeedback
              placeholder={'Add template'}
              size={'large'}
            />
            <ExclamationCircleOutlined style={{ color: '#7f67ff' }} /> Shop
            <br />
            <br />
            <Field
              subscription={{ value: true }}
              wrapperCol={wrapperCol}
              name="shirteeShop"
              component={MyTextField}
              hasFeedback
              placeholder={'Add Shop'}
              size={'large'}
            />
            <ExclamationCircleOutlined style={{ color: '#7f67ff' }} /> Marketplace
            <br/>
            <br/>
            <Field
              subscription={{ value: true }}
              wrapperCol={wrapperCol}
              name="shirteeMarketplace"
              // component={MyTextField}
              hasFeedback
              size={'large'}>
              {(props) => (
                <MyRadioGroup {...props}>
                  <Radio value={'en'}>English</Radio>
                  <Radio value={'de'}>German</Radio>
                  <Radio value={'es'}>Shirtee ES</Radio>
                  <Radio value={'it'}>Shirtee IT</Radio>
                  <Radio value={'fr'}>Shirtee FR</Radio>
                </MyRadioGroup>
              )}
            </Field>
            <ExclamationCircleOutlined style={{ color: '#7f67ff' }} /> Description Template
            <br/>
            <br/>
            <Field
              subscription={{ value: true }}
              wrapperCol={wrapperCol}
              name="shirteeDescriptionTemplate"
              // component={MyTextField}
              hasFeedback
              size={'large'}>
              {(props) => (
                <MySelect {...props}>
                  <Option value={'Deutsch (Deutschland-Österreich-Schweiz)'}>German</Option>
                  <Option value={'English (US/UK)'}>English</Option>
                  <Option value={'French (France)'}>French</Option>
                  <Option value={'Italiana (Italian)'}>Italian</Option>
                  <Option value={'Español (Spain)'}>Spain</Option>
                </MySelect>
              )}
            </Field>
          </Panel>
        </Collapse>
        <br/>
        <Row gutter={[16, 16]}>
          <Col xs={24} sm={24} md={12} lg={12} xl={12} xxl={8}>
            {!id.input.value && <SaveNewModal data={fetchUserTemplates.data && fetchUserTemplates.data[HYDRAMEMBER]}
                                              handleSubmit={handleSubmit} fetchUserTemplates={fetchUserTemplates}
                                              form={form} />}
            {id.input.value && <SaveExistedModal data={fetchUserTemplates.data && fetchUserTemplates.data[HYDRAMEMBER]}
                                                 handleSubmit={handleSubmit} fetchUserTemplates={fetchUserTemplates}
                                                 form={form} />}
          </Col>
          <Col xs={24} sm={24} md={12} lg={12} xl={12} xxl={8}>
            {fetchUserTemplates.data && (
              <LoadModal data={fetchUserTemplates.data[HYDRAMEMBER]} form={form}
                         fetchUserTemplates={fetchUserTemplates} />
            )}
          </Col>
        </Row>
      </Spin>
    </form>
  );
};

export default ProductFields;
