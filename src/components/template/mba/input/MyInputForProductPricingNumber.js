import React from 'react';
import { Form, InputNumber } from 'antd';

const FormItem = Form.Item;

const MyInputForProductPricingNumber = ({
                                          input,
                                          meta,
                                          children,
                                          hasFeedback,
                                          label,
                                          labelCol,
                                          wrapperCol,
                                          placeholder,
                                          size,
                                          marketplace,
                                          disabled,
                                          min,
                                          max,
                                          ...rest
                                        }) => {
  const hasError = meta.touched && meta.invalid && !meta.dirtySinceLastSubmit;
  return (
    <FormItem
      label={label}
      validateStatus={hasError ? "error" : "success"}
      hasFeedback={hasFeedback && hasError}
      help={(hasError && meta.error) || (hasError && meta.submitError)}
      labelCol={labelCol}
      wrapperCol={wrapperCol}
      {...rest}
    >
      {/*<Input {...input} size={size} allowClear placeholder={placeholder} />*/}
      <InputNumber
        {...input}
        size={'small'}
        min={min??1}
        max={max}
        // defaultValue={15}
        formatter={value => `${marketplace.currencySymbol} ${value}`}
        disabled={disabled}
        placeholder={marketplace.currencySymbol}
        stringMode
      />
    </FormItem>
  );
};

export default MyInputForProductPricingNumber;
