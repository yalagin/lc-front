import React, { useRef } from "react";
import { Form, notification } from "antd";
import { ColorCheckBox } from "../../../styles/ColorCheckBox";
import { useField } from "react-final-form";
import _ from "lodash";

const FormItem = Form.Item;

const MyColorSelectorInput = ({
                                input,
                                meta,
                                children,
                                hasFeedback,
                                label,
                                labelCol,
                                wrapperCol,
                                placeholder,
                                size,
                                color,
                                colorUrl,
                                maxColors,
                                ...rest
                              }) => {
  const hasError = meta.touched && meta.invalid && !meta.dirtySinceLastSubmit;
  const ref = useRef(null)
  const { input: {value:mbaColorsArray}, } = useField( input.name, {subscription: {value: true}});

  return (
    <FormItem
      label={label}
      validateStatus={hasError ? "error" : "success"}
      hasFeedback={hasFeedback && hasError}
      help={(hasError && meta.error) || (hasError && meta.submitError)}
      labelCol={labelCol}
      wrapperCol={wrapperCol}
      {...rest}
    >
      <input style={{display:'none'}}  ref={ref} {...input}/>
      <ColorCheckBox
        type="checkbox"
        color={color}
        colorUrl={colorUrl}
        checked={input.checked}
        onClick={() => {
          if(maxColors&&!input.checked) {
            if (!_.isArray(mbaColorsArray) || mbaColorsArray.length < maxColors) {
              ref.current.click();
            } else {
              notification.error({
                message: 'Color Limit',
                description: `Can not select more than ${maxColors} colors`,
              });
            }
          } else {
            ref.current.click();
          }
        }} />
    </FormItem>
  );
};

export default MyColorSelectorInput;
