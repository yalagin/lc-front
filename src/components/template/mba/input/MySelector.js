import React from 'react';
import {Form, Input, Select} from 'antd';
import _ from "lodash";

const FormItem = Form.Item;

const MySelector  =  ({
                     input:{value,onChange},
                     meta,
                     children,
                     hasFeedback,
                     label,
                     labelCol,
                     wrapperCol,
                     placeholder,
                     size,
                     ...rest
                   }) => {
  const hasError = meta.touched && meta.invalid && !meta.dirtySinceLastSubmit;
  return (
    <FormItem
      label={label}
      validateStatus={hasError ? 'error' : 'success'}
      hasFeedback={hasFeedback && hasError}
      help={(hasError && meta.error) || (hasError && meta.submitError)}
      labelCol={labelCol}
      wrapperCol={wrapperCol}
      {...rest}
    >
      <Select
        value={_.isEmpty(value)?[]:value} onChange={onChange} size={size} allowClear placeholder={placeholder}
        accordion={'true'}
        mode="tags"
        tokenSeparators={[',']}
      >
        {children}
      </Select>
    </FormItem>
  );
};

export default MySelector;
