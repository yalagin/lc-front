import React from 'react';
import {HYDRAMEMBER} from '../../../utils/constants/constants';
import Product from './Product';

const MbaPanel = ({data, ...rest}) => {
  return (
    data && data[HYDRAMEMBER].map(product => <Product key={product.id} product={product} {...rest} />)
  );
};

export default MbaPanel;
