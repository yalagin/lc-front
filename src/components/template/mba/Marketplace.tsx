import React, { useEffect } from "react";
import styled from 'styled-components';
import { CentredDiv, DownCentredDiv } from '../../styles/TemplateStyles';
import { Field, useField } from 'react-final-form';
import MyInputForProductPricingNumber from './input/MyInputForProductPricingNumber';
import MyCheckbox from '../../input/MyFields/MyCheckbox';
import { TemplateSettingsMbaProductPrice } from "../../../interfaces/templatesettingsmbaproductprice";

export const MarketplaceSelectHolder = styled.div`
  border: 1px solid #ebedf4;
  box-sizing: border-box;
`;

const Marketplace = ({product, prefix ="", priceForMarketplace}
                       : {product:string,prefix:string,priceForMarketplace:TemplateSettingsMbaProductPrice}) => {
  const {marketPlace:marketplace} =  priceForMarketplace;
  const { input: { value: isNotDisabled }, } = useField(`${prefix}mbaProductCheckbox[${product}[${marketplace["@id"]}]`, { subscription: { value: true } });
  const { input: { value: mbaProductPublished }, } = useField(`${prefix}mbaProductPublished[${product}[${marketplace["@id"]}]`, { subscription: { value: true } });
  const { input: {onChange }, } = useField(`${prefix}mbaProductPrice[${product}][${marketplace["@id"]}]`, { subscription: { value: true } });

  useEffect(() => {
    if(isNotDisabled===false){
      onChange(null);
    }
  }, [isNotDisabled]);

  return (
    <MarketplaceSelectHolder>
      <CentredDiv>
        <Field
          name={`${prefix}mbaProductCheckbox[${product}[${marketplace["@id"]}]`}
          subscription={{ value: true }}
          component={MyCheckbox}
          hasFeedback
          type="checkbox"
          disabled={mbaProductPublished}
        >
          {marketplace.name}
        </Field>
      </CentredDiv>
      <DownCentredDiv>
        <Field
          name={`${prefix}mbaProductPrice[${product}][${marketplace["@id"]}]`}
          component={MyInputForProductPricingNumber}
          subscription={{ value: true }}
          hasFeedback
          disabled={(!isNotDisabled||mbaProductPublished)}
          marketplace={marketplace}
          allowNull={false}
          min={priceForMarketplace.lowerBoundary}
          max={priceForMarketplace.upperBoundary}
        />
      </DownCentredDiv>
    </MarketplaceSelectHolder>
  );
};

export default Marketplace;
