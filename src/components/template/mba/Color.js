import React from 'react';
import { Tooltip } from 'antd';
import { ENTRYPOINT } from '../../../config/entrypoint';
import { Field } from 'react-final-form';
import MyColorSelectorInput from './input/MyColorSelectorInput';

const Color = ({ productColor, setImage, product,prefix= "",maxColors }) => {
  const onHover = () => {
    setImage(productColor?.image);
  };

  return (
    <Tooltip placement="bottomLeft" title={productColor.color.name}>
      <Field
        subscription={{ value: true }}
        component={MyColorSelectorInput}
        name={`${prefix}mbaColors[${product}]`}
        hasFeedback
        value={productColor["@id"]}
        color={productColor.color.color}
        type="checkbox"
        colorUrl={
          productColor.color.image
            ? new URL(productColor.color.image.contentUrl, ENTRYPOINT)
            : productColor.color.colorUrl
        }
        onMouseEnter={onHover}
        onMouseLeave={onHover}
        onMouseOut={onHover}
        maxColors={maxColors}
      />
    </Tooltip>
  );
};

export default Color;
