import React, { useState } from 'react';
import { Typography } from 'antd';
import Marketplace from './Marketplace';
import Color from './Color';
import _ from 'lodash';
import Image from '../../mediaobject/image/Image';
import {
  CentredDiv,
  ColorHolder,
  DivCol,
  DivRow,
  DownTitleCentredDiv,
  ImageContainer,
  MarginLeft,
  MarketplaceHolder,
  MarketplaceTitleHolder,
  ProductHeader,
  ProductInformationHolder,
  SelectMarketplacePricing,
  StyledCard,
} from '../../styles/TemplateStyles';
import { Field } from 'react-final-form';
import MyValidationField from '../../input/MyFields/MyValidationField';
import MySlider from '../../input/MyFields/MySlider';

const { Text } = Typography;

const Product = ({ product, prefix = "" }) => {
  const [image, setImage] = useState(
    !_.isEmpty(product.productColors[0]) ? product.productColors[0].image : null
  );

  const maxColors = 10;
  const maxArrayCountValue = value => !_.isArray(value) || value.length <= maxColors ? undefined : `Can not select more than ${maxColors} colors`;
  const composeValidators = (...validators) => value => validators.reduce((error, validator) => error || validator(value), undefined);


  const Colors = product.productColors.filter(color => !color.isNotAvilableInJapan).map(color =>
    <Color maxColors={maxColors} product={product["@id"]} setImage={setImage} productColor={color} key={color.id}
           prefix={prefix} />)

  const ColorsThatAreNotAvailableInJapan = product.productColors.filter(color => color.isNotAvilableInJapan).map(color =>
    <Color maxColors={maxColors} product={product["@id"]} setImage={setImage} productColor={color} key={color.id}
           prefix={prefix} />)

  return (
    <StyledCard>
      <div>
        <ProductHeader> <Text strong>{product.name}</Text></ProductHeader>
        <ImageContainer><Image value={image} preview={true} /></ImageContainer>
      </div>
      <ProductInformationHolder>
        <SelectMarketplacePricing>Select Marketplace and Pricing</SelectMarketplacePricing>
        <MarketplaceHolder>
          <MarketplaceTitleHolder>
            <CentredDiv><Text type="secondary">Marketplaces</Text></CentredDiv>
            <DownTitleCentredDiv><Text type="secondary">Pricing</Text></DownTitleCentredDiv>
          </MarketplaceTitleHolder>
          {product.priceForMarketplace.map(price => <Marketplace key={price['@id']}
                                                                 priceForMarketplace={price}
                                                                 product={product["@id"]}
                                                                 prefix={prefix} />)}
        </MarketplaceHolder>
        <MarginLeft>Select Colors</MarginLeft>
        <ColorHolder>
          {!_.isEmpty(Colors) && (
            <DivCol>
              <Text type="secondary">All marketplaces</Text>
              <DivRow>{Colors}</DivRow>
            </DivCol>
          )}
          {!_.isEmpty(ColorsThatAreNotAvailableInJapan) && (
            <DivCol>
              <Text type="secondary">Additional colors (not available in japan)</Text>
              <DivRow>{ColorsThatAreNotAvailableInJapan}</DivRow>
            </DivCol>
          )}
          <Field
            // subscription={{ value: true }}
            component={MyValidationField}
            name={`${prefix}mbaColors[${product["@id"]}]`}
            validate={maxArrayCountValue}
          />
        </ColorHolder>
        <MarginLeft>Select Scale</MarginLeft>
        <MarginLeft>
          <Field
            component={MySlider}
            name={`${prefix}mbaScale[${product["@id"]}]`}
          />
        </MarginLeft>
      </ProductInformationHolder>
    </StyledCard>
  );
};

export default Product;
