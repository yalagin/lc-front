import React from 'react';
import {Form} from 'react-final-form';
import {fetch2} from '../../utils/dataAccess';
import {useMutation, useQuery} from 'react-query';
import arrayMutators from 'final-form-arrays';
import ProductFields from './ProductFields';
import {message} from 'antd';
import {mbaProducts} from '../../utils/urls';

const ProductsForm = () => {
  const saveTemplate = useMutation(
    template =>
      fetch2('/templates', {
        method: 'POST',
        body: JSON.stringify(template),
      }),
    {
      onSuccess: (data, variables, context) => {
        message.info(`Saved!`);
        fetchUserTemplates.refetch();
      },
    }
  );

  const editTemplate = useMutation(
    template =>
      fetch2(template['@id'], {
        method: 'PUT',
        headers: new Headers({'Content-Type': 'application/ld+json'}),
        body: JSON.stringify(template),
      }),
    {
      onSuccess: (data, variables, context) => {
        message.info(`Saved!`);
        fetchUserTemplates.refetch();
      },
      onError:()=>{
        message.error(`Something is wrong!`);
      }
    }
  );

  function onSubmit(values, form) {
    console.log(values);
    if (values['@id']) {
      editTemplate.mutate(values);
    } else {
      saveTemplate.mutate(values);
    }
    form.restart();
  }

  const {isLoading, error, data: products} = useQuery('mba_products', mbaProducts, {
    staleTime: 60 * 60 * 1000,
  });

  const fetchUserTemplates = useQuery('templates', () =>
    fetch2('/templates?pagination=false&owner=' + localStorage.getItem('id'))
  );

  const {
    isLoading: isLoadingColor,
    error: errorColor,
    data: dataColor,
  } = useQuery('template_settings_colors?isTeepublic=true', () =>
    fetch2('/template_settings_colors?pagination=false&isTeepublic=true')
  );

  if (isLoading || isLoadingColor) return 'Loading...';
  if (error || errorColor || saveTemplate.isError) return 'Error =(';

  return (
    <Form
      subscription={{submitting: true}}
      onSubmit={onSubmit}
      mutators={{
        // potentially other mutators could be merged here
        ...arrayMutators,
      }}
      initialValues={{
        name: null,
        spreadshirtEuSelectDesignTemplate: null,
        spreadshirtComSelectDesignTemplate: null,
        shirteeSelectDesignTemplate: null,
        zazzleSelectDesignTemplate: null,
        redBubbleDuplicationURL: null,
        displateColorPicker: null,
        zazzleStore: null,
        society6Colors: null,
        printfulStoreName: null,
        printfulProductName: null,
        printfulColors: null,
        teespringDuplicationURL: null,
        teepublicColors: null,
        mbaProductPrice: null,
        mbaColors: null,
        mbaScale: null,
        mbaProductCheckbox: {},
        mbaProductPublished: {},
        createdAt: null,
        updatedAt: null,
        teepublicShirtPrintingSelection: 'front',
      }}
      fetchUserTemplates={fetchUserTemplates}
      dataColor={dataColor}
      products={products}
      saveTemplate={saveTemplate}
      component={ProductFields}
      debug={process.env.REACT_APP_SPY && console.log}
    />
  );
};

export default ProductsForm;
