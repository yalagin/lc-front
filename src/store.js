import {applyMiddleware, combineReducers, createStore} from 'redux';
import {reducer as form} from 'redux-form';
import user from './reducers/user';
import person from './reducers/person';
import project from './reducers/project';
import mediaobject from './reducers/mediaobject';
import design from './reducers/design';
import refreshToken from './reducers/auth/refreshToken';
import {composeWithDevTools} from 'redux-devtools-extension';
import {Jwt} from './actions/auth/refreshToken';
import thunk from 'redux-thunk';

const store = () =>
  createStore(
    combineReducers({
      form,
      /* reducers */
      user,
      person,
      project,
      mediaobject,
      design,
      auth: refreshToken,
    }),
    composeWithDevTools(applyMiddleware(Jwt,  thunk))
  );

export default store;
