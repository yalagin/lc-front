import { combineReducers } from 'redux';
import jwt_decode from "jwt-decode";

export function error(state = null, action) {
  switch (action.type) {
    case 'USER_LOGIN_ERROR':
      return action.error;

    default:
      return state;
  }
}

export function loading(state = false, action) {
  switch (action.type) {
    case 'USER_LOGIN_LOADING':
      return action.loading;

    default:
      return state;
  }
}

export function loggedIn(state = null, action) {
  switch (action.type) {
    case 'USER_LOGIN_SUCCESS':
      return action.loggedIn;

    default:
      return state;
  }
}

export default combineReducers({ error, loading, loggedIn });
