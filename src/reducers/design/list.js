import { combineReducers } from 'redux';

export function error(state = null, action) {
  switch (action.type) {
    case 'DESIGN_LIST_ERROR':
      return action.error;

    case 'DESIGN_LIST_MERCURE_DELETED':
      return `${action.retrieved['@id']} has been deleted by another user.`;

    case 'DESIGN_LIST_RESET':
      return null;

    default:
      return state;
  }
}

export function loading(state = false, action) {
  switch (action.type) {
    case 'DESIGN_LIST_LOADING':
      return action.loading;

    case 'DESIGN_LIST_RESET':
      return false;

    default:
      return state;
  }
}

export function retrieved(state = {'hydra:member' : [] }, action) {
  switch (action.type) {
    case 'DESIGN_LIST_SUCCESS':
      return action.retrieved;

    case 'DESIGN_LIST_RESET':
      return  {'hydra:member' : [] };

    case 'DESIGN_LIST_MERCURE_MESSAGE':
      return {
        ...state,
        'hydra:member': state['hydra:member'].map(item =>
          item['@id'] === action.retrieved['@id'] ? action.retrieved : item
        )
      };

    case 'DESIGN_LIST_MERCURE_DELETED':
      return {
        ...state,
        'hydra:member': state['hydra:member'].filter(
          item => item['@id'] !== action.retrieved['@id']
        )
      };

    default:
      return state;
  }
}

export function eventSource(state = null, action) {
  switch (action.type) {
    case 'DESIGN_LIST_MERCURE_OPEN':
      return action.eventSource;

    case 'DESIGN_LIST_RESET':
      return null;

    default:
      return state;
  }
}

export function resetForm(state = false, action) {
  switch (action.type) {
    case 'DESIGN_LIST_RESET_DESIGN_FORM':
      return true;

    case 'DESIGN_LIST_RESET_DESIGN_FORM_DONE':
      return false;
    case 'DESIGN_LIST_SET_FAKE_DATA':
      return 'DESIGN_LIST_SET_FAKE_DATA';
    default:
      return state;
  }
}

export default combineReducers({ error, loading, retrieved, eventSource, resetForm });
