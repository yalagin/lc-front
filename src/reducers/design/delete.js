import { combineReducers } from 'redux';

export function error(state = null, action) {
  switch (action.type) {
    case 'DESIGN_DELETE_ERROR':
      return action.error;
    case 'DESIGN_DELETE_RESET':
      return null;

    default:
      return state;
  }
}

export function loading(state = false, action) {
  switch (action.type) {
    case 'DESIGN_DELETE_LOADING':
      return action.loading;

    case 'DESIGN_DELETE_RESET':
      return false;

    default:
      return state;
  }
}

export function deleted(state = null, action) {
  switch (action.type) {
    case 'DESIGN_DELETE_SUCCESS':
      return action.deleted;

    case 'DESIGN_DELETE_RESET':
      return null;

    default:
      return state;
  }
}

export default combineReducers({ error, loading, deleted });
