import React from 'react';
import { Route, Navigate} from 'react-router-dom';

export const PrivateRouteOld = ({ component: Component, ...rest }) => {
    return (
        <Route {...rest} render={(props) => (
            localStorage.getItem('token')
                ? (<Component {...props} />)
                : (<Navigate to={"/"}   state={{login:true}} />)
        )} />
    );
};

export default PrivateRouteOld;
