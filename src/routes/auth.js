import React from 'react';
import { Route } from 'react-router-dom';
import Registration from "../pages/Registraton/Registration";
import Login from "../pages/Login/Login";

export default [
    <Route path="/register" element={<Registration/>} key="user_create" />,
    <Route path="/login" element={<Login/>} key="user_login" />,
];
