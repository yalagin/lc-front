import React from 'react';
import { Route } from 'react-router-dom';
import Create  from '../components/user/Create';

export default [
  <Route path="/users/create" element={<Create/>} key="create" />,
];
