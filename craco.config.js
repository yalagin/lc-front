const CracoLessPlugin = require('craco-less');

module.exports = {
  plugins: [
    {
      plugin: CracoLessPlugin,
      options: {
        lessLoaderOptions: {
          lessOptions: {
            modifyVars: {
              '@primary-color': '#7f67ff' ,
              '@menu-item-color': 'rgba(27, 46, 75, 0.7)' ,
              // "@menu-item-active-bg" : '#7f67ff' ,
            },
            javascriptEnabled: true,
          },
        },
      },
    },
  ],
};
